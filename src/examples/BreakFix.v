(* An example of a proof on skeleton. *)

Set Implicit Arguments.

Require TLC.LibSet.
Require Import TLC.LibInt TLC.LibString.
Require Export WellFormedness Concrete Abstract.

(** * Language Definition **)

(** This example is a very simple example with two main filters: one that breaks an invariant,
  and one that fixes it back to normal.  The skeletal semantics has been designed to include
  all constructs of skeletons and to have a non-trivial property: the invariant is indeed an
  invariant.  For the property to be non-trivial, a subterm is called from the main term.  This
  subterm is itself recursive, and is can be called either on the broken or fixed invariant. **)

(** ** Definitions of Sorts **)

Definition base_sort := unit.

Definition program_sort := unit.

Definition flow_sort := unit.

Instance base_sort_Comparable : Comparable base_sort.
  prove_comparable.
Defined.

Instance program_sort_Comparable : Comparable program_sort.
  prove_comparable.
Defined.

Instance flow_sort_Comparable : Comparable flow_sort.
  prove_comparable.
Defined.

Instance base_sort_Inhab : Inhab base_sort.
  apply prove_Inhab. repeat constructors~.
Defined.

Instance program_sort_Inhab : Inhab program_sort.
  apply prove_Inhab. repeat constructors~.
Defined.

Instance flow_sort_Inhab : Inhab flow_sort.
  apply prove_Inhab. repeat constructors~.
Defined.

Let term_sort : Type := term_sort base_sort program_sort.
Let sort_base : base_sort -> term_sort := @sort_base _ _.
Let sort_program : program_sort -> term_sort := @sort_program _ _.
Coercion sort_base : base_sort >-> term_sort.
Coercion sort_program : program_sort >-> term_sort.
Let sort : Type := sort base_sort program_sort flow_sort.
Let sort_term : term_sort -> sort := @sort_term _ _ _.
Let sort_flow : flow_sort -> sort := @sort_flow _ _ _.
Coercion sort_term : term_sort >-> sort.
Coercion sort_flow : flow_sort >-> sort.

Definition in_sort (_ : program_sort) := tt : flow_sort.
Definition out_sort (_ : program_sort) := tt : flow_sort.


(** ** Definition of Constructors **)

(** To make the example non-trivial, two terms are defined: [Sub], which is just the identity
  function written in an unusual way, and [Main], which calls [Sub] through a hook. **)

Inductive constructor :=
  | Main
  | Sub
  .

Instance constructor_Comparable : Comparable constructor.
  constructors. intros [|] [|];
    (solve [ applys* Decidable_equiv true_decidable ]
     || applys Decidable_equiv false_decidable; iff; false~).
Defined.

Instance constructor_Inhab : Inhab constructor.
  apply prove_Inhab. repeat constructors~.
Defined.

Definition constructor_signature (_ : constructor) : list term_sort * program_sort := ([], tt).


(** ** Definition of Filters **)

(** The filter [Break] breaks the invariant, and [Fix] puts it back. **)

Inductive filter :=
  | Id
  | Break
  | Fix
  .

Definition filter_signature f :=
  match f with
  | Id => ([(tt : flow_sort) : sort], [(tt : flow_sort) : sort])
  | Break => ([(tt : flow_sort) : sort], [(tt : flow_sort) : sort])
  | Fix => ([(tt : flow_sort) : sort], [(tt : flow_sort) : sort])
  end.


(** ** Definition of the Skeletal Semantics **)

Let skeleton_instance : Type := skeleton_instance constructor filter.
Let skeletal_semantics : Type := skeletal_semantics constructor filter.

(** The skeleton of [Main] calls [Break], then [Fix].  To make things less trivial,
  a hook to [Sub] is called in-between.
  The skeleton of [Sub] behaves as the identity function.  If given an invariant,
  it returns it back, if given a broken invariant, it also returns it.
  Note that the fact that this skeletal semantics preserves the invariant can’t be
  proven with only one induction (without using very heavy type-dependent invariants,
  at least), as the state at the beginning of [Sub] can either fullfil the invariant
  or be broken, whilst the state at the beginning of [Main] must fullfil the invariant. **)

Definition skeletal_semantics_example : skeletal_semantics := fun c =>
  match c with
  | Main =>
    make_skeleton_instance (skeleton_variable_term := False) []
      [F Break [_X_sigma] [_X_f 1];
       H (X_f 1) (_term_constructor Sub []) (X_f 2);
       F Fix [_X_f 2] [_X_o]]
  | Sub =>
    make_skeleton_instance (skeleton_variable_term := False) (skeleton_variable_flow := False) []
      [B [
          [F Id [_X_sigma] [_X_o]];
          [H X_sigma (_term_constructor Sub []) X_o]
       ] [_X_o]]
  end.

Instance skeleton_instance_term_example_Comparable : forall c,
    Comparable (skeleton_variable_term (skeletal_semantics_example c)).
  intros c. destruct c; simpl; prove_comparable.
Defined.

Instance skeleton_instance_flow_example_Comparable : forall c,
    Comparable (skeleton_variable_flow (skeletal_semantics_example c)).
  intros c. destruct c; simpl; prove_comparable.
Defined.


(** * Instantiation of Interpretations **)

(** ** Well-formedness **)

Lemma skeletal_semantics_wellformed_example :
  skeletal_semantics_wellformed in_sort out_sort
                                filter_signature constructor_signature
                                skeletal_semantics_example _ _.
Proof. prove_wellformedness. Qed.

(** ** Concrete Interpretation **)

Definition basic_term := unit.

Definition basic_term_sort : basic_term -> base_sort -> Prop := fun _ _ => True.

Lemma basic_term_sort_unique : forall b s1 s2,
  basic_term_sort b s1 ->
  basic_term_sort b s2 ->
  s1 = s2.
Proof. introv S1 S2. destruct s1, s2. reflexivity. Qed.

(** We assume a state, which can follow the invariant or not. **)
Variable state : Type.
Hypothesis state_Inhab : Inhab state.

(** The following two predicates describe the invariant and the broken invariant. **)
Variables P Q : state -> Prop.

(** We assume two partial functions for breaking and fixing the states. **)
Variables fbreak ffix : state -> option state.

Hypothesis fbreak_spec : forall st st',
  fbreak st = Some st' ->
  P st ->
  Q st'.
Hypothesis ffix_spec : forall st st',
  ffix st = Some st' ->
  Q st ->
  P st'.

Definition flow_value := state.

Definition flow_value_sort : flow_value -> flow_sort -> Prop := fun _ _ => True.

Lemma flow_value_sort_unique : forall v s1 s2,
  flow_value_sort v s1 ->
  flow_value_sort v s2 ->
  s1 = s2.
Proof. introv S1 S2. destruct s1, s2. reflexivity. Qed.

Let closed_term_free : Type := closed_term_free constructor basic_term.
Let cvalue : Type := value constructor basic_term flow_value.
Let cvalue_term : closed_term_free -> cvalue := @value_term _ _ _.
Let cvalue_flow : flow_value -> cvalue := @value_flow _ _ _.
Coercion cvalue_term : closed_term_free >-> cvalue.
Coercion cvalue_flow : flow_value >-> cvalue.

Let closed_term_free_from_basic : basic_term -> closed_term_free := @closed_term_free_from_basic _ _.
Coercion closed_term_free_from_basic : basic_term >-> closed_term_free.

Let cvalue_sort : cvalue -> sort -> Prop :=
  value_sort constructor_signature basic_term_sort flow_value_sort.

Inductive filter_interpretation_concrete : filter -> list cvalue -> list cvalue -> Prop :=
  | filter_interpretation_concrete_Id : forall st : flow_value,
    filter_interpretation_concrete Id [st : cvalue] [st : cvalue]
  | filter_interpretation_concrete_Break : forall st st' : flow_value,
    fbreak st = Some st' ->
    filter_interpretation_concrete Break [st : cvalue] [st' : cvalue]
  | filter_interpretation_concrete_Fix : forall st st' : flow_value,
    ffix st = Some st' ->
    filter_interpretation_concrete Fix [st : cvalue] [st' : cvalue]
  .

Let concrete c : triple_based_interpretation constructor filter basic_term _ _ :=
  concrete filter_interpretation_concrete
           (skeleton_instance_term_example_Comparable c)
           (skeleton_instance_flow_example_Comparable c).

Definition concrete_semantics_example : cvalue -> closed_term_free -> cvalue -> Prop :=
  concrete_semantics filter_interpretation_concrete
                     in_sort constructor_signature basic_term_sort flow_value_sort
                     skeletal_semantics_example.

Lemma filter_interpretation_concrete_consistent_with_sort_example : forall f vs1 vss1 vs2 vss2,
  filter_interpretation_concrete f vs1 vs2 ->
  filter_signature f = (vss1, vss2) ->
  Forall2 cvalue_sort vs1 vss1 ->
  Forall2 cvalue_sort vs2 vss2.
Proof. introv D E F. inverts D; inverts E; repeat inverts F as ? F; repeat constructors~. Qed.

Lemma concrete_semantics_wellformed_example :
  wellformed_set_of_triples in_sort out_sort constructor_signature
                            basic_term_sort cvalue_sort cvalue_sort concrete_semantics_example.
Proof.
  applys concrete_semantics_wellformed filter_signature; try typeclass.
  - apply basic_term_sort_unique.
  - apply filter_interpretation_concrete_consistent_with_sort_example.
  - apply skeletal_semantics_wellformed_example.
Qed.


(** * Invariant Proof **)

(** We first prove that [Sub] behaves like the identity. **)

Lemma id_in_sub : forall st : flow_value,
  concrete_semantics_example st (closed_term_free_from_False (term_constructor Sub [])) st.
Proof.
  introv. apply concrete_semantics_intro with (T := fun _ _ _ => False).
  - autos*.
  - constructors.
    + apply same_structure_constructor with (V2 := False). rewrite map_nil. constructors.
    + repeat constructors.
    + repeat constructors.
    + reflexivity.
    + constructors.
      * constructors.
        -- refine (@Forall2_cons _ _ _ _ _ (Some _) _ _ _).
           ++ introv E. inverts E. constructors.
              ** do 3 constructors.
                 --- constructors.
                     +++ rewrite_read_typed_environment. reflexivity.
                     +++ constructors.
                 --- constructors.
                 --- constructors.
              ** repeat constructors.
           ++ apply Forall2_cons with (x1 := None).
              ** introv A. inverts~ A.
              ** constructors~.
        -- do 2 constructors.
           ++ apply Nth_here.
           ++ wellformedness_reduce_basic_goals.
           ++ applys typed_environment_equivalent_refl value_term_inj value_flow_inj; typeclass.
      * repeat constructors.
    + rewrite_read_typed_environment; typeclass.
Qed.

Lemma sub_in_id_gen : forall (st : flow_value) t (o : cvalue),
  closed_term_free_eq t (closed_term_free_from_False (term_constructor Sub [])) ->
  concrete_semantics_example st t o ->
  o = st.
Proof.
  introv E D. lets (n&D'): concrete_semantics_alternative_forwards (rm D).
  gen st t o. induction n; introv Et D; simpl in D; tryfalse.
  inverts D as E1 Ss SSigma E2 D E3.
  apply closed_term_free_eq_sym in E1. forwards E1': closed_term_free_eq_trans (rm E1) Et.
  inverts E1' as F. destruct ts; inverts F.
  forwards Ss': sort_of_term_closed_eq (rm Ss) Et. inverts Ss' as E4 F. inverts E4. inverts F.
  inverts E2.
  compute in Sigma'.
  simpl in D. inverts D as D1 D2.
  do 2 inverts D2 as D2.
  inverts D1 as F D.
  inverts F as F1 F2. inverts F2 as F2 F3. inverts F3.
  inverts D as D. inverts D as N I E.
  simpl in E3. erewrite (typed_environment_equivalent_flow _ _ _ _ _
                           (@value_term_inj _ _ _) (@value_flow_inj _ _ _) _ _ _ _ _ _ E) in E3.
  clear E.
  erewrite (typed_environment_read_flow_merge_right _ _ _ cvalue_term cvalue_flow) in E3;
    try typeclass; rewrite_typed_environment_domain; try typeclass; try solve_in_notin.
  erewrite (typed_environment_read_flow_restrict_Mem _ _ _ cvalue_term _ (@value_flow_inj _ _ _))
    in E3; try typeclass.
  repeat inverts N as N.
  - (* First branch: the [Id] filter. *)
    clear F2 IHn. forwards~ I1: (rm F1).
    inverts I1 as D1 D2.
    repeat inverts D2 as D2.
    do 2 inverts D1 as D1. inverts D1 as F DId W.
    inverts F as Ex F. inverts F.
    inverts DId.
    inverts W.
    gen Ex. rewrite_read_typed_environment. simpl. introv Ex. inverts Ex.
    gen E3. rewrite_read_typed_environment. introv E3. inverts E3.
    reflexivity.
  - (* Second branch: the hook. *)
    clear F1. forwards~ I2: (rm F2).
    inverts I2 as D1 D2.
    do 2 inverts D2 as D2.
    inverts D1 as D. inverts D as D1 D2.
    + (* Normal hook *)
      inverts D2 as R1 R2.
      lets (t2&Ht2&Et2): (rm R1). inverts Ht2 (* WTF: [as Ht2 F] doesn’t work here. *); tryfalse.
      match goal with F : Forall2 _ _ _ |- _ => inverts F end. (* Circumventing this bug. *)
      inverts Et2 as F Et2; tryfalse. inverts F.
      gen R2. rewrite_read_typed_environment. introv R2. inverts R2.
      gen E3. rewrite_read_typed_environment. introv E3. inverts E3.
      applys (rm IHn) D1.
      unfolds. rewrite <- Et2. simpl. repeat constructors~.
    + (* Shorcut: this can’t happen in the concrete interpretation. *)
      inverts D1.
Qed.

Lemma sub_in_id : forall (st : flow_value) (o : cvalue),
  concrete_semantics_example st (closed_term_free_from_False (term_constructor Sub [])) o ->
  o = st.
Proof. introv D. applys sub_in_id_gen (rm D). repeat constructors~. Qed.

Lemma concrete_semantics_invariants : forall (st : flow_value) t (o : cvalue),
  P st ->
  concrete_semantics_example st t o ->
  exists (st' : flow_value), P st' /\ o = st'.
Proof.
  introv I D. forwards D1: concrete_semantics_fixpoint_backwards D.
  inverts D1 as Et Ss SSigma E2 D2 E3.
  destruct c.
  - (* [Main] *)
    destruct ts; inverts E2.
    forwards S1: sort_of_term_closed_eq (rm Ss) Et. inverts S1 as E4 F. inverts E4. inverts F.
    inverts E3 as E3.
    compute in Sigma'.
    simpl in D2. inverts D2 as D1 D2.
    inverts D2 as D2 D3.
    inverts D3 as D3 D4.
    repeat inverts D4 as D4.
    (* First bone: [F Break]. *)
    do 2 inverts D1 as D1. inverts D1 as F Ef W.
    inverts F as F1 F2. inverts F2.
    inverts Ef as E1.
    inverts W.
    gen F1. rewrite_read_typed_environment. introv E'. inverts E'.
    (* Second bone: the hook. *)
    inverts D2 as D2. inverts D2 as RC H.
    + (* Normal hook *)
      inverts H as R1 R2.
      gen R2. rewrite_read_typed_environment. introv E'. inverts E'.
      lets (t'&Ht'&Et'): (rm R1).
      inverts Ht' (* WTF: [as Ht2 F] doesn’t work here. *); tryfalse.
      match goal with F : Forall2 _ _ _ |- _ => inverts F end. (* Circumventing this bug. *)
      forwards Ev: sub_in_id_gen RC.
      { apply closed_term_free_eq_sym in Et'.
        applys closed_term_free_eq_trans (rm Et').
        repeat constructors~. }
      inverts Ev.
      clear Et' Ht' RC.
      (* Third bone: [F Fix]. *)
      do 2 inverts D3 as D3. inverts D3 as F Ef W.
      inverts F as F1 F2. inverts F2.
      inverts Ef as E2.
      inverts W.
      gen F1. rewrite_read_typed_environment. introv E'. inverts E'.
      (* Conclusion. *)
      gen E3. rewrite_read_typed_environment. introv E'. inverts E'.
      eexists. splits; [| reflexivity ].
      applys ffix_spec E2.
      applys~ fbreak_spec E1 I.
    + (* Shortcut, which can’t happen. *)
      inverts RC.
  - (* [Sub] *)
    destruct ts; inverts E2.
    forwards: sub_in_id_gen D.
    { applys closed_term_free_eq_trans (rm Et). repeat constructors~. }
    substs*.
Qed.

(** An example of instanciation of the formalism, on the While language. **)

Set Implicit Arguments.

Require TLC.LibSet.
Require Import TLC.LibInt TLC.LibString.
Require Export WellFormedness Concrete Abstract.

(** * Language Definition **)

(** ** Definitions of Sorts **)

(** Base sorts are sorts that appear in programs, but are not executable. **)

Inductive base_sort :=
  | Literal
  | Identifier
  .

(** Program sorts are the sorts of executable terms. **)

Inductive program_sort :=
  | Expression
  | Statement
  .

(** Flow sorts are the sort of values appearing in intermediary variables,
  that are not base or program sorts. **)

Inductive flow_sort :=
  | Store
  | Val
  | Int
  | Bool
  .


(** We prove some instances for these sorts. **)

Instance base_sort_Comparable : Comparable base_sort.
  prove_comparable.
Defined.

Instance program_sort_Comparable : Comparable program_sort.
  prove_comparable.
Defined.

Instance flow_sort_Comparable : Comparable flow_sort.
  prove_comparable.
Defined.

(** We assume these sorts to be inhabited.
  We can always get them inhabited by adding a dummy element if really needed. **)

Instance base_sort_Inhab : Inhab base_sort.
  apply prove_Inhab. repeat constructors~.
Defined.

Instance program_sort_Inhab : Inhab program_sort.
  apply prove_Inhab. repeat constructors~.
Defined.

Instance flow_sort_Inhab : Inhab flow_sort.
  apply prove_Inhab. repeat constructors~.
Defined.

(** We instantiate our types for these specific sorts. **)
(** The coercions are facultative, but they really help making the
  example concise and pleasant to read. **)
Let term_sort : Type := term_sort base_sort program_sort.
Let sort_base : base_sort -> term_sort := @sort_base _ _.
Let sort_program : program_sort -> term_sort := @sort_program _ _.
Coercion sort_base : base_sort >-> term_sort.
Coercion sort_program : program_sort >-> term_sort.
Let sort : Type := sort base_sort program_sort flow_sort.
Let sort_term : term_sort -> sort := @sort_term _ _ _.
Let sort_flow : flow_sort -> sort := @sort_flow _ _ _.
Coercion sort_term : term_sort >-> sort.
Coercion sort_flow : flow_sort >-> sort.

(** As program sorts are executable, then can be executed in a specific sort
  and return a specific sort when executed.
  The following constructs define these two sorts. **)
(** These definitions correspond to Figure 7 of the paper. **)

Definition in_sort s :=
  match s with
  | Expression => Store
  | Statement => Store
  end.

Definition out_sort s :=
  match s with
  | Expression => Val
  | Statement => Store
  end.


(** ** Definition of Constructors **)

(** The term constructors.  Note that they are just symbols with no arity,
  which is given by [constructor_arity], which depends on
  [constructor_signature] defined below . **)

Inductive constructor :=
  | Const
  | Var
  | Add
  | Eq
  | Neg
  | Skip
  | Asn
  | Seq
  | IfThenElse
  | While
  .

Instance constructor_Comparable : Comparable constructor.
  prove_comparable.
Defined.

Instance constructor_Inhab : Inhab constructor.
  apply prove_Inhab. repeat constructors~.
Defined.

(** Constructors are associated a signature.  It defines its arity, as well as
  the sorts of its arguments and the sort of the term built by the constructor. **)
(** This definition corresponds to Figure 5 of the paper. **)

Definition constructor_signature c :=
  match c with
  | Const => ([Literal : term_sort], Expression)
  | Var => ([Identifier : term_sort], Expression)
  | Add => ([Expression : term_sort; Expression : term_sort], Expression)
  | Eq => ([Expression : term_sort; Expression : term_sort], Expression)
  | Neg => ([Expression : term_sort], Expression)
  | Skip => ([], Statement)
  | Asn => ([Identifier : term_sort; Expression : term_sort], Statement)
  | Seq => ([Statement : term_sort; Statement : term_sort], Statement)
  | IfThenElse => ([Expression : term_sort; Statement : term_sort; Statement : term_sort], Statement)
  | While => ([Expression : term_sort; Statement : term_sort], Statement)
  end.


(** ** Definition of Filters **)

(** Filters are the basic operations found in a semantic.
  They can compute results, but also serve as a simple predicate (hence their name).
  They can also do both: the [Read] filter both checks that its given variable is
  in the heap and, if it is so, returns its associated value.
  Filters are not given a semantics yet: see the concrete and abstract interpretations
  below for such a definition. **)

Inductive filter :=
  | LitToInt (** [LitInt] in the paper. **)
  | IntToVal (** [IntVal] in the paper. **)
  | BoolToVal
  | Read
  | IsInt
  | AddNum
  | EqNum
  | IsBool
  | NegBool
  | Write
  | Id
  | IsTrue
  | IsFalse
  .

(** Filters are associated a signature, corresponding with their input and output arity,
  as well as the sorts (usually flow sorts) returned by the filter.
  Filters that serve as pure predicate (such as [IsTrue]) typically returns an empty
  list of sorts. **)
(** This definition corresponds to Figure 8 of the paper. **)

Definition filter_signature f :=
  match f with
  | LitToInt => ([Literal : sort], [Int : sort])
  | IntToVal => ([Int : sort], [Val : sort])
  | BoolToVal => ([Bool : sort], [Val : sort])
  | Read => ([Identifier : sort; Store : sort], [Val : sort])
  | IsInt => ([Val : sort], [Int : sort])
  | AddNum => ([Int : sort; Int : sort], [Int : sort])
  | EqNum => ([Int : sort; Int : sort], [Bool : sort])
  | IsBool => ([Val : sort], [Bool : sort])
  | NegBool => ([Bool : sort], [Bool : sort])
  | Write => ([Identifier : sort; Store : sort; Val : sort], [Store : sort])
  | Id => ([Store : sort], [Store : sort])
  | IsTrue => ([Bool : sort], [])
  | IsFalse => ([Bool : sort], [])
  end.


(** ** Definition of the Skeletal Semantics **)

Let skeleton_instance : Type := skeleton_instance constructor filter.
Let skeletal_semantics : Type := skeletal_semantics constructor filter.

(** Each constructor is associated a skeleton.
  In contrary to the paper, we enable the type of variables to change between
  each constructor.  This enable the semantics-designer to choose local types
  that would be more adapted to practical local usage. **)
(** This definition corresponds to Figure 6 of the paper. **)

(** The constructor [make_skeleton_instance] takes as first (explicit) argument
  the list of all term variables used to represent the arguments of the
  current constructor.  For instance, the constructor [IfThenElse] takes three
  arguments, and we reference them as ["e"], ["s1"], and ["s2"], leading to the
  three term variables [X_t "e"], [X_t "s1"], and [X_t "s2"].  If being used
  in a hook, as we required terms in hook to be free of basic variables, one
  need to use the basic-free variant [_X_t "e"], [_X_t "s1"], and [_X_t "s2"]
  for the skeletal semantics to type-check.  More complex terms in hooks can be
  built using the [_term_constructor] constructor. In the example of [IfThenElse],
  we used strings, but we could use any other type to represent them.  For
  instance, [Var] used the [unit] type to represent its unique term variable
  [X_t tt].  In the case of [Skip], there are no given term variable: it is
  necessary to tell Coq a type for them—knowing that any type would work, but
  [False] is usually easier to manipulate—using the [(skeleton_variable_term
  := False)] annotation.  Similarly, although the type of flow variables is
  usually inferred by Coq, we have to tell Coq what type to use in the case
  of constructors without any flow variable (such as [Const]) through the
  [(skeleton_variable_flow := False)] annotation.  Flow variables are referred
  using the [X_f] constructor (alternatively, [X_sigma] or [X_o] for the
  special input and output flow variables) in the arguments of hooks, and
  [_X_f] (or alternatively [_X_sigma] or [_X_o]) in the arguments of filters. **)

Definition skeletal_semantics_example : skeletal_semantics := fun c =>
  match c with
  | Const =>
    (** [Lit] in the paper.
      We note [X_t tt] and [X_f tt] to respectively represent [x_t] and [x_{f_1}] of the paper. **)
    make_skeleton_instance [tt]
      [F LitToInt [X_t tt] [_X_f tt]; F IntToVal [_X_f tt] [_X_o]]
  | Var =>
    (** We note [X_t tt] to represent [x_t] of the paper. **)
    make_skeleton_instance (skeleton_variable_flow := False) [tt]
      [F Read [X_t tt; _X_sigma] [_X_o]]
  | Add =>
    (** We note [X_t n], [X_f (false, n)], and [X_f (true, n)] to respectively
      represent [x_{t_n}], [x_{f_n}], and [x_{f_{n'}}] of the paper. **)
    make_skeleton_instance [1; 2]
      [H X_sigma (_X_t 1) (X_f (false, 1)); F IsInt [_X_f (false, 1)] [_X_f (true, 1)];
       H X_sigma (_X_t 2) (X_f (false, 2)); F IsInt [_X_f (false, 2)] [_X_f (true, 2)];
       F AddNum [_X_f (true, 1); _X_f (true, 2)] [_X_f (false, 3)];
       F IntToVal [_X_f (false, 3)] [_X_o]]
  | Eq =>
    (** We note [X_t n], [X_f (false, n)], and [X_f (true, n)] to respectively
      represent [x_{t_n}], [x_{f_n}], and  as [x_{f_{n'}}] of the paper. **)
    make_skeleton_instance [1; 2]
      [H X_sigma (_X_t 1) (X_f (false, 1)); F IsInt [_X_f (false, 1)] [_X_f (true, 1)];
       H X_sigma (_X_t 2) (X_f (false, 2)); F IsInt [_X_f (false, 2)] [_X_f (true, 2)];
       F EqNum [_X_f (true, 1); _X_f (true, 2)] [_X_f (false, 3)];
       F BoolToVal [_X_f (false, 3)] [_X_o]]
  | Neg =>
    (** We note [X_t tt] and [X_f n] to respectively represent
      [x_t] and [x_{f_n}] of the paper. **)
    make_skeleton_instance [tt]
      [H X_sigma (_X_t tt) (X_f 1); F IsBool [_X_f 1] [_X_f 2];
       F NegBool [_X_f 2] [_X_f 3]; F BoolToVal [_X_f 3] [_X_o]]
  | Skip =>
    make_skeleton_instance (skeleton_variable_term := False) (skeleton_variable_flow := False) []
      [F Id [_X_sigma] [_X_o]]
  | Asn =>
    (** We note [X_f tt], [X_t "x"] and [X_t "e"] to respectively represent
      [x_{f_1}], [x_{t_1}] and [x_{t_2}] of the paper. **)
    make_skeleton_instance ["x"; "e"]
      [H X_sigma (_X_t "e") (X_f tt); F Write [X_t "x"; _X_sigma; _X_f tt] [_X_o]]
  | Seq =>
    (** We note [X_t n] and [X_f tt] to respectively represent [x_{s_n}] and
      [x_{f_1}] of the paper. **)
    make_skeleton_instance [1; 2]
      [H X_sigma (_X_t 1) (X_f tt); H (X_f tt) (_X_t 2) X_o]
  | IfThenElse =>
    (** We note [X_t s], [X_f false], and [X_f true] to respectely represent
      [x_1], [x_{s}], and [x'_1] of the paper. **)
    make_skeleton_instance ["e"; "s1"; "s2"]
      [H X_sigma (_X_t "e") (X_f false); F IsBool [_X_f false] [_X_f true]; B [
          [F IsTrue [_X_f true] []; H X_sigma (_X_t "s1") X_o];
          [F IsFalse [_X_f true] []; H X_sigma (_X_t "s2") X_o]
       ] [_X_o]]
  | While =>
    (** We note [X_t "e"], [X_t "s"], [X_f 1], [X_f 2], and [X_f 3] to respectively
      represent [x_{t_1}], [x_{t_2}], [x_{f_1}], [x_{f_{1'}}], and [x_{f_2}]
      of the paper. **)
    make_skeleton_instance ["e"; "s"]
      [H X_sigma (_X_t "e") (X_f 1); F IsBool [_X_f 1] [_X_f 2]; B [
          [F IsTrue [_X_f 2] []; H X_sigma (_X_t "s") (X_f 3);
           H (X_f 3) (_term_constructor While [_X_t "e"; _X_t "s"]) X_o];
          [F IsFalse [_X_f 2] []; F Id [_X_sigma] [_X_o]]
       ] [_X_o]]
  end%string.

(** We need to make sure that the variables used in each constructor are
  comparable.  The following instances check this. **)

Instance skeleton_instance_term_example_Comparable : forall c,
    Comparable (skeleton_variable_term (skeletal_semantics_example c)).
  intros c. destruct c; simpl; prove_comparable.
Defined.

Instance skeleton_instance_flow_example_Comparable : forall c,
    Comparable (skeleton_variable_flow (skeletal_semantics_example c)).
  intros c. destruct c; simpl; prove_comparable.
Defined.


(** * Instantiation of Interpretations **)

(** Once that we defined a skeleton, we can instantiate all of our generic
  interpretations, their associated semantics, and their relating theorems.
  Only local informations on filters need to be proven to get these general
  results applied to this particular skeleton. **)

(** Interpretations only require lemmas on filters, which are usually in
  a small number compared to the size of the entire semantics (at least in
  the case of real-world semantics).  We encourage the reader to test it
  by editing the skeletal semantics above, by adding or removing constructs,
  or by slightly changing the skeletal instance associated with a
  constructor: if the filters are left unchanged, the interpretations and
  their associated properties below won’t have to be updated. **)


(** ** Well-formedness **)

(** The skeletal semantics of the example is well-formed. **)
(** Warning: this proof takes quite a long time to compile.
  The tactic [prove_wellformedness] is indeed quite heavy for now.
  We think that reflexion could significantly speed it up.
  In the case where the tactic [prove_wellformedness] fails, it only
  leaves goals about the leftover constructors that failed to check;
  this helps catching errors in the skeletal declaration. **)
Lemma skeletal_semantics_wellformed_example :
  skeletal_semantics_wellformed in_sort out_sort
                                filter_signature constructor_signature
                                skeletal_semantics_example _ _.
Proof. prove_wellformedness. Qed.

(** Let us examine the axioms used by this proof.
<<
Print Assumptions skeletal_semantics_wellformed_example.
>>
We get the following axioms:
  - [prop_ext], [indefinite_description], and [func_ext_dep] are base axioms of the TLC library.
    They express proof irrelevance, the indefinite description axiom, as well as (dependent)
    function extensionality.
  - [LibHeap.not_indom_empty], [LibHeap.HeapList.indom_equiv_binds],
    [LibHeap.HeapList.read_option_def] and [LibHeap.HeapList.binds_equiv_read] are temporary
    axioms of the TLC library.  They are meant to be proven in the next version of the library.
  - [to_list_dom] is an axiom that we added due to a lack of specification of the behaviour of
    the [to_list] construct of TLC. This axiom could be relatively easily included as a lemma
    and proven in TLC.
**)

(** ** Concrete Interpretation **)

(** We can now define the concrete interpretation, starting with the basic terms. **)
(** Basic terms are the only term component that can be changed between interpretations. **)

Definition literal := nat.
Definition identifier := string.

Inductive basic_term :=
  | TLiteral : literal -> basic_term
  | TIdentifier : identifier -> basic_term
  .

(** The following predicate links the basic terms with their sorts. **)
Inductive basic_term_sort : basic_term -> base_sort -> Prop :=
  | basic_term_sort_literal : forall l,
    basic_term_sort (TLiteral l) Literal
  | basic_term_sort_identifier : forall x,
    basic_term_sort (TIdentifier x) Identifier
  .

(** Each basic term is associated only one sort. **)
Lemma basic_term_sort_unique : forall b s1 s2,
  basic_term_sort b s1 ->
  basic_term_sort b s2 ->
  s1 = s2.
Proof. introv S1 S2. inverts S1; inverts~ S2. Qed.


(** We define the (flow) values used in the concrete semantics. **)
(** Flow values are not to be mixed with the values of our While language:
  the sort [Val] is a sort of the While language, and its corresponding
  concrete type [val] is one of the flow values of the While language,
  but states are flow values too. **)

Definition val : Type := nat + bool.
Definition store := identifier -> option val.

Inductive flow_value :=
  | VStore : store -> flow_value
  | VVal : val -> flow_value
  | VInt : nat -> flow_value
  | VBool : bool -> flow_value
  .

(** The sort of flow values naturally follows. **)
Inductive value_flow_sort : flow_value -> flow_sort -> Prop :=
  | value_flow_sort_store : forall st,
    value_flow_sort (VStore st) Store
  | value_flow_sort_val : forall v,
    value_flow_sort (VVal v) Val
  | value_flow_sort_int : forall i,
    value_flow_sort (VInt i) Int
  | value_flow_sort_bool : forall b,
    value_flow_sort (VBool b) Bool
  .

(** This definition of flow value sorts is functionnal. **)
Lemma value_flow_sort_unique : forall v s1 s2,
  value_flow_sort v s1 ->
  value_flow_sort v s2 ->
  s1 = s2.
Proof. introv S1 S2. inverts S1; inverts~ S2. Qed.

Instance flow_value_Inhab : Inhab flow_value.
  apply prove_Inhab. repeat constructors~.
Defined.

(** As above, we instantiate our types and coercions for these specific values. **)
Let closed_term_free : Type := closed_term_free constructor basic_term.
Let cvalue : Type := value constructor basic_term flow_value.
Let cvalue_term : closed_term_free -> cvalue := @value_term _ _ _.
Let cvalue_flow : flow_value -> cvalue := @value_flow _ _ _.
Coercion cvalue_term : closed_term_free >-> cvalue.
Coercion cvalue_flow : flow_value >-> cvalue.

Let closed_term_free_from_basic : basic_term -> closed_term_free := @closed_term_free_from_basic _ _.
Coercion closed_term_free_from_basic : basic_term >-> closed_term_free.

Let cvalue_sort : cvalue -> sort -> Prop :=
  value_sort constructor_signature basic_term_sort value_flow_sort.

(** The concrete semantics of filters has to be given in the form of a relation.
  This relation has to match the signature declared in [filter_signature]. **)
(** The semantics of filters is typically very simple. **)

Inductive filter_interpretation_concrete : filter -> list cvalue -> list cvalue -> Prop :=
  | filter_interpretation_concrete_LitToInt : forall l,
    filter_interpretation_concrete LitToInt [TLiteral l : cvalue]
                                            [VInt l : cvalue]
  | filter_interpretation_concrete_IntToVal : forall i,
    filter_interpretation_concrete IntToVal [VInt i : cvalue]
                                            [VVal (inl i) : cvalue]
  | filter_interpretation_concrete_BoolToVal : forall b,
    filter_interpretation_concrete BoolToVal [VBool b : cvalue]
                                             [VVal (inr b) : cvalue]
  | filter_interpretation_concrete_Read : forall x st v,
    st x = Some v ->
    filter_interpretation_concrete Read [TIdentifier x : cvalue; VStore st : cvalue]
                                        [VVal v : cvalue]
  | filter_interpretation_concrete_IsInt : forall i,
    filter_interpretation_concrete IsInt [VVal (inl i) : cvalue]
                                         [VInt i : cvalue]
  | filter_interpretation_concrete_AddNum : forall i1 i2,
    filter_interpretation_concrete AddNum [VInt i1 : cvalue; VInt i2 : cvalue]
                                          [VInt (i1 + i2) : cvalue]
  | filter_interpretation_concrete_EqNum_eq : forall i,
    filter_interpretation_concrete EqNum [VInt i : cvalue; VInt i : cvalue]
                                         [VBool true : cvalue]
  | filter_interpretation_concrete_EqNum_neq : forall i1 i2,
    i1 <> i2 ->
    filter_interpretation_concrete EqNum [VInt i1 : cvalue; VInt i2 : cvalue]
                                         [VBool false : cvalue]
  | filter_interpretation_concrete_IsBool : forall b,
    filter_interpretation_concrete IsBool [VVal (inr b) : cvalue]
                                          [VBool b : cvalue]
  | filter_interpretation_concrete_NegBool : forall b,
    filter_interpretation_concrete NegBool [VBool b : cvalue]
                                           [VBool (negb b) : cvalue]
  | filter_interpretation_concrete_Write : forall x st v,
    filter_interpretation_concrete Write [TIdentifier x : cvalue; VStore st : cvalue; VVal v : cvalue]
                                         [VStore (fun y => ifb x = y then Some v else st y) : cvalue]
  | filter_interpretation_concrete_Id : forall st,
    filter_interpretation_concrete Id [VStore st : cvalue] [VStore st : cvalue]
  | filter_interpretation_concrete_IsTrue :
    filter_interpretation_concrete IsTrue [VBool true : cvalue] []
  | filter_interpretation_concrete_IsFalse :
    filter_interpretation_concrete IsFalse [VBool false : cvalue] []
  .

(** We instantiate the concrete interpretation to this skeletal semantics. **)
Let concrete c : triple_based_interpretation constructor filter basic_term _ _ :=
  concrete filter_interpretation_concrete
           (skeleton_instance_term_example_Comparable c)
           (skeleton_instance_flow_example_Comparable c).

(** We now define our concrete semantics by connecting the interpretation of filters
  to the skeletal semantics [skeletal_semantics_example].  The resulting semantics
  is a set of triples [(sigma, t, o)], meaning that the term [t] in the context
  [sigma] evaluates into [o]. **)
Definition concrete_semantics_example : cvalue -> closed_term_free -> cvalue -> Prop :=
  concrete_semantics filter_interpretation_concrete
                     in_sort constructor_signature basic_term_sort value_flow_sort
                     skeletal_semantics_example.

(** As said above, the two definitions [filter_interpretation_concrete]
  and [filter_signature] should match.  This lemma is straightforward to
  prove in almost all semantics. **)
Lemma filter_interpretation_concrete_consistent_with_sort_example : forall f vs1 vss1 vs2 vss2,
  filter_interpretation_concrete f vs1 vs2 ->
  filter_signature f = (vss1, vss2) ->
  Forall2 cvalue_sort vs1 vss1 ->
  Forall2 cvalue_sort vs2 vss2.
Proof. introv D E F. inverts D; inverts E; repeat inverts F as ? F; repeat constructors~. Qed.

(** We can then conclude that the concrete semantics is well-formed by
  applying the theorem [concrete_semantics_wellformed]. **)
Lemma concrete_semantics_wellformed_example :
  wellformed_set_of_triples in_sort out_sort constructor_signature
                            basic_term_sort cvalue_sort cvalue_sort concrete_semantics_example.
Proof.
  applys concrete_semantics_wellformed filter_signature; try typeclass.
  - apply basic_term_sort_unique.
  - apply filter_interpretation_concrete_consistent_with_sort_example.
  - apply skeletal_semantics_wellformed_example.
Qed.

(** The axioms used by this theorem are the same than
  [skeletal_semantics_wellformed_example] above.
<<
Print Assumptions concrete_semantics_wellformed_example.
>> **)


(** ** Abstract Interpretation **)

(** We provide four different implementations of the abstract interpretation,
  with different domains. **)

(** *** An Interval Analysis **)

(** **** Abstract Domains **)

(** We start by defining abstract domains.
  This task can be long, but has to be done in any abstract interpretation framework. **)

(** We redeclare the poset notation to ease readability. **)
Notation "a ⊑ b" := (Poset_order a b) (at level 40).

(** Unfortunately, the library [TLC.LibSet] imports notations from
  [TLC.LibBag] that conflict with the list notation.  There is
  unfortunately no mechanism in the current version of Coq to prevent
  this to happen.  We circumvent this issue by defining a local module
  [Domains] and immediately importing it.  This is just a notation
  issue and does not impact how to use our framework. **)

Module Domains.

Import TLC.LibSet.

(** We define the (flow) values used in the abstract semantics.
  For this, we define the domain of intervals.  In this case, the intervals form a lattice,
  but this is not enforced by our framework: a poset is enough. **)

Inductive interval :=
  | interval_normal : nat -> nat -> interval (** Represents a simple interval [[i; j]]. **)
  | interval_open : nat -> interval (** Represents an open interval [[i; \infty]]. **)
  | interval_empty : interval (** Represents the empty set. **)
  .

(** This representation of intervals is a little clumsy as there are several representations
  for the empty set (for instance [interval_empty] and [interval_normal 4 2]).  This is not
  an issue for the correctness of our framework.  We could of course have used a different
  representation for intervals, but we found that this representation is simple enough for
  our example. **)

(** The concretisation of intervals follows naturally. **)
Inductive gamma_interval : interval -> set nat :=
  | gamma_interval_normal : forall ia ib n,
    n >= ia ->
    n <= ib ->
    gamma_interval (interval_normal ia ib) n
  | gamma_interval_open : forall ia n,
    n >= ia ->
    gamma_interval (interval_open ia) n
  .

(** We now define the order between two intervals. **)
Definition interval_order i1 i2 :=
  match i1, i2 with
  | interval_empty, _ => True
  | _, interval_empty => False
  | interval_open _, interval_normal _ _ => False
  | interval_normal i1a _, interval_open i2a => i1a >= i2a
  | interval_open i1a, interval_open i2a => i1a >= i2a
  | interval_normal i1a i1b, interval_normal i2a i2b => i1a >= i2a /\ i1b <= i2b
  end.

(** Let us prove that the order [interval_order] indeed forms a poset. **)
(** Declaring this poset as an instance enables us to later us the [a ⊑ b] notation. **)
Instance interval_Poset : Poset interval.
  applys make_Poset interval_order.
  - intro x. destruct x; simpl; autos~; math.
  - intros x y I1 I2. destruct x, y; simpls~; tryfalse; fequals; math.
  - intros x y z I1 I2. destruct x, y, z; simpls~; tryfalse; fequals; math.
Defined.

(** This order is compatible with the concretisation function. **)
Lemma gamma_interval_compatible_with_order : forall i1 i2,
  i1 ⊑ i2 ->
  gamma_interval i1 \c gamma_interval i2.
Proof.
  introv O. rewrite incl_in_eq. introv G.
  inverts G as O1 O2; destruct i2; simpl in O; tryfalse; constructor; math.
Qed.

(** We also define some operations over intervals.
  These will be useful to define abstract filters. **)

(** The singleton interval only contains one number. **)
Definition singleton_interval n := interval_normal n n.

(** We define the addition over intervals as follows **)
Definition interval_add i1 i2 :=
  match i1, i2 with
  | interval_empty, _ => interval_empty
  | _, interval_empty => interval_empty
  | interval_normal i1a i1b, interval_normal i2a i2b =>
    interval_normal (i1a + i2a) (i1b + i2b)
  | interval_open i1a, interval_normal i2a _ =>
    interval_open (i1a + i2a)
  | interval_normal i1a _, interval_open i2a =>
    interval_open (i1a + i2a)
  | interval_open i1a, interval_open i2a =>
    interval_open (i1a + i2a)
  end.

Lemma gamma_interval_singleton : forall n m,
  gamma_interval (singleton_interval n) m <-> n = m.
Proof.
  iff G.
  - inverts G. math.
  - substs. constructor; math.
Qed.

Lemma gamma_interval_add : forall i1 i2 n1 n2,
  gamma_interval i1 n1 ->
  gamma_interval i2 n2 ->
  gamma_interval (interval_add i1 i2) (n1 + n2)%nat.
Proof. introv I1 I2. inverts I1; inverts I2; constructor; math. Qed.

Definition interval_intersects i1 i2 :=
  exists n, gamma_interval i1 n /\ gamma_interval i2 n.


(** We directly abstract booleans by sets of booleans.
  If we would like to build a collecting semantics, we would abstract
  every domains this way. **)

Definition abool := set bool.

Definition bottom_abool : abool := \{}.

Definition top_abool : abool := fun _ => True.

Definition singleton_abool b : abool := \{ b }.

Definition gamma_abool := id : abool -> set bool.

(** We order this abstract domain by the usual set inclusion. **)
Instance abool_Poset : Poset abool.
  applys make_Poset (fun b1 b2 : abool => b1 \c b2).
  - intro x. apply incl_refl.
  - intros x y I1 I2. erewrite incl_in_eq in I1, I2. apply* set_ext.
  - intros x y z I1 I2. applys incl_trans I1 I2.
Defined.

Lemma gamma_abool_compatible_with_order : forall b1 b2,
  b1 ⊑ b2 ->
  gamma_abool b1 \c gamma_abool b2.
Proof. introv O. apply O. Qed.


(** We abstract [val] by the lexicographic order over both its possible constructors. **)
Definition aval : Type := interval * abool.

Definition bottom_aval : aval := (interval_empty, bottom_abool).

Definition gamma_aval (va : aval) :=
  let (i, ba) := va in
  set_st (fun v : val =>
    match v with
    | inl n => gamma_interval i n
    | inr b => gamma_abool ba b
    end).

(** The reader may be surprised that we define [aval] as a product whilst [val]
  is defined as a sum.  We could have defined [aval] as a sum, but the resulting
  semantics would have been less precise.  Indeed, recall that our While language
  is untyped: the same variable can both receive a boolean and an integer in the
  same program execution.  If one wants to precisely abstract such a mix of types,
  the product is needed.  If the abstract of [val] would be a sum, then the
  abstraction of such mixed-typed values would probably be as imprecise as [⊤].
  This is however not important for the example: any abstract domain for [val]
  would have worked here. **)

Definition aval_order (va1 va2 : aval) :=
  let (i1, ba1) := va1 in
  let (i2, ba2) := va2 in
  i1 ⊑  i2 /\ ba1 ⊑ ba2.

Instance aval_Poset : Poset aval.
  applys make_Poset aval_order.
  - intros (i&ba). splits; apply Poset_refl.
  - intros (i1&ba1) (i2&ba2) (Oi1&Ob1) (Oi2&Ob2). fequals; apply~ @Poset_antisym.
  - intros (i1&ba1) (i2&ba2) (i3&ba3) (Oi1&Ob1) (Oi2&Ob2). splits; apply* @Poset_trans.
Defined.

Lemma gamma_aval_compatible_with_order : forall v1 v2,
  v1 ⊑ v2 ->
  gamma_aval v1 \c gamma_aval v2.
Proof.
  intros (i1&b1) (i2&b2) (Oi&Ob). rewrite incl_in_eq.
  unfolds gamma_aval. intros v. do 2 rewrite in_set_st_eq. destruct v as [i|b].
  - forwards~ I: gamma_interval_compatible_with_order Oi. rewrite incl_in_eq in I. apply I.
  - forwards~ I: gamma_abool_compatible_with_order Ob. rewrite incl_in_eq in I. apply I.
Qed.


(** We abstract the store by a pointwise abstraction.
  To keep precision, one however needs to keep track of whether the current
  variable could be undefined.  Typically, what would be the store resulting
  after the execution of a statement such as [if ? then x := 1 else skip]
  when [x] starts with no associated value (and [?] represents a too complex
  expression to be analysed, returning [⊤] in the abstract execution)?  We
  would here represent value to associated [x] as [(1♯, true)] to state
  that the value of [x] may be undefined, but if it is defined, its value
  is soundly abstracted by [1♯].  A value associated to [y] such as
  [(v♯, false)] means that we know that Variable [y] is defined and is
  soundly abstracted by [v♯], and a value such as [(⊥, true)] means that
  we know that the variable is not defined.
  Again, this is not important for the example: any abstract domain for
  [astore] would have worked here. **)
Definition astore := identifier -> (aval * bool).

Definition gamma_astore (sta : astore) :=
  set_st (fun st : store =>
    forall x,
      match st x with
      | None => snd (sta x) = true
      | Some v => gamma_aval (fst (sta x)) v
      end).

Definition astore_order (sta1 sta2 : astore) :=
  forall x,
    fst (sta1 x) ⊑ fst (sta2 x)
    /\ (snd (sta1 x) -> snd (sta2 x)).

Instance astore_Poset : Poset astore.
  applys make_Poset astore_order.
  - intros ast x. splits~. apply Poset_refl.
  - intros ast1 ast2 O1 O2. unfolds astore. extens. intro x.
    forwards (O1a&O1b): (rm O1) x. forwards (O2a&O2b): (rm O2) x.
    destruct (ast1 x) as [v1 b1], (ast2 x) as [v2 b2]. fequals.
    + apply~ @Poset_antisym.
    + simpls. destruct b1, b2; autos~; false~.
  - intros ast1 ast2 ast3 O1 O2 x.
    forwards (O1a&O1b): (rm O1) x. forwards (O2a&O2b): (rm O2) x.
    splits~; apply* @Poset_trans.
Defined.

Lemma gamma_astore_compatible_with_order : forall sta1 sta2,
  sta1 ⊑ sta2 ->
  gamma_astore sta1 \c gamma_astore sta2.
Proof.
  introv O. rewrite incl_in_eq. unfolds gamma_astore. intros st. do 2 rewrite in_set_st_eq.
  intros G x. forwards Gx: (rm G) x. forwards (O1&O2): (rm O) x. destruct (st x).
  - forwards~ I: gamma_aval_compatible_with_order O1. rewrite incl_in_eq in I. applys I Gx.
  - applys O2 Gx.
Qed.

Definition bottom_astore : astore := fun _ => (bottom_aval, false).


(** We can now define the abstract flow values. **)
(** The sort of flow variables are guaranteed by the well-formedness of the
  skeletal semantics: there is no need to abstract the sum-type [flow_value]
  by something else than a sum-type. **)
Inductive abstract_flow_value :=
  | aVStore : astore -> abstract_flow_value
  | aVVal : aval -> abstract_flow_value
  | aVInt : interval -> abstract_flow_value
  | aVBool : abool -> abstract_flow_value
  .

Definition gamma_abstract_flow_value v :=
  match v with
  | aVStore ast => map_set VStore (gamma_astore ast)
  | aVVal v => map_set VVal (gamma_aval v)
  | aVInt i => map_set VInt (gamma_interval i)
  | aVBool b => map_set VBool (gamma_abool b)
  end.

Inductive abstract_flow_value_order : abstract_flow_value -> abstract_flow_value -> Prop :=
  | abstract_flow_value_order_store : forall ast1 ast2,
    ast1 ⊑ ast2 ->
    abstract_flow_value_order (aVStore ast1) (aVStore ast2)
  | abstract_flow_value_order_val : forall v1 v2,
    v1 ⊑ v2 ->
    abstract_flow_value_order (aVVal v1) (aVVal v2)
  | abstract_flow_value_order_int : forall i1 i2,
    i1 ⊑ i2 ->
    abstract_flow_value_order (aVInt i1) (aVInt i2)
  | abstract_flow_value_order_bool : forall b1 b2,
    b1 ⊑ b2 ->
    abstract_flow_value_order (aVBool b1) (aVBool b2)
  .

Instance abstract_flow_value_Poset : Poset abstract_flow_value.
  applys make_Poset abstract_flow_value_order.
  - intro x. destruct x; constructor; apply Poset_refl.
  - intros x y O1 O2. inverts O1 as O1; inverts O2 as O2; fequals; applys @Poset_antisym O1 O2.
  - intros x y z O1 O2. inverts O1 as O1; inverts O2 as O2; constructor; applys @Poset_trans O1 O2.
Defined.

Lemma gamma_abstract_flow_value_compatible_with_order : forall v1 v2,
  v1 ⊑ v2 ->
  gamma_abstract_flow_value v1 \c gamma_abstract_flow_value v2.
Proof.
  introv O. inverts O as O; apply incl_map_set.
  - applys gamma_astore_compatible_with_order O.
  - applys gamma_aval_compatible_with_order O.
  - applys gamma_interval_compatible_with_order O.
  - applys gamma_abool_compatible_with_order O.
Qed.

(** The sort of abstract flow values follows naturally. **)
Inductive abstract_value_flow_sort : abstract_flow_value -> flow_sort -> Prop :=
  | abstract_value_flow_sort_store : forall sta,
    abstract_value_flow_sort (aVStore sta) Store
  | abstract_value_flow_sort_val : forall v,
    abstract_value_flow_sort (aVVal v) Val
  | abstract_value_flow_sort_int : forall i,
    abstract_value_flow_sort (aVInt i) Int
  | abstract_value_flow_sort_bool : forall b,
    abstract_value_flow_sort (aVBool b) Bool
  .

Lemma abstract_value_flow_sort_unique : forall v s1 s2,
  abstract_value_flow_sort v s1 ->
  abstract_value_flow_sort v s2 ->
  s1 = s2.
Proof. introv S1 S2. inverts S1; inverts~ S2. Qed.

(** Each flow sort is supposed to have a least element. **)
Definition bottom_flow s :=
  match s with
  | Store => aVStore bottom_astore
  | Val => aVVal bottom_aval
  | Int => aVInt interval_empty
  | Bool => aVBool bottom_abool
  end.

(** These bottom elements are supposed to have the right sorts. **)
Lemma bottom_flow_sort : forall s,
  abstract_value_flow_sort (bottom_flow s) s.
Proof. intro s. destruct s; constructors. Qed.

(** These sorts are compatible with the order over abstract flow values. **)
Lemma abstract_flow_Poset_sort : forall v1 v2,
  v1 ⊑ v2 ->
  abstract_value_flow_sort v1 = abstract_value_flow_sort v2.
Proof. introv O. extens. introv. inverts~ O; iff S; inverts S; constructors~. Qed.

(** In this example, we choose not to abstract basic terms (this is
  almost only useful to perform some sorts of symbolic executions,
  typically when abstracting functions).  The concretisation function
  for basic terms is thus trivial. **)

Definition gamma_basic_term : basic_term -> set basic_term := fun t => \{ t }.

End Domains.

Import Domains.


(** **** Abstract Filters **)

Section Intervals.

(** Once the domains have been defined, we can instantiate the abstract interpretation. **)
(** Similarly to the concrete interpretation, we start by defining abstract basic terms. **)
(** In this example however, we choose not to abstract terms: our abstract basic terms are
  the same than the concrete basic terms. **)

(** Again, we instantiate our types and coercions for these specific values. **)
Let avalue : Type := abstract_value constructor base_sort program_sort basic_term abstract_flow_value.
Let aterm : Type := abstract_term constructor base_sort program_sort basic_term.
Let abstract_term_closed : closed_term_free -> aterm := @abstract_term_closed _ _ _ _.
Let abstract_value_term : aterm -> avalue := @abstract_value_term _ _ _ _ _.
Let abstract_value_flow : abstract_flow_value -> avalue := @abstract_value_flow _ _ _ _ _.
Coercion abstract_term_closed : closed_term_free >-> aterm.
Coercion abstract_value_term : aterm >-> avalue.
Coercion abstract_value_flow : abstract_flow_value >-> avalue.

Let avalue_sort : avalue -> sort -> Prop :=
  sort_of_abstract_value constructor_signature basic_term_sort abstract_value_flow_sort.

Let gamma_value : avalue -> LibSet.set cvalue :=
  gamma_value gamma_basic_term gamma_abstract_flow_value.


(** We can now define the abstract interpretation of filters. **)
(** Abstract filters have to overapproximate the behaviour of concrete
  filters.  For instance, let us consider the [IsInt] case: because of
  the way we abstract values, there exist abstract values whose
  concretisation contain both integers and booleans.  The concrete
  version of [IsInt] is only defined on these integers.  To be in sync
  with the concrete interpretation, the abstract filter [IsInt] has
  thus to only consider these integers, leaving the booleans behind.
  This is why filters are called filters in our formalism: they filter
  acceptable values.
  The [None] cases of each filter correspond to the cases where the
  concretisation of their inputs are empty.  These cases could be
  removed from the abstract semantics (and the resulting abstract
  semantics would still be sound), but it may lead to an abstract
  semantics will less triples, and thus less useful to prove a
  particular analyser sound. **)
(** This definition corresponds to Figure 12 of the paper. **)
Inductive filter_interpretation_abstract : filter -> list avalue -> option (list avalue) -> Prop :=
  | filter_interpretation_abstract_LitToInt : forall l,
    filter_interpretation_abstract LitToInt [TLiteral l : avalue]
                                            (Some [aVInt (singleton_interval l) : avalue])
  | filter_interpretation_abstract_IntToVal : forall i,
    filter_interpretation_abstract IntToVal [aVInt i : avalue]
                                            (Some [aVVal (i, bottom_abool) : avalue])
  | filter_interpretation_abstract_IntToVal_bottom : forall i,
    (~ exists n, gamma_interval i n) ->
    filter_interpretation_abstract IntToVal [aVInt i : avalue] None
  | filter_interpretation_abstract_BoolToVal : forall b,
    filter_interpretation_abstract BoolToVal [aVBool b : avalue]
                                             (Some [aVVal (interval_empty, b) : avalue])
  | filter_interpretation_abstract_BoolToVal_bottom :
    filter_interpretation_abstract BoolToVal [aVBool bottom_abool : avalue] None
  | filter_interpretation_abstract_Read : forall x ast v u,
    ast x = (v, u) ->
    filter_interpretation_abstract Read [TIdentifier x : avalue; aVStore ast : avalue]
                                        (Some [aVVal v : avalue])
  | filter_interpretation_abstract_Read_bottom : forall x y ast va,
    ast y = (va, false) ->
    (~ exists v, gamma_aval va v) ->
    filter_interpretation_abstract Read [TIdentifier x : avalue; aVStore ast : avalue] None
  | filter_interpretation_abstract_IsInt : forall i b,
    filter_interpretation_abstract IsInt [aVVal (i, b) : avalue] (Some [aVInt i : avalue])
  | filter_interpretation_abstract_IsInt_bottom : forall i b,
    (~ exists n, gamma_interval i n) ->
    filter_interpretation_abstract IsInt [aVVal (i, b) : avalue] None
  | filter_interpretation_abstract_AddNum : forall i1 i2,
    filter_interpretation_abstract AddNum [aVInt i1 : avalue; aVInt i2 : avalue]
                                          (Some [aVInt (interval_add i1 i2) : avalue])
  | filter_interpretation_abstract_AddNum_bottom_left : forall i1 i2,
    (~ exists n, gamma_interval i1 n) ->
    filter_interpretation_abstract AddNum [aVInt i1 : avalue; aVInt i2 : avalue] None
  | filter_interpretation_abstract_AddNum_bottom_right : forall i1 i2,
    (~ exists n, gamma_interval i2 n) ->
    filter_interpretation_abstract AddNum [aVInt i1 : avalue; aVInt i2 : avalue] None
  | filter_interpretation_abstract_EqNum_eq : forall n,
    filter_interpretation_abstract EqNum [aVInt (singleton_interval n) : avalue; aVInt (singleton_interval n) : avalue]
                                         (Some [aVBool (singleton_abool true) : avalue])
  | filter_interpretation_abstract_EqNum_neq : forall i1 i2,
    ~ interval_intersects i1 i2 ->
    filter_interpretation_abstract EqNum [aVInt i1 : avalue; aVInt i2 : avalue]
                                         (Some [aVBool (singleton_abool false) : avalue])
  | filter_interpretation_abstract_EqNum_top : forall i1 i2,
    interval_intersects i1 i2 ->
    i1 <> i2 ->
    filter_interpretation_abstract EqNum [aVInt i1 : avalue; aVInt i2 : avalue]
                                         (Some [aVBool top_abool : avalue])
  | filter_interpretation_abstract_EqNum_bottom_left : forall i1 i2,
    (~ exists n, gamma_interval i1 n) ->
    filter_interpretation_abstract EqNum [aVInt i1 : avalue; aVInt i2 : avalue] None
  | filter_interpretation_abstract_EqNum_bottom_right : forall i1 i2,
    (~ exists n, gamma_interval i2 n) ->
    filter_interpretation_abstract EqNum [aVInt i1 : avalue; aVInt i2 : avalue] None
  | filter_interpretation_abstract_IsBool : forall i b,
    b <> bottom_abool ->
    filter_interpretation_abstract IsBool [aVVal (i, b) : avalue]
                                          (Some [aVBool b : avalue])
  | filter_interpretation_abstract_IsBool_bottom : forall i,
    filter_interpretation_abstract IsBool [aVVal (i, bottom_abool) : avalue] None
  | filter_interpretation_abstract_NegBool : forall b,
    b <> bottom_abool ->
    filter_interpretation_abstract NegBool [aVBool b : avalue]
                                           (Some [aVBool (map_set negb b) : avalue])
  | filter_interpretation_abstract_NegBool_bottom :
    filter_interpretation_abstract NegBool [aVBool bottom_abool : avalue] None
  | filter_interpretation_abstract_Write : forall x ast v,
    filter_interpretation_abstract Write [TIdentifier x : avalue; aVStore ast : avalue; aVVal v : avalue]
                                         (Some [aVStore (fun y => ifb x = y then (v, false) else ast y) : avalue])
  | filter_interpretation_abstract_Write_bottom : forall x y ast va va',
    ast y = (va, false) ->
    (~ exists v, gamma_aval va v) ->
    filter_interpretation_abstract Write [TIdentifier x : avalue; aVStore ast : avalue; aVVal va' : avalue]
                                         None
  | filter_interpretation_abstract_Id : forall sta,
    filter_interpretation_abstract Id [aVStore sta : avalue] (Some [aVStore sta : avalue])
  | filter_interpretation_abstract_Id_bottom : forall ast y va,
    ast y = (va, false) ->
    (~ exists v, gamma_aval va v) ->
    filter_interpretation_abstract Id [aVStore ast : avalue] None
  | filter_interpretation_abstract_IsTrue : forall b,
    gamma_abool b true ->
    filter_interpretation_abstract IsTrue [aVBool b : avalue] (Some [])
  | filter_interpretation_abstract_IsTrue_bottom : forall b,
    ~ gamma_abool b true ->
    filter_interpretation_abstract IsTrue [aVBool b : avalue] None
  | filter_interpretation_abstract_IsFalse : forall b,
    gamma_abool b false ->
    filter_interpretation_abstract IsFalse [aVBool b : avalue] (Some [])
  | filter_interpretation_abstract_IsFalse_bottom : forall b,
    ~ gamma_abool b false ->
    filter_interpretation_abstract IsFalse [aVBool b : avalue] None
  .

(** We instantiate the abstract interpretation to this skeletal semantics.
  As we don’t abstact basic terms, we directly reuse the corresponding
  functions from the concrete semantics.  To abstract the basic terms,
  a function [basic_term_sort] would have to be defined for the abstract
  basic terms. **)
Let abstract c : triple_based_interpretation constructor filter basic_term _ _ :=
  abstract out_sort filter_signature constructor_signature basic_term_sort
           abstract_flow_value_Poset bottom_flow
           filter_interpretation_abstract
           (skeleton_instance_term_example_Comparable c)
           (skeleton_instance_flow_example_Comparable c).

(** We now define our abstract semantics by connecting the abstract
  interpretation of filters to the skeletal semantics
  [skeletal_semantics_example].  As in the concrete case, we get a
  set of triples [(sigma, t, o)] where [o] is the (abstract) result
  of the evaluation of [t] in the context of [sigma]. **)
Definition abstract_semantics_example : avalue -> closed_term_free -> avalue -> Prop :=
  abstract_semantics in_sort out_sort filter_signature constructor_signature
                     basic_term_sort abstract_value_flow_sort
                     abstract_flow_value_Poset bottom_flow
                     filter_interpretation_abstract
                     skeletal_semantics_example _ _.

(** The two definitions [filter_interpretation_abstract] and [filter_signature] should match.
  This lemma should be straightforward to prove in almost all semantics. **)
Lemma filter_interpretation_abstract_consistent_with_sort_example : forall f vs1 vss1 vs2 vss2,
  filter_interpretation_abstract f vs1 (Some vs2) ->
  filter_signature f = (vss1, vss2) ->
  Forall2 avalue_sort vs1 vss1 ->
  Forall2 avalue_sort vs2 vss2.
Proof. introv D E F. inverts D; inverts E; repeat inverts F as ? F; repeat constructors~. Qed.

(** More interesting is to prove that [filter_interpretation_abstract] and
  [filter_interpretation_concrete] are in sync.  This lemma states that the
  concrete filters are soundly abstracted by the abstract filters. **)
(** The proof of this lemma will be in most semantics the most difficult
  lemma to prove: for each filter, you will have to prove that your
  abstraction indeed matches the behaviour of the concrete filter.  This
  proof is thus very dependent on how you defined your abstract domains. **)
(** It corresponds to Lemma 5.12 of the paper. **)
Lemma filter_interpretations_consistent : forall f vs vs' avs avs',
  filter_interpretation_concrete f vs vs' ->
  filter_interpretation_abstract f avs (Some avs') ->
  Forall2 gamma_value avs vs ->
  Forall2 gamma_value avs' vs'.
Proof.
  introv C A G. inverts C.
  - (** [LitToInt] case **)
    inverts G as G G'. inverts G'. inverts A. repeat inverts G as G. repeat constructors~.
  - (** [IntToVal] case **)
    inverts G as G G'. inverts G'. inverts A. do 2 inverts G as G. repeat constructors~.
  - (** [BoolToVal] case **)
    inverts G as G G'. inverts G'. inverts A. do 2 inverts G as G. repeat constructors~.
  - (** [Read] case **)
    inverts G as G1 G'. inverts G' as G2 G'. inverts G'. inverts A.
    repeat inverts G1 as G1. repeat inverts G2 as G2. repeat constructors~.
    forwards G': G2 x2. rewrite H in G'. rewrite H3 in G'. apply G'.
  - (** [IsInt] case **)
    inverts G as G G'. inverts G'. inverts A. do 2 inverts G as G. repeat constructors~.
  - (** [AddNum] case **)
    inverts G as G1 G'. inverts G' as G2 G'. inverts G'. inverts A.
    do 2 inverts G1 as G1. do 2 inverts G2 as G2. repeat constructors~.
    applys gamma_interval_add G1 G2.
  - (** [EqNum], equality case **)
    inverts G as G1 G'. inverts G' as G2 G'. inverts G'. inverts A.
    + do 2 inverts G1 as G1. do 2 inverts G2 as G2. repeat constructors~.
    + do 2 inverts G1 as G1. do 2 inverts G2 as G2. false H2. eexists. splits*.
    + do 2 inverts G1 as G1. do 2 inverts G2 as G2. repeat constructors~.
  - (** [EqNum], inequality case **)
    inverts G as G1 G'. inverts G' as G2 G'. inverts G'. inverts A.
    + do 3 inverts G1 as G1. do 3 inverts G2 as G2. false H. math.
    + do 2 inverts G1 as G1. do 2 inverts G2 as G2. repeat constructors~.
    + do 2 inverts G1 as G1. do 2 inverts G2 as G2. repeat constructors~.
  - (** [IsBool] case **)
    inverts G as G G'. inverts G'. inverts A. do 2 inverts G as G. repeat constructors~.
  - (** [NegBool] case **)
    inverts G as G G'. inverts G'. inverts A. do 2 inverts G as G. repeat constructors~.
  - (** [Write] case **)
    inverts G as G1 G'. inverts G' as G2 G'. inverts G' as G3 G'. inverts G'. inverts A.
    repeat inverts G1 as G1. repeat inverts G2 as G2. repeat inverts G3 as G3. repeat constructors~.
    intro x. cases_if as C; fold_bool; rew_refl in C.
    + subst. apply G3.
    + apply G2.
  - (** [Id] case **)
    inverts G as G G'. inverts G'. inverts A. repeat inverts G as G. repeat constructors~.
  - (** [IsTrue] case **)
    inverts A. constructors~.
  - (** [IsFalse] case **)
    inverts A. constructors~.
Qed.

(** We also prove the similar lemma: when the concrete filters are defined,
  then so does the abstract filters.  In this context, [None] is the
  equivalent of the [⊥] construct of the paper. **)
Lemma filter_interpretations_consistent_None : forall f vs vs' avs,
  Forall2 gamma_value avs vs ->
  filter_interpretation_concrete f vs vs' ->
  ~ filter_interpretation_abstract f avs None.
Proof.
  introv G C A. inverts C.
  - (** [LitToInt] case **)
    inverts G as G G'. inverts G'. inverts A.
  - (** [IntToVal] case **)
    inverts G as G G'. inverts G'. inverts A. do 2 inverts G as G. false*.
  - (** [BoolToVal] case **)
    inverts G as G G'. inverts G'. inverts A. repeat inverts G as G.
  - (** [Read] case **)
    inverts G as G1 G'. inverts G' as G2 G'. inverts G'. inverts A.
    repeat inverts G2 as G2. forwards Gy: G2 y. rewrite H2 in Gy.
    destruct (st y); tryfalse. false* H3.
  - (** [IsInt] case **)
    inverts G as G G'. inverts G'. inverts A. do 2 inverts G as G. false* H0.
  - (** [AddNum] case **)
    inverts G as G1 G'. inverts G' as G2 G'. inverts G'. inverts A.
    + do 2 inverts G1 as G1. false* H1.
    + do 2 inverts G2 as G2. false* H1.
  - (** [EqNum], equality case **)
    inverts G as G1 G'. inverts G' as G2 G'. inverts G'. inverts A.
    + do 2 inverts G1 as G1. false* H1.
    + do 2 inverts G2 as G2. false* H1.
  - (** [EqNum], inequality case **)
    inverts G as G1 G'. inverts G' as G2 G'. inverts G'. inverts A.
    + do 2 inverts G1 as G1. false* H2.
    + do 2 inverts G2 as G2. false* H2.
  - (** [IsBool] case **)
    inverts G as G G'. inverts G'. inverts A. repeat inverts G as G.
  - (** [NegBool] case **)
    inverts G as G G'. inverts G'. inverts A. repeat inverts G as G.
  - (** [Write] case **)
    inverts G as G1 G'. inverts G' as G2 G'. inverts G' as G3 G'. inverts G'. inverts A.
    repeat inverts G2 as G2. forwards Gy: G2 y. rewrite H2 in Gy.
    destruct (st y); tryfalse. false* H3.
  - (** [Id] case **)
    inverts G as G G'. inverts G'. inverts A. repeat inverts G as G.
    forwards Gy: G y. rewrite H0 in Gy. destruct (st y); tryfalse. false* H1.
  - (** [IsTrue] case **)
    inverts G as G G'. inverts G'. inverts A. repeat inverts G as G. false~.
  - (** [IsFalse] case **)
    inverts G as G G'. inverts G'. inverts A. repeat inverts G as G. false~.
Qed.


(** We can then conclude that the abstract semantics is well-formed by applying the
  theorem [abstract_semantics_wellformed]. **)
Lemma abstract_semantics_wellformed_example :
  wellformed_set_of_triples in_sort out_sort constructor_signature
                            basic_term_sort avalue_sort avalue_sort abstract_semantics_example.
Proof. applys abstract_semantics_wellformed filter_signature; try typeclass. Qed.

(** The axioms used by this theorem are included in the ones used by
  [skeletal_semantics_wellformed_example].
<<
Print Assumptions abstract_semantics_wellformed_example.
>> **)

Let correct_abstract_triple_set : abstract_triple_set _ _ _ _ _ -> Prop :=
  correct_abstract_triple_set in_sort out_sort constructor_signature basic_term_sort
                              abstract_value_flow_sort value_flow_sort basic_term_sort
                              gamma_basic_term gamma_abstract_flow_value filter_interpretation_concrete
                              skeletal_semantics_example _ _.

(** Having a well-sorted abstract semantics is interesting, but the theorem
  that we really want is for it to be sound with respect to the concrete
  semantics.  This is expressed by the following theorem. **)
(** The [abstract_semantics_correct] theorem requires a lot of properties to
  be proven.  However, each of these properties are local and should be
  simple to prove in most semantics. **)
(** It corresponds to Lemma 5.13 of the paper. **)
Lemma abstract_semantics_correct_example : correct_abstract_triple_set abstract_semantics_example.
Proof.
  apply abstract_semantics_correct; try typeclass.
  - apply basic_term_sort_unique.
  - apply abstract_flow_Poset_sort.
  - apply bottom_flow_sort.
  - apply filter_interpretation_abstract_consistent_with_sort_example.
  - apply basic_term_sort_unique.
  - apply gamma_abstract_flow_value_compatible_with_order.
  - apply filter_interpretation_concrete_consistent_with_sort_example.
  - apply filter_interpretations_consistent.
  - apply filter_interpretations_consistent_None.
  - apply skeletal_semantics_wellformed_example.
Qed.

(** The axioms used by this theorem are the same than
  [skeletal_semantics_wellformed_example] above.
<<
Print Assumptions abstract_semantics_correct_example.
>> **)

End Intervals.


(** *** Flat Domains **)

(** The interval analysis follows a common pattern of abstraction.
  We now present a much simpler approach consisting of using much
  simpler domains.  These domains are exactly the concrete elements,
  with an added top and bottom elements.  In this instantiation,
  we also abstract basic terms. **)

Module Flat.

Import TLC.LibSet.

Section Parametrised.

Variable T sort : Type.

(** We suppose the value of type [T] to be sorted. **)
Variable Tsort : T -> sort -> Prop.

(** In our framework, we impose every values to be sorted:
  this also applies to the top and bottom elements. **)
Inductive flat :=
  | flat_direct : T -> flat
  | flat_top : sort -> flat
  | flat_bottom : sort -> flat
  .
Coercion flat_direct : T >-> flat.

Inductive gamma_flat : flat -> set T :=
  | gamma_flat_direct : forall t : T,
    gamma_flat t t
  | gamma_flat_top : forall t s,
    Tsort t s ->
    gamma_flat (flat_top s) t
  .

Inductive flat_sort : flat -> sort -> Prop :=
  | flat_sort_direct : forall t s,
    Tsort t s ->
    flat_sort t s
  | flat_sort_top : forall s,
    flat_sort (flat_top s) s
  | flat_sort_bottom : forall s,
    flat_sort (flat_bottom s) s
  .

Definition flat_order t1 t2 :=
  match t1, t2 with
  | flat_bottom s, _ => flat_sort t2 s
  | _, flat_top s => flat_sort t1 s
  | flat_direct t1, flat_direct t2 => t1 = t2
  | _, _ => False
  end.

Hypothesis Tsort_unique : forall t s1 s2,
  Tsort t s1 ->
  Tsort t s2 ->
  s1 = s2.

Lemma flat_sort_unique : forall t s1 s2,
  flat_sort t s1 ->
  flat_sort t s2 ->
  s1 = s2.
Proof. introv S1 S2. inverts S1 as S1; inverts S2 as S2; autos~. applys Tsort_unique S1 S2. Qed.

Instance flat_Poset : Poset flat.
  applys make_Poset flat_order.
  - intro x. destruct x; constructors.
  - intros x y O1 O2. destruct x, y; inverts O1; inverts O2; substs~.
  - intros x y z O1 O2. destruct x, y, z; inverts O1 as O1; inverts O2 as O2; substs; try constructors~.
    forwards: Tsort_unique O1 O2. substs. constructors.
Defined.

Lemma gamma_flat_compatible_with_order : forall v1 v2,
  v1 ⊑ v2 ->
  gamma_flat v1 \c gamma_flat v2.
Proof.
  intros v1 v2 O. rewrite incl_in_eq. introv G.
  inverts G; destruct v2; simpls; try inverts O as O; substs; constructors~.
Qed.

Lemma gamma_flat_sort : forall v vc,
  gamma_flat v vc ->
  flat_sort v = Tsort vc.
Proof.
  introv G. extens. inverts G as G.
  - iff S.
    + inverts~ S.
    + constructors~.
  - iff S.
    + inverts~ S.
    + forwards: Tsort_unique G S. substs. constructors.
Qed.

Lemma flat_order_compatible_with_sort : forall v1 v2,
  v1 ⊑ v2 ->
  flat_sort v1 = flat_sort v2.
Proof.
  introv O. destruct v1 as [v1|s1|s1], v2 as [v2|s2|s2]; inverts O as O; autos~; extens.
  - iff S.
    + inverts S as S. forwards~: Tsort_unique O S. substs. constructors~.
    + inverts S. constructors~.
  - iff S.
    + inverts S. constructors~.
    + inverts S as S. forwards~: Tsort_unique O S. substs. constructors~.
  - iff S; inverts S as S; constructors~.
Qed.

End Parametrised.

End Flat.

Import Flat.

Section FlatDomains.

Let aflow_value := flat flow_value flow_sort.
Let abasic_term := flat basic_term base_sort.

Let avalue : Type := abstract_value constructor base_sort program_sort abasic_term aflow_value.
Let aterm : Type := abstract_term constructor base_sort program_sort abasic_term.
Let aclosed_term_free : Type := Terms.closed_term_free constructor abasic_term.
Let abstract_term_closed : aclosed_term_free -> aterm := @abstract_term_closed _ _ _ _.
Let abstract_value_term : aterm -> avalue := @abstract_value_term _ _ _ _ _.
Let abstract_value_flow : aflow_value -> avalue := @abstract_value_flow _ _ _ _ _.
Coercion abstract_term_closed : aclosed_term_free >-> aterm.
Coercion abstract_value_term : aterm >-> avalue.
Coercion abstract_value_flow : aflow_value >-> avalue.

Let avalue_eq : avalue -> avalue -> Prop :=
  @abstract_value_eq _ _ _ _ _.

Let cvalue_eq : cvalue -> cvalue -> Prop :=
  @value_eq _ _ _.

Let abasic_term_sort : abasic_term -> base_sort -> Prop :=
  flat_sort basic_term_sort.
Let aflow_value_sort : aflow_value -> flow_sort -> Prop :=
  flat_sort value_flow_sort.

Let avalue_sort : avalue -> sort -> Prop :=
  sort_of_abstract_value constructor_signature abasic_term_sort aflow_value_sort.

Let gamma_abasic_term : abasic_term -> _ :=
  gamma_flat basic_term_sort.
Let gamma_aflow_value : aflow_value -> _ :=
  gamma_flat value_flow_sort.

Let gamma_value : avalue -> LibSet.set cvalue :=
  gamma_value gamma_abasic_term gamma_aflow_value.

Let is_bottom : avalue -> Prop := is_bottom (@flat_bottom _ _).

Let aflow_value_Poset : Poset aflow_value := flat_Poset _ value_flow_sort_unique.

Inductive abasic_term_same : abasic_term -> basic_term -> Prop :=
  | abasic_term_same_intro : forall b,
    abasic_term_same (flat_direct _ b) b
  .

(** States that the abstract and concrete values are the same, with some constructor
  [flat_direct] in the way. **)
Inductive same_flat_structure : avalue -> cvalue -> Prop :=
  | same_flat_structure_term : forall ta t,
    same_structure (fun _ _ => False) abasic_term_same
                   (closed_term_free_term ta) (closed_term_free_term t) ->
    same_flat_structure (ta : aclosed_term_free) (t : closed_term_free)
  | same_flat_structure_flow : forall v,
    same_flat_structure (flat_direct _ v : aflow_value) v
  .

Lemma abasic_term_same_gamma_abasic_term : forall ab b,
  abasic_term_same ab b ->
  gamma_abasic_term ab b.
Proof. introv E. inverts E. constructors. Qed.

(** The relation [same_flat_structure] is compatible with the concretisation function. **)
Lemma same_flat_structure_gamma_value : forall av v,
  same_flat_structure av v ->
  gamma_value av v.
Proof.
  introv E. inverts E as E; constructor~.
  - applys~ same_structure_weaken abasic_term_same_gamma_abasic_term E.
  - constructors.
Qed.

(** The relation [same_flat_structure] is compatible with equality. **)
Lemma same_flat_structure_eq : forall av1 av2 v1 v2,
  avalue_eq av1 av2 ->
  cvalue_eq v1 v2 ->
  same_flat_structure av1 v1 ->
  same_flat_structure av2 v2.
Proof.
  introv E1 E2 E3. inverts E3 as E3; inverts E1 as E1; inverts E2 as E2.
  - inverts E1 as E1. constructors. apply same_structure_swap_sym in E1.
    applys same_structure_trans E2; simpl.
    + introv _ A _. apply A.
    + introv A ?. substs. apply A.
    + applys~ same_structure_trans E1 E3. simpl. intros. substs~.
  - constructors.
Qed.

(** The concretisation function [same_flat_structure] is compatible with sort. **)
Lemma same_flat_structure_sort : forall av cv,
  same_flat_structure av cv ->
  avalue_sort av = cvalue_sort cv.
Proof.
  introv E. extens. inverts E as E.
  - iff S.
    + do 2 inverts S as S. constructors~. applys~ sort_of_term_same_structure S E.
      introv G. inverts G. applys~ gamma_flat_sort basic_term_sort_unique. constructors.
    + inverts S as S. do 2 constructors~.
      applys sort_of_term_same_structure S; [| | applys~ same_structure_sym_swap E ]; simpl.
      * introv G. inverts G. symmetry. applys~ gamma_flat_sort basic_term_sort_unique. constructors.
      * autos~.
  - iff S.
    + do 2 inverts S as S. constructors~.
    + inverts S as S. do 2 constructors~.
Qed.

(** In contrary to the usual [filter_interpretation_abstract], where we reimplemented
  the filters in the abstract domains, we here define the abstract interpretation of
  filters from their concrete interpretation. **)
Inductive filter_interpretation_flat : filter -> list avalue -> option (list avalue) -> Prop :=
  | filter_interpretation_flat_normal : forall f vs vs' vsa vsa',
    filter_interpretation_concrete f vs vs' ->
    Forall2 same_flat_structure vsa vs ->
    Forall2 same_flat_structure vsa' vs' ->
    filter_interpretation_flat f vsa (Some vsa')
  | filter_interpretation_flat_bottom : forall f vsa,
    Exists is_bottom vsa ->
    filter_interpretation_flat f vsa None
  | filter_interpretation_flat_top : forall f vss1 vss2,
    filter_signature f = (map (id : flow_sort -> sort) vss1, map (id : flow_sort -> sort) vss2) ->
    filter_interpretation_flat f (map ((@flat_top _ _ : flow_sort -> aflow_value) : _ -> avalue) vss1)
                                 (Some (map ((@flat_top _ _ : flow_sort -> aflow_value) : _ -> avalue) vss2))
  .

Let abstract c : triple_based_interpretation constructor filter abasic_term _ _ :=
  abstract out_sort filter_signature constructor_signature abasic_term_sort
           aflow_value_Poset (@flat_bottom _ _)
           filter_interpretation_flat
           (skeleton_instance_term_example_Comparable c)
           (skeleton_instance_flow_example_Comparable c).

Definition flat_semantics_example : avalue -> aclosed_term_free -> avalue -> Prop :=
  abstract_semantics in_sort out_sort filter_signature constructor_signature
                     abasic_term_sort aflow_value_sort
                     aflow_value_Poset (@flat_bottom _ _)
                     filter_interpretation_flat
                     skeletal_semantics_example _ _.

(** We prove the local lemmas relating the abstract filters with their signatures. **)
Lemma filter_interpretation_flat_consistent_with_sort_example : forall f vs1 vss1 vs2 vss2,
  filter_interpretation_flat f vs1 (Some vs2) ->
  filter_signature f = (vss1, vss2) ->
  Forall2 avalue_sort vs1 vss1 ->
  Forall2 avalue_sort vs2 vss2.
Proof.
  introv D E F. inverts D as D F1 F2.
  - rewrite Forall2_iff_forall_Nth in *. lets (El&F'): (rm F).
    lets (El1&F1'): (rm F1). lets (El2&F2'): (rm F2).
    forwards~ F3: filter_interpretation_concrete_consistent_with_sort_example D E.
    { rewrite Forall2_iff_forall_Nth. splits.
      - rewrite~ <- El1.
      - introv N1 N2. forwards (s&N3): length_Nth_lt.
        { rewrite El. applys~ Nth_lt_length N2. }
        forwards~ E': F1' N3 N1. rewrites <- >> same_flat_structure_sort E'.
        applys~ F' N3 N2. }
    rewrite Forall2_iff_forall_Nth in F3. lets (El3&F3'): (rm F3). splits.
    + rewrite~ El2.
    + introv N1 N2. forwards (s&N3): length_Nth_lt.
      { rewrite El3. applys~ Nth_lt_length N2. }
      forwards~ E': F2' N1 N3. rewrites >> same_flat_structure_sort E'. applys~ F3' N3 N2.
  - rewrite D in E. inverts E. rewrite Forall2_iff_forall_Nth in *. lets (_&F'): (rm F).
    repeat rewrite length_map. splits~.
    introv N1 N2. forwards (s1&N1'&?): map_Nth_inv (rm N1). forwards (s2&N2'&?): map_Nth_inv (rm N2).
    forwards: Nth_func N1' N2'. substs. repeat constructors.
Qed.

Lemma filter_interpretation_concrete_deterministic : forall f vs1 vs1' vs2 vs2',
  filter_interpretation_concrete f vs1 vs2 ->
  filter_interpretation_concrete f vs1' vs2' ->
  Forall2 cvalue_eq vs1 vs1' ->
  Forall2 cvalue_eq vs2 vs2'.
Proof.
  introv D1 D2 F. set (f' := f). inverts D1; inverts D2;
    repeat (let E := fresh "E" in inverts F as E F; repeat inverts E as E);
    tryfalse~; repeat constructors~.
  - (** [Read] case. **)
    rewrite H0 in H. inverts H. constructors.
Qed.

(** We prove the local lemmas relating the abstract filters with the concrete filters. **)
Lemma filter_interpretations_flat_consistent : forall f vs vs' avs avs',
  filter_interpretation_concrete f vs vs' ->
  filter_interpretation_flat f avs (Some avs') ->
  Forall2 gamma_value avs vs ->
  Forall2 gamma_value avs' vs'.
Proof.
  introv D Dc F. inverts Dc as D' F1 F2.
  - rewrite Forall2_iff_forall_Nth in *. lets (El&F'): (rm F).
    lets (El1&F1'): (rm F1). lets (El2&F2'): (rm F2).
    asserts F3: (Forall2 cvalue_eq vs vs0).
    { rewrite Forall2_iff_forall_Nth. splits.
      - rewrite~ <- El.
      - introv N1 N2. forwards (av&N3): length_Nth_lt.
        { rewrite El1. applys~ Nth_lt_length N2. }
        forwards~ G: F' N3 N1. forwards~ E: F1' N3 N2. inverts E as E; inverts G as G.
        + apply same_structure_swap_sym in G. constructors.
          forwards E': same_structure_trans (fun b1 b2 : basic_term => b1 = b2) G E; simpl.
          * introv _ A _. apply A.
          * introv G' E'. inverts E'. inverts~ G'.
          * applys~ same_structure_weaken E'. simpl. autos~.
        + inverts G. constructors~. }
    forwards F4: filter_interpretation_concrete_deterministic D D' F3.
    rewrite Forall2_iff_forall_Nth in F4. lets (El4&F4'): (rm F4). splits.
    + rewrite~ El4.
    + introv N1 N2. forwards (av&N3): length_Nth_lt.
      { rewrite <- El4. applys~ Nth_lt_length N2. }
      forwards E1: F4' N2 N3. forwards E2: F2' N1 N3.
      apply same_flat_structure_gamma_value. applys~ same_flat_structure_eq E2.
      * apply~ abstract_value_eq_refl.
      * applys~ value_eq_sym E1.
  - forwards F1: filter_interpretation_concrete_consistent_with_sort_example D D'.
    { rewrite Forall2_iff_forall_Nth in *. lets (El&F'): (rm F).
      rewrite length_map in *. splits~.
      introv N1 N2. forwards (s&N3&E3): map_Nth_inv (rm N2). substs. forwards G: F' N1.
      { applys~ map_Nth N3. }
      inverts G as G. forwards~ S: gamma_flat_sort value_flow_sort_unique G.
      constructors. rewrite <- S. constructors~. }
    rewrite Forall2_iff_forall_Nth in F1. lets (El&F1'): (rm F1).
    rewrite Forall2_iff_forall_Nth. rewrite length_map in *. splits~.
    introv N1 N2. forwards (v3&N3&E3): map_Nth_inv (rm N1). substs. forwards S: F1' N2.
    { applys~ map_Nth N3. }
    inverts S. do 2 constructors~.
Qed.

Lemma filter_interpretations_flat_consistent_None : forall f vs vs' avs,
  Forall2 gamma_value avs vs ->
  filter_interpretation_concrete f vs vs' ->
  ~ filter_interpretation_flat f avs None.
Proof.
  introv F D Dc. inverts Dc as E. rewrite Exists_iff_exists_mem in E. lets (av&M&B): (rm E).
  rewrite Forall2_iff_forall_Nth in F. lets (El&F'): (rm F).
  lets (n&N1): mem_Nth (rm M). forwards (v&N2): length_Nth_lt.
  { rewrite <- El. applys~ Nth_lt_length N1. }
  forwards G: F' N1 N2. inverts B. destruct s; repeat inverts G as G.
Qed.

Lemma flat_semantics_wellformed_example :
  wellformed_set_of_triples in_sort out_sort constructor_signature
                            abasic_term_sort avalue_sort avalue_sort flat_semantics_example.
Proof. applys abstract_semantics_wellformed filter_signature; try typeclass. Qed.

Let correct_flat_triple_set : abstract_triple_set _ _ _ _ _ -> Prop :=
  correct_abstract_triple_set in_sort out_sort constructor_signature abasic_term_sort
                              aflow_value_sort value_flow_sort basic_term_sort
                              gamma_abasic_term gamma_aflow_value filter_interpretation_concrete
                              skeletal_semantics_example _ _.

Lemma flat_semantics_correct_example : correct_flat_triple_set flat_semantics_example.
Proof.
  apply abstract_semantics_correct; try typeclass.
  - applys flat_sort_unique basic_term_sort_unique.
  - apply flat_order_compatible_with_sort.
  - apply filter_interpretation_flat_consistent_with_sort_example.
  - apply basic_term_sort_unique.
  - apply gamma_flat_compatible_with_order.
  - apply filter_interpretation_concrete_consistent_with_sort_example.
  - apply filter_interpretations_flat_consistent.
  - apply filter_interpretations_flat_consistent_None.
  - apply skeletal_semantics_wellformed_example.
Qed.

End FlatDomains.


(** *** Collecting Semantics **)

(* TODO *)


(** *** Collapsing Domains **)

(* TODO *)

(** Definitions of terms. **)

Set Implicit Arguments.

Require Export Common.

Section Sorts.

(** * Terms **)

(** We first introduce sorts, constructors, and symbols used by the
  semantics. **)

(** ** Main Definitions **)

(** *** Sorts **)

(** Sorts are used to ensure that terms are well-formed.
  They are divided between base sorts and program sorts.
  Program sorts are the sort of “executable” terms, whilst base sorts
  are non-executable (typically, basic data structures such as numbers).
  In particular, only program sorts are associated an in and out sort
  in the well-formedness interpretation. **)
Variable base_sort : Type.
Variable program_sort : Type.

Inductive term_sort : Type :=
  | sort_base : base_sort -> term_sort
  | sort_program : program_sort -> term_sort
  .
Coercion sort_base : base_sort >-> term_sort.
Coercion sort_program : program_sort >-> term_sort.

(** We suppose these types to be comparable. Most of these
  [Comparable] hypotheses are only supposed for convenience:
  most definitions and lemmas don’t actually use these
  hypotheses, and Coq removes the useless dependency when
  closing the final section. **)

Hypothesis base_sort_Comparable : Comparable base_sort.
Hypothesis program_sort_Comparable : Comparable program_sort.

Global Instance term_sort_Comparable : Comparable term_sort.
  prove_comparable.
Defined.

Global Instance term_sort_Inhab : Inhab program_sort -> Inhab term_sort.
  introv I. applys prove_Inhab (sort_program arbitrary).
Defined.


(** *** Term constructors **)

(** Terms are build using constructors. **)
Variable constructor : Type.

Hypothesis constructor_Comparable : Comparable constructor.

(** Each constructor is given a signature, that is the list of sorts
  that they expect to be given, and the resulting (program) sort. **)
Variable constructor_signature : constructor -> list term_sort * program_sort.

(** The arity of constructors directly follows from their signature. **)
Definition constructor_arity c := length (fst (constructor_signature c)).


(** *** Definition of Terms **)

(** Base terms can change depending on interpretations.
  As such, they need a section of their own. **)
Section VariableTerm.

(** The type of basic terms (also called base terms). **)
Variable basic_term : Type.

Hypothesis basic_term_Comparable : Comparable basic_term.

(** The type of term variables. **)
Variable variable_term : Type.

Hypothesis variable_term_Comparable : Comparable variable_term.

(** A term is either a variable, a base term, or a constructor
  associated with its subterms.  We do not enforce in this
  definition that the subterms correspond to the signature of
  the current constructor: it will be enforced by the
  [sort_of_term] predicate. **)
Inductive term : Type :=
  | term_variable : variable_term -> term
  | term_basic : basic_term -> term
  | term_constructor : constructor -> list term -> term
  .
Coercion term_variable : variable_term >-> term.
Coercion term_basic : basic_term >-> term.


(** *** Recursion Principles **)

(** Because of the [list term], the recursion principled defined by Coq is
  not very useful.  Here is an alternative. **)
Fixpoint term_rect' (P : term -> Type) (Q : list term -> Type)
    (f_variable : forall x, P (term_variable x))
    (f_basic : forall b, P (term_basic b))
    (f_constructor : forall c ts, Q ts -> P (term_constructor c ts))
    (g_nil : Q nil)
    (g_cons : forall t ts, P t -> Q ts -> Q (t :: ts))
    (t : term) : P t :=
  match t with
  | term_variable x => f_variable x
  | term_basic b => f_basic b
  | term_constructor c ts =>
    f_constructor c ts (list_rect Q g_nil (fun t ts H =>
      g_cons _ _ (term_rect' P Q f_variable f_basic f_constructor g_nil g_cons t) H) ts)
  end.
Definition term_ind' (P : term -> Prop) (Q : list term -> Prop) :=
  term_rect' P Q.

(** In the cases where we just have to apply recursively the same function,
  we can use this simplified (but non-dependent) version. **)
Definition term_rect_list T f_variable f_basic f_constructor :=
  term_rect' (fun _ => T) (fun _ => list T) f_variable f_basic f_constructor
    nil (fun _ _ => cons).

(** This is the equivalent of [term_rect_list], but in the propositional case. **)
Definition term_ind_Forall P f_variable f_basic f_constructor :=
  term_ind' P (Forall P) f_variable f_basic f_constructor
    (Forall_nil P) (fun t ts => @Forall_cons _ P ts t).

(** Sometimes, we need to carry some invariants to build a term.
  The following inductive predicate combines both [term_rect'] and [term_ind_Forall]
  to this end. **)
Definition term_rect_list_such_that (P : _ -> Prop) T f_variable f_basic f_constructor :=
  term_rect' (fun t => P t -> T) (fun l => Forall P l -> list T) f_variable f_basic f_constructor
    (fun _ => nil) (fun _ _ t ts F => t (Forall_inv_head F) :: ts (Forall_inv_tail F)).

Lemma term_rect_list_unfold : forall T f_variable f_basic f_constructor c ts,
  (term_rect_list f_variable f_basic f_constructor (term_constructor c ts) : T)
  = f_constructor c ts (LibList.map (term_rect_list f_variable f_basic f_constructor) ts).
Proof.
  introv. do 2 unfolds. fequals. induction ts.
  - reflexivity.
  - simpls. rew_list in *. fequals.
Qed.


(** *** Instances **)

Global Instance term_Inhab : Inhab constructor -> Inhab term.
  introv C. apply prove_Inhab. apply term_constructor; apply arbitrary.
Defined.

Global Instance term_Comparable : Comparable term.
  constructor. intros t1 t2. gen t2.
  eapply term_rect' with (P := fun t1 => forall t2, Decidable (t1 = t2)) (Q := fun ts1 =>
    forall ts2, Decidable (ts1 = ts2)).
  - intros v1. destruct t2 as [v2|b2|].
    + applys Decidable_equiv (v1 = v2).
      * iff I; inverts~ I.
      * typeclass.
    + applys Decidable_equiv False.
      * iff I; inverts~ I.
      * typeclass.
    + applys Decidable_equiv False.
      * iff I; inverts~ I.
      * typeclass.
  - intros b1. destruct t2 as [v2|b2|].
    + applys Decidable_equiv False.
      * iff I; inverts~ I.
      * typeclass.
    + applys Decidable_equiv (b1 = b2).
      * iff I; inverts~ I.
      * typeclass.
    + applys Decidable_equiv False.
      * iff I; inverts~ I.
      * typeclass.
  - introv F. introv. destruct t2 as [| |c2 ts2].
    + applys Decidable_equiv False.
      * iff I; inverts~ I.
      * typeclass.
    + applys Decidable_equiv False.
      * iff I; inverts~ I.
      * typeclass.
    + applys Decidable_equiv (c = c2 /\ ts = ts2).
      * iff I; inverts~ I.
      * typeclass.
  - introv. destruct ts2.
    + applys Decidable_equiv True.
      * iff~ I.
      * typeclass.
    + applys Decidable_equiv False.
      * iff I; inverts~ I.
      * typeclass.
  - introv D1 D2. introv. destruct ts2 as [|t2 ts2].
    + applys Decidable_equiv False.
      * iff I; inverts~ I.
      * typeclass.
    + applys Decidable_equiv (t = t2 /\ ts = ts2).
      * iff I; inverts~ I.
      * typeclass.
Defined.


(** ** Special Terms and Operations **)

(** *** Closed Terms and Free Variables **)

(** A term is closed if it contains no free variable. **)
Inductive closed_term : term -> Prop :=
  | closed_term_basic : forall b,
    closed_term (term_basic b)
  | closed_term_constructor : forall c ts,
    Forall closed_term ts ->
    closed_term (term_constructor c ts)
  .

Global Instance closed_term_Decidable : forall t,
    Decidable (closed_term t).
  induction t using term_rect' with (Q := fun l =>
    Decidable (Forall closed_term l)).
  - applys Decidable_equiv False.
    + iff C; tryfalse. inverts C.
    + typeclass.
  - applys Decidable_equiv True.
    + iff~ C. constructors~.
    + typeclass.
  - applys Decidable_equiv IHt. iff C.
    + constructors~.
    + inverts~ C.
  - applys Decidable_equiv True.
    + iff~ C. constructors~.
    + typeclass.
  - applys Decidable_equiv (closed_term t /\ Forall closed_term ts).
    + iff I.
      * lets (C&F): (rm I). constructors~.
      * inverts~ I.
    + typeclass.
Defined.

(** The following definition relates a term to the set of its free (term) variables.
  It is noted [Tvar] in the paper. **)
Inductive term_free_variable : term -> LibSet.set variable_term :=
  | term_free_variable_variable : forall x : variable_term,
    term_free_variable (term_variable x) x
  | term_free_variable_contructor : forall (c : constructor) (ts : list term) t x,
    term_free_variable t x ->
    Mem t ts ->
    term_free_variable (term_constructor c ts) x
  .

Global Instance term_free_variable_Decidable : forall t x,
    Decidable (term_free_variable t x).
  introv. induction t using term_rect' with (Q := fun l =>
    Decidable (Exists (fun t' => term_free_variable t' x) l)).
  - applys Decidable_equiv (x0 = x).
    + iff C.
      * substs. constructors~.
      * inverts~ C.
    + typeclass.
  - applys Decidable_equiv False.
    + iff C; tryfalse. inverts~ C.
    + typeclass.
  - applys Decidable_equiv IHt. rewrite Exists_iff_exists_mem. iff C.
    + lets (t'&M&F): (rm C). constructors*. rewrite~ Mem_mem.
    + inverts~ C. eexists. splits*. rewrite~ <- Mem_mem.
  - applys Decidable_equiv False.
    + iff C; tryfalse. inverts C.
    + typeclass.
  - applys Decidable_equiv (term_free_variable t x \/ Exists (fun t' => term_free_variable t' x) ts).
    + iff I.
      * inverts I as I; [ apply~ Exists_here | apply~ Exists_next ].
      * inverts~ I.
    + typeclass.
Defined.

(** The predicate [closed_term] exactly corresponds to terms without free variables. **)
Lemma closed_term_no_free_variable : forall t,
  closed_term t <-> forall x, ~ term_free_variable t x.
Proof.
  induction t using term_ind_Forall.
  - iff I.
    + inverts~ I.
    + false I. constructors*.
  - iff I.
    + introv F. inverts~ F.
    + constructors*.
  - rewrite Forall_iff_forall_mem in H. iff I.
    + inverts I as I. introv A. inverts A as F M.
      rewrite Forall_iff_forall_mem in I. rewrite Mem_mem in M. forwards C: I M.
      forwards A: H M. rewrite A in C. false* C.
    + constructors. rewrite Forall_iff_forall_mem. introv M.
      forwards A: H M. rewrite A. introv F. false I. constructors*. rewrite~ Mem_mem.
Qed.

(** We provide a computable version of [term_free_variable] in the form of a list. **)
Definition term_free_variable_list : term -> list variable_term :=
  term_rect_list (fun x => [x]) (fun _ => []) (fun _ _ => concat).

Lemma term_free_variable_list_spec : forall t x,
  Mem x (term_free_variable_list t) <-> term_free_variable t x.
Proof.
  induction t using term_ind_Forall.
  - iff M; repeat inverts M as M; constructors~.
  - iff M; inverts~ M.
  - introv. unfold term_free_variable_list. rewrite term_rect_list_unfold. fold term_free_variable_list.
    rewrite Mem_mem. rewrite concat_mem. rewrite Forall_iff_forall_mem in H. iff M.
    + lets (L&M1&M2): (rm M). rewrite map_mem in M1. lets (t&M3&E): (rm M1). substs.
      forwards E: H M3. rewrite Mem_mem in E. rewrite E in M2.
      constructors*. rewrite~ Mem_mem.
    + inverts M as F M. rewrite Mem_mem in M. forwards E: H M. rewrite Mem_mem in E. rewrite <- E in F.
      eexists. splits*. rewrite* map_mem.
Qed.

Global Instance term_free_variable_Pickable : forall t, Pickable_list (term_free_variable t).
  introv. applys~ pickable_list_make term_free_variable_list_spec.
Defined.


(** *** Sorts and Terms **)

Section VariableSort.

(** Each basic term is sorted. **)
(** In contrary to the paper, we assume given a predicate instead of a function
  associating basic terms to their (base) sorts.  This is more general as it
  does not force this sorting function to be computable.
  This furthermore enables future usage of this file where terms would be
  associated more than one sort (typically in type systems with resources,
  like separation logic).  In such cases, the [*_unique] lemmas present in
  this file would no longer apply. **)
(** In a first read, the reader can consider that [basic_term_sort] is
  intuitively of the form of [fun t s => basic_term_sort_compute t = Some s],
  where [basic_term_sort_compute] is some function of type
  [basic_term -> option base_sort].  This pattern is used all over the
  formalisation where sorts are defined. **)
Variable basic_term_sort : basic_term -> base_sort -> Prop.

(** Given a partial mapping from term variables to sort, we can provide sorts to terms.
  Again, we provide [variable_term_sort] as a predicate and not as a function. **)
Variable variable_term_sort : variable_term -> term_sort -> Prop.

(** The following predicate states that the given term has the given sort.
  It is noted [Sort]_[variable_term_sort] in the paper, or simply [Sort] when
  [variable_term_sort] is empty. **)
Inductive sort_of_term : term -> term_sort -> Prop :=
  | sort_of_term_variable : forall (x : variable_term) s,
    variable_term_sort x s ->
    sort_of_term x s
  | sort_of_term_basic : forall b s,
    basic_term_sort b s ->
    sort_of_term b s
  | sort_of_term_constructor : forall (c : constructor) (ts : list term) cas crs,
    constructor_signature c = (cas, crs) ->
    Forall2 sort_of_term ts cas ->
    sort_of_term (term_constructor c ts) crs
  .

(** In order to show the computability of this predicate, we need to assume
  that the base and variable terms are computable too. **)

Hypothesis basic_term_sort_Pickable : forall b,
  Pickable_list (basic_term_sort b).

Hypothesis variable_term_sort_Pickable : forall b,
  Pickable_list (variable_term_sort b).

(** The following function is a computable version of the [sort_of_term] predicate. **)
Definition sort_of_term_compute : term -> list term_sort :=
  term_rect_list
    (fun x => pick_list (variable_term_sort x))
    (fun b => map (id : base_sort -> term_sort) (pick_list (basic_term_sort b)))
    (fun c ts ss =>
      let (cas, crs) := constructor_signature c in
      ifb Forall2 (fun so s => Mem s so) ss cas then
        [crs : term_sort]
      else nil).

Lemma sort_of_term_compute_eq : forall t s,
  sort_of_term t s <-> Mem s (sort_of_term_compute t).
Proof.
  induction t using term_ind_Forall; introv.
  - iff I.
    + inverts I as I. apply~ pick_list_correct.
    + constructors. apply~ pick_list_correct.
  - iff I.
    + inverts I as I. apply pick_list_correct in I. applys~ Mem_map I.
    + unfold sort_of_term_compute in I. unfold term_rect_list in I. simpl in I.
      forwards (s'&M&E): Mem_map_inv (rm I). apply pick_list_correct in M. substs. constructors~.
  - unfold sort_of_term_compute. rewrite term_rect_list_unfold.
    rewrite Forall_iff_forall_mem in H. iff I.
    + inverts I as Ec I. rewrite Forall2_iff_forall_Nth in I. lets (E&F): (rm I).
      rewrite Ec. case_if as n; autos~. false (rm n).
      rewrite Forall2_iff_forall_Nth. splits.
      * rewrite <- E. rewrite~ length_map.
      * introv N1 N2. forwards (t&Nt&Et): map_Nth_inv (rm N1). substs.
        fold (sort_of_term_compute t). apply~ H; [ applys Nth_mem Nt |].
        applys~ F Nt.
    + destruct constructor_signature eqn: Ec. cases_if as C; repeat inverts I as I.
      fold_bool. rew_refl in C. constructors*.
      rewrite Forall2_iff_forall_Nth in *. lets (E&F): (rm C). splits.
      * rewrite <- E. rewrite~ length_map.
      * introv N1 N2. apply H; [ applys Nth_mem N1 |].
        applys~ F N2. applys map_Nth N1.
Qed.

Global Instance sort_of_term_Pickable : forall t, Pickable_list (sort_of_term t).
  introv. eapply pickable_list_make. introv. symmetry. apply~ sort_of_term_compute_eq.
Defined.

(** In the case where at most one sort is associated with a variable or a base term,
  then so does for any term. **)

Hypothesis variable_term_sort_unique : forall x s1 s2,
  variable_term_sort x s1 ->
  variable_term_sort x s2 ->
  s1 = s2.

Hypothesis basic_term_sort_unique : forall b s1 s2,
  basic_term_sort b s1 ->
  basic_term_sort b s2 ->
  s1 = s2.

Lemma sort_of_term_unique : forall t s1 s2,
  sort_of_term t s1 ->
  sort_of_term t s2 ->
  s1 = s2.
Proof.
  introv W1 W2. induction t using term_ind_Forall.
  - inverts W1 as W1. inverts W2 as W2. applys~ variable_term_sort_unique W1 W2.
  - inverts W1 as W1. inverts W2 as W2. fequals. applys~ basic_term_sort_unique W1 W2.
  - inverts W1 as E1 F1. inverts W2 as E2 F2. rewrite E1 in E2. inverts~ E2.
Qed.

(** If a term is sortable, then all its free variables are associated to a sort. **)
Lemma sort_of_term_free_variable_defined : forall t s x,
  sort_of_term t s ->
  term_free_variable t x ->
  exists s, variable_term_sort x s.
Proof.
  introv S FV. gen s x. induction t using term_ind_Forall; introv S FV.
  - inverts FV. inverts* S.
  - inverts FV.
  - rename H into F. inverts FV as FV M. inverts S as E F2.
    rewrite Forall2_iff_forall_Nth in F2. lets (El&F2'): (rm F2).
    rewrite Forall_iff_forall_mem in F. lets (n&N): Mem_Nth M. lets (ca&N'): length_Nth_lt.
    { rewrite <- El. applys Nth_lt_length N. }
    forwards S': F2' N N'. applys F S' FV. rewrite~ <- Mem_mem.
Qed.

(** A term is well-formed if we can associate it with a sort.
  This lemma states that we can decide this last property. **)
Global Instance sort_of_term_exists_Decidable : forall t,
    Decidable (exists s, sort_of_term t s).
  introv. applys Decidable_equiv (sort_of_term_compute t <> nil).
  - iff I.
    + destruct sort_of_term_compute eqn: E; tryfalse. eexists.
      apply sort_of_term_compute_eq. rewrite* E.
    + lets (s&T): (rm I). apply sort_of_term_compute_eq in T. introv E. rewrite E in T. inverts~ T.
  - typeclass.
Defined.

End VariableSort.

End VariableTerm.

Arguments term_variable [_ _].
Arguments term_basic [_ _].


(** *** Terms Sharing the Same Structure **)

(** It happens that we want to compare terms using different basic terms of variables.
  In such cases, the Leibniz equality (or even John Major’s) is not enough.  Typically,
  if two terms are closed, they might be structurally the same whilst having different
  types: as none are using the variable case, it does not matter what type we chose for
  variables in both side to be structurally equals, but it does matter for Leibniz’s
  equality.  We thus introduce the following predicate that states that two terms share
  a similar structure, that is that the constructors are the same all over both terms. **)
(** This predicate is parameterised by subpredicates to state what the meaning of “the
  same structure” is on variables and basic terms.  This definition is very general:
  in practice, most of these predicates will be constant to [False] in most cases. **)
Inductive same_structure_precise (B1 B2 V1 V2 : Type)
    PVV PBB PVB PBV PVC PCV : term B1 V1 -> term B2 V2 -> Prop :=
  | same_structure_variable_variable : forall v1 v2,
    (PVV v1 v2 : Prop) ->
    same_structure_precise PVV PBB PVB PBV PVC PCV (term_variable v1) (term_variable v2)
  | same_structure_basic_basic : forall b1 b2,
    (PBB b1 b2 : Prop) ->
    same_structure_precise PVV PBB PVB PBV PVC PCV (term_basic b1) (term_basic b2)
  | same_structure_constructor : forall c ts1 ts2,
    Forall2 (same_structure_precise PVV PBB PVB PBV PVC PCV) ts1 ts2 ->
    same_structure_precise PVV PBB PVB PBV PVC PCV (term_constructor c ts1) (term_constructor c ts2)
  | same_structure_variable_basic : forall v b,
    (PVB v b : Prop) ->
    same_structure_precise PVV PBB PVB PBV PVC PCV (term_variable v) (term_basic b)
  | same_structure_basic_variable : forall v b,
    (PBV v b : Prop) ->
    same_structure_precise PVV PBB PVB PBV PVC PCV (term_basic b) (term_variable v)
  | same_structure_variable_constructor : forall v c ts,
    (PVC v c ts : Prop) ->
    same_structure_precise PVV PBB PVB PBV PVC PCV (term_variable v) (term_constructor c ts)
  | same_structure_constructor_variable : forall c ts v,
    (PCV v c ts : Prop) ->
    same_structure_precise PVV PBB PVB PBV PVC PCV (term_constructor c ts) (term_variable v)
  .

(** In a lot of circumstances, we only consider symetrical cases. **)
Definition same_structure B1 B2 V1 V2 P1 P2 : term B1 V1 -> term B2 V2 -> Prop :=
  same_structure_precise P1 P2 (fun _ _ => False) (fun _ _ => False)
                           (fun _ _ _ => False) (fun _ _ _ => False).

Lemma same_structure_precise_weaken : forall B1 B2 V1 V2 (P1 P2 PB1 PB2 : _ -> _ -> Prop)
    (PVB1 PVB2 PBV1 PBV2 : _ -> _ -> Prop) (PVC1 PVC2 PCV1 PCV2 : _ -> _ -> _ -> Prop) t1 t2,
  (forall x1 x2,
    term_free_variable t2 x2 ->
    P1 x1 x2 ->
    P2 x1 x2) ->
  (forall b1 b2, PB1 b1 b2 -> PB2 b1 b2) ->
  (forall x b, PVB1 x b -> PVB2 x b) ->
  (forall x b, PBV1 x b -> PBV2 x b) ->
  (forall x c ts, PVC1 x c ts -> PVC2 x c ts) ->
  (forall x c ts, PCV1 x c ts -> PCV2 x c ts) ->
  @same_structure_precise B1 B2 V1 V2 P1 PB1 PVB1 PBV1 PVC1 PCV1 t1 t2 ->
  same_structure_precise P2 PB2 PVB2 PBV2 PVC2 PCV2 t1 t2.
Proof.
  introv I1 I2 I3 I4 I5 I6 S. gen t2. eapply term_ind' with (P := fun t1 => forall t2 : term B2 V2,
      (forall x1 x2, _ t2 x2 -> _ x1 x2 -> _ x1 x2) ->
      same_structure_precise P1 PB1 PVB1 PBV1 PVC1 PCV1 t1 t2 ->
      same_structure_precise P2 PB2 PVB2 PBV2 PVC2 PCV2 t1 t2)
    (Q := fun ts1 => forall ts2 : list (term B2 V2),
       Forall (fun t2 => forall x1 x2,
                 term_free_variable t2 x2 ->
                 P1 x1 x2 ->
                 P2 x1 x2) ts2 ->
       Forall2 _ ts1 ts2 -> Forall2 _ ts1 ts2).
  - introv I1 S. inverts S; constructors~. apply~ I1; constructors~.
  - introv I1 S. inverts S; constructors~.
  - introv Ss I1 S. inverts S as F; constructors~. applys~ Ss F.
    rewrite Forall_iff_forall_mem. introv M FV1 FV2. apply~ I1; constructors*. rewrite~ Mem_mem.
  - introv I1 F. inverts F. constructors~.
  - introv I Is I1 F. inverts F as S F. inverts I1. constructors~.
Qed.

Lemma same_structure_weaken : forall B1 B2 V1 V2 (P1 P2 PB1 PB2 : _ -> _ -> Prop) t1 t2,
  (forall x1 x2,
    term_free_variable t2 x2 ->
    P1 x1 x2 ->
    P2 x1 x2) ->
  (forall b1 b2, PB1 b1 b2 -> PB2 b1 b2) ->
  @same_structure B1 B2 V1 V2 P1 PB1 t1 t2 ->
  same_structure P2 PB2 t1 t2.
Proof. introv I1 I2. applys* same_structure_precise_weaken I1 I2. Qed.

Lemma same_structure_refl : forall B V P PB PVB PBV PVC PCV t,
  (forall v, P v v : Prop) ->
  (forall b, PB b b : Prop) ->
  @same_structure_precise B _ V _ P PB PVB PBV PVC PCV t t.
Proof.
  introv R1 R2. induction t using term_ind_Forall.
  - introv. constructors~.
  - introv. constructors~.
  - constructors~. rewrite Forall2_iff_forall_Nth. splits~.
    introv N1 N2. forwards: Nth_func N1 N2. substs.
    rewrite Forall_iff_forall_mem in H. applys H. applys~ Nth_mem N1.
Qed.

(** If two elements of the same type are structurally the same, then they
  are equal by Leibniz’s equality. **)
Lemma same_structure_refl_inv : forall B V (t1 t2 : term B V),
  same_structure (fun x y => x = y) (fun a b => a = b) t1 t2 ->
  t1 = t2.
Proof.
  introv S. gen t2. induction t1 using term_ind_Forall; introv S.
  - inverts~ S; tryfalse.
  - inverts~ S; tryfalse.
  - inverts S as F; tryfalse. fequals. forwards F': Forall2_Forall (rm H) (rm F).
    rewrite Forall2_iff_forall_Nth in F'. lets (El&F): (rm F').
    applys Nth_equiv. introv. tests I: (LibOrder.lt n (length ts)).
    + forwards (t1&N1): length_Nth_lt I. rewrite El in I. forwards (t2&N2): length_Nth_lt I.
      forwards (IH&F'): (rm F) N1 N2. forwards~: IH F'. substs~. iff N.
      * forwards~: Nth_func N N1. substs~.
      * forwards~: Nth_func N N2. substs~.
    + iff N; forwards I': Nth_lt_length N; try rewrite <- El in I'; false~ I.
Qed.

Lemma same_structure_precise_sym : forall B1 B2 V1 V2 (P1 P2 PB1 PB2 : _ -> _ -> Prop)
    PVB PBV PVC PCV t1 t2,
  (forall v1 v2, P1 v1 v2 -> P2 v2 v1) ->
  (forall b1 b2, PB1 b1 b2 -> PB2 b2 b1) ->
  @same_structure_precise B1 B2 V1 V2 P1 PB1 PVB PBV PVC PCV t1 t2 ->
  same_structure_precise P2 PB2 PBV PVB PCV PVC t2 t1.
Proof.
  introv SP SPB S. gen t2. eapply term_ind' with (P := fun t1 => forall t2 : term B2 V2,
      same_structure_precise P1 PB1 PVB PBV PVC PCV t1 t2 ->
      same_structure_precise P2 PB2 PBV PVB PCV PVC t2 t1)
    (Q := fun ts1 => forall ts2 : list (term B2 V2),
       Forall2 _ ts1 ts2 -> Forall2 _ ts2 ts1).
  - introv S. inverts S; constructors~.
  - introv S. inverts S; constructors~.
  - introv Ss S. inverts S as F; constructors~. applys~ Ss F.
  - introv F. inverts F. constructors~.
  - introv I Is F. inverts F as S F. constructors~.
Qed.

Lemma same_structure_sym : forall B1 B2 V1 V2 (P1 P2 PB1 PB2 : _ -> _ -> Prop) t1 t2,
  (forall v1 v2, P1 v1 v2 -> P2 v2 v1) ->
  (forall b1 b2, PB1 b1 b2 -> PB2 b2 b1) ->
  @same_structure B1 B2 V1 V2 P1 PB1 t1 t2 ->
  same_structure P2 PB2 t2 t1.
Proof. introv. apply~ same_structure_precise_sym. Qed.

Lemma same_structure_sym_swap : forall B1 B2 V1 V2 (P PB : _ -> _ -> Prop) t1 t2,
  @same_structure B1 B2 V1 V2 (fun v1 v2 => P v2 v1) (fun b1 b2 => PB b2 b1) t1 t2 ->
  same_structure P PB t2 t1.
Proof. introv. apply~ same_structure_sym. Qed.

Lemma same_structure_swap_sym : forall B1 B2 V1 V2 (P PB : _ -> _ -> Prop) t1 t2,
  @same_structure B1 B2 V1 V2 P PB t1 t2 ->
  same_structure (fun v1 v2 => P v2 v1) (fun b1 b2 => PB b2 b1) t2 t1.
Proof. introv. apply~ same_structure_sym. Qed.

Lemma same_structure_trans : forall B1 B2 B3 V1 V2 V3 (P1 P2 P3 PB1 PB2 PB3 : _ -> _ -> Prop) t1 t2 t3,
  (forall v1 v2 v3, P1 v1 v2 -> P2 v2 v3 -> P3 v1 v3) ->
  (forall b1 b2 b3, PB1 b1 b2 -> PB2 b2 b3 -> PB3 b1 b3) ->
  @same_structure B1 B2 V1 V2 P1 PB1 t1 t2 ->
  @same_structure B2 B3 V2 V3 P2 PB2 t2 t3 ->
  same_structure P3 PB3 t1 t3.
Proof.
  introv T1 T2. gen t2 t3. eapply term_ind' with (P := fun t1 =>
      forall t2 t3, _ t1 t2 ->  _ t2 t3 -> same_structure P3 PB3 t1 t3)
    (Q := fun ts1 => forall ts2 ts3,
       Forall2 _ ts1 ts2 -> Forall2 _ ts2 ts3 -> Forall2 _ ts1 ts3).
  - introv S1 S2. inverts S1 as S1; tryfalse. inverts S2 as S2; tryfalse. constructors*.
  - introv S1 S2. inverts S1 as S1; tryfalse. inverts S2 as S2; tryfalse. constructors*.
  - introv Ss S1 S2. inverts S1 as F1; tryfalse. inverts S2 as F2; tryfalse. constructors~.
    applys~ Ss F2. apply~ F1.
  - introv F1 F2. inverts F1. inverts F2. constructors~.
  - introv I Is F1 F2. inverts F1 as S1 F1. inverts F2 as S1 F2. constructors*. applys* I.
Qed.

(** Terms with the same structure have the same sorts. **)
Lemma sort_of_term_same_structure : forall B1 B2 V1 V2 (PB : B1 -> B2 -> Prop) (P : V1 -> V2 -> Prop)
    basic_term_sort1 basic_term_sort2 variable_term_sort1 variable_term_sort2 t1 t2 s,
  (forall b1 b2, PB b1 b2 -> basic_term_sort1 b1 = basic_term_sort2 b2) ->
  (forall x1 x2, P x1 x2 -> variable_term_sort1 x1 = variable_term_sort2 x2) ->
  sort_of_term basic_term_sort1 variable_term_sort1 t1 s ->
  same_structure P PB t1 t2 ->
  sort_of_term basic_term_sort2 variable_term_sort2 t2 s.
Proof.
  introv I1 I2. gen s t2. induction t1 using term_ind_Forall; introv S E.
  - inverts E as E; tryfalse. inverts S as S. constructors~. rewrites~ <- >> I2 E.
  - inverts E as Px; tryfalse. inverts S as E. constructors~. rewrites~ <- >> I1 Px.
  - inverts E as F1; tryfalse. inverts S as E F2. constructors*.
    clear E. gen ts2 cas. induction H; introv F1 F2.
    + inverts F2. inverts F1. constructors~.
    + inverts F1 as B1 F1. inverts F2 as B2 F2. constructors*.
Qed.

(** Free variables are conserved by the [same_structure] relation. **)
Lemma term_free_variable_same_structure : forall V B1 B2 (t1 : term B1 V) (t2 : term B2 V) bf x,
  same_structure (fun x1 x2 => x1 = x2) bf t1 t2 ->
  term_free_variable t1 x ->
  term_free_variable t2 x.
Proof.
  introv S FV. gen t2. induction FV; introv S.
  - inverts S; tryfalse. constructors~.
  - inverts S as F; tryfalse. rewrite Forall2_iff_forall_Nth in F.
    lets (El&F'): (rm F). lets (n&N): Mem_Nth (rm H). forwards (t'&N'): length_Nth_lt.
    { rewrite <- El. applys Nth_lt_length N. }
    constructors~.
    + apply~ IHFV. applys~ F' N N'.
    + applys~ Nth_Mem N'.
Qed.

(** Being close is conserved by the [same_structure] relation. **)
Lemma same_structure_closed : forall B1 B2 V1 V2 P PB t1 t2,
  @same_structure B1 B2 V1 V2 P PB t1 t2 ->
  closed_term t1 ->
  closed_term t2.
Proof.
  introv S C. gen t2 C. eapply term_ind' with (P := fun t1 =>
      forall t2, same_structure P PB t1 t2 -> closed_term t1 -> closed_term t2)
    (Q := fun ts1 => forall ts2,
       Forall2 _ ts1 ts2 -> Forall _ ts1 -> Forall _ ts2).
  - introv S C. inverts~ C.
  - introv S C. inverts~ S; tryfalse. constructors~.
  - introv F S C. inverts S as FS; tryfalse. inverts C as FC. constructors. applys~ F FS FC.
  - introv F1 F2. inverts F1. constructors~.
  - introv I Is F1 F2. inverts F1 as S F1. inverts F2 as C F2. constructors~.
Qed.

(** In the closed case, the variable case can be replaced by [fun _ _ => False]:
  there is indeed no variable in the terms.
  This lemma is usually combined with [same_structure_weaken]. **)
Lemma same_structure_True_closed_False : forall B1 B2 V1 V2 P PB t1 t2,
  closed_term t1 ->
  @same_structure B1 B2 V1 V2 P PB t1 t2 ->
  same_structure (fun _ _ => False) PB t1 t2.
Proof.
  induction t1 using term_ind_Forall; introv C E.
  - inverts~ C.
  - inverts E; tryfalse. constructors~.
  - inverts E as F; tryfalse. inverts C as C. constructors~.
    forwards F2: Forall_Forall (rm H) (rm C). forwards F3: Forall2_Forall (rm F2) (rm F).
    applys Forall2_weaken (rm F3). introv ((I&C)&E). applys~ I C E.
Qed.

Global Instance same_structure_precise_Decidable : forall B1 B2 V1 V2 P PB PVB PBV PVC PCV t1 t2,
    (forall v1 v2, Decidable (P v1 v2)) ->
    (forall b1 b2, Decidable (PB b1 b2)) ->
    (forall v1 b2, Decidable (PVB v1 b2)) ->
    (forall b1 v2, Decidable (PBV b1 v2)) ->
    (forall v1 c2 ts2, Decidable (PVC v1 c2 ts2)) ->
    (forall v2 c1 ts1, Decidable (PCV v2 c1 ts1)) ->
    Decidable (@same_structure_precise B1 B2 V1 V2 P PB PVB PBV PVC PCV t1 t2).
  intros. gen t2. induction t1 using term_rect' with (P := fun t1 => forall t2,
      Decidable (_ t1 t2)) (Q := fun ts1 => forall ts2,
      Decidable (Forall2 (same_structure_precise P PB PVB PBV PVC PCV) ts1 ts2)); introv.
  - destruct t2; evar (Psol : Prop);
      (applys Decidable_equiv Psol; [ iff S; [| inverts S as S; exact S ] |];
         unfolds Psol; [ constructors~ | typeclass ]).
  - destruct t2; try solve [ evar (Psol : Prop);
      (applys Decidable_equiv Psol; [ iff S; [| inverts S as S; exact S ] |];
         unfolds Psol; [ constructors~ | typeclass ]) ].
    applys Decidable_equiv False; [ iff S; inverts S | typeclass ].
  - destruct t2; try solve [ evar (Psol : Prop);
      (applys Decidable_equiv Psol; [ iff S; [| inverts S as S; exact S ] |];
         unfolds Psol; [ constructors~ | typeclass ]) ].
    + applys Decidable_equiv False; [ iff S; inverts S | typeclass ].
    + applys Decidable_equiv (c = c0 /\ Forall2 (same_structure_precise P PB PVB PBV PVC PCV) ts l).
      * iff S.
        -- lets (?&F): (rm S). substs. constructors~.
        -- inverts* S.
      * typeclass.
  - destruct ts2.
    + applys Decidable_equiv True.
      * iff~ S. applys Forall2_nil.
      * typeclass.
    + applys Decidable_equiv False.
      * iff S; inverts~ S.
      * typeclass.
  - destruct ts2.
    + applys Decidable_equiv False.
      * iff S; inverts~ S.
      * typeclass.
    + applys Decidable_equiv (same_structure_precise P PB PVB PBV PVC PCV t1 t
                              /\ Forall2 (same_structure_precise P PB PVB PBV PVC PCV) ts ts2).
      * iff S.
        -- lets (S'&F): (rm S). constructors~.
        -- inverts* S.
      * typeclass.
Defined.

Global Instance same_structure_Decidable : forall B1 B2 V1 V2 P PB t1 t2,
    (forall v1 v2, Decidable (P v1 v2)) ->
    (forall b1 b2, Decidable (PB b1 b2)) ->
    Decidable (@same_structure B1 B2 V1 V2 P PB t1 t2).
  intros. apply~ same_structure_precise_Decidable; intros; typeclass.
Defined.


(** *** Substitution **)

Section BasicTerm.

Variable basic_term : Type.

Hypothesis basic_term_Comparable : Comparable basic_term.

Let term : Type -> Type := term basic_term.

(** A common operation on terms is to replace all variables
  by terms tacken from an environment (here given as a function
  [f]).  The following predicate expresses that one term is the
  result of the substitution of another term. **)

Definition same_structure_subst V1 V2 f : term V1 -> term V2 -> Prop :=
  same_structure_precise (fun v1 v2 => f v1 = term_variable v2)
                         (fun b1 b2 => b1 = b2)
                         (fun v1 b2 => f v1 = term_basic b2)
                         (fun _ _ => False)
                         (fun v1 c2 ts2 => f v1 = term_constructor c2 ts2)
                         (fun _ _ _ => False).

(** Given a term [t1] and a function [f] from the variables found in [t1] to terms,
  the function [subst_variables_in_term] replaces every variable of [t1] by the
  result of its application to [f].  This may change the overall type of the term.
  This is typically used when considering a term in a term environment.
  This function is based on [subst_variables_in_term_aux]. **)

Definition subst_variables_in_term_aux V1 V2 f (t1 : term V1) :
    { t2 : term V2 | same_structure_subst f t1 t2 }.
  eapply term_rect' with (P := fun t1 => { t2 : term V2 | _ t1 t2 }) (Q := fun ts =>
      { ts' : list (term V2) | Forall2 (same_structure_precise _ _ _ _ _ _) ts ts'}).
  - intro v1. exists (f v1). destruct (f v1) eqn: E; constructors~.
  - intro b1. eexists. constructors*.
  - introv _ts'. forwards (ts'&Hts'): (rm _ts').
    exists (term_constructor c ts'). constructors~.
    rewrite Forall2_iff_forall_Nth in Hts'. lets (El&F): (rm Hts').
    rewrite Forall2_iff_forall_Nth. splits*.
  - eexists. constructors~.
  - introv _t' _ts'. forwards (ts'&Hts'): (rm _ts').
    forwards (t'&Ht'): (rm _t'). eexists. constructors*.
Defined.

Definition subst_variables_in_term V1 V2 f (t1 : term V1) : term V2 :=
  proj1_sig (subst_variables_in_term_aux f t1).

(** This lemma states that the result of the substitution has the same
  structure than the original term. **)
Lemma subst_variables_in_term_spec : forall V1 V2 (f : V1 -> term V2) t1,
  same_structure_subst f t1 (subst_variables_in_term f t1).
Proof. introv. apply~ proj2_sig. Qed.

(** If the function [f] only returns closed terms, then the result of the substitution
  is also closed.  In other words, if we substitute the variables of a term by terms
  without any free variables, then the resulting substituted term has no free variables. **)
Lemma subst_variables_in_term_closed : forall V1 V2 (f : V1 -> term V2) t,
  (forall x,
    term_free_variable t x ->
    closed_term (f x)) ->
  closed_term (subst_variables_in_term f t).
Proof.
  introv C. induction t using term_ind_Forall.
  - introv. apply C. constructors~.
  - introv. constructors~.
  - rename H into F. forwards S: subst_variables_in_term_spec f (term_constructor c ts).
    inverts S as F2 E; tryfalse. constructors~.
    rewrite Forall2_iff_forall_Nth in F2. lets (El&F2'): (rm F2).
    rewrite Forall_iff_forall_mem in F. rewrite Forall_iff_forall_mem.
    introv M. lets (n&N): mem_Nth (rm M). lets (t&N'): length_Nth_lt.
    { rewrite El. applys Nth_lt_length N. }
    forwards S': F2' N' N. forwards C': F.
    + applys~ Nth_mem N'.
    + introv FV. apply C. constructors*. applys~ Nth_Mem N'.
    + clear - S' C'. forwards S2: subst_variables_in_term_spec f t.
      gen C' S2. generalize (subst_variables_in_term f t). introv C S.
      gen x t0 S' S C. induction t using term_ind_Forall.
      * introv S1 S2 C. inverts S2 as S2.
        -- inverts~ C.
        -- rewrite <- S2 in C. inverts S1 as S1; rewrite~ <- S1.
        -- rewrite <- S2 in C. inverts S1 as S1; rewrite~ <- S1.
      * introv S1 S2 C. inverts S1 as S1; tryfalse. constructors~.
      * rename H into F. introv S1 S2 C. inverts S2 as F2; tryfalse. inverts S1 as F1; tryfalse.
        inverts C as F'. constructors. rewrite Forall2_iff_forall_Nth in F2. lets (E2&F2'): (rm F2).
        rewrite Forall2_iff_forall_Nth in F1. lets (E1&F1'): (rm F1).
        rewrite Forall_iff_forall_mem in F'. rewrite Forall_iff_forall_mem in F.
        rewrite Forall_iff_forall_mem. introv M. lets (n&N): mem_Nth (rm M). lets (t1&N1): length_Nth_lt.
        { rewrite E1. applys Nth_lt_length N. }
        forwards S: F1' N1 N. lets (t2&N2): length_Nth_lt.
        { rewrite <- E2. applys Nth_lt_length N1. }
        forwards S': F2' N1 N2. applys~ F S S'.
        -- applys~ Nth_mem N1.
        -- apply F'. applys~ Nth_mem N2.
Qed.

(** The function [subst_variables_in_term] assumes that the function [f] is total.
  This is usually not the case, typically when [f] reads its value in an environment.
  The following instance covers the case where [f] is not total. **)

Definition same_structure_subst_option V1 V2 f : term V1 -> term V2 -> Prop :=
  same_structure_precise (fun v1 v2 => f v1 = Some (term_variable v2))
                         (fun b1 b2 => b1 = b2)
                         (fun v1 b2 => f v1 = Some (term_basic b2))
                         (fun _ _ => False)
                         (fun v1 c ts => f v1 = Some (term_constructor c ts))
                         (fun _ _ _ => False).

(** The predicate [same_structure_subst_option] is computable. **)
Global Instance same_structure_subst_option_Pickable_option : forall V1 V2
      (f : V1 -> option (term V2)) t1,
    Comparable V1 ->
    Pickable_option (same_structure_subst_option f t1).
  introv C. eapply term_rect' with (P := fun t1 => Pickable_option (_ t1))
    (Q := fun ts1 => forall t1, Mem t1 ts1 -> Pickable_option (same_structure_subst_option f t1)).
  - intro v1. applys pickable_option_make (f v1).
    + intros t E. destruct t; constructors~.
    + intros (t&E). inverts* E.
  - intro b1. applys pickable_option_make (Some (term_basic b1 : term V2)).
    + intros t E. inverts E. constructors~.
    + intros (t&E). exists*.
  - introv F. sets_eq l: (list_rect_Mem _ nil (fun t ts' M l' =>
      @pick_option _ (same_structure_subst_option f t) (F _ M) :: l')).
    destruct (decide (Forall (<> None) l)) eqn: B; fold_bool; rew_refl in B.
    + asserts (l'&Fl'): { l' | Forall2 (fun li li' => li = Some li') l l' }.
      { clear - B. induction l as [|li l].
        - eexists. constructors*.
        - forwards (l'&F): IHl; [ inverts* B |]. destruct li as [li|].
          + eexists. constructors*.
          + false. inverts* B. }
      applys pickable_option_make (Some (term_constructor c l')).
      * introv E. inverts E. constructors.
        clear B. gen l l' ts. induction l; introv F' E.
        -- inverts F'. destruct ts; [| inverts* E ]. constructors~.
        -- inverts F' as F'. destruct ts; simpl in E; inverts E as E. constructors~.
           ++ symmetry in E. apply pick_option_correct in E. apply~ E.
           ++ apply~ IHl. fequals.
      * introv (t'&Ht'). exists*.
    + applys pickable_option_make (@None (term V2)).
      * introv E. inverts~ E.
      * introv (t'&Ht'). false B. inverts Ht' as F'; tryfalse.
        substs. clear - F'. gen ts2. induction ts; introv F'.
        -- constructors~.
        -- simpl. inverts F' as S F'. constructors~.
           ++ intro A. forwards Ex: (ex_intro (same_structure_subst_option f a) _ S).
              forwards (t&Ht): (@pick_option_defined _ _ (F _ (@Mem_here _ _ _)) Ex).
              rewrite A in Ht. inverts Ht.
           ++ applys~ IHts. apply~ F'.
  - introv M. false. inverts~ M.
  - introv P F M. destruct (decide (t = t0)) eqn: M'; fold_bool; rew_refl in M'.
    + substs~.
    + applys~ F. inverts~ M. false~ M'.
Defined.

(** Similarly to [subst_variables_in_term_closed], if we replace the variables
  of a term by closed term, then the resulting term is closed. **)
Lemma same_structure_subst_option_closed : forall V1 V2 (f : V1 -> option (term V2)) t t',
  (forall x t',
    term_free_variable t x ->
    f x = Some t' ->
    closed_term t') ->
  same_structure_subst_option f t t' ->
  closed_term t'.
Proof.
  introv C S. gen t'. induction t using term_ind_Forall; introv S.
  - inverts S as S; applys~ C S; constructors~.
  - inverts S as S; tryfalse. constructors~.
  - rename H into F. inverts S as F'; tryfalse. constructors.
    rewrite Forall2_iff_forall_Nth in F'. lets (El&F2): (rm F').
    rewrite Forall_iff_forall_mem in F. rewrite Forall_iff_forall_mem.
    introv M. lets (n&N): mem_Nth (rm M). lets (t&N'): length_Nth_lt.
    { rewrite El. applys Nth_lt_length N. }
    forwards S': F2 N' N. applys F S'. applys Nth_mem N'.
    introv FV. apply~ C. constructors*. applys~ Nth_Mem N'.
Qed.

(** In the variable case, the [same_structure_subst_option] predicates
  collapses into a much simpler predicate. **)
Lemma same_structure_subst_option_variable : forall V1 V2 (f : V1 -> option (term V2)) (x : V1) t,
  same_structure_subst_option f (term_variable x) t <-> f x = Some t.
Proof.
  introv. iff S.
  - inverts~ S.
  - destruct t eqn: E; constructors*.
Qed.

(** The free variables of the result of a substitution were free variables
  of terms that substituted a variable in the original term.  In other words,
  the only variables present in the result of a substitution come from the
  substitution ifself. **)
Lemma same_structure_subst_option_free_variable : forall V1 V2 (f : V1 -> option (term V2)) t t' t0 x x',
  same_structure_subst_option f t t' ->
  term_free_variable t x ->
  f x = Some t0 ->
  term_free_variable t0 x' ->
  term_free_variable t' x'.
Proof.
  introv S FV1 Ef FV2. gen t'. induction FV1; introv S.
  - apply same_structure_subst_option_variable in S. rewrite Ef in S. inverts~ S.
  - inverts S as F; tryfalse. rewrite Forall2_iff_forall_Nth in F. lets (E&F2): (rm F).
    lets (n&N): Mem_Nth (rm H). lets (t'&N'): length_Nth_lt.
    { rewrite <- E. applys Nth_lt_length N. }
    applys term_free_variable_contructor; [| applys~ Nth_Mem N' ].
    applys~ IHFV1. applys~ F2 N N'.
Qed.

(** If the predicate [same_structure_subst_option] is defined, then [f] is
  defined on all free variables of the substituted term. **)
Lemma same_structure_subst_option_free_variable_inv : forall V V' f t t' x,
  @same_structure_subst_option V V' f t t' ->
  term_free_variable t x ->
  f x <> None.
Proof.
  introv S FV. gen t'. induction FV; introv S.
  - inverts S as S; rewrite~ S; discriminate.
  - inverts S as S; tryfalse. rewrite Forall2_iff_forall_Nth in S. lets (El&F): (rm S).
    forwards (n&N): Mem_Nth (rm H). forwards (t2&N2): length_Nth_lt.
    { rewrite <- El. applys~ Nth_lt_length N. }
    forwards S: F N N2. applys~ IHFV S.
Qed.

(** Conversely, if the function [f] is defined on every free variables of [t],
  then [same_structure_subst_option f t] is defined. **)
Lemma same_structure_subst_option_defined : forall V1 V2 (f : V1 -> option (term V2)) t,
  (forall x, term_free_variable t x -> f x <> None) ->
  exists t', same_structure_subst_option f t t'.
Proof.
  introv F. induction t using term_ind_Forall.
  - forwards~ D: F; [ constructors* |]. destruct f as [t|] eqn: E; tryfalse.
    apply same_structure_subst_option_variable in E. autos*.
  - eexists. constructors*.
  - forwards (ts'&Fts'): Forall_exists.
    { forwards F': Forall_Forall H Forall_Mem. applys Forall_weaken F'.
      introv E. apply~ E. introv FV. apply F. constructors*. }
    eexists. constructors*.
Qed.

(** The following lemma states that [sort_of_term] is compatible with substitution:
  if we substitute each variables by a term of the same sort, this does not change
  the final sort of the substituted term.  There is a subtelty when we substitute
  variables by non-close variables, as we need to consider the sorting environment
  [Gamma'] for variables in the sorting process. **)
(** This lemma is a generalisation of Lemma 2.1 of the paper. **)
Lemma same_structure_subst_option_sort : forall V1 V2 (f : V1 -> option (term V2))
    basic_term_sort (Gamma Gamma' : _ -> _ -> Prop) t t' s,
  (forall x t' s,
    term_free_variable t x ->
    f x = Some t' ->
    Gamma x s ->
    sort_of_term basic_term_sort Gamma' t' s) ->
  same_structure_subst_option f t t' ->
  sort_of_term basic_term_sort Gamma t s ->
  sort_of_term basic_term_sort Gamma' t' s.
Proof.
  introv F S Ss. gen s t'. induction t using term_ind_Forall; introv Ss S.
  - apply same_structure_subst_option_variable in S. inverts Ss as E. applys~ F S E. constructors~.
  - inverts S; tryfalse. inverts Ss. constructors~.
  - inverts S as F2; tryfalse. inverts Ss as E F3. constructors*.
    rewrite Forall2_iff_forall_Nth in F2. lets (El&F2'): (rm F2).
    rewrite Forall2_iff_forall_Nth in F3. lets (El'&F3'): (rm F3).
    rewrite Forall2_iff_forall_Nth. splits~.
    + rewrite~ <- El'.
    + introv N1 N2. lets (t&N3): length_Nth_lt.
      { rewrite El. applys Nth_lt_length N1. }
      forwards S': F2' N3 N1. forwards Ss': F3' N3 N2.
      rewrite Forall_iff_forall_mem in H. applys~ H Ss' S'.
      * applys~ Nth_mem N3.
      * introv FV. apply~ F. constructors*. applys~ Nth_Mem N3.
Qed.

(** The next two lemmas state that [same_structure_subst_option] is compatible with
  [same_structure].  In some sense, these are transitivity lemmas. **)

Lemma same_structure_subst_option_same_structure : forall V1 V2 V3 (t1 t2 t3 : term _) f f',
  (forall x b,
    term_free_variable t1 x ->
    f x = Some (term_basic b) ->
    f' x = Some (term_basic b)) ->
  (forall x c ts ts',
    term_free_variable t1 x ->
    Forall2 (same_structure (fun x1 x2 => False) (fun b1 b2 => b1 = b2)) ts ts' ->
    f x = Some (term_constructor c ts) ->
    f' x = Some (term_constructor c ts')) ->
  @same_structure_subst_option V1 V2 f t1 t2 ->
  same_structure (fun x1 x2 => False) (fun b1 b2 => b1 = b2) t2 t3 ->
  @same_structure_subst_option V1 V3 f' t1 t3.
Proof.
  introv I1 I2 E1 E2. gen t1 t3.
  induction t2 using term_ind_Forall; introv I1 I2 E1 E2; tryfalse; substs~.
  - inverts E2; false~.
  - inverts E2; tryfalse. inverts E1; constructors~. forwards*: I1. constructors~.
  - inverts E2; tryfalse. inverts E1; constructors~.
    + rewrite Forall_iff_forall_mem in H. rewrite Forall2_iff_forall_Nth in *.
      lets (El3&F3): (rm H3). lets (El2&F2): (rm H2). splits.
      * rewrite~ <- El3.
      * introv N1 N2. forwards (v3&N3): length_Nth_lt.
        { rewrite El3. applys~ Nth_lt_length N2. }
        forwards E3: F3 N3 N2. forwards E2: F2 N1 N3. applys~ H E2 E3. applys~ Nth_mem N3.
        -- introv FV. apply~ I1. constructors*. applys~ Nth_Mem N1.
        -- introv FV. apply~ I2. constructors*. applys~ Nth_Mem N1.
    + apply* I2. constructors~.
Qed.

Lemma same_structure_same_structure_subst_option : forall V1 V2 V3 (t1 t2 t3 : term _) P f f',
  (forall x1 x2,
     (P x1 x2 : Prop) ->
     f x2 = f' x1) ->
  same_structure P (fun b1 b2 => b1 = b2) t1 t2 ->
  @same_structure_subst_option V2 V3 f t2 t3 ->
  @same_structure_subst_option V1 V3 f' t1 t3.
Proof.
  introv E E1 E2. gen t1 t3. induction t2 using term_ind_Forall; introv E1 E2; tryfalse; substs~.
  - inverts E1; tryfalse. inverts E2; constructors; erewrite <- E; autos*.
  - inverts E1; tryfalse. inverts E2; constructors~.
  - inverts E1; tryfalse. inverts E2; constructors~.
    rewrite Forall_iff_forall_mem in H. rewrite Forall2_iff_forall_Nth in *.
    lets (El2&F2): (rm H2). lets (El4&F4): (rm H4). splits.
    + rewrite~ <- El4.
    + introv N1 N2. forwards (v3&N3): length_Nth_lt.
      { rewrite El4. applys~ Nth_lt_length N2. }
      forwards E2: F2 N1 N3. forwards E4: F4 N3 N2. applys~ H E2 E4. applys~ Nth_mem N3.
Qed.

(** There is a unique substitution for a given term [t] (up-to [same_structure]). **)
Lemma same_structure_subst_option_unique : forall V V1 V2 (t t1 t2 : term _) P PB f1 f2,
  @same_structure_subst_option V V1 f1 t t1 ->
  @same_structure_subst_option V V2 f2 t t2 ->
  (forall x v1 v2,
    term_free_variable t x ->
    f1 x = Some v1 ->
    f2 x = Some v2 ->
    same_structure P PB v1 v2) ->
  (forall b, PB b b) ->
  same_structure P PB t1 t2.
Proof.
  introv E1 E2 E R. gen t1 t2. induction t using term_ind_Forall; introv E1 E2.
  - inverts E1; inverts E2; applys* E; constructors~.
  - inverts E1; tryfalse. inverts E2; tryfalse. constructors~.
  - inverts E1 as F1; tryfalse. inverts E2 as F2; tryfalse. constructors~.
    rewrite Forall2_iff_forall_Nth in *. lets (El1&F1'): (rm F1). lets (El2&F2'): (rm F2). splits.
    + rewrite~ <- El1.
    + introv N1 N2. forwards (v&N): length_Nth_lt.
      { rewrite El1. applys~ Nth_lt_length N1. }
      forwards E1: (rm F1') N N1. forwards E2: (rm F2') N N2.
      rewrite Forall_iff_forall_mem in H. applys~ H E1 E2.
      * applys~ Nth_mem N.
      * introv FV. apply~ E. constructors; try apply FV. applys~ Nth_Mem N.
Qed.

(** We temporary close the [BasicTerm] section to state a more generic lemma. **)
End BasicTerm.

(** If [t1] and [t2] have the same structure, then substituting them will
  result in terms compatible with the [same_structure] predicate. **)
(** This lemma doesn’t consider the base case, and is thus not a generalisation
  of [same_structure_subst_option_unique]. **)
Lemma same_structure_same_structure_subst_option_unique : forall B1 B2 V V1 V2
    (t1 t2 t1' t2' : term _ _) P PB f1 f2,
  same_structure (fun x1 x2 => forall v1 v2,
    term_free_variable t1 x1 ->
    term_free_variable t2 x2 ->
    f1 x1 = Some v1 ->
    f2 x2 = Some v2 ->
    same_structure P PB v1 v2) (fun _ _ => False) t1 t2 ->
  @same_structure_subst_option B1 V V1 f1 t1 t1' ->
  @same_structure_subst_option B2 V V2 f2 t2 t2' ->
  same_structure P PB t1' t2'.
Proof.
  introv E E1 E2. gen t2 t1' t2'. induction t1 using term_ind_Forall; introv E E1 E2.
  - inverts E1; inverts E as E; tryfalse; inverts E2; applys* E; constructors~.
  - inverts E1; tryfalse. inverts E; false~.
  - inverts E1 as F1; tryfalse. inverts E as E; tryfalse. inverts E2 as F2; tryfalse. constructors~.
    rewrite Forall2_iff_forall_Nth in *. lets (El1&F1'): (rm F1). lets (El2&F2'): (rm F2).
    lets (El&F'): (rm E). rewrite Forall_iff_forall_mem in H. splits.
    + rewrite~ <- El1. rewrite~ El.
    + introv N1 N2. forwards (v&N): length_Nth_lt.
      { rewrite El1. applys~ Nth_lt_length N1. }
      forwards (v'&N'): length_Nth_lt.
      { rewrite <- El. applys~ Nth_lt_length N. }
      forwards E1: (rm F1') N N1. forwards E2: (rm F2') N' N2. applys~ H E1 E2.
      * applys~ Nth_mem N.
      * forwards S: F' N N'. applys~ same_structure_weaken S.
        introv FV F FV1 FV2. apply~ F; constructors*; apply* Nth_Mem.
Qed.

Section BasicTerm'.

Variable basic_term : Type.

Hypothesis basic_term_Comparable : Comparable basic_term.

Let term : Type -> Type := term basic_term.


(** *** Generalization of Closed Terms **)

(** **** Definition **)

(** Closed terms don’t use term variables.  As a consequence,
  it is a burden to have a type for term variables declared in
  their types: instead of manipulating terms of the form [term V B],
  we would like to manipulate closed terms without this parameter,
  that is of type [closed_term_free B].  This is exactly what this
  dependent record is defining. **)
Record closed_term_free := make_closed_term_free {
    closed_term_free_variable_term : Type ;
    closed_term_free_term : term closed_term_free_variable_term ;
    closed_term_free_term_closed : closed_term closed_term_free_term
  }.

Global Instance closed_term_free_Inhab : Inhab constructor -> Inhab closed_term_free.
  introv I. apply prove_Inhab. apply make_closed_term_free with False (term_constructor arbitrary nil).
  repeat constructors~.
Defined.

(** A smart constructor for [closed_term_free] **)
Definition build_closed_term_free V (t : term V) : decide (closed_term t) -> closed_term_free.
  intro D. refine {| closed_term_free_term := t |}. rew_refl~ in D.
Defined.

Lemma closed_term_free_False : forall (t : term False), closed_term t.
Proof. introv. induction t using term_ind_Forall; tryfalse; constructors~. Qed.

(** Another smart constructor for [closed_term_free], based on the [False] type. **)
Definition closed_term_free_from_False (t : term False) : closed_term_free :=
  {| closed_term_free_term_closed := closed_term_free_False t |}.

Definition closed_term_free_from_basic b : closed_term_free :=
  closed_term_free_from_False (term_basic b).


(** A closed term never uses the [term_variable] case, and thus the
  [variable_term] parameter.  We can thus change the it (typically,
  to [False]). **)

Definition closed_term_free_to_variable_term_aux (t : closed_term_free) (V' : Type) :
    { t' : term V' | same_structure (fun _ _ => True) (fun b1 b2 => b1 = b2)
                                    (closed_term_free_term t) t' }.
  destruct t as [X t C]. simpl. gen C.
  eapply term_rect' with (P := fun t => closed_term t -> { t' : term V' | _ t t' }) (Q := fun ts =>
      Forall (@closed_term _ _) ts ->
      { ts' : list (term V') | Forall2 (@same_structure _ _ _ _ _ _) ts ts'}).
  - introv C. false. inverts~ C.
  - introv C. eexists. constructors*.
  - introv _ts' C. forwards (ts'&Hts'): (rm _ts').
    + inverts~ C.
    + exists (term_constructor c ts'). constructors~.
      rewrite Forall2_iff_forall_Nth in Hts'. lets (El&F): (rm Hts').
      rewrite Forall2_iff_forall_Nth. splits*.
  - introv ?. eexists. constructors~.
  - introv _t' _ts' F. forwards (ts'&Hts'): (rm _ts').
    + inverts~ F.
    + forwards (t'&Ht'): (rm _t').
      * inverts~ F.
      * eexists. constructors*.
Defined.

(** Wrapping up, this function produces a term with the same structure than
  the (closed) initial term, but with a different type for variables. **)
Definition closed_term_free_to_variable_term (V' : Type) (t : closed_term_free) :=
  proj1_sig (closed_term_free_to_variable_term_aux t V').

Lemma closed_term_free_to_variable_term_spec : forall V' t,
  same_structure (fun _ _ => False) (fun b1 b2 => b1 = b2)
                 (closed_term_free_term t) (closed_term_free_to_variable_term V' t).
Proof.
  introv. applys~ same_structure_True_closed_False closed_term_free_term_closed.
  apply~ proj2_sig.
Qed.

Lemma closed_term_free_to_variable_term_closed : forall t variable_term,
  closed_term (closed_term_free_to_variable_term variable_term t).
Proof.
  introv. destruct t as [X t C]. applys same_structure_closed C.
  unfold closed_term_free_to_variable_term. apply proj2_sig.
Qed.

Lemma same_structure_refl_inv_closed : forall V P (t1 t2 : term V),
  same_structure P (fun b1 b2 => b1 = b2) t1 t2 ->
  closed_term t1 ->
  t1 = t2.
Proof.
  introv S C. forwards S': same_structure_True_closed_False C S.
  applys~ same_structure_refl_inv. applys* same_structure_weaken S'. simpls*.
Qed.

(** The function [closed_term_free_to_variable_term] is idempotent
  if asked to convert a (closed) term to a term of the same type. **)
Lemma closed_term_free_to_variable_term_idem : forall t,
  closed_term_free_term t = closed_term_free_to_variable_term _ t.
Proof.
  introv. destruct t as [X t C]. simpl. applys~ same_structure_refl_inv_closed C.
  unfold closed_term_free_to_variable_term. apply proj2_sig.
Qed.

(** Updating the variable type twice provides the same result than directly
  updating the variable type to its final value. **)
Lemma closed_term_free_to_variable_term_trans : forall t X1 X2,
  closed_term_free_to_variable_term X2 t =
    closed_term_free_to_variable_term _
      {| closed_term_free_term_closed := closed_term_free_to_variable_term_closed t X1 |}.
Proof.
  introv. applys same_structure_refl_inv_closed (fun (_ _ : X2) => True).
  - unfold closed_term_free_to_variable_term.
    eapply same_structure_trans with (P1 := fun _ _ => True) (P2 := fun _ _ => True)
                                     (PB1 := fun b1 b2 => b1 = b2) (PB2 := fun b2 b3 => b2 = b3).
    + autos~.
    + introv E1 E2. substs~.
    + eapply same_structure_sym; [| | apply proj2_sig ]; autos~.
    + eapply same_structure_trans; [ autos~ | | | apply proj2_sig ]; [| apply proj2_sig ].
      introv E1 E2. substs~.
  - apply~ closed_term_free_to_variable_term_closed.
Qed.

(** Updating the type for variable doesn’t change the term structure. **)
Lemma closed_term_free_to_variable_term_same_structure : forall V1 V2 t,
  same_structure (fun _ _ => False) (fun b1 b2 => b1 = b2)
    (closed_term_free_to_variable_term V1 t)
    (closed_term_free_to_variable_term V2 t).
Proof.
  introv. applys~ same_structure_trans (fun (_ : V1) (_ : closed_term_free_variable_term t) => False)
    (fun b1 b2 : basic_term => b1 = b2) closed_term_free_to_variable_term_spec.
  - introv E1 E2. substs~.
  - applys* same_structure_sym closed_term_free_to_variable_term_spec.
Qed.

Lemma closed_term_free_to_variable_term_closed_term_free_constructor : forall V t c ts,
  closed_term_free_term t = term_constructor c ts ->
  exists ts',
    closed_term_free_to_variable_term V t = term_constructor c ts'
    /\ Forall2 (same_structure (fun _ _ => False) (fun b1 b2 => b1 = b2)) ts ts'.
Proof.
  introv E. forwards S: closed_term_free_to_variable_term_spec V t.
  destruct t as [X t C]. simpls. destruct t as [| |c' ts']; inverts E.
  inverts S as F E; tryfalse. eexists. splits*.
Qed.

Lemma closed_term_free_to_variable_term_closed_term_free_basic : forall V t b,
  closed_term_free_to_variable_term V t = term_basic b <->
  closed_term_free_term t = term_basic b.
Proof.
  introv. forwards S: closed_term_free_to_variable_term_spec V t.
  destruct t as [X t C]. simpls.
  destruct t as [| |c' ts']; iff E; inverts~ E; inverts S; tryfalse.
Qed.


(** **** Equality Relation Between Closed Terms **)

(** We define the following equivalence relation for the type [closed_term_free]. **)
Definition closed_term_free_eq t1 t2 :=
  same_structure (fun _ _ => True) (fun b1 b2 => b1 = b2)
                 (closed_term_free_term t1) (closed_term_free_term t2).

Lemma closed_term_free_eq_refl : forall t,
  closed_term_free_eq t t.
Proof. introv. apply~ same_structure_refl. Qed.

Lemma closed_term_free_eq_sym : forall t1 t2,
  closed_term_free_eq t1 t2 ->
  closed_term_free_eq t2 t1.
Proof. introv E. applys~ same_structure_sym E. introv E'. substs~. Qed.

Lemma closed_term_free_eq_trans : forall t1 t2 t3,
  closed_term_free_eq t1 t2 ->
  closed_term_free_eq t2 t3 ->
  closed_term_free_eq t1 t3.
Proof. introv E1 E2. applys~ same_structure_trans E1 E2. introv E1' E2'. substs~. Qed.

(** Given any closed term, we can rename its internal type. **)
Lemma closed_term_free_variable_exists : forall t V,
  exists t',
    closed_term_free_variable_term t' = V
    /\ closed_term_free_eq t t'.
Proof.
  introv. exists {| closed_term_free_term_closed := closed_term_free_to_variable_term_closed t V |}.
  splits~. forwards E: closed_term_free_to_variable_term_same_structure t.
  rewrite <- closed_term_free_to_variable_term_idem in E. applys* same_structure_weaken E.
Qed.

Lemma closed_term_free_eq_to_variable_term : forall V t1 t2,
  closed_term_free_eq t1 t2 ->
  closed_term_free_to_variable_term V t1 = closed_term_free_to_variable_term V t2.
Proof.
  introv E. applys same_structure_refl_inv_closed closed_term_free_to_variable_term_closed.
  applys same_structure_trans (fun _ _ : V => False) (fun b1 b2 : basic_term => b1 = b2)
      closed_term_free_to_variable_term_spec;
    simpls; intros; tryfalse; substs~.
  applys same_structure_trans (fun (_ : V) (_ : closed_term_free_variable_term t1) => False)
      (fun (_ : V) (_ : closed_term_free_variable_term t2) => False)
      (fun b1 b2 : basic_term => b1 = b2) E;
    simpls; intros; tryfalse; substs~.
  applys same_structure_sym closed_term_free_to_variable_term_spec;
    simpls; intros; tryfalse; substs~.
Qed.


(** **** Sorting Closed Terms **)

(** This section defines [sort_of_term_closed], the equivalent of
  [sort_of_term] on the [closed_term_free] type, and shows that the
  lemmas about [sort_of_term] still applies on [sort_of_term_closed]. **)

Section VariableSort.

(** Each basic term are sorted. **)
Variable basic_term_sort : basic_term -> base_sort -> Prop.

Definition sort_of_term_closed : closed_term_free -> term_sort -> Prop :=
  fun t => sort_of_term basic_term_sort (fun _ _ => False) (closed_term_free_term t).

Lemma sort_of_term_closed_sort_of_term : forall t s tv,
  sort_of_term_closed t s <-> sort_of_term basic_term_sort tv (closed_term_free_term t) s.
Proof.
  introv. destruct t as [X t C]. unfolds sort_of_term_closed. simpls.
  gen s. induction t using term_ind_Forall; introv.
  - inverts~ C.
  - iff I; inverts I; constructor~.
  - inverts C as FC. forwards F1: Forall_Forall (rm H) (rm FC).
    iff I; inverts I as I F; constructors*;
      forwards F2: Forall2_Forall (rm F1) (rm F); applys Forall2_weaken F2;
      introv (E&S); apply* E.
Qed.

Lemma sort_of_term_closed_eq : forall t1 t2 s,
  sort_of_term_closed t1 s ->
  closed_term_free_eq t1 t2 ->
  sort_of_term_closed t2 s.
Proof.
  introv S E. destruct t1 as [X1 t1 C1], t2 as [X2 t2 C2].
  compute in *. forwards~ E': same_structure_True_closed_False (rm E).
  applys~ sort_of_term_same_structure S E'. simpl. intros. substs*.
Qed.

Hypothesis basic_term_sort_unique : forall b s1 s2,
  basic_term_sort b s1 ->
  basic_term_sort b s2 ->
  s1 = s2.

Lemma sort_of_term_closed_unique : forall t s1 s2,
  sort_of_term_closed t s1 ->
  sort_of_term_closed t s2 ->
  s1 = s2.
Proof. introv S1 S2. applys~ sort_of_term_unique S1 S2. simpl. intros. false~. Qed.

Hypothesis basic_term_sort_Pickable : forall b,
  Pickable_list (basic_term_sort b).

Definition sort_of_term_closed_compute t :=
  sort_of_term_compute basic_term_sort (fun (_ : closed_term_free_variable_term t) _ => False)
                       _ _ (closed_term_free_term t).

Lemma sort_of_term_closed_compute_eq : forall t s,
  sort_of_term_closed t s <-> Mem s (sort_of_term_closed_compute t).
Proof. introv. unfolds sort_of_term_closed. rewrite* sort_of_term_compute_eq. Qed.


(** **** A Constructor for Closed Terms **)

(** [closed_term_free_constructor] behaves similarly than [term_constructor],
  but taking a list of [closed_term_free] terms instead of a list of [term _].
  It internally uses [closed_term_free_constructor_aux] to update the internal
  type for variables in each subterms to a common type. **)

Lemma closed_term_free_constructor_aux : forall V c ts,
  closed_term (term_constructor c (map (closed_term_free_to_variable_term V) ts)).
Proof.
  introv. constructors. rewrite Forall_iff_forall_mem.
  introv M. apply map_mem in M. lets (y&M'&E): (rm M). substs.
  apply~ closed_term_free_to_variable_term_closed.
Qed.

Definition closed_term_free_constructor V c ts :=
  {| closed_term_free_term_closed := closed_term_free_constructor_aux V c ts |}.

Lemma closed_term_free_constructor_cons : forall V t c ts,
  closed_term_free_term t = term_constructor c ts ->
  exists tcs, closed_term_free_eq t (closed_term_free_constructor V c tcs).
Proof.
  introv E. destruct t as [X t C]. simpls. substs.
  asserts F: (Forall (fun t => exists tc, t = closed_term_free_to_variable_term _ tc) ts).
  { inverts C as F. applys Forall_weaken (rm F). introv C.
    exists {| closed_term_free_term_closed := C |}. apply~ same_structure_refl_inv.
    forwards~ S: closed_term_free_to_variable_term_spec {| closed_term_free_term_closed := C |}.
    applys* same_structure_weaken S. }
  forwards (tcs&E): Forall_exists (rm F). exists tcs.
  unfolds. constructors~. rewrite Forall2_iff_forall_Nth in E. lets (El&F): (rm E).
  rewrite Forall2_iff_forall_Nth. splits~.
  - rewrite~ length_map.
  - introv N1 N2. forwards (t'&N3&E): map_Nth_inv (rm N2). forwards: (rm F) N1 N3. substs~.
    applys* same_structure_weaken closed_term_free_to_variable_term_same_structure.
Qed.

Lemma closed_term_free_constructor_inv : forall V t c tcs,
  closed_term_free_eq t (closed_term_free_constructor V c tcs) ->
  exists ts, closed_term_free_term t = term_constructor c ts.
Proof.
  introv E0. forwards E: same_structure_True_closed_False closed_term_free_term_closed (rm E0).
  destruct t as [X t C]. inverts E as F E; tryfalse. exists*.
Qed.

(** As [term_constructor], [closed_term_free_constructor] is injective
  (up to [closed_term_free_eq]. **)
Lemma closed_term_free_double_constructor_inv : forall V1 V2 c1 c2 ts1 ts2,
  closed_term_free_eq (closed_term_free_constructor V1 c1 ts1)
                      (closed_term_free_constructor V2 c2 ts2) ->
  c1 = c2 /\ Forall2 closed_term_free_eq ts1 ts2.
Proof.
  introv E. inverts E as E. splits~.
  rewrite Forall2_iff_forall_Nth in *. lets (El&F): (rm E). repeat rewrite length_map in El. splits~.
  introv N1 N2. forwards E: F; try apply* map_Nth.
  applys~ same_structure_trans closed_term_free_to_variable_term_spec.
  - simpl. introv ? ? I. apply I.
  - simpl. introv ? Eb. substs. apply Eb.
  - applys~ same_structure_trans E.
    + simpl. introv ? Eb. substs. apply Eb.
    + applys~ same_structure_sym closed_term_free_to_variable_term_spec.
      simpl. introv A. apply A.
Qed.

End VariableSort.

End BasicTerm'.


(** *** Terms Without Basic Terms **)

(** Basic terms change between interpretations.
  Thus, skeleton can’t manipulate them directly, but can manipulate
  variables representing basic terms.
  This section formalises this. **)

(** **** Predicate **)

Section Parametrised.

Variables basic_term variable_term : Type.

Let term : Type := term basic_term variable_term.
Let term_variable : variable_term -> term := @term_variable _ _.

(** The predicate [basic_free] states that no basic term appear in a particular term.
  Note that in such a term, the base case is not necessarily a variable case: it can
  also be a constructor with no subterm. **)
Inductive basic_free : term -> Prop :=
  | basic_free_variable : forall x,
    basic_free (term_variable x)
  | basic_free_constructor : forall c ts,
    Forall basic_free ts ->
    basic_free (term_constructor c ts)
  .

Global Instance basic_free_Decidable : forall t,
    Decidable (basic_free t).
  induction t using term_rect' with (Q := fun l =>
    Decidable (Forall basic_free l)).
  - applys Decidable_equiv True.
    + iff~ C. constructors~.
    + typeclass.
  - applys Decidable_equiv False.
    + iff C; tryfalse. inverts C.
    + typeclass.
  - applys Decidable_equiv IHt. iff C.
    + constructors~.
    + inverts~ C.
  - applys Decidable_equiv True.
    + iff~ C. constructors~.
    + typeclass.
  - applys Decidable_equiv (basic_free t /\ Forall basic_free ts).
    + iff I.
      * lets (C&F): (rm I). constructors~.
      * inverts~ I.
    + typeclass.
Defined.

End Parametrised.

Section VariableParameter.

Variables variable_term : Type.

Let term : Type -> Type := fun basic_term => term basic_term variable_term.

(** If the type associated to [basic_term] is [False], then the term doesn’t
  contain any base term. **)
Lemma basic_free_term_False : forall (t : term False), basic_free t.
Proof. introv. induction t using term_ind_Forall; tryfalse; constructors~. Qed.


(** **** Type-free Version **)

(** Because basic-free terms don’t use basic terms, we can define a
  variant of basic-free terms without the [basic_term] parameter.
  This type is very similar to [closed_term_free], but based on the
  [basic_free] predicate instead of the [closed_term] predicate. **)
Record basic_free_term := make_basic_free_term {
    basic_free_term_basic_term : Type ;
    basic_free_term_term : term basic_free_term_basic_term ;
    basic_free_term_basic_free : basic_free basic_free_term_term
  }.

Global Instance basic_free_term_Inhab : Inhab constructor -> Inhab basic_free_term.
  introv I. apply prove_Inhab. apply make_basic_free_term with False (term_constructor arbitrary nil).
  repeat constructors~.
Defined.

(** A smart constructor for [basic_free_term]. **)
Definition build_basic_free_term B (t : term B) : decide (basic_free t) -> basic_free_term.
  intro D. refine {| basic_free_term_term := t |}. rew_refl~ in D.
Defined.

(** Another smart constructor for [basic_free_term], based on the [False] type. **)
Definition basic_free_term_from_False (t : term False) : basic_free_term :=
  {| basic_free_term_basic_free := basic_free_term_False t |}.

(** A lemma similar than [same_structure_True_closed_False], but for basic-free terms. **)
Lemma same_structure_True_basic_free_False : forall B1 B2 V1 V2 P PB t1 t2,
  basic_free t1 ->
  @same_structure B1 B2 V1 V2 P PB t1 t2 ->
  same_structure P (fun _ _ => False) t1 t2.
Proof.
  induction t1 using term_ind_Forall; introv C E.
  - inverts E; tryfalse. constructors~.
  - inverts~ C.
  - inverts E as F; tryfalse. inverts C as C. constructors~.
    forwards F2: Forall_Forall (rm H) (rm C). forwards F3: Forall2_Forall (rm F2) (rm F).
    applys Forall2_weaken (rm F3). introv ((I&C)&E). applys~ I C E.
Qed.

(** A basic-free term never uses the [term_basic] case, and thus the
  [basic_term] parameter.  We can thus change the it (typically, to
  [False]).
  This definition is the [basic_free] equivalent of
  [closed_term_free_to_variable_term_aux]. **)

Definition basic_free_term_to_basic_term_aux (t : basic_free_term) (B' : Type) :
    { t' : term B' | same_structure (fun x1 x2 => x1 = x2) (fun _ _ => True)
                                    (basic_free_term_term t) t' }.
  destruct t as [X t C]. simpl. gen C.
  eapply term_rect' with (P := fun t => basic_free t -> { t' : term B' | _ t t' }) (Q := fun ts =>
      Forall (@basic_free _ _) ts ->
      { ts' : list (term B') | Forall2 (@same_structure _ _ _ _ _ _) ts ts'}).
  - introv C. eexists. constructors*.
  - introv C. false. inverts~ C.
  - introv _ts' C. forwards (ts'&Hts'): (rm _ts').
    + inverts~ C.
    + exists (term_constructor c ts'). constructors~.
      rewrite Forall2_iff_forall_Nth in Hts'. lets (El&F): (rm Hts').
      rewrite Forall2_iff_forall_Nth. splits*.
  - introv ?. eexists. constructors~.
  - introv _t' _ts' F. forwards (ts'&Hts'): (rm _ts').
    + inverts~ F.
    + forwards (t'&Ht'): (rm _t').
      * inverts~ F.
      * eexists. constructors*.
Defined.

(** Wrapping up, this function produces a term with the same structure than
  the (basic-free) initial term, but with a different associated type for
  basic terms (which it won’t use anyway). **)

Definition basic_free_term_to_basic_term (B' : Type) (t : basic_free_term) :=
  proj1_sig (basic_free_term_to_basic_term_aux t B').

Lemma basic_free_term_to_basic_term_spec : forall B' t,
  same_structure (fun x1 x2 => x1 = x2) (fun _ _ => False)
                 (basic_free_term_term t) (basic_free_term_to_basic_term B' t).
Proof.
  introv. applys~ same_structure_True_basic_free_False basic_free_term_basic_free.
  apply~ proj2_sig.
Qed.

(** The [basic_free] predicate is compatible with [same_structure]. **)
Lemma same_structure_basic_free : forall B1 B2 V1 V2 P PB t1 t2,
  @same_structure B1 B2 V1 V2 P PB t1 t2 ->
  basic_free t1 ->
  basic_free t2.
Proof.
  introv S C. gen t2 C. eapply term_ind' with (P := fun t1 =>
      forall t2, same_structure P PB t1 t2 -> basic_free t1 -> basic_free t2)
    (Q := fun ts1 => forall ts2,
       Forall2 _ ts1 ts2 -> Forall _ ts1 -> Forall _ ts2).
  - introv S C. inverts~ S; tryfalse. constructors~.
  - introv S C. inverts~ C.
  - introv F S C. inverts S as FS; tryfalse. inverts C as FC. constructors. applys~ F FS FC.
  - introv F1 F2. inverts F1. constructors~.
  - introv I Is F1 F2. inverts F1 as S F1. inverts F2 as C F2. constructors~.
Qed.

Lemma basic_free_term_to_basic_term_basic_free : forall t basic_term,
  basic_free (basic_free_term_to_basic_term basic_term t).
Proof.
  introv. destruct t as [X t C]. applys same_structure_basic_free C.
  unfold basic_free_term_to_basic_term. apply proj2_sig.
Qed.

Lemma same_structure_refl_inv_basic_free : forall B PB (t1 t2 : term B),
  same_structure (fun x1 x2 => x1 = x2) PB t1 t2 ->
  basic_free t1 ->
  t1 = t2.
Proof.
  introv S C. forwards S': same_structure_True_basic_free_False C S.
  applys~ same_structure_refl_inv. applys* same_structure_weaken S'. simpls*.
Qed.

(** The function [basic_free_term_to_basic_term_idem] is idempotent
  if asked to convert a (basic-free) term to a term of the same type. **)
Lemma basic_free_term_to_basic_term_idem : forall t,
  basic_free_term_term t = basic_free_term_to_basic_term _ t.
Proof.
  introv. destruct t as [X t C]. simpl. applys~ same_structure_refl_inv_basic_free C.
  unfold basic_free_term_to_basic_term. apply proj2_sig.
Qed.

(** Updating the type of basic terms twice provides the same result than directly
  updating the type of basic terms to its final value. **)
Lemma basic_free_term_to_basic_term_trans : forall t X1 X2,
  basic_free_term_to_basic_term X2 t =
    basic_free_term_to_basic_term _
      {| basic_free_term_basic_free := basic_free_term_to_basic_term_basic_free t X1 |}.
Proof.
  introv. applys same_structure_refl_inv_basic_free (fun (_ _ : X2) => True).
  - unfold basic_free_term_to_basic_term.
    eapply same_structure_trans with (P1 := fun x1 x2 => x1 = x2) (P2 := fun x1 x2 => x1 = x2)
                                     (PB1 := fun _ _ => True) (PB2 := fun _ _ => True).
    + introv E1 E2. substs~.
    + autos~.
    + eapply same_structure_sym; [| | apply proj2_sig ]; autos~.
    + eapply same_structure_trans; [ | autos~ | | apply proj2_sig ]; [| apply proj2_sig ].
      introv E1 E2. substs~.
  - apply~ basic_free_term_to_basic_term_basic_free.
Qed.

(** Updating the type of basic terms doesn’t change the term structure. **)
Lemma basic_free_term_to_basic_term_same_structure : forall B1 B2 t,
  same_structure (fun x1 x2 => x1 = x2) (fun _ _ => False)
    (basic_free_term_to_basic_term B1 t)
    (basic_free_term_to_basic_term B2 t).
Proof.
  introv. applys~ same_structure_trans (fun x1 x2 : variable_term => x1 = x2)
    (fun (_ : B1) (_ : basic_free_term_basic_term t) => False)
    basic_free_term_to_basic_term_spec.
  - introv E1 E2. substs~.
  - applys* same_structure_sym basic_free_term_to_basic_term_spec.
Qed.

Lemma basic_free_term_to_basic_term_basic_free_term_constructor : forall B t c ts,
  basic_free_term_term t = term_constructor c ts ->
  exists ts',
    basic_free_term_to_basic_term B t = term_constructor c ts'
    /\ Forall2 (same_structure (fun x1 x2 => x1 = x2) (fun _ _ => False)) ts ts'.
Proof.
  introv E. forwards S: basic_free_term_to_basic_term_spec B t.
  destruct t as [X t C]. simpls. destruct t as [| |c' ts']; inverts E.
  inverts S as F E; tryfalse. eexists. splits*.
Qed.

Lemma basic_free_term_to_basic_term_basic_free_term_variable : forall B t x,
  basic_free_term_term t = term_variable x <->
  basic_free_term_to_basic_term B t = term_variable x.
Proof.
  introv. forwards S: basic_free_term_to_basic_term_spec B t.
  destruct t as [X t C]. simpls.
  destruct t as [| |c' ts']; iff E; inverts~ E; inverts S as F E; tryfalse.
Qed.


(** **** Equality Relation Between Basic Free Terms **)

(** We define the following equivalence relation for the type [basic_free_term]. **)
Definition basic_free_term_eq t1 t2 :=
  same_structure (fun x1 x2 => x1 = x2) (fun _ _ => True)
                 (basic_free_term_term t1) (basic_free_term_term t2).

Lemma basic_free_term_eq_refl : forall t,
  basic_free_term_eq t t.
Proof. introv. apply~ same_structure_refl. Qed.

Lemma basic_free_term_eq_sym : forall t1 t2,
  basic_free_term_eq t1 t2 ->
  basic_free_term_eq t2 t1.
Proof. introv E. applys~ same_structure_sym E. introv E'. substs~. Qed.

Lemma basic_free_term_eq_trans : forall t1 t2 t3,
  basic_free_term_eq t1 t2 ->
  basic_free_term_eq t2 t3 ->
  basic_free_term_eq t1 t3.
Proof. introv E1 E2. applys~ same_structure_trans E1 E2. introv E1' E2'. substs~. Qed.

(** Given any basic-free term, we can rename its internal type. **)
Lemma basic_free_term_basic_exists : forall t B,
  exists t',
    basic_free_term_basic_term t' = B
    /\ basic_free_term_eq t t'.
Proof.
  introv. exists {| basic_free_term_basic_free := basic_free_term_to_basic_term_basic_free t B |}.
  splits~. forwards E: basic_free_term_to_basic_term_same_structure t.
  rewrite <- basic_free_term_to_basic_term_idem in E. applys* same_structure_weaken E.
Qed.

Lemma basic_free_term_eq_to_basic_term : forall B t1 t2,
  basic_free_term_eq t1 t2 ->
  basic_free_term_to_basic_term B t1 = basic_free_term_to_basic_term B t2.
Proof.
  introv E. applys same_structure_refl_inv_basic_free basic_free_term_to_basic_term_basic_free.
  applys same_structure_trans (fun x1 x2 : variable_term => x1 = x2) (fun b1 b2 : B => False)
      basic_free_term_to_basic_term_spec;
    simpls; intros; tryfalse; substs~.
  applys same_structure_trans (fun x1 x2 : variable_term => x1 = x2)
      (fun (_ : B) (_ : basic_free_term_basic_term t1) => False)
      (fun (_ : B) (_ : basic_free_term_basic_term t2) => False) E;
    simpls; intros; tryfalse; substs~.
  applys same_structure_sym basic_free_term_to_basic_term_spec;
    simpls; intros; tryfalse; substs~.
Qed.


(** **** Sorting Basic-Free Terms **)

(** This section defines [sort_of_term_basic_free], the equivalent of
  [sort_of_term] on the [basic_free_term] type, and shows that the
  lemmas about [sort_of_term] still applies on [sort_of_term_basic_free]. **)

Section VariableSort.

(** Each variable is sorted. **)
Variable variable_term_sort : variable_term -> term_sort -> Prop.

Definition sort_of_term_basic_free : basic_free_term -> term_sort -> Prop :=
  fun t => sort_of_term (False_rect _) variable_term_sort
                        (basic_free_term_to_basic_term False t).

Lemma sort_of_term_basic_free_sort_of_term : forall t s tb,
  sort_of_term_basic_free t s
  <-> sort_of_term tb variable_term_sort (basic_free_term_term t) s.
Proof.
  introv. forwards S: (basic_free_term_to_basic_term_spec False t).
  sets_eq t': (basic_free_term_to_basic_term False t).
  destruct t as [X t C]. unfolds sort_of_term_basic_free. simpls.
  rewrite <- EQt'. clear EQt'. gen s.
  induction t using term_ind_Forall; introv; inverts S as S; tryfalse.
  - iff I; inverts I; constructor~.
  - inverts C as FC. rewrite Forall2_iff_forall_Nth in S. lets (El&FS): (rm S).
    rename H into Fts. rewrite Forall_iff_forall_mem in Fts. rewrite Forall_iff_forall_mem in FC.
    iff I; inverts I as I F; constructors*;
      rewrite Forall2_iff_forall_Nth in F; lets (El'&F'): (rm F);
      rewrite Forall2_iff_forall_Nth; (splits~; [ rewrite~ <- El' |]); introv N1 N2.
    + forwards (v3&N3): length_Nth_lt.
      { rewrite El'. applys~ Nth_lt_length N2. }
      forwards~ S: FS N1 N3. forwards Sv: F' N3 N2.
      applys~ sort_of_term_same_structure (fun x1 x2 : variable_term => x1 = x2) Sv;
        try solve [ intros; tryfalse; substs~ ].
      applys~ same_structure_sym (fun (_ : False) (_ : X) => False) S. simpl. intros. substs~.
    + forwards (v3&N3): length_Nth_lt.
      { rewrite El'. applys~ Nth_lt_length N2. }
      forwards~ S: FS N3 N1. forwards Sv: F' N3 N2.
      applys~ sort_of_term_same_structure (fun x1 x2 : variable_term => x1 = x2) Sv S;
        intros; tryfalse; substs~.
Qed.

Lemma sort_of_term_basic_free_eq : forall t1 t2 s,
  sort_of_term_basic_free t1 s ->
  basic_free_term_eq t1 t2 ->
  sort_of_term_basic_free t2 s.
Proof. introv S E. unfolds in S. unfolds. rewrites~ <- >> basic_free_term_eq_to_basic_term E. Qed.

Hypothesis variable_term_sort_unique : forall x s1 s2,
  variable_term_sort x s1 ->
  variable_term_sort x s2 ->
  s1 = s2.

Lemma sort_of_term_basic_free_unique : forall t s1 s2,
  sort_of_term_basic_free t s1 ->
  sort_of_term_basic_free t s2 ->
  s1 = s2.
Proof. introv S1 S2. applys~ sort_of_term_unique S1 S2. intros. false~. Qed.


(** **** A Constructor for Basic-free Terms **)

(** [basic_free_term_constructor] behaves similarly than [term_constructor],
  but taking a list of [basic_free_term] terms instead of a list of [term _].
  It internally uses [basic_free_term_constructor_aux] to update the internal
  type for basic terms in each subterms to a common type. **)

Lemma basic_free_term_constructor_aux : forall B c ts,
  basic_free (term_constructor c (map (basic_free_term_to_basic_term B) ts)).
Proof.
  introv. constructors. rewrite Forall_iff_forall_mem.
  introv M. apply map_mem in M. lets (y&M'&E): (rm M). substs.
  apply~ basic_free_term_to_basic_term_basic_free.
Qed.

Definition basic_free_term_constructor B c ts :=
  {| basic_free_term_basic_free := basic_free_term_constructor_aux B c ts |}.

Lemma basic_free_term_constructor_cons : forall B t c ts,
  basic_free_term_term t = term_constructor c ts ->
  exists tcs, basic_free_term_eq t (basic_free_term_constructor B c tcs).
Proof.
  introv E. destruct t as [X t C]. simpls. substs.
  asserts F: (Forall (fun t => exists tc, t = basic_free_term_to_basic_term _ tc) ts).
  { inverts C as F. applys Forall_weaken (rm F). introv C.
    exists {| basic_free_term_basic_free := C |}. apply~ same_structure_refl_inv.
    forwards~ S: basic_free_term_to_basic_term_spec {| basic_free_term_basic_free := C |}.
    applys* same_structure_weaken S. }
  forwards (tcs&E): Forall_exists (rm F). exists tcs.
  unfolds. constructors~. rewrite Forall2_iff_forall_Nth in E. lets (El&F): (rm E).
  rewrite Forall2_iff_forall_Nth. splits~.
  - rewrite~ length_map.
  - introv N1 N2. forwards (t'&N3&E): map_Nth_inv (rm N2). forwards: (rm F) N1 N3. substs~.
    applys* same_structure_weaken basic_free_term_to_basic_term_same_structure.
Qed.

Lemma basic_free_term_constructor_inv : forall V t c tcs,
  basic_free_term_eq t (basic_free_term_constructor V c tcs) ->
  exists ts, basic_free_term_term t = term_constructor c ts.
Proof.
  introv E0. forwards E: same_structure_True_basic_free_False basic_free_term_basic_free (rm E0).
  destruct t as [X t C]. inverts E as F E; tryfalse. exists*.
Qed.

End VariableSort.

End VariableParameter.

End Sorts.

Global Arguments sort_base [_ _].
Global Arguments sort_program [_ _].

Global Arguments term_variable [_ _ _].
Global Arguments term_constructor [_ _ _].


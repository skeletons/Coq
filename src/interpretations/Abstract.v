(** Definition of the abstract interpretation. **)

Set Implicit Arguments.

Require Import LibFunc LibSet LibHeap.
Require Export WellFormedness Concrete.

(** * The Abstract Interpretation **)

Section Common.

Variables constructor filter : Type.

Hypothesis constructor_Comparable : Comparable constructor.
Hypothesis filter_Comparable : Comparable filter.

Hypothesis constructor_Inhab : Inhab constructor.

Variable base_sort program_sort flow_sort : Type.

Hypothesis base_sort_Comparable : Comparable base_sort.
Hypothesis program_sort_Comparable : Comparable program_sort.
Hypothesis flow_sort_Comparable : Comparable flow_sort.

(** For simplicity, we assume that these types are inhabited,
  which is always satisfied in non-trivial semantics.
  We might optimise some proofs to get rid of these hypotheses. **)
Hypothesis flow_sort_Inhab : Inhab flow_sort.
Hypothesis base_sort_Inhab : Inhab base_sort.
Hypothesis program_sort_Inhab : Inhab program_sort.

Let term_sort : Type := term_sort base_sort program_sort.
Let sort : Type := sort base_sort program_sort flow_sort.
Let sort_term : term_sort -> sort := @sort_term _ _ _.
Let sort_flow : flow_sort -> sort := @sort_flow _ _ _.
Local Coercion sort_term : term_sort >-> sort.
Local Coercion sort_flow : flow_sort >-> sort.


(** ** Values **)

(** We here need to assume similar variables than in the concrete interpretation. **)

(** We assume abstract basic terms.
  In contrary to what is written in the paper, we here assume that abstract basic terms
  are abstractions of concrete basic terms (and not extensions).
  We do not assume any particular property on abstract basic terms.  This enables in
  particular to define them as an extension of the concrete basic terms, but we don’t
  enforce it. **)
Variable abstract_basic_term : Type.

Hypothesis abstract_basic_term_Comparable : Comparable abstract_basic_term.

Let abstract_closed_term_free : Type := closed_term_free constructor abstract_basic_term.

(** We assume abstract (flow) values. **)
Variable abstract_flow_value : Type.

Hypothesis abstract_flow_value_Comparable : Comparable abstract_flow_value.

(** Abstract terms are usual closed terms extended with a bottom
  value.  Each term are sorted, included the bottom terms.  These are
  used to abort the abstract analysis when getting unreachable points. **)
Inductive abstract_term :=
  | abstract_term_closed : abstract_closed_term_free -> abstract_term
  | abstract_term_bottom : term_sort -> abstract_term
  .
Coercion abstract_term_closed : abstract_closed_term_free >-> abstract_term.

Global Instance abstract_term_Inhab : Inhab abstract_term.
  asserts: (Inhab abstract_closed_term_free).
  { unfolds abstract_closed_term_free. typeclass. }
  applys prove_Inhab (abstract_term_closed arbitrary).
Defined.

(** We lift [closed_term_free_eq] to abstract terms. **)
Inductive abstract_term_eq : abstract_term -> abstract_term -> Prop :=
  | abstract_term_eq_normal : forall t1 t2,
    closed_term_free_eq t1 t2 ->
    abstract_term_eq (abstract_term_closed t1) (abstract_term_closed t2)
  | abstract_term_eq_bottom : forall s,
    abstract_term_eq (abstract_term_bottom s) (abstract_term_bottom s)
  .

(** The relation [abstract_term_eq] is an equivalence relation. **)
Lemma abstract_term_eq_refl : forall t,
  abstract_term_eq t t.
Proof. introv. destruct t; constructor~. apply~ closed_term_free_eq_refl. Qed.

Lemma abstract_term_eq_sym : forall t1 t2,
  abstract_term_eq t1 t2 ->
  abstract_term_eq t2 t1.
Proof. introv E. inverts E as E; constructor~. applys~ closed_term_free_eq_sym E. Qed.

Lemma abstract_term_eq_trans : forall t1 t2 t3,
  abstract_term_eq t1 t2 ->
  abstract_term_eq t2 t3 ->
  abstract_term_eq t1 t3.
Proof.
  introv E1 E2. inverts E1 as E1; inverts E2 as E2; constructor~.
  applys~ closed_term_free_eq_trans E1 E2.
Qed.

(** Abstract values are similar than concrete values, but are based on
  abstract terms instead of usual (closed) terms. **)
Inductive abstract_value :=
  | abstract_value_term : abstract_term -> abstract_value
  | abstract_value_flow : abstract_flow_value -> abstract_value
  .
Coercion abstract_value_term : abstract_term >-> abstract_value.
Coercion abstract_value_flow : abstract_flow_value >-> abstract_value.

Global Instance abstract_value_Inhab : Inhab abstract_value.
  applys prove_Inhab (abstract_value_term arbitrary).
Defined.

(** We lift [abstract_term_eq] to [abstract_value]. **)
Inductive abstract_value_eq : abstract_value -> abstract_value -> Prop :=
  | abstract_value_eq_term : forall t1 t2,
    abstract_term_eq t1 t2 ->
    abstract_value_eq t1 t2
  | abstract_value_eq_flow : forall v : abstract_flow_value,
    abstract_value_eq v v
  .

(** The relation [abstract_value_eq] is an equivalence relation. **)

Lemma abstract_value_eq_refl : forall v,
  abstract_value_eq v v.
Proof. introv. destruct v; constructor~. apply~ abstract_term_eq_refl. Qed.

Lemma abstract_value_eq_sym : forall v1 v2,
  abstract_value_eq v1 v2 ->
  abstract_value_eq v2 v1.
Proof. introv E. inverts E as E; constructor~. applys~ abstract_term_eq_sym E. Qed.

Lemma abstract_value_eq_trans : forall v1 v2 v3,
  abstract_value_eq v1 v2 ->
  abstract_value_eq v2 v3 ->
  abstract_value_eq v1 v3.
Proof.
  introv E1 E2. inverts E1 as E1; inverts E2 as E2; constructor~.
  applys~ abstract_term_eq_trans E1 E2.
Qed.


Variables in_sort out_sort : program_sort -> flow_sort.

Variable filter_signature : filter -> list sort * list sort.
Variable constructor_signature : constructor -> list term_sort * program_sort.
Variable abstract_basic_term_sort : abstract_basic_term -> base_sort -> Prop.

Let sort_of_term_closed_abstract : abstract_closed_term_free -> term_sort -> Prop :=
  sort_of_term_closed constructor_signature abstract_basic_term_sort.

(** Sorting naturally extends to abstract terms. **)
Inductive sort_of_abstract_term : abstract_term -> term_sort -> Prop :=
  | sort_of_abstract_term_closed : forall t s,
    sort_of_term_closed_abstract t s ->
    sort_of_abstract_term t s
  | sort_of_abstract_term_bottom : forall s,
    sort_of_abstract_term (abstract_term_bottom s) s
  .

Hypothesis abstract_basic_term_Pickable : forall b,
  Pickable_list (abstract_basic_term_sort b).

Definition sort_of_abstract_term_compute (t : abstract_term) : list term_sort :=
  match t with
  | abstract_term_bottom s => [s]
  | abstract_term_closed t =>
    sort_of_term_closed_compute _ _ constructor_signature abstract_basic_term_sort _ t
  end.

Lemma sort_of_abstract_term_compute_eq : forall t s,
  sort_of_abstract_term t s <-> Mem s (sort_of_abstract_term_compute t).
Proof.
  introv. destruct t.
  - simpl. unfolds term_sort. rewrite <- sort_of_term_closed_compute_eq.
    iff E; [ inverts~ E | constructors~ ].
  - iff E; repeat inverts E as E; constructors~.
Qed.

Hypothesis abstract_basic_term_sort_unique : forall b s1 s2,
  abstract_basic_term_sort b s1 ->
  abstract_basic_term_sort b s2 ->
  s1 = s2.

Lemma sort_of_abstract_term_eq_unique : forall t1 t2 s1 s2,
  abstract_term_eq t1 t2 ->
  sort_of_abstract_term t1 s1 ->
  sort_of_abstract_term t2 s2 ->
  s1 = s2.
Proof.
  introv E S1 S2. inverts E as E; inverts S1 as S1; inverts S2 as S2; autos~.
  forwards S1': sort_of_term_closed_eq S1 E.
  applys~ sort_of_term_unique S1' S2. simpl. intros. false~.
Qed.

Lemma sort_of_abstract_term_unique : forall t s1 s2,
  sort_of_abstract_term t s1 ->
  sort_of_abstract_term t s2 ->
  s1 = s2.
Proof. introv S1 S2. applys~ sort_of_abstract_term_eq_unique abstract_term_eq_refl S1 S2. Qed.

Variable abstract_value_flow_sort : abstract_flow_value -> flow_sort -> Prop.

Inductive sort_of_abstract_value : abstract_value -> sort -> Prop :=
  | sort_of_abstract_value_term : forall t s,
    sort_of_abstract_term t s ->
    sort_of_abstract_value t (sort_term s)
  | sort_of_abstract_value_flow : forall v s,
    abstract_value_flow_sort v s ->
    sort_of_abstract_value v s
  .

Hypothesis abstract_value_flow_sort_Pickable : forall v,
  Pickable_list (abstract_value_flow_sort v).

Definition sort_of_abstract_value_compute (v : abstract_value) :=
  match v with
  | abstract_value_term t => map (id : term_sort -> sort) (sort_of_abstract_term_compute t)
  | abstract_value_flow v => map (id : flow_sort -> sort) (pick_list (abstract_value_flow_sort v))
  end.

Lemma sort_of_abstract_value_compute_eq : forall v s,
  sort_of_abstract_value v s <-> Mem s (sort_of_abstract_value_compute v).
Proof.
  introv. destruct v.
  - simpl. iff E.
    + inverts E as E. rewrite sort_of_abstract_term_compute_eq in E. applys~ Mem_map E.
    + forwards (s'&M&E'): Mem_map_inv (rm E). substs.
      rewrite~ <- sort_of_abstract_term_compute_eq in M. constructors~.
  - iff E.
    + inverts E as E . apply Mem_map. rewrite~ pick_list_correct.
    + forwards (s'&M&E'): Mem_map_inv (rm E). substs. apply pick_list_correct in M. constructors~.
Qed.

Hypothesis abstract_value_flow_sort_unique : forall v s1 s2,
  abstract_value_flow_sort v s1 ->
  abstract_value_flow_sort v s2 ->
  s1 = s2.

Lemma sort_of_abstract_value_eq_unique : forall v1 v2 s1 s2,
  abstract_value_eq v1 v2 ->
  sort_of_abstract_value v1 s1 ->
  sort_of_abstract_value v2 s2 ->
  s1 = s2.
Proof.
  introv E S1 S2. inverts E as E; inverts S1 as S1; inverts S2 as S2; autos~.
  - fequals. applys~ sort_of_abstract_term_eq_unique E S1 S2.
  - fequals. applys~ abstract_value_flow_sort_unique S1 S2.
Qed.

Lemma sort_of_abstract_value_unique : forall v s1 s2,
  sort_of_abstract_value v s1 ->
  sort_of_abstract_value v s2 ->
  s1 = s2.
Proof. introv S1 S2. applys~ sort_of_abstract_value_eq_unique abstract_value_eq_refl S1 S2. Qed.


(** We define posets as follows. **)
Class Poset A : Type := make_Poset {
    Poset_order : A -> A -> Prop ;
    Poset_refl : forall x, Poset_order x x ;
    Poset_antisym : forall x y, Poset_order x y -> Poset_order y x -> x = y ;
    Poset_trans : forall x y z, Poset_order x y -> Poset_order y z -> Poset_order x z
  }.
Notation "a ⊑ b" := (Poset_order a b) (at level 40).

(** Each sort comes with an order relation.
  Note that abstract interpretation usually requires a lattice,
  but we don’t need that much here. **)
Variable abstract_flow_Poset : Poset abstract_flow_value.

(** The order should be compatible with the sorts. **)
Hypothesis abstract_flow_Poset_sort : forall v1 v2,
  v1 ⊑ v2 ->
  abstract_value_flow_sort v1 = abstract_value_flow_sort v2.

(** The poset over abstract term is only defined by stating that bottom
  terms are smaller than any other abstract term of the same sort. **)
Inductive abstract_term_order : abstract_term -> abstract_term -> Prop :=
  | abstract_term_order_refl : forall t : abstract_closed_term_free,
    abstract_term_order t t
  | abstract_term_order_bottom : forall t s,
    sort_of_abstract_term t s ->
    abstract_term_order (abstract_term_bottom s) t
  .

Global Instance abstract_term_Poset : Poset abstract_term.
  applys make_Poset abstract_term_order.
  - introv. destruct x as [t|s]; repeat constructors~.
  - introv I1 I2. inverts I1 as I1; inverts~ I2. inverts I1. fequals~.
  - introv I1 I2. inverts I1 as I1; inverts I2 as I2; constructors~. inverts~ I1.
Defined.

Inductive abstract_value_order : abstract_value -> abstract_value -> Prop :=
  | abstract_value_order_term : forall t1 t2 : abstract_term,
    t1 ⊑ t2 ->
    abstract_value_order t1 t2
  | abstract_value_order_flow : forall v1 v2 : abstract_flow_value,
    v1 ⊑ v2 ->
    abstract_value_order v1 v2
  .

Instance abstract_value_Poset : Poset abstract_value.
  applys make_Poset abstract_value_order.
  - introv. destruct x as [t|v]; constructors; apply~ @Poset_refl.
  - introv I1 I2. inverts I1 as I1; inverts I2 as I2; fequals; applys~ @Poset_antisym I1 I2.
  - introv I1 I2. inverts I1 as I1; inverts I2 as I2; constructors; applys~ @Poset_trans I1 I2.
Defined.

(** We assume that each flow sort is associated a least element
  in the poset of flow values. **)
Variable bottom_flow : flow_sort -> abstract_flow_value.

Hypothesis bottom_flow_sort : forall s,
  abstract_value_flow_sort (bottom_flow s) s.

Hypothesis bottom_flow_spec : forall v s,
  abstract_value_flow_sort v s ->
  bottom_flow s ⊑ v.

Definition bottom s :=
  match s return abstract_value with
  | WellFormedness.sort_term s => abstract_term_bottom s
  | WellFormedness.sort_flow s => bottom_flow s
  end.

Lemma bottom_spec : forall v s,
  sort_of_abstract_value v s ->
  bottom s ⊑ v.
Proof. introv S. inverts S; repeat constructors~. Qed.

Lemma bottom_sort : forall s,
  sort_of_abstract_value (bottom s) s.
Proof.
  introv. destruct s as [s|s].
  - repeat constructors.
  - applys~ sort_of_abstract_value_flow bottom_flow_sort.
Qed.

Inductive is_bottom : abstract_value -> Prop :=
  | is_bottom_intro : forall s,
    is_bottom (bottom s)
  .

Instance is_bottom_Decidable : forall v, Decidable (is_bottom v).
  introv. destruct v as [t|v].
  - destruct t as [t|s].
    + applys Decidable_equiv False.
      * iff B; tryfalse. inverts B as E. destruct s; inverts E.
      * typeclass.
    + applys Decidable_equiv True.
      * iff~ B. sets_eq b: (bottom (s : sort)). lets EQb': EQb. simpl in EQb.
        rewrite <- EQb. rewrite EQb'. constructors~.
      * typeclass.
  - applys Decidable_equiv (Mem v (map bottom_flow (pick_list (abstract_value_flow_sort v)))).
    + iff E.
      * forwards (s&M&E'): Mem_map_inv (rm E). substs. apply pick_list_correct in M.
        sets_eq b: (bottom (s : sort)). lets EQb': EQb. simpl in EQb.
        rewrite <- EQb. rewrite EQb'. constructors~.
      * inverts E as E. destruct s; inverts E. apply~ Mem_map. apply~ pick_list_correct.
    + apply Mem_decidable. (* FIXME: Why [typeclass] takes so long there? *)
Defined.

Lemma sort_of_abstract_term_order : forall t1 t2,
  t1 ⊑ t2 ->
  sort_of_abstract_term t1 = sort_of_abstract_term t2.
Proof.
  introv I. extens. introv. inverts I as I.
  - reflexivity.
  - iff S.
    + inverts~ S.
    + forwards: sort_of_abstract_term_unique I S. substs. constructors.
Qed.

Lemma sort_of_abstract_value_order : forall v1 v2,
  v1 ⊑ v2 ->
  sort_of_abstract_value v1 = sort_of_abstract_value v2.
Proof.
  introv I. extens. introv. inverts I as I.
  - iff S; inverts S as S; constructors; rewrites~ >> sort_of_abstract_term_order I in *.
  - iff S; inverts S as S.
    + rewrites >> abstract_flow_Poset_sort I in S. constructors~.
    + rewrites <- >> abstract_flow_Poset_sort I in S. constructors~.
Qed.

Local Instance abstract_flow_value_Inhab : Inhab abstract_flow_value.
  applys prove_Inhab (bottom_flow arbitrary).
Defined.

Local Instance value_Inhab : Inhab abstract_value.
  applys prove_Inhab (abstract_value_flow arbitrary).
Defined.

(** In contrary to the paper, we define the interpretation of filters
  as a relation and not a function.  This comes with very few
  differences with the presentation of the paper.  In particular, we
  provide a definition [abstract_filter_interpretation_from_function]
  at the end of this file to get a relation from a function.  To
  express that the abstract filter returns [⊥] within this
  presentation, this relation has to accept [None].  We enable this
  relation to be empty, but it would only make the abstract semantics
  less useful, and undefined on some inputs: this would be sound, but
  less useful than an abstract semantics returning a top element.
  Expressing this relation as a relation and not as a function enables
  us to express abstract semantics in uncommon domains, typically in
  non-lattice domains where there is no best abstract result for a
  given input.  Such domains are unadvisable, but are used in
  real-world abstract interpretation from time to time. **)
Variable abstract_filter_interpretation :
  filter -> list abstract_value -> option (list abstract_value) -> Prop.

(** Filter interpretations are considered consistent with sorting. **)
Hypothesis abstract_filter_interpretation_consistent_with_sort : forall f vs1 vss1 vs2 vss2,
  abstract_filter_interpretation f vs1 (Some vs2) ->
  filter_signature f = (vss1, vss2) ->
  Forall2 sort_of_abstract_value vs1 vss1 ->
  Forall2 sort_of_abstract_value vs2 vss2.

Section Variables.

Variables variable_term variable_flow : Type.

Hypothesis variable_term_Comparable : Comparable variable_term.
Hypothesis variable_flow_Comparable : Comparable variable_flow.

Let variable : Type := variable variable_term variable_flow.
Let variable_flow_extended : Type := variable_flow_extended variable_flow.
Let term : Type := term constructor abstract_basic_term variable_term.
Let bone : Type := bone constructor filter variable_term variable_flow.
Let skeleton : Type := skeleton constructor filter variable_term variable_flow.
Let basic_free_term : Type := basic_free_term constructor variable_term.

Local Instance : Comparable variable_flow_extended :=
  variable_flow_extended_Comparable variable_flow_Comparable.

Let wellformedness : interpretation _ _ variable_term variable_flow :=
  wellformedness in_sort out_sort filter_signature constructor_signature _ _.

Section AbstractState.

(** Abstract environments map flow variables to abstract values,
  and term variables to either (closed) terms or bottom terms
  (represented in the environment by their sorts).
  We base ourselves on [typed_environment] from [aux/Environments.v] to define them. **)

Definition abstract_state : Type :=
  typed_environment abstract_term abstract_flow_value variable_term variable_flow.

Let typed_environment_domain : abstract_state -> set variable :=
  typed_environment_domain abstract_value_term abstract_value_flow (variable_flow := _).

Let typed_environment_read_variable : abstract_state -> variable -> option abstract_value :=
  typed_environment_read_variable abstract_value_term abstract_value_flow (variable_flow := _).

Let typed_environment_write_variables
    : abstract_state -> list variable -> list abstract_value -> option abstract_state :=
  typed_environment_write_variables abstract_value_term abstract_value_flow
                                    abstract_value_rect (variable_flow := _).

(** The flag expresses whether the current skeleton has no concrete
  counterpart ([false]) or that it may still have one ([true]).
  This flag enables the abstract interpretation to perform shortcuts:
  when the flag is [false], it is propagated all along the skeleton. **)
Definition flag := bool.

Definition flag_abstract_state : Type := flag * abstract_state.

(** States that the (term) variable [x] is associated to an abstract
  term of sort [s] in the environment [Sigma]. **)
Definition sort_of_term_basic_free_aux (Sigma : abstract_state) x s :=
  exists t,
    typed_environment_read_term Sigma x = Some t
    /\ sort_of_abstract_term t s.

(** We can then lift this predicate to (open) terms. **)
Let sort_of_term_basic_free Sigma :=
   sort_of_term_basic_free constructor_signature (sort_of_term_basic_free_aux Sigma).

Instance sort_of_term_basic_free_aux_Pickable : forall Sigma x,
    Pickable_list (sort_of_term_basic_free_aux Sigma x).
  introv. unfolds sort_of_term_basic_free_aux.
  destruct (typed_environment_read_term Sigma x) as [t|] eqn: E.
  - applys pickable_list_make (sort_of_abstract_term_compute t).
    introv. rewrite <- sort_of_abstract_term_compute_eq. iff S.
    + exists*.
    + lets (t'&E'&S'): (rm S). inverts~ E'.
  - applys pickable_list_make (@nil term_sort). introv. iff I; repeat inverts I as I.
Defined.

(** Similarly to [read_term_from_concrete_state_direct] in the concrete interpretation,
  the following function reads a (term) variable [x] in the environment
  [Sigma] and then converts it into a term of type [term].
  Bottom terms are not associated any term. **)
Definition read_variable_in_abstract_state T (Sigma : abstract_state) x :=
  LibOption.apply_on (typed_environment_read_term Sigma x)
    (fun tx : abstract_term =>
     match tx with
     | abstract_term_closed tx0 => Some (closed_term_free_to_variable_term T tx0)
     | abstract_term_bottom _ => None
     end).

Lemma read_variable_in_abstract_state_closed : forall T Sigma x t,
  read_variable_in_abstract_state T Sigma x = Some t ->
  closed_term t.
Proof.
  introv E. unfolds in E. destruct typed_environment_read_term as [t'|]; simpl in E; tryfalse.
  destruct t'; inverts E. apply~ closed_term_free_to_variable_term_closed.
Qed.

(** The following predicate is the abstract equivalent of the concrete function
  [read_closed_term_from_concrete_state_from_basic_free_term]: from a basic-free
  term [t], it associates its substitution (an abstract term) using the environment
  [Sigma]. **)
Inductive read_term_from_abstract_state_from_basic_free_term (Sigma : abstract_state) :
    basic_free_term -> abstract_term -> Prop :=
  | read_term_from_abstract_state_term : forall t t',
    same_structure_subst_option (read_variable_in_abstract_state _ Sigma)
                                (basic_free_term_to_basic_term _ t) (closed_term_free_term t') ->
    read_term_from_abstract_state_from_basic_free_term Sigma t (abstract_term_closed t')
  | read_term_from_abstract_state_bottom : forall t x s s',
    term_free_variable (basic_free_term_term t) x ->
    typed_environment_read_term Sigma x = Some (abstract_term_bottom s) ->
    sort_of_term_basic_free Sigma t s' ->
    map_set (@X_t _ _) (term_free_variable (basic_free_term_term t))
      \c typed_environment_domain Sigma ->
    read_term_from_abstract_state_from_basic_free_term Sigma t (abstract_term_bottom s')
  .

(** The predicate [read_term_from_abstract_state_from_basic_free_term] behaves
  well with sorts. **)
Lemma read_term_from_abstract_state_from_basic_free_term_sort : forall Sigma t t' s,
  read_term_from_abstract_state_from_basic_free_term Sigma t t' ->
  sort_of_term_basic_free Sigma t s ->
  sort_of_abstract_term t' s.
Proof.
  introv R S. inverts R as R E S'.
  - unfolds in S. rewrite sort_of_term_basic_free_sort_of_term in S.
    constructors. applys same_structure_subst_option_sort R;
        [| applys sort_of_term_same_structure
                    (fun (_ : basic_free_term_basic_term t) (_ : base_sort) => False)
                    S basic_free_term_to_basic_term_spec ];
      try solve [ simpl; intros; false | introv ?; tryfalse; substs; reflexivity ].
    introv ? E1 (t2&E2&E3). unfolds read_variable_in_abstract_state. rewrite E2  in E1. simpls.
    destruct t2; inverts E1. inverts E3 as E3.
    applys~ sort_of_term_same_structure E3 closed_term_free_to_variable_term_spec.
    introv ?. substs~.
  - forwards~: sort_of_term_basic_free_unique S S'.
    { simpl. introv (t1&R1&S1) (t2&R2&S2). rewrite R1 in R2. inverts R2.
      applys~ sort_of_abstract_term_unique S1 S2. }
    substs. apply~ sort_of_abstract_term_bottom.
Qed.

(** The two defining cases of the predicate
  [read_term_from_abstract_state_from_basic_free_term] are disjoint. **)
Lemma read_term_from_abstract_state_from_basic_free_term_disjoint : forall Sigma t t' x s,
  same_structure_subst_option (read_variable_in_abstract_state _ Sigma)
                              (basic_free_term_to_basic_term _ t) (closed_term_free_term t') ->
  term_free_variable (basic_free_term_term t) x ->
  typed_environment_read_term Sigma x = Some (abstract_term_bottom s) ->
  False.
Proof.
  introv S FV E. applys same_structure_subst_option_free_variable_inv S.
  { applys~ term_free_variable_same_structure FV. rewrite basic_free_term_to_basic_term_idem.
    apply~ basic_free_term_to_basic_term_same_structure. }
  unfolds. rewrite~ E.
Qed.

(** If a term can be read from a state, then all its free variables
  are in the domain of the state. **)
Lemma read_term_from_abstract_state_closed_term_free_variable : forall (Sigma : abstract_state) t t',
  read_term_from_abstract_state_from_basic_free_term Sigma t t' ->
  map_set (@X_t _ _) (term_free_variable (basic_free_term_term t)) \c typed_environment_domain Sigma.
Proof.
  introv R. inverts R as R; autos~.
  apply incl_prove. introv I. forwards (x'&I'&E'): in_map_set_inv (rm I). substs.
  unfolds typed_environment_domain, Environments.typed_environment_domain. rewrite in_set_st_eq.
  simpl. forwards~ D: same_structure_subst_option_free_variable_inv R.
  { applys~ term_free_variable_same_structure basic_free_term_to_basic_term_spec I'. }
  unfold read_variable_in_abstract_state in D.
  destruct typed_environment_read_term; simpl in D; tryfalse. discriminate.
Qed.

(** The predicate [read_term_from_abstract_state_from_basic_free_term] is computable. **)
Instance read_term_from_abstract_state_from_basic_free_term_Pickable_option : forall Sigma t,
    Pickable_option (read_term_from_abstract_state_from_basic_free_term Sigma t).
  introv. asserts ID: (Decidable (map_set (@X_t _ _) (term_free_variable (basic_free_term_term t))
                                  \c typed_environment_domain Sigma)).
  { applys Decidable_equiv (map (@X_t _ _) (term_free_variable_list (basic_free_term_term t))
                            \c typed_environment_domain_list _ _ _ _ Sigma).
    - iff I; apply incl_prove; introv Ix.
      + rewrite in_map_set_eq in Ix. lets (x'&I'&?): (rm Ix). substs.
        forwards I2: @incl_inv I; try typeclass.
        { apply Mem_map. rewrite term_free_variable_list_spec. apply~ I'. }
        simpl in I2. rewrite typed_environment_domain_list_spec in I2. apply I2.
      + simpls. rewrite typed_environment_domain_list_spec.
        forwards (x'&I'&?): Mem_map_inv Ix. substs.
        rewrite term_free_variable_list_spec in I'.
        applys @incl_inv I; try typeclass; autos*.
    - apply Comparable_BagIncl_Decidable. (* FIXME: [typeclass] takes forever here. *) }
  destruct ID as [IDd IDdspec]. destruct IDd; fold_bool; rew_refl in IDdspec.
  - forwards~ PO: same_structure_subst_option_Pickable_option
                    (read_variable_in_abstract_state abstract_basic_term Sigma)
                    (basic_free_term_to_basic_term abstract_basic_term t).
    destruct (@pick_option _ _ PO) as [t'|] eqn: P.
    + asserts C: (closed_term t').
      { apply pick_option_correct in P. applys same_structure_subst_option_closed P.
        introv ? E. applys~ read_variable_in_abstract_state_closed E. }
      applys pickable_option_make (Some (abstract_term_closed (make_closed_term_free C))).
      * introv E. inverts E. constructors~. applys~ pick_option_correct P.
      * introv (t''&E). eexists. reflexivity.
    + asserts Sx: (exists x, term_free_variable (basic_free_term_to_basic_term abstract_basic_term t) x
                             /\ read_variable_in_abstract_state abstract_basic_term Sigma x = None).
      { apply not_not_elim. introv A. rew_logic in A. forwards (?&E): pick_option_defined.
        - applys same_structure_subst_option_defined constructor_signature.
          introv FV. forwards Ax: A x. rew_logic in Ax. inverts* Ax.
        - rewrite E in P. inverts P. }
      asserts Sx': (exists x, term_free_variable (basic_free_term_term t) x
                             /\ read_variable_in_abstract_state abstract_basic_term Sigma x = None).
      { lets (x&FV&R): (rm Sx). exists x. splits~.
        applys term_free_variable_same_structure (fun (_ : abstract_basic_term)
                                                      (_ : basic_free_term_basic_term t) => False) FV.
        applys~ same_structure_sym basic_free_term_to_basic_term_spec;
          simpl; intros; tryfalse; substs~. }
      asserts PS: (Pickable_option (sort_of_term_basic_free Sigma t)).
      { apply Pickable_list_Pickable_option. apply~ sort_of_term_Pickable.
        - intros. false~.
        - apply~ sort_of_term_basic_free_aux_Pickable. }
      applys pickable_option_make (LibOption.map_on (pick_option (sort_of_term_basic_free Sigma t))
                                                    abstract_term_bottom).
      * introv E. destruct (pick_option (sort_of_term_basic_free Sigma t)) as [s|] eqn: E'; inverts E.
        apply pick_option_correct in E'. lets (x&FV&R): (rm Sx').
        unfolds in R. destruct typed_environment_read_term as [t'|] eqn: E.
        -- destruct t' as [|s']; tryfalse. constructors*.
        -- forwards (s'&Es'): sort_of_term_free_variable_defined
                                (fun (_ : basic_free_term_basic_term t) (_ : base_sort) => False) FV.
           { applys sort_of_term_same_structure (fun (_ : False) (_ : basic_free_term_basic_term t) =>
                                                   False) E';
               [| | applys same_structure_sym basic_free_term_to_basic_term_spec ];
             try solve [ introv I; apply I | simpl; intros; tryfalse; substs; reflexivity ]. }
           lets (?&A&?): (rm Es'). rewrite E in A. inverts~ A.
      * introv (t'&R). inverts R as E1 E2 E3.
        -- forwards (?&E): @pick_option_defined PO.
           { eexists. applys same_structure_subst_option_same_structure E1.
             - introv ? E. unfolds read_variable_in_abstract_state.
               destruct typed_environment_read_term as [tx|]; tryfalse.
               destruct tx as [tx|]; inverts E as E. simpl. fequals.
               destruct tx as [X tx C]. destruct tx.
               + inverts~ C.
               + erewrite closed_term_free_to_variable_term_closed_term_free_basic in E; try reflexivity.
                 inverts E. reflexivity.
               + forwards (ts'&E'&F'): closed_term_free_to_variable_term_closed_term_free_constructor
                                        (make_closed_term_free C).
                 { reflexivity. }
                 rewrite E' in E. inverts~ E.
             - introv ? F E. unfolds read_variable_in_abstract_state.
               destruct typed_environment_read_term as [tx|]; tryfalse.
               destruct tx as [tx|]; inverts E as E. simpl. fequals.
               destruct tx as [X tx C]. destruct tx.
               + inverts~ C.
               + forwards Ea: closed_term_free_to_variable_term_closed_term_free_basic a.
                 rewrites >> proj2 Ea in E; inverts~ E.
               + forwards (ta&Ea&Fa): closed_term_free_to_variable_term_closed_term_free_constructor
                                        (make_closed_term_free C).
                 { reflexivity. }
                 rewrite Ea in E. inverts~ E.
                 forwards (tb&Eb&Fb): closed_term_free_to_variable_term_closed_term_free_constructor
                                        (make_closed_term_free C).
                 { reflexivity. }
                 rewrite Eb. fequals. apply Forall2_eq. rewrite Forall2_iff_forall_Nth in *.
                 lets (Fal&Fa'): (rm Fa). lets (Fbl&Fb'): (rm Fb). lets (Fl&F'): (rm F). splits~.
                 * rewrite <- Fbl. rewrite~ Fal.
                 * introv N1 N2. forwards (li&N3): length_Nth_lt.
                   { rewrite Fbl. applys~ Nth_lt_length N1. }
                   forwards (l'&N4): length_Nth_lt.
                   { rewrite <- Fal. applys~ Nth_lt_length N3. }
                   forwards E': F' N4 N2. forwards Eb': Fb' N3 N1. forwards Ea': Fa' N3 N4.
                   applys same_structure_refl_inv. applys same_structure_trans
                       (fun (_ : abstract_basic_term) (_ : closed_term_free_variable_term t'0) => False)
                       (fun b1 b2 : abstract_basic_term => b1 = b2)
                       (fun b1 b2 : abstract_basic_term => b1 = b2) E';
                     try solve [ intros; tryfalse; substs~ ].
                   applys same_structure_trans (fun (_ : abstract_basic_term) (_ : X) => False)
                       (fun b1 b2 : abstract_basic_term => b1 = b2) Ea';
                     try solve [ intros; tryfalse; substs~ ].
                   applys same_structure_sym Eb'; simpl; intros; tryfalse; substs~.
             - apply closed_term_free_to_variable_term_spec. }
           rewrite E in P. false*.
        -- forwards (?&E): pick_option_defined.
           { exists*. }
           rewrite E. exists*.
  - applys pickable_option_make (@None abstract_term).
    + introv E. inverts E.
    + introv (t'&R). false IDdspec. applys~ read_term_from_abstract_state_closed_term_free_variable R.
Defined.


(** ** Abstract Interpretation **)

(** The abstract interpretation is a triple-based interpretation, as
  the concrete interpretation.  Triples are of the form [(asigma, t,
  ao)], where [asigma] and [ao] are [abstract_value]s and [t] is a
  possibly-bottom closed term (of type [term_with_bottom]).  Hooks
  directly pick the given [(asigma, t, ao)] from the set of triples,
  filters apply an abstract interpretation of filters which can be
  seen as a partial function (the result of this function should
  soundly abstract all concrete result possibly returned by the
  concrete [filter_interpretation]), and branches consider all the
  paths in the subskeletons that return a value.  This last point is
  the main difference between concrete and abstract interpretations:
  the abstract interpretation considers all paths whilst the concrete
  interpretation only consider one path at a time. **)

(** These definitions correspond to Figure 10 of the paper. **)

Inductive abstract_interpretation_hook_shortcut :
    variable_flow_extended -> basic_free_term -> variable_flow_extended ->
    flag_abstract_state -> flag_abstract_state -> Prop :=
  | abstract_interpretation_hook_false : forall xf1 t xf2 Sigma Sigma' t' s,
    read_term_from_abstract_state_from_basic_free_term Sigma t t' ->
    sort_of_abstract_term t' (sort_program s) ->
    Sigma' = typed_environment_write_flow Sigma xf2 (bottom_flow (out_sort s)) ->
    abstract_interpretation_hook_shortcut xf1 t xf2 (false, Sigma) (false, Sigma')
  .

Inductive abstract_interpretation_hook :
    variable_flow_extended -> basic_free_term -> variable_flow_extended ->
    abstract_value -> abstract_closed_term_free -> abstract_value ->
    flag_abstract_state -> flag_abstract_state -> Prop :=
  | abstract_interpretation_hook_true_to_false : forall xf1 v1 t t' xf2 Sigma Sigma' v1' t'' s2 s,
    read_term_from_abstract_state_from_basic_free_term Sigma t t' ->
    t' ⊑ abstract_term_closed t'' ->
    typed_environment_read_flow Sigma xf1 = Some v1 ->
    v1 ⊑ v1' ->
    sort_of_abstract_term t'' (sort_program s) ->
    Sigma' = typed_environment_write_flow Sigma xf2 (bottom_flow (out_sort s)) ->
    abstract_interpretation_hook xf1 t xf2 v1' t'' (abstract_term_bottom s2)
                                 (true, Sigma) (false, Sigma')
  | abstract_interpretation_hook_true : forall xf1 v1 t t' xf2 v2 Sigma Sigma' v1' t'' v2',
    read_term_from_abstract_state_from_basic_free_term Sigma t t' ->
    t' ⊑ abstract_term_closed t'' ->
    typed_environment_read_flow Sigma xf1 = Some v1 ->
    v1 ⊑ v1' ->
    v2 ⊑ v2' ->
    Sigma' = typed_environment_write_flow Sigma xf2 v2' ->
    abstract_interpretation_hook xf1 t xf2 v1' t'' v2 (true, Sigma) (true, Sigma')
  .

Inductive abstract_interpretation_filter :
    filter -> list variable -> list variable ->
    flag_abstract_state -> flag_abstract_state -> Prop :=
  | abstract_interpretation_filter_false : forall f xs ys Sigma Sigma' vssi vsso,
    filter_signature f = (vssi, vsso) ->
    typed_environment_write_variables Sigma ys (map bottom vsso) = Some Sigma' ->
    abstract_interpretation_filter f xs ys (false, Sigma) (false, Sigma')
  | abstract_interpretation_filter_true_to_false : forall f xs ys Sigma Sigma' vxs vxs' vssi vsso,
    Forall2 (fun x v => typed_environment_read_variable Sigma x = Some v) xs vxs ->
    Forall2 (fun vx vx' => vx ⊑ vx') vxs vxs' ->
    abstract_filter_interpretation f vxs' None ->
    filter_signature f = (vssi, vsso) ->
    typed_environment_write_variables Sigma ys (map bottom vsso) = Some Sigma' ->
    abstract_interpretation_filter f xs ys (true, Sigma) (false, Sigma')
  | abstract_interpretation_filter_true : forall f xs ys Sigma Sigma' vxs vxs' vys,
    Forall2 (fun x v => typed_environment_read_variable Sigma x = Some v) xs vxs ->
    Forall2 (fun vx vx' => vx ⊑ vx') vxs vxs' ->
    abstract_filter_interpretation f vxs' (Some vys) ->
    typed_environment_write_variables Sigma ys vys = Some Sigma' ->
    abstract_interpretation_filter f xs ys (true, Sigma) (true, Sigma')
  .

Inductive abstract_interpretation_merge :
    list (option flag_abstract_state) -> list variable ->
    flag_abstract_state -> flag_abstract_state -> Prop :=
  | abstract_interpretation_merge_false : forall O V Sigma Sigma' f Sigma0,
    length O >= 1 ->
    Forall (fun Oi => exists Sigmai,
      Oi = Some (false, Sigmai)
      /\ to_set V \c typed_environment_domain Sigmai) O ->
    (forall i j Sigmai Sigmaj,
      i <> j ->
      Nth i O (Some (false, Sigmai)) ->
      Nth j O (Some (false, Sigmaj)) ->
      typed_environment_equivalent (typed_environment_restrict Sigmai V)
                                   (typed_environment_restrict Sigmaj V)) ->
    Nth 0 O (Some (false, Sigma0)) ->
    typed_environment_equivalent Sigma'
      (typed_environment_merge Sigma (typed_environment_restrict Sigma0 V)) ->
    abstract_interpretation_merge O V (f, Sigma) (false, Sigma')
  | abstract_interpretation_merge_true : forall O V Sigma Sigma' E,
    Forall (fun Oi => Oi <> None) O ->
    E = set_st (fun Sigmai => exists n, Nth n O (Some (true, Sigmai))) ->
    E <> \{} ->
    (forall_ Sigmai \in E,
      to_set V \c typed_environment_domain Sigmai
      /\ typed_environment_equivalent Sigma'
           (typed_environment_merge Sigma (typed_environment_restrict Sigmai V))) ->
    abstract_interpretation_merge O V (true, Sigma) (true, Sigma')
  .

Definition abstract := {|
    triple_based_interpretation_hook_shortcut := abstract_interpretation_hook_shortcut ;
    triple_based_interpretation_hook := abstract_interpretation_hook ;
    triple_based_interpretation_filter := abstract_interpretation_filter ;
    triple_based_interpretation_merge := abstract_interpretation_merge
  |}.

End AbstractState.


(** ** Consistency of the Well-formedness and the Abstract Interpretation **)

(** We show that the abstract interpretation only produces well-formed triples
  from a well-formed set of triples. **)

Let wellformed_set_of_triples
     : triple_based_interpretation_set_of_triples abstract -> Prop :=
  wellformed_set_of_triples in_sort out_sort constructor_signature
                             abstract_basic_term_sort
                             sort_of_abstract_value sort_of_abstract_value.

(** The well-formedness and abstract environments are in sync when
  they have the same domains and that every variable in this domain is
  associated a sort that corresponds to its associated value. **)
Definition WFabstractOKout (GammaD : interpretation_result wellformedness)
                           (fSigma : interpretation_result abstract) :=
  let (Gamma, D) := GammaD in
  let (f, Sigma) := fSigma in
  typed_environment_domain sort_term sort_flow Gamma
  = typed_environment_domain abstract_value_term abstract_value_flow Sigma
  /\ forall x v s,
       typed_environment_read_variable abstract_value_term abstract_value_flow Sigma x = Some v ->
       typed_environment_read_variable sort_term sort_flow Gamma x = Some s ->
       sort_of_abstract_value v s.

(** We also require the triple set to be well-formed. **)
Definition WFabstractOKst (GammaD : interpretation_state wellformedness)
                          (fSigmaT : interpretation_state abstract) :=
  let (fSigma, T) := fSigmaT in
  wellformed_set_of_triples T
  /\ WFabstractOKout GammaD fSigma.

(** This lemma states that the well-formedness and abstract interpretations
  are universally consistent. **)
(** It corresponds to Lemma 5.1 of the paper. **)
(** Note how this lemma is proven: no induction is performed.  Instead,
  the lemma [local_universal_consistency] is directly applied, which
  changes the goal into local properties to prove. **)
Lemma WFabstract_universally_consistent :
  universally_consistent wellformedness abstract WFabstractOKst WFabstractOKout.
Proof.
  apply local_universal_consistency.
  - intros (Gamma&D) ((f&Sigma)&T) (Gamma'&D') (f'&Sigma') (OKT&E&F) D1 D2.
    inverts D1 as E1. inverts D2. splits~.
    + rewrite <- E. applys~ typed_environment_domain_equivalent term_sort_inj flow_sort_inj.
      applys~ typed_environment_equivalent_sym E1; [ apply term_sort_inj | apply flow_sort_inj ].
    + introv Ea Eb. applys F Ea.
      rewrites >> typed_environment_equivalent_spec term_sort_inj flow_sort_inj in E1. rewrite* E1.
  - intros xf1 t xf2 (Gamma&D) ((f&Sigma)&T) (Gamma'&D') (f'&Sigma') (OKT&E&F) D1 D2.
    inverts D1 as DI1 Dxf11 F1 S1 EG1 NDxf21 E1.
    asserts Ef: (read_to_set (typed_environment_read_term Gamma)
                 = sort_of_term_basic_free_aux Sigma).
    { asserts Ef0: (read_to_set (typed_environment_read_term Gamma)
                   = fun x => set_st (sort_of_term_basic_free_aux Sigma x)).
      { extens. intro x. unfolds sort_of_term_basic_free_aux.
        tests I: (X_t x \in typed_environment_domain sort_term sort_flow Gamma).
        - lets I': I. rewrite E in I'. unfold typed_environment_domain in I, I'.
          rewrite in_set_st_eq in I, I'. simpl in I, I'.
          destruct (typed_environment_read_term Gamma) eqn: Ea; tryfalse~.
          destruct (typed_environment_read_term Sigma) eqn: Eb; tryfalse~.
          forwards Ss: F (X_t x : variable).
          + simpl. rewrite* Eb.
          + simpl. unfolds term_sort. rewrite* Ea.
          + unfolds read_to_set. rewrite Ea. rewrite option_to_set_Some. apply set_ext.
            introv. rewrite in_single_eq. rewrite in_set_st_eq. iff I0.
            * substs. eexists. splits*. inverts~ Ss.
            * lets (?&E'&Sa): (rm I0). inverts~ E'. inverts Ss as Sb.
              applys~ sort_of_abstract_term_unique Sa Sb.
        - lets I': I. rewrite E in I'. unfold typed_environment_domain in I, I'.
          rewrite in_set_st_eq in I, I'. simpl in I, I'.
          destruct (typed_environment_read_term Gamma) eqn: Ea; [ false I; discriminate |].
          destruct (typed_environment_read_term Sigma) eqn: Eb; [ false I'; discriminate |].
          unfolds read_to_set. rewrite Ea. rewrite option_to_set_None. symmetry. apply is_empty_prove.
          introv (?&A&?). inverts~ A. }
      rewrite Ef0. extens. introv. apply set_ext. introv. rewrite in_set_st_eq. iff I; apply I. }
    inverts D2 as Tt2 H2.
    + inverts H2 as R2 I2 R2' I2' S2.
      * do 2 splits~.
        -- rewrites >> typed_environment_domain_equivalent E1; try solve [ introv E'; inverts~ E' ].
           do 2 rewrite typed_environment_domain_write_flow. fequals~.
        -- introv Ea Eb. rewrites >> typed_environment_equivalent_spec in E1;
             [ apply term_sort_inj | apply flow_sort_inj |].
           rewrites (rm E1) in Eb. tests Dx: (_X_ xf2 = x).
           ++ simpl in Ea. rewrite typed_environment_read_write_flow_eq in Ea. inverts Ea.
              forwards (c&ts&s'&E0&S0&Vsigma&Vo): OKT Tt2.
              simpl in Eb. rewrite typed_environment_read_write_flow_eq in Eb. inverts Eb.
              forwards Ss: read_term_from_abstract_state_from_basic_free_term_sort R2.
              { rewrite <- Ef. apply~ S1. }
              rewrites >> sort_of_abstract_term_order I2 in Ss.
              forwards~ E': sort_of_abstract_term_unique Ss S2. inverts~ E'.
              applys_eq sort_of_abstract_value_flow 1.
              unfolds @id. apply~ bottom_flow_sort. reflexivity.
           ++ applys~ F x.
              ** rewrites~ <- >> typed_environment_read_write_variable_neq abstract_value_rect
                                   (bottom_flow (out_sort s0)) Dx;
                   try apply~ Ea.
                 reflexivity.
              ** rewrites~ <- >> typed_environment_read_write_variable_neq
                                    (@sort_rect _ _ _ : forall (P : sort -> _), _)
                                    (out_sort s : sort) Dx;
                   try apply~ Eb.
                 reflexivity.
      * do 2 splits~.
        -- rewrites >> typed_environment_domain_equivalent E1; try solve [ introv E'; inverts~ E' ].
           do 2 rewrite typed_environment_domain_write_flow. fequals~.
        -- introv Ea Eb. rewrites >> typed_environment_equivalent_spec in E1;
             [ apply term_sort_inj | apply flow_sort_inj |].
           rewrites (rm E1) in Eb. tests Dx: (_X_ xf2 = x).
           ++ simpl in Ea. rewrite typed_environment_read_write_flow_eq in Ea. inverts Ea.
              forwards (c&ts&s'&E0&S0&Vsigma&Vo): OKT Tt2.
              simpl in Eb. rewrite typed_environment_read_write_flow_eq in Eb. inverts Eb.
              forwards Ss: read_term_from_abstract_state_from_basic_free_term_sort R2.
              { rewrite <- Ef. apply~ S1. }
              rewrites >> sort_of_abstract_term_order I2 in Ss.
              forwards~ E': sort_of_abstract_term_unique Ss.
              { constructor. apply~ S0. }
              inverts~ E'. applys_eq~ sort_of_abstract_value_flow 1.
              unfolds @Datatypes.id. rewrites~ <- >> abstract_flow_Poset_sort S2.
              inverts Vo as S. apply~ S.
           ++ applys~ F x.
              ** rewrites~ <- >> typed_environment_read_write_variable_neq abstract_value_rect v2' Dx;
                   try apply~ Ea.
                 reflexivity.
              ** rewrites~ <- >> typed_environment_read_write_variable_neq
                                    (@sort_rect _ _ _ : forall (P : sort -> _), _)
                                    (out_sort s : sort) Dx;
                   try apply~ Eb.
                 reflexivity.
    + inverts Tt2 as R2 S2. do 2 splits~.
      * rewrites >> typed_environment_domain_equivalent E1; try solve [ introv E'; inverts~ E' ].
        do 2 rewrite typed_environment_domain_write_flow. fequals~.
      * introv Ea Eb. rewrites >> typed_environment_equivalent_spec in E1;
          [ apply term_sort_inj | apply flow_sort_inj |].
        rewrites (rm E1) in Eb. tests Dx: (_X_ xf2 = x).
        -- simpl in Ea. rewrite typed_environment_read_write_flow_eq in Ea. inverts Ea.
           simpl in Eb. rewrite typed_environment_read_write_flow_eq in Eb. inverts Eb.
           forwards Ss: read_term_from_abstract_state_from_basic_free_term_sort R2.
           { rewrite <- Ef. apply~ S1. }
           forwards~ E': sort_of_abstract_term_unique S2 Ss. inverts E'.
           applys_eq~ sort_of_abstract_value_flow 1.
           unfolds @id. apply~ bottom_flow_sort.
        -- applys~ F x.
           ** rewrites~ <- >> typed_environment_read_write_variable_neq abstract_value_rect
                                (bottom_flow (out_sort s0)) Dx;
                try apply~ Ea.
              reflexivity.
           ** rewrites~ <- >> typed_environment_read_write_variable_neq
                                 (@sort_rect _ _ _ : forall (P : sort -> _), _)
                                 (out_sort s : sort) Dx;
                try apply~ Eb.
              reflexivity.
  - intros f xs ys (Gamma&D) ((f0&Sigma)&T) (Gamma'&D') ((f'&Sigma')&T') (OKT&E&F) D1 D2.
    inverts D1 as DI F1 ND F2 F3 Ef EGamma'. inverts D2 as Tf.
    inverts Tf as F4 F5 FI ESigma' ESigma''.
    + do 2 splits~.
      * unfold sort_term, sort_flow, term_sort, sort.
        rewrites~ >> typed_environment_domain_write_variables EGamma'.
        rewrites~ >> typed_environment_domain_write_variables F5. fequals~.
      * introv Ea Eb. tests I: (Mem x ys).
        -- lets (n&Ny): Mem_Nth (rm I). forwards (v'&Nv): length_Nth_lt.
           { rewrites <- >> typed_environment_read_write_variables_length F5.
             applys~ Nth_lt_length Ny. }
           forwards~ Ea': typed_environment_read_write_variables_Nth Ny Nv F5.
           rewrite Ea' in Ea. inverts Ea. lets (v'&Nv'&E'): map_Nth_inv (rm Nv). substs.
           forwards (ys'&Nys): length_Nth_lt.
           { rewrites <- >> typed_environment_read_write_variables_length EGamma'.
             applys~ Nth_lt_length Ny. }
           forwards~ Eb': typed_environment_read_write_variables_Nth Ny Nys EGamma'.
           unfolds sort_term, sort_flow, term_sort, sort.
           rewrite Eb' in Eb. inverts Eb.
           rewrite Ef in F4. inverts F4. forwards: Nth_func Nv' Nys. substs. apply bottom_sort.
        -- rewrites~ >> typed_environment_read_write_variables_not_in I F5 in Ea.
           unfolds sort_term, sort_flow, term_sort, sort.
           rewrites~ >> typed_environment_read_write_variables_not_in I EGamma' in Eb.
           applys~ F Ea Eb.
    + do 2 splits~.
      * unfold sort_term, sort_flow, term_sort, sort.
        rewrites~ >> typed_environment_domain_write_variables EGamma'.
        rewrites~ >> typed_environment_domain_write_variables ESigma''. fequals~.
      * introv Ea Eb. tests I: (Mem x ys).
        -- lets (n&Ny): Mem_Nth (rm I). forwards (v'&Nv): length_Nth_lt.
           { rewrites <- >> typed_environment_read_write_variables_length ESigma''.
             applys~ Nth_lt_length Ny. }
           forwards~ Ea': typed_environment_read_write_variables_Nth Ny Nv ESigma''.
           rewrite Ea' in Ea. inverts Ea. lets (v'&Nv'&E'): map_Nth_inv (rm Nv). substs.
           forwards (ys'&Nys): length_Nth_lt.
           { rewrites <- >> typed_environment_read_write_variables_length EGamma'.
             applys~ Nth_lt_length Ny. }
           forwards~ Eb': typed_environment_read_write_variables_Nth Ny Nys EGamma'.
           unfolds sort_term, sort_flow, term_sort, sort.
           rewrite Eb' in Eb. inverts Eb.
           rewrite Ef in ESigma'. inverts ESigma'.
           forwards: Nth_func Nv' Nys. substs. apply bottom_sort.
        -- rewrites~ >> typed_environment_read_write_variables_not_in I ESigma'' in Ea.
           unfolds sort_term, sort_flow, term_sort, sort.
           rewrites~ >> typed_environment_read_write_variables_not_in I EGamma' in Eb.
           applys~ F Ea Eb.
    + do 2 splits~.
      * unfold sort_term, sort_flow, term_sort, sort.
        rewrites~ >> typed_environment_domain_write_variables EGamma'.
        rewrites~ >> typed_environment_domain_write_variables ESigma'. fequals~.
      * introv Ea Eb. tests I: (Mem x ys).
        -- lets (n&Ny): Mem_Nth (rm I). forwards (v'&Nv): length_Nth_lt.
           { rewrites <- >> typed_environment_read_write_variables_length ESigma'.
             applys~ Nth_lt_length Ny. }
           forwards~ Ea': typed_environment_read_write_variables_Nth Ny Nv ESigma'.
           rewrite Ea' in Ea. inverts Ea. forwards (ys'&Nys): length_Nth_lt.
           { rewrites <- >> typed_environment_read_write_variables_length EGamma'.
             applys~ Nth_lt_length Ny. }
           forwards~ Eb': typed_environment_read_write_variables_Nth Ny Nys EGamma'.
           unfolds sort_term, sort_flow, term_sort, sort.
           rewrite Eb' in Eb. inverts Eb.
           forwards F6: abstract_filter_interpretation_consistent_with_sort FI Ef.
           { rewrite Forall2_iff_forall_Nth in F3. lets (El3&F3'): (rm F3).
             rewrite Forall2_iff_forall_Nth in F4. lets (El4&F4'): (rm F4).
             rewrite Forall2_iff_forall_Nth in F5. lets (El5&F5'): (rm F5).
             rewrite Forall2_iff_forall_Nth. splits~.
             - rewrite <- El3. rewrite~ <- El5.
             - introv N1 N2. forwards (x'&N3): length_Nth_lt.
               { rewrite El3. applys~ Nth_lt_length N2. }
               forwards (v'&N4): length_Nth_lt.
               { rewrite El5. applys~ Nth_lt_length N1. }
               forwards O: F5' N4 N1. rewrites~ <- >> sort_of_abstract_value_order O. applys~ F.
               + applys~ F4' N3 N4.
               + applys~ F3' N3 N2. }
           rewrite Forall2_iff_forall_Nth in F6. lets (El6&F6'): (rm F6). applys~ F6' Nv Nys.
        -- rewrites~ >> typed_environment_read_write_variables_not_in I ESigma' in Ea.
           unfolds sort_term, sort_flow, term_sort, sort.
           rewrites~ >> typed_environment_read_write_variables_not_in I EGamma' in Eb.
           applys~ F Ea Eb.
  - intros V O1 O2 (Gamma&D) ((f&Sigma)&T) (Gamma'&D') ((f'&Sigma')&T') (OKT&E&F) F' D1 D2.
    inverts D1 as O1l DI F2 F3 F4 F5 F6. inverts D2 as Tm. inverts Tm as FD NE F7 N ESigma'.
    + inverts F2 as F2; try solve [ inverts~ O1l ]. inverts F6 as EGamma' F6.
      do 2 splits~.
      * rewrites >> typed_environment_domain_equivalent ESigma'; try solve [ introv E'; inverts~ E' ].
        rewrites >> typed_environment_domain_equivalent EGamma'; try solve [ introv E'; inverts~ E' ].
        rewrite_typed_environment_domain;
          try solve [ apply~ term_sort_Inhab | apply~ closed_term_free_Inhab | typeclass ].
        rewrite E. repeat fequals~. inverts N. inverts F' as W. forwards~ W': W. lets~ (E'&?): W'.
      * introv Ea Eb.
        rewrites >> typed_environment_equivalent_spec abstract_value_term
                      abstract_value_flow in ESigma'; try solve [ introv I; inverts~ I ].
        rewrite ESigma' in Ea. rewrites >> typed_environment_equivalent_spec in EGamma';
          [ apply term_sort_inj | apply flow_sort_inj |].
        rewrite EGamma' in Eb. forwards DV: get_disjunction_from_merge_case F5.
        { rew_list. rewrite Forall3_iff_forall_Nth in F2. lets (El12&El23&F2'): (rm F2).
          rewrite <- El23. rew_list in O1l. rewrite El12 in O1l. apply~ O1l. }
        rewrite disjoint_eq in DV. tests Dx: (x \in V).
        -- asserts NGamma: (~ x \in typed_environment_domain sort_term sort_flow Gamma).
           { introv Ix. false DV Dx. rewrite incl_in_eq in DI. applys~ DI Ix. }
           rewrite~ typed_environment_read_variable_merge_right in Eb;
             try solve [ apply~ term_sort_Inhab ].
           rewrite~ typed_environment_read_restrict_Mem in Eb;
             try solve [ apply~ term_sort_Inhab ].
           rewrites~ >> typed_environment_read_variable_merge_right in Ea;
             try solve [ apply~ term_sort_Inhab | apply~ closed_term_free_Inhab | typeclass ].
           { rewrite~ <- E. }
           rewrite~ typed_environment_read_restrict_Mem in Ea; try typeclass.
           inverts F' as W F'. inverts N. forwards~ W': (rm W). lets (Ed&F0): (rm W'). applys~ F0 Ea Eb.
        -- rewrite~ typed_environment_read_variable_merge_left in Ea; try typeclass.
           ++ rewrite~ typed_environment_read_variable_merge_left in Eb; try typeclass.
              rewrite~ typed_environment_domain_restrict; try typeclass. introv Ix. false Dx.
              rewrite in_inter_eq in Ix. lets (Ix'&Vx): (rm Ix). unfold to_set in Vx.
              rewrite in_set_st_eq in Vx. apply~ Vx.
           ++ rewrite~ typed_environment_domain_restrict; try typeclass. introv Ix. false Dx.
              rewrite in_inter_eq in Ix. lets (Ix'&Vx): (rm Ix). unfold to_set in Vx.
              rewrite in_set_st_eq in Vx. apply~ Vx.
    + asserts (Sigmai&n&NSigmai): (exists Sigmai n, Nth n O2 (Some (true, Sigmai))).
      { apply not_not_elim. introv A. rew_logic in A. false NE.
        apply~ @is_empty_prove. typeclass. }
      rewrite Forall2_iff_forall_Nth in F'. lets (El&F''): (rm F').
      rewrite Forall3_iff_forall_Nth in F2. lets (El2a&El2b&F2'): (rm F2).
      forwards (Gammai&NGammai): length_Nth_lt.
      { rewrite <- El2a. simpl in El. unfolds wellformedness_environment. rewrite El.
        applys~ Nth_lt_length NSigmai. }
      forwards (o1&N1): length_Nth_lt.
      { rewrite El2a. applys~ Nth_lt_length NGammai. }
      forwards (Di&N2): length_Nth_lt.
      { rewrite <- El2b. applys~ Nth_lt_length NGammai. }
      forwards: F2' N1 NGammai N2. substs. forwards~ W: F'' N1 NSigmai. lets (Ed&F'): (rm W).
      rewrite Forall_iff_forall_mem in FD, F6. do 2 splits~.
      * rewrites >> typed_environment_domain_equivalent F7; try solve [ introv E'; inverts~ E' ].
        { rewrite in_set_st_eq. exists* n. }
        rewrites >> typed_environment_domain_equivalent F6; try solve [ introv E'; inverts~ E' ].
        { applys~ Nth_mem NGammai. }
        rewrite_typed_environment_domain;
          try solve [ apply~ term_sort_Inhab | apply~ closed_term_free_Inhab | typeclass ].
        rewrite E. repeat fequals~.
      * introv Ea Eb. forwards~ DV: get_disjunction_from_merge_case F5.
        { rewrite <- El2b. rewrite~ <- El2a. }
        rewrite disjoint_eq in DV. forwards (IV&ESigma'): F7.
        { rewrite in_set_st_eq. eexists. apply~ NSigmai. }
        rewrites >> typed_environment_equivalent_spec abstract_value_term
                      abstract_value_flow in ESigma'; try solve [ introv I; inverts~ I ].
        rewrite ESigma' in Ea. forwards EGamma': F6.
        { applys~ Nth_mem NGammai. }
        rewrites~ >> typed_environment_equivalent_spec term_sort_inj flow_sort_inj in EGamma'.
        rewrite EGamma' in Eb. tests Dx: (x \in V).
        -- asserts NGamma: (~ x \in typed_environment_domain sort_term sort_flow Gamma).
           { introv Ix. false DV Dx. rewrite incl_in_eq in DI. applys~ DI Ix. }
           rewrite~ typed_environment_read_variable_merge_right in Eb;
             try solve [ apply~ term_sort_Inhab ].
           rewrite~ typed_environment_read_restrict_Mem in Eb;
             try solve [ apply~ term_sort_Inhab ].
           rewrites~ >> typed_environment_read_variable_merge_right in Ea;
             try solve [ apply~ term_sort_Inhab | apply~ closed_term_free_Inhab | typeclass ].
           { rewrite~ <- E. }
           rewrite~ typed_environment_read_restrict_Mem in Ea; try typeclass.
        -- rewrite~ typed_environment_read_variable_merge_left in Ea; try typeclass.
           ++ rewrite~ typed_environment_read_variable_merge_left in Eb; try typeclass.
              rewrite~ typed_environment_domain_restrict; try typeclass. introv Ix. false Dx.
              rewrite in_inter_eq in Ix. lets (Ix'&Vx): (rm Ix). unfold to_set in Vx.
              rewrite in_set_st_eq in Vx. apply~ Vx.
           ++ rewrite~ typed_environment_domain_restrict; try typeclass. introv Ix. false Dx.
              rewrite in_inter_eq in Ix. lets (Ix'&Vx): (rm Ix). unfold to_set in Vx.
              rewrite in_set_st_eq in Vx. apply~ Vx.
Qed.

End Variables.


(** ** Abstract Interpretation of Skeletal Semantics **)

(** This section looks surprisingly similar to the results and proofs
  about the concrete interpretations.  This is because the concrete and
  the abstract interpretations are actually very similar in structure.
  There however exists some key differences making the proofs difficult
  to factorise. **)

Section Skeleton.

Variable S : skeletal_semantics constructor filter.

Hypothesis skeleton_instance_term_Comparable : forall c, Comparable (skeleton_variable_term (S c)).
Hypothesis skeleton_instance_flow_Comparable : forall c, Comparable (skeleton_variable_flow (S c)).

Hypothesis S_wellformed :
  skeletal_semantics_wellformed in_sort out_sort
                                filter_signature constructor_signature S _ _.

Let abstract c : triple_based_interpretation _ _ _ _ _ :=
  abstract (skeleton_instance_term_Comparable c)
           (skeleton_instance_flow_Comparable c).

Let abstract_state c : Type :=
  abstract_state (skeleton_variable_term (S c)) (skeleton_variable_flow (S c)).

Definition abstract_triple_set : Type :=
  triple_set constructor abstract_basic_term abstract_value abstract_value.

Let wellformed_set_of_triples
     : abstract_triple_set -> Prop :=
  wellformed_set_of_triples in_sort out_sort constructor_signature abstract_basic_term_sort
                            sort_of_abstract_value sort_of_abstract_value.

Let typed_environment_read_variable c :
    abstract_state c -> variable_skeleton (S c) -> option abstract_value :=
  typed_environment_read_variable abstract_value_term abstract_value_flow (variable_flow := _).

Let typed_environment_write_variables c
    : abstract_state c -> list (variable_skeleton (S c)) ->
      list abstract_value -> option (abstract_state c) :=
  typed_environment_write_variables abstract_value_term abstract_value_flow
                                    abstract_value_rect (variable_flow := _).

(** The abstract initial environment associates [X_sigma] to the starting
  abstract value [sigma] of the current triple, and each term variables of the
  current rule to their instantiation in the current triple. **)
Definition abstract_initial_state c sigma ts :=
  typed_environment_write_variables typed_environment_empty
    (_X_ X_sigma :: map (@X_t _ _) (skeleton_term_variables (S c)))
    (abstract_value_flow sigma :: map (id : abstract_closed_term_free -> abstract_value) ts).

(** Although [abstract_initial_state] is an option type,
  it is actually defined. **)
Lemma abstract_initial_state_defined : forall c (sigma : abstract_flow_value) ts,
  length (skeleton_term_variables (S c)) = length ts ->
  exists Sigma, abstract_initial_state c sigma ts = Some Sigma.
Proof.
  introv E. unfolds abstract_initial_state. simpl.
  generalize (typed_environment_write_flow (typed_environment_empty : abstract_state c)
                                           X_sigma sigma) as Sigma.
  gen E. generalize (skeleton_term_variables (S c)) as ts'. induction ts; introv E; introv.
  - eexists. destruct ts'; tryfalse. reflexivity.
  - destruct ts' as [|ts0 ts']; tryfalse. forwards~ (Sigma'&ESigma): (rm IHts) ts'.
    eexists. apply ESigma.
Qed.

Lemma abstract_initial_state_defined_inv : forall c (sigma : abstract_flow_value) ts Sigma,
  abstract_initial_state c sigma ts = Some Sigma ->
  length (skeleton_term_variables (S c)) = length ts.
Proof.
  introv E. do 2 unfolds in E.
  forwards El: typed_environment_read_write_variables_length E. rew_list in El.
  repeat rewrite length_map in El. inverts~ El.
Qed.

(** The domain of the initial environment is exactly [X_sigma] and each
  term variables associated with the current constructor [c]. **)
Lemma abstract_initial_state_domain : forall c sigma ts Sigma,
  abstract_initial_state c sigma ts = Some Sigma ->
  typed_environment_domain abstract_value_term abstract_value_flow Sigma =
    \{ _X_ X_sigma }
    \u set_st (fun x => exists x0, x = X_t x0 /\ Mem x0 (skeleton_term_variables (S c))).
Proof.
  introv E. do 2 unfolds in E. rewrites~ >> typed_environment_domain_write_variables E.
  rewrites~ typed_environment_domain_empty; try typeclass.
  rewrite for_set_union_empty_l. rewrite~ to_set_cons. fequals.
  rewrite set_ext_eq. intro x. unfolds to_set. repeat rewrite in_set_st_eq.
  rewrite Mem_mem. rewrite map_mem. iff (x'&A&B); exists x'; rewrite Mem_mem in *; splits~.
Qed.

(** The following two lemmas specify what is the value mapped from particular
  variables in the initial environment. **)

Lemma abstract_initial_state_X_sigma : forall c sigma ts Sigma,
  abstract_initial_state c sigma ts = Some Sigma ->
  typed_environment_read_flow Sigma X_sigma = Some sigma.
Proof.
  introv E. unfolds in E. simpl in E.
  gen Sigma. generalize (typed_environment_empty : abstract_state c) as Gamma0,
    (skeleton_term_variables (S c)) as l2.
  induction ts using list_ind_last; introv E.
  - destruct l2 as [|t l2]; inverts E. apply~ typed_environment_read_write_flow_eq.
  - forwards [El2|(t&l2'&El2)]: last_case l2; rewrite El2 in E; rew_map in E.
    + destruct map in E; inverts* E.
    + unfolds in E.
      destruct (typed_environment_write_variables
        (typed_environment_write_flow Gamma0 X_sigma sigma)
        (map (X_t (variable_flow:=skeleton_variable_flow (S c))) l2')
        (map (id : abstract_closed_term_free -> abstract_value) ts)) eqn: E1.
      * rewrites >> typed_environment_write_variables_last in E; [ apply E1 |].
        forwards~ E2: IHts E1. inverts E. rewrite~ typed_environment_read_flow_write_term.
      * rewrites >> typed_environment_write_variables_last_None in E; [ apply E1 |]. inverts E.
Qed.

Lemma abstract_initial_state_X_t : forall c sigma ts Sigma x sx n,
  No_duplicates (skeleton_term_variables (S c)) ->
  abstract_initial_state c sigma ts = Some Sigma ->
  Nth n (skeleton_term_variables (S c)) x ->
  Nth n ts sx ->
  typed_environment_read_term Sigma x = Some (sx : abstract_term).
Proof.
  introv ND E N1 N2. do 2 unfolds in E. forwards~ E': typed_environment_read_write_variables_Nth E.
  - constructors.
    + introv M. forwards (t&M'&E'): Mem_map_inv (rm M). inverts* E'.
    + apply~ Nth_No_duplicates. introv N1' N2'.
      forwards (t1&N1''&E1'): map_Nth_inv (rm N1'). forwards (t2&N2''&E2'): map_Nth_inv (rm N2').
      substs. inverts E2'. applys~ No_duplicates_Nth ND N1'' N2''.
  - apply~ Nth_next. applys map_Nth N1.
  - apply~ Nth_next. applys map_Nth N2.
  - simpl in E'. destruct typed_environment_read_term; inverts E'. fequals~.
Qed.

(** The abstract immediate consequence builds new triples from a set of triples. **)
(** This definition corresponds to Figure 11 of the paper. **)
Inductive abstract_immediate_consequence (T : abstract_triple_set) : abstract_triple_set :=
  | abstract_immediate_consequence_intro : forall c V sigma t v ts s (Sigma Sigma' : abstract_state c) f,
    closed_term_free_eq t (closed_term_free_constructor V c ts) ->
    sort_of_term_closed_abstract t (sort_program s) ->
    sort_of_abstract_value (abstract_value_flow sigma) (in_sort s) ->
    abstract_initial_state c sigma ts = Some Sigma ->
    interpretation_skeleton (abstract c) (skeleton_skeleton (S c)) (true, Sigma, T) (f, Sigma') ->
    typed_environment_read_variable Sigma' (_X_ X_o) = Some v ->
    abstract_immediate_consequence T sigma t v
  .

(** The immediate consequence is compatible with the equality over closed terms. **)
Lemma abstract_immediate_consequence_closed_term_free_eq : forall T sigma t1 t2 v,
  abstract_immediate_consequence T sigma t1 v ->
  closed_term_free_eq t1 t2 ->
  abstract_immediate_consequence T sigma t2 v.
Proof.
  introv H E. inverts H as E1 S1 S2 E2 D E3. constructors*.
  - applys~ closed_term_free_eq_trans E1. applys~ closed_term_free_eq_sym E.
  - applys~ sort_of_term_closed_eq S1 E.
Qed.

(** This lemma states that the abstract immediate consequence,
  seen as a function from triple sets to triple sets, is monotonic. **)
(** It corresponds to Lemma 5.2 of the paper. **)
Lemma abstract_immediate_consequence_weaken : forall (T1 T2 : abstract_triple_set) sigma t v,
  (forall sigma t v, T1 sigma t v -> T2 sigma t v) ->
  abstract_immediate_consequence T1 sigma t v ->
  abstract_immediate_consequence T2 sigma t v.
Proof.
  introv I D. inverts D as E1 S1 S2 E2 D E3.
  applys~ @abstract_immediate_consequence_intro f E1 S1 S2 E2; [| apply E3 ].
  gen D. generalize (skeleton_skeleton (S c)). clear - I. intro s'.
  eapply (skeleton_ind' (fun b => forall f f' Sigma Sigma',
      interpretation_bone (abstract c) b (f, Sigma, T1) (f', Sigma', T1) ->
      interpretation_bone (abstract c) b (f, Sigma, T2) (f', Sigma', T2))
    (Q := fun s => forall f f' Sigma Sigma',
      _ s (f, Sigma, T1) (f', Sigma') -> (_ s (f, Sigma, T2) (f', Sigma') : Prop))).
  - introv D. inverts D as D; tryfalse. constructors~. inverts D as D H; tryfalse.
    + applys~ triple_based_interpretation_hook_interpretation_pick.
      * applys~ I D.
      * autos*.
    + applys~ triple_based_interpretation_hook_interpretation_shortcut D.
  - introv D. repeat (inverts D as D; constructors~).
  - introv F D. inverts D as F' D. constructors~.
    + forwards Fc: Forall2_Forall (rm F) (Forall2_swap (rm F')).
      applys Forall2_weaken (Forall2_swap (rm Fc)).
      introv (I1&I2) E. destruct Oiv. applys~ I1 I2.
    + inverts D as D. constructors~.
  - introv D. repeat (inverts D as D; constructors~).
  - introv IH F D. inverts D as D1 D2. destruct Sigma'1 as [[f1' Sigma1'] T1'].
    lets: (triple_based_interpretation_transmit_set D1). substs. constructors.
    + applys~ IH D1.
    + applys~ F D2.
Qed.

(** The abstract and well-formedness initial environments are consistent. **)
Lemma WFabstractOKout_initial : forall c s ss Gamma sigma ts Sigma D,
  wellformedness_initial_environment in_sort S c s ss = Some Gamma ->
  abstract_initial_state c sigma ts = Some Sigma ->
  No_duplicates (skeleton_term_variables (S c)) ->
  sort_of_abstract_value sigma (in_sort s) ->
  Forall2 sort_of_term_closed_abstract ts ss ->
  WFabstractOKout (Gamma, D) (true, Sigma).
Proof.
  introv E1 E2 ND VS F.
  asserts ED: (typed_environment_domain sort_term sort_flow Gamma
               = typed_environment_domain abstract_value_term abstract_value_flow Sigma).
  { unfold term_sort, sort, sort_term, sort_flow. unfolds in E1.
    rewrites~ >> wellformedness_initial_environment_domain E1; [ apply~ term_sort_Inhab |].
    rewrites* >> abstract_initial_state_domain E2. }
  splits~. introv E3 E4.
  asserts I: (x \in typed_environment_domain abstract_value_term abstract_value_flow Sigma).
  { unfolds typed_environment_domain. rewrite in_set_st_eq. rewrite E3. discriminate. }
  rewrites~ >> abstract_initial_state_domain E2 in I. rewrite in_union_eq in I. inverts I as I.
  - rewrite in_single_eq in I. substs.
    simpl in E3. rewrites~ >> abstract_initial_state_X_sigma E2 in E3. inverts E3.
    simpl in E4. unfolds in E1. unfolds term_sort, sort, sort_term, sort_flow.
    rewrites~ >> wellformedness_initial_environment_X_sigma E1 in E4. inverts~ E4.
  - rewrite in_set_st_eq in I. lets (xt&Ext&M): (rm I). lets (n&N): Mem_Nth (rm M). substs.
    forwards (t&Nt): length_Nth_lt.
    { rewrites~ <- >> abstract_initial_state_defined_inv E2. applys~ Nth_lt_length N. }
    forwards (s'&Ns'): length_Nth_lt.
    { rewrites~ <- >> wellformedness_initial_environment_defined_inv E1. applys~ Nth_lt_length N. }
    forwards~ E5: abstract_initial_state_X_t ND E2 N Nt.
    unfolds in E3. rewrite E5 in E3. inverts E3.
    forwards~ E6: wellformedness_initial_environment_X_t ND E1 N Ns'.
    unfolds in E4. unfolds term_sort. rewrite E6 in E4. inverts E4.
    do 2 constructors. rewrite Forall2_iff_forall_Nth in F. applys~ F; [ apply Nt | apply Ns' ].
Qed.

(** This lemma states that the immediate consequence preserves the well-formedness of triple sets. **)
(** It corresponds to Lemma 5.3 of the paper. **)
Lemma abstract_immediate_consequence_wellformed_set_of_triples : forall (T : abstract_triple_set),
  wellformed_set_of_triples T ->
  wellformed_set_of_triples (abstract_immediate_consequence T).
Proof.
  introv OK IC. inverts IC as E1 S1 S2 E2 D E3. forwards (ts'&E4): closed_term_free_constructor_inv E1.
  forwards (ts2&Ets2&Fts2): closed_term_free_to_variable_term_closed_term_free_constructor E4.
  do 3 eexists. splits*.
  forwards~ (ND&ss&Gamma&Gamma'&D1&D2&E5&F1&E6&D'&E7):
    skeletal_semantics_wellformed_instantiation S_wellformed c ts2.
  { applys sort_of_term_same_structure (fun b1 b2 : abstract_basic_term => b1 = b2) S1.
    - introv ?. substs~.
    - autos~.
    - rewrite E4. constructors~. apply~ Fts2. }
  forwards (E8&F2): WFabstract_universally_consistent D' D.
  { splits~. applys~ WFabstractOKout_initial E5 E2 ND S2.
    rewrite Forall2_iff_forall_Nth in F1. lets (El&F1'): (rm F1).
    asserts Fts: (Forall2 (fun t =>
      same_structure (fun _ _ => False) (fun b1 b2 : abstract_basic_term => b1 = b2)
                     (closed_term_free_term t)) ts ts2).
    { clear - E1 Ets2. destruct t as [X t C].
      forwards (ts'&Ets'): closed_term_free_constructor_inv E1.
      forwards (ts3&Ets3&F): closed_term_free_to_variable_term_closed_term_free_constructor Ets'.
      rewrite Ets3 in Ets2. simpls. inverts Ets2.
      unfolds in E1. simpl in E1. substs. inverts E1 as F2.
      rewrite Forall2_iff_forall_Nth in F. lets (El&F'): (rm F).
      rewrite Forall2_iff_forall_Nth in F2. lets (El2&F2'): (rm F2).
      rewrite Forall2_iff_forall_Nth. splits~.
      - rewrite <- El. rewrite El2. rewrite~ length_map.
      - introv N1 N2. forwards (t'&N3): length_Nth_lt.
        { rewrite El. applys~ Nth_lt_length N2. }
        forwards S1: F' N3 N2. forwards S2: F2' N3.
        { applys~ map_Nth N1. }
        applys~ same_structure_trans (fun (_ : closed_term_free_variable_term v1) (_ : X) => False)
                                     (fun b1 b2 : abstract_basic_term => b1 = b2) S1.
        { simpl. intros. substs~. }
        apply same_structure_swap_sym in S2.
        forwards~ S3: same_structure_True_closed_False closed_term_free_to_variable_term_closed S2.
        applys~ same_structure_trans closed_term_free_to_variable_term_spec S3.
        simpl. intros. substs~. }
    rewrite Forall2_iff_forall_Nth in Fts. lets (El'&Fts'): (rm Fts).
    rewrite Forall2_iff_forall_Nth. splits~.
    - unfolds term_sort. rewrite <- El. apply~ El'.
    - introv N1 N2. forwards (t'&N3): length_Nth_lt.
      { rewrite <- El'. applys~ Nth_lt_length N1. }
      forwards Ss: F1' N3 N2. forwards St: Fts' N1 N3.
      apply same_structure_swap_sym in St.
      applys~ sort_of_term_same_structure Ss St.
      simpl. intros. substs~. }
  unfolds typed_environment_read_variable. applys~ F2 E3 E7.
Qed.

(** The abstract semantics recursively applies the immediate consequence.
  It is defined as the greatest fixed point of the abstract immediate consequence
  restricted to well-formed sets of triples.
  This definition is equivalent to the union of all of these set of triples [T]. **)
(** It corresponds to Lemma 5.4 of the paper. **)
Inductive abstract_semantics : abstract_triple_set :=
  | abstract_semantics_intro : forall (T : abstract_triple_set) sigma t o,
    (forall sigma t o, T sigma t o -> abstract_immediate_consequence T sigma t o) ->
    wellformed_set_of_triples T ->
    T sigma t o ->
    abstract_semantics sigma t o.

(** This lemma states that the abstract semantics is well-formed. **)
(** It corresponds to Lemma 5.5 of the paper. **)
Lemma abstract_semantics_wellformed : wellformed_set_of_triples abstract_semantics.
Proof. introv I. inverts I as E I. apply~ I. Qed.

Lemma abstract_semantics_fixpoint_backwards : forall sigma t o,
  abstract_semantics sigma t o ->
  abstract_immediate_consequence abstract_semantics sigma t o.
Proof.
  introv D. inverts D as I W D. applys~ abstract_immediate_consequence_weaken I.
  introv D'. constructors~.
Qed.

Lemma abstract_semantics_fixpoint_forwards : forall sigma t o,
  abstract_immediate_consequence abstract_semantics sigma t o ->
  abstract_semantics sigma t o.
Proof.
  introv D. applys~ abstract_semantics_intro D.
  - introv HD. applys~ abstract_immediate_consequence_weaken abstract_semantics_fixpoint_backwards.
  - applys abstract_immediate_consequence_wellformed_set_of_triples abstract_semantics_wellformed.
Qed.

(** The abstract semantics is a fixpoint of the abstract immediate consequence. **)
Lemma abstract_semantics_fixpoint :
  abstract_immediate_consequence abstract_semantics = abstract_semantics.
Proof.
  apply func_ext_dep. introv. extens. introv. iff C.
  - applys~ abstract_semantics_fixpoint_forwards C.
  - applys~ abstract_semantics_fixpoint_backwards C.
Qed.

(** Symmetrically than the concrete case, we can define an alternative
  representation of the abstract semantics. **)
Definition abstract_semantics_alternative : abstract_triple_set := fun sigma t o =>
  forall n,
    applyn n abstract_immediate_consequence (fun _ _ _ => True) sigma t o.

Lemma abstract_semantics_alternative_forwards : forall sigma t o,
  abstract_semantics sigma t o ->
  abstract_semantics_alternative sigma t o.
Proof.
  introv D. inverts D as I W D. introv. gen sigma t o. induction n; introv D.
  - simpl. autos~.
  - rewrite applyn_fix. rewrite applyn_altern. applys~ abstract_immediate_consequence_weaken IHn.
Qed.

Lemma iter_abstract_immediate_consequence_weaken_immediate : forall n sigma t o,
  applyn (1 + n) abstract_immediate_consequence (fun _ _ _ => True) sigma t o ->
  applyn n abstract_immediate_consequence (fun _ _ _ => True) sigma t o.
Proof.
  induction n; introv D.
  - simpl. autos~.
  - rewrite applyn_fix in *. rewrite applyn_altern in *.
    applys~ abstract_immediate_consequence_weaken D.
Qed.

Lemma iter_abstract_immediate_consequence_weaken : forall n m sigma t o,
  n <= m ->
  applyn m abstract_immediate_consequence (fun _ _ _ => True) sigma t o ->
  applyn n abstract_immediate_consequence (fun _ _ _ => True) sigma t o.
Proof.
  introv I D. gen sigma t o. induction~ I; introv D.
  apply iter_abstract_immediate_consequence_weaken_immediate in D. applys~ IHI D.
Qed.

Lemma abstract_immediate_consequence_only_use_finite_number_of_triples : forall T sigma t o,
  abstract_immediate_consequence T sigma t o ->
  exists L,
    (forall sigma t o, list_to_triple_set L sigma t o -> T sigma t o)
    /\ abstract_immediate_consequence (list_to_triple_set L) sigma t o.
Proof.
  introv C. inverts C as C St Sv E1 D E2.
  forwards (L&I&D'): triple_based_interpretation_only_use_finite_number_of_triples_skeleton
                       (abstract c) D.
  exists L. splits~. constructors*.
Qed.

(* FIXME
Lemma abstract_semantics_alternative_increase : forall sigma t o,
  abstract_semantics_alternative sigma t o ->
  abstract_immediate_consequence abstract_semantics_alternative sigma t o.
Proof.
  introv C. unfolds abstract_semantics_alternative.
  forwards C1: C 1. inverts C1.
  constructors*.
  forwards (L&I&D): abstract_immediate_consequence_only_use_finite_number_of_triples (rm C).
  asserts (n&I'): (exists n, forall sigma t o,
    list_to_triple_set L sigma t o -> iter immediate_consequence n (fun _ _ _ => False) sigma t o).
  { clear D sigma t o. induction L as [|[[sigma t] o] L].
    - exists 0. introv M. inverts~ M.
    - lets (n&I'): (rm IHL).
      + introv M. apply~ I. applys~ Mem_next M.
      + forwards (n'&I''): I Mem_here. exists (max n n'). introv M. inverts M as M.
        * applys~ iter_immediate_consequence_weaken I''. apply~ PeanoNat.Nat.le_max_r.
        * applys~ iter_immediate_consequence_weaken I'. apply~ PeanoNat.Nat.le_max_l. }
  eexists. rewrite iter_S_l. applys~ immediate_consequence_weaken I' D.
Qed.

Lemma abstract_semantics_alternative_backwards : forall sigma t o,
  abstract_semantics_alternative sigma t o ->
  abstract_semantics sigma t o.
Proof.
  introv C. applys~ abstract_semantics_intro abstract_semantics_alternative_increase C.
  skip. (* TODO *)
Qed.

(** This lemma states that both abstract semantics are the same. **)
Lemma abstract_semantics_alternative_abstract_semantics :
  abstract_semantics = abstract_semantics_alternative.
Proof.
  apply func_ext_dep. introv. extens. introv. iff C.
  - applys~ abstract_semantics_alternative_forwards C.
  - applys~ abstract_semantics_alternative_backwards C.
Qed.
*)

(* TODO: Explain why an inductive definition would not fit. *)

End Skeleton.


(** ** Consistency of Concrete and Abstract Interpretations **)

Section Concretisation.

(** We assume a concretisation function for basic terms
  ([gamma_basic_term]) and for flow values ([gamma_flow]).
  The other associated concretisation functions can be
  inferred from these two functions. **)

Variable basic_term flow_value : Type.

Variable value_flow_sort : flow_value -> flow_sort -> Prop.

Hypothesis basic_term_Comparable : Comparable basic_term.
Hypothesis flow_value_Comparable : Comparable flow_value.

Hypothesis flow_value_Inhab : Inhab flow_value.

Variable basic_term_sort : basic_term -> base_sort -> Prop.

Hypothesis basic_term_sort_unique : forall b s1 s2,
  basic_term_sort b s1 ->
  basic_term_sort b s2 ->
  s1 = s2.

Let closed_term_free : Type := closed_term_free constructor basic_term.
Let sort_of_term_closed : closed_term_free -> term_sort -> Prop :=
  sort_of_term_closed constructor_signature basic_term_sort.

Let value : Type := value constructor basic_term flow_value.
Let value_term : closed_term_free -> value := value_term _.
Let value_flow : flow_value -> value := @value_flow _ _ _.
Coercion value_term : closed_term_free >-> value.
Coercion value_flow : flow_value >-> value.
Let value_sort : value -> sort -> Prop :=
  value_sort constructor_signature basic_term_sort value_flow_sort.

Variable gamma_basic_term : abstract_basic_term -> set basic_term.

(** We extend this concretisation function to closed terms. **)
Definition gamma_closed_term_free : abstract_closed_term_free -> set closed_term_free := fun t t' =>
  same_structure (fun _ _ => False) gamma_basic_term
     (closed_term_free_term t) (closed_term_free_term t').

Lemma gamma_closed_term_free_eq : forall ta1 ta2 t1 t2,
  closed_term_free_eq ta1 ta2 ->
  closed_term_free_eq t1 t2 ->
  gamma_closed_term_free ta1 t1 ->
  gamma_closed_term_free ta2 t2.
Proof.
  introv Ea E G. applys same_structure_trans E.
  - simpl. introv ? A ?. apply A.
  - simpl. introv Eb ?. substs. apply Eb.
  - applys same_structure_trans (fun b1 b2 : abstract_basic_term => b1 = b2) G.
    + simpl. intros. false~.
    + simpl. intros. substs~.
    + applys same_structure_sym Ea.
      * simpl. introv I. apply I.
      * simpl. intros. substs~.
Qed.

Definition gamma_term t : set closed_term_free :=
  match t with
  | abstract_term_closed t => gamma_closed_term_free t
  | abstract_term_bottom _ => fun _ => False
  end.

Variable gamma_flow : abstract_flow_value -> set flow_value.

Hypothesis gamma_flow_bottom : forall s v,
  ~ gamma_flow (bottom_flow s) v.

Definition gamma_value v : set value :=
  match v with
  | abstract_value_term t => map_set value_term (gamma_term t)
  | abstract_value_flow v => map_set value_flow (gamma_flow v)
  end.

(** Concretisation functions are supposed to be compatible with their
  associated order. **)

Hypothesis gamma_flow_compatible_with_order : forall v1 v2,
  v1 ⊑ v2 ->
  gamma_flow v1 \c gamma_flow v2.

Lemma gamma_term_compatible_with_order : forall t1 t2,
  t1 ⊑ t2 ->
  gamma_term t1 \c gamma_term t2.
Proof.
  introv O. apply~ @incl_prove; try typeclass.
  introv I. destruct t1 as [t|]; tryfalse. inverts~ O.
Qed.

Lemma gamma_value_compatible_with_order : forall v1 v2,
  v1 ⊑ v2 ->
  gamma_value v1 \c gamma_value v2.
Proof.
  introv O. inverts O as O.
  - apply incl_map_set. applys~ gamma_term_compatible_with_order O.
  - apply incl_map_set. applys~ gamma_flow_compatible_with_order O.
Qed.

(** Concretisation functions should also be compatible with sorts. **)

Hypothesis gamma_basic_term_compatible_with_sort : forall b b',
  gamma_basic_term b b' ->
  abstract_basic_term_sort b = basic_term_sort b'.

Lemma gamma_term_compatible_with_sort : forall t t' s,
  gamma_term t t' ->
  sort_of_abstract_term t s ->
  sort_of_term_closed t' s.
Proof.
  introv G S. inverts S as S; simpl in G; tryfalse.
  applys sort_of_term_same_structure S G.
  - introv G'. applys gamma_basic_term_compatible_with_sort G'.
  - simpl. autos~.
Qed.

Hypothesis gamma_flow_compatible_with_sort : forall v v',
  gamma_flow v v' ->
  abstract_value_flow_sort v = value_flow_sort v'.

Lemma gamma_value_compatible_with_sort : forall v v' s,
  gamma_value v v' ->
  sort_of_abstract_value v s ->
  value_sort v' s.
Proof.
  introv G S. inverts S as S; inverts G as G.
  - constructors. applys gamma_term_compatible_with_sort G S.
  - constructors. rewrites~ <- >> gamma_flow_compatible_with_sort G.
Qed.

(** The concrete and abstract filter interpretations are supposed
  consistent with each other. **)

Variable concrete_filter_interpretation : filter -> list value -> list value -> Prop.

Hypothesis concrete_filter_interpretation_consistent_with_sort : forall f vs1 vss1 vs2 vss2,
  concrete_filter_interpretation f vs1 vs2 ->
  filter_signature f = (vss1, vss2) ->
  Forall2 value_sort vs1 vss1 ->
  Forall2 value_sort vs2 vss2.

Hypothesis filter_interpretations_consistent : forall f vs vs' avs avs',
  concrete_filter_interpretation f vs vs' ->
  abstract_filter_interpretation f avs (Some avs') ->
  Forall2 gamma_value avs vs ->
  Forall2 gamma_value avs' vs'.

Hypothesis filter_interpretations_consistent_None : forall f vs vs' avs,
  Forall2 gamma_value avs vs ->
  concrete_filter_interpretation f vs vs' ->
  ~ abstract_filter_interpretation f avs None.

Let concrete_triple_set : Type :=
  concrete_triple_set constructor basic_term flow_value.

Let wellformed_abstract_triple_set
     : abstract_triple_set -> Prop :=
  wellformed_set_of_triples in_sort out_sort constructor_signature
                             abstract_basic_term_sort
                             sort_of_abstract_value sort_of_abstract_value.
Let wellformed_concrete_triple_set
     : concrete_triple_set -> Prop :=
  wellformed_set_of_triples in_sort out_sort constructor_signature
                             basic_term_sort value_sort value_sort.

Section Variables.

Variables variable_term variable_flow : Type.

Hypothesis variable_term_Comparable : Comparable variable_term.
Hypothesis variable_flow_Comparable : Comparable variable_flow.

Let variable : Type := variable variable_term variable_flow.
Let variable_flow_extended : Type := variable_flow_extended variable_flow.
Let term : Type := term constructor abstract_basic_term variable_term.
Let bone : Type := bone constructor filter variable_term variable_flow.
Let skeleton : Type := skeleton constructor filter variable_term variable_flow.
Let basic_free_term : Type := basic_free_term constructor variable_term.

Local Instance : Comparable variable_flow_extended :=
  variable_flow_extended_Comparable _.

Let concrete_state : Type :=
  concrete_state constructor basic_term flow_value variable_term variable_flow.
Let concrete_environment_domain : concrete_state -> set variable :=
  typed_environment_domain value_term value_flow (variable_flow := _).
Let abstract_state : Type :=
  abstract_state variable_term variable_flow.
Let abstract_environment_domain : abstract_state -> set variable :=
  typed_environment_domain abstract_value_term abstract_value_flow (variable_flow := _).

Let concrete : triple_based_interpretation constructor filter basic_term variable_term variable_flow :=
  concrete concrete_filter_interpretation _ _.
Let abstract : triple_based_interpretation constructor filter abstract_basic_term
                                           variable_term variable_flow :=
  abstract _ _.


(** This lemma states that [gamma_term] is compatible with substitution. **)
(** It corresponds to Lemma 5.6 of the paper. **)
Lemma gamma_term_same_structure_subst_option : forall (Sigma : concrete_state) (aSigma : abstract_state)
    t tc ta,
  (forall x tc ta,
     x \in term_free_variable (basic_free_term_term t) ->
     typed_environment_read_term Sigma x = Some tc ->
     typed_environment_read_term aSigma x = Some ta ->
     gamma_term ta tc) ->
  read_closed_term_from_concrete_state_from_basic_free_term _ Sigma t tc ->
  read_term_from_abstract_state_from_basic_free_term _ _ aSigma t ta ->
  gamma_term ta tc.
Proof.
  introv I R1 R2. inverts R2 as R2 E S.
  - simpl. lets (t0&R1'&E): (rm R1). unfolds in E. simpls. unfolds.
    forwards E': same_structure_True_closed_False (rm E).
    { applys~ read_term_from_concrete_state_closed R1'. }
    forwards R1'': @same_structure_subst_option_same_structure
                     (read_term_from_concrete_state_direct _ (closed_term_free_variable_term tc) Sigma)
                     R1' E'.
    + introv FV E. unfolds. unfolds in E. destruct typed_environment_read_term; inverts E as E.
      simpl. fequals. apply closed_term_free_to_variable_term_closed_term_free_basic in E.
      apply closed_term_free_to_variable_term_closed_term_free_basic. rewrite~ E.
    + introv FV F E. unfolds. unfolds in E. destruct typed_environment_read_term; inverts E as E.
      simpl. fequals. eapply same_structure_refl_inv_closed.
      * applys~ same_structure_trans closed_term_free_to_variable_term_same_structure;
          [| | rewrite E; constructors~; apply~ F ].
        -- simpl. introv ? ? A. apply A.
        -- simpl. intros. substs~.
      * apply~ closed_term_free_to_variable_term_closed.
    + applys same_structure_same_structure_subst_option_unique R2 R1''.
      applys~ same_structure_weaken basic_free_term_to_basic_term_same_structure.
      simpl. introv FV ? FV1 FV2 E1 E2. substs. unfolds in E1. unfolds in E2.
      destruct (typed_environment_read_term aSigma) as [[t3|]|] eqn: E3; tryfalse.
      destruct (typed_environment_read_term Sigma) eqn: E4; tryfalse.
      inverts E2. inverts E1. forwards~ G: I E4 E3.
      { applys~ term_free_variable_same_structure FV.
        applys~ same_structure_sym
              (fun (_ : basic_term) (_ : basic_free_term_basic_term t) => False)
              basic_free_term_to_basic_term_spec;
          simpl; intros; tryfalse; substs~. }
      applys same_structure_trans closed_term_free_to_variable_term_spec.
      * simpl. introv ? A ?. apply A.
      * simpl. introv G' ?. substs. apply G'.
      * applys same_structure_trans (fun b1 b2 : abstract_basic_term => b1 = b2) G.
        -- simpl. introv ? A ?. apply A.
        -- intros. substs~.
        -- applys same_structure_sym closed_term_free_to_variable_term_spec;
             simpl; intros; tryfalse; substs~.
  - forwards D: read_term_from_concrete_state_closed_term_free_variable R1.
    rewrite incl_in_eq in D. forwards Dx: D.
    { apply in_map_set. applys term_free_variable_same_structure R2.
      apply~ basic_free_term_to_basic_term_spec. }
    unfolds concrete_environment_domain, typed_environment_domain. rewrite in_set_st_eq in Dx.
    simpl in Dx. unfolds closed_term_free.
    destruct (typed_environment_read_term Sigma x) eqn: E'; tryfalse~. false~ I E' E.
Qed.

(** This definition corresponds to Definition 5.7 of the paper. **)
Definition gamma_triple_set (aT : abstract_triple_set) (T : concrete_triple_set) :=
  forall sigmaa (ta : abstract_closed_term_free) va sigma t v,
    T sigma t v ->
    aT sigmaa ta va ->
    gamma_value sigmaa sigma ->
    gamma_term ta t ->
    gamma_value va v.

(** The concrete and abstract environments are in sync when the abstract
  did not return [⊥], when they have the same domain, and when each
  variable is associated related values in both environments. **)
Definition concreteabstractOKout (Sigma : interpretation_result concrete)
                                 (fSigmaa : interpretation_result abstract) :=
  let (f, Sigmaa) := fSigmaa in
  f = true
  /\ typed_environment_domain value_term value_flow Sigma
     = typed_environment_domain abstract_value_term abstract_value_flow Sigmaa
  /\ forall x va v,
       typed_environment_read_variable abstract_value_term abstract_value_flow Sigmaa x = Some va ->
       typed_environment_read_variable value_term value_flow Sigma x = Some v ->
       gamma_value va v.

(** We also require the triple set to be well-formed and consistent. **)
Definition concreteabstractOKst (SigmaT : interpretation_state concrete)
                                (fSigmaaTa : interpretation_state abstract) :=
  let (Sigma, T) := SigmaT in
  let (fSigmaa, Ta) := fSigmaaTa in
  wellformed_concrete_triple_set T
  /\ wellformed_abstract_triple_set Ta
  /\ gamma_triple_set Ta T
  /\ concreteabstractOKout Sigma fSigmaa.

(** This lemma states that the concrete and abstract interpretations
  are universally consistent. **)
(** It corresponds to Lemma 5.8 of the paper. **)
(** Again, note how this lemma is proven from [local_universal_consistency]. **)
Lemma concreteabstract_universally_consistent :
  universally_consistent concrete abstract concreteabstractOKst concreteabstractOKout.
Proof.
  apply local_universal_consistency.
  - intros (Sigma&T) ((f&Sigmaa)&Ta) (Sigma'&T') (f'&Sigmaa') (OKT&OKta&G&OK) D Da.
    inverts D. inverts Da. apply~ OK.
  - intros xf1 t xf2 (Sigma&T) ((f&Sigmaa)&Ta) (Sigma'&T') (f'&Sigmaa') (OKT&OKTa&E&OK) D Da.
    lets (?&Ed&Gf): (rm OK). substs. inverts D as Tt H; tryfalse.
    inverts H as R' R. lets (t'&Rc&Et'): (rm R'). inverts Da as Tta Ha.
    + inverts Ha as R1 O1 R2 O2 O3.
      * forwards~ G: E Tt Tta.
        -- forwards G: Gf (_X_ xf1 : variable).
           ++ simpl. rewrite R2. reflexivity.
           ++ simpl. unfolds closed_term_free. rewrite R. reflexivity.
           ++ forwards I: gamma_flow_compatible_with_order O2.
              constructors~. rewrite incl_in_eq in I. apply~ I. inverts~ G.
        -- forwards Gt: gamma_term_same_structure_subst_option R1;
             try solve [ repeat eexists; apply~ Et' ].
           { introv I E1 E2. forwards G: Gf (X_t x : variable).
             - simpl. rewrite E2. reflexivity.
             - simpl. unfolds closed_term_free. rewrite E1. reflexivity.
             - inverts~ G. }
           apply gamma_term_compatible_with_order in O1.
           erewrite @incl_in_eq in O1; try typeclass. applys~ O1 Gt.
        -- inverts~ G.
      * do 2 splits~.
        -- rewrite_typed_environment_domain. fequals~.
        -- introv E1 E2. tests Dx: (_X_ xf2 = x).
           ++ simpl in E2. rewrite typed_environment_read_write_flow_eq in E2. inverts E2.
              simpl in E1. rewrite typed_environment_read_write_flow_eq in E1. inverts E1.
              forwards~ G: E Tt Tta.
              ** forwards G: Gf (_X_ xf1 : variable).
                 --- simpl. rewrite R2. reflexivity.
                 --- simpl. unfolds closed_term_free. rewrite R. reflexivity.
                 --- forwards I: gamma_flow_compatible_with_order O2.
                     constructors~. rewrite incl_in_eq in I. apply~ I. inverts~ G.
              ** forwards Gt: gamma_term_same_structure_subst_option R1;
                   try solve [ repeat eexists; apply~ Et' ].
                 { introv I E1 E2. forwards G: Gf (X_t x : variable).
                   - simpl. rewrite E2. reflexivity.
                   - simpl. unfolds closed_term_free. rewrite E1. reflexivity.
                   - inverts~ G. }
                 apply gamma_term_compatible_with_order in O1.
                 erewrite @incl_in_eq in O1; try typeclass. applys~ O1 Gt.
              ** inverts G as G. constructors. forwards I: gamma_flow_compatible_with_order O3.
                 erewrite @incl_in_eq in I; try typeclass. applys~ I G.
           ++ applys~ Gf.
              ** rewrites~ <- >> typed_environment_read_write_variable_neq abstract_value_rect v2' Dx;
                   try apply~ E1.
                 reflexivity.
              ** rewrites~ <- >> typed_environment_read_write_variable_neq
                                    (@value_rect _ _ _ : forall (P : value -> _), _)
                                    (v2 : value) Dx;
                   try apply~ E2.
                 reflexivity.
    + inverts~ Tta.
  - intros f xs ys (Sigma&T) ((f0&Sigmaa)&Ta) (Sigma'&T') ((f'&Sigmaa')&Ta') (OKT&OKta&E&OK) D1 D2.
    lets (?&Ed&Gf): (rm OK). substs. inverts D1 as Tf. inverts Tf as F4 FI ESigma'.
    inverts D2 as Tf. inverts Tf as F5 F6 FIa ESigmaa' ESigmaa''.
    + false filter_interpretations_consistent_None FI FIa.
      rewrite Forall2_iff_forall_Nth in F6. lets (El6&F6'): (rm F6).
      rewrite Forall2_iff_forall_Nth in F5. lets (El5&F5'): (rm F5).
      rewrite Forall2_iff_forall_Nth in F4. lets (El4&F4'): (rm F4).
      rewrite Forall2_iff_forall_Nth. splits~.
      * rewrite <- El6. rewrite~ <- El5.
      * introv N1 N2. forwards (v0&N3): length_Nth_lt.
        { rewrite El6. applys~ Nth_lt_length N1. }
        forwards (x'&N4): length_Nth_lt.
        { rewrite El4. applys~ Nth_lt_length N2. }
        forwards O: F6' N3 N1. apply gamma_value_compatible_with_order in O.
        rewrite incl_in_eq in O. apply O.
        forwards E1': F4' N4 N2. forwards E2': F5' N4 N3.
        applys Gf E2' E1'.
    + do 2 splits~.
      * rewrites >> typed_environment_domain_write_variables; try apply ESigma'; try reflexivity.
        rewrites >> typed_environment_domain_write_variables ESigmaa'; try reflexivity. fequals~.
      * introv Ea Eb. tests I: (Mem x ys).
        -- forwards F: filter_interpretations_consistent FI FIa.
           { rewrite Forall2_iff_forall_Nth in F6. lets (El6&F6'): (rm F6).
             rewrite Forall2_iff_forall_Nth in F5. lets (El5&F5'): (rm F5).
             rewrite Forall2_iff_forall_Nth in F4. lets (El4&F4'): (rm F4).
             rewrite Forall2_iff_forall_Nth. splits~.
             - rewrite <- El6. rewrite~ <- El5.
             - introv N1 N2. forwards (v0&N3): length_Nth_lt.
               { rewrite El6. applys~ Nth_lt_length N1. }
               forwards (x'&N4): length_Nth_lt.
               { rewrite El4. applys~ Nth_lt_length N2. }
               forwards O: F6' N3 N1. apply gamma_value_compatible_with_order in O.
               rewrite incl_in_eq in O. apply O.
               forwards E1': F4' N4 N2. forwards E2': F5' N4 N3.
               applys Gf E2' E1'. }
           forwards~ G: typed_environment_write_variables_Forall2 ESigma' ESigmaa' I Eb Ea.
           { applys~ Forall2_swap F. }
           apply~ G.
        -- rewrites~ >> typed_environment_read_write_variables_not_in I ESigmaa' in Ea.
           unfolds value_term, value_flow, closed_term_free, value.
           rewrites~ >> typed_environment_read_write_variables_not_in I ESigma' in Eb.
           applys~ Gf Ea Eb.
  - intros V O1 O2 (Sigma&T) ((f&Sigmaa)&Ta) (Sigma'&T') ((f'&Sigmaa')&Ta') (OKT&OKTaE&G&OK) F' D1 D2.
    lets (?&Ed&Gf): (rm OK). substs. inverts D1 as Tm. inverts Tm as N IV ESigma'.
    inverts D2 as Tm. inverts Tm as FD NE F7 Na ESigmaa'.
    + false. rewrite Forall2_iff_forall_Nth in F'. lets (El&F2): (rm F').
      forwards (Oi&N'): length_Nth_lt.
      { rewrite <- El. applys~ Nth_lt_length N. }
      rewrite Forall_iff_forall_mem in NE. forwards (Sigmai'&E&I): NE.
      { applys~ Nth_mem N'. }
      substs. forwards~ OK: F2 N N'. lets (A&Ed'&Gf'): (rm OK). inverts~ A.
    + asserts (Sigmaai&Na): (exists Sigmaai, Nth n O2 (Some (true, Sigmaai))).
      { rewrite Forall2_iff_forall_Nth in F'. lets (El&F2): (rm F').
        forwards (Oi&N'): length_Nth_lt.
        { rewrite <- El. applys~ Nth_lt_length N. }
        destruct Oi as [[? ?]|].
        - forwards~ OK: F2 N N'. lets (A&Ed'&Gf'): (rm OK). substs. eexists. apply~ N'.
        - rewrite Forall_iff_forall_mem in FD. false~ FD. applys~ Nth_mem N'. }
      forwards (Id&ESigmaa'): F7.
      { rewrite in_set_st_eq. eexists. apply~ Na. }
      rewrite Forall2_iff_forall_Nth in F'. lets (El'&F2): (rm F'). forwards~ Hi: F2 N Na.
      lets (?&Edi&Gfi): (rm Hi). do 2 splits~.
      * rewrites~ >> typed_environment_domain_equivalent ESigma'; try solve [ introv E; inverts~ E ].
        rewrites~ >> typed_environment_domain_equivalent ESigmaa'; try solve [ introv E; inverts~ E ].
        rewrite_typed_environment_domain; try solve [ apply~ closed_term_free_Inhab | typeclass ].
        repeat fequals~.
      * introv Ea Eb.
        rewrites >> typed_environment_equivalent_spec abstract_value_term
                      abstract_value_flow in ESigmaa'; try solve [ introv I; inverts~ I ].
        rewrite ESigmaa' in Ea. rewrites >> typed_environment_equivalent_spec in ESigma';
          [ apply value_term_inj | apply value_flow_inj |].
        rewrites (rm ESigma') in Eb.
        asserts Edr: (typed_environment_domain value_term value_flow
                        (typed_environment_restrict Sigmai V)
                      = typed_environment_domain abstract_value_term abstract_value_flow
                          (typed_environment_restrict Sigmaai V)).
        { rewrite_typed_environment_domain;
            try solve [ apply~ closed_term_free_Inhab | typeclass ].
          rewrite~ Edi. }
        tests Dx: (x \in typed_environment_domain value_term value_flow
                           (typed_environment_restrict Sigmai V)).
        -- lets Dx': Dx. rewrite Edr in Dx'.
           unfold typed_environment_domain in Dx, Dx'. rewrite in_set_st_eq in Dx, Dx'.
           destruct (typed_environment_read_variable _ _ (typed_environment_restrict Sigmai V)) eqn: E1;
             try solve [ false~ Dx ].
           destruct (typed_environment_read_variable _ _ (typed_environment_restrict Sigmaai V)) eqn: E2;
             try solve [ false~ Dx' ].
          rewrites >> typed_environment_read_variable_merge_right_precise E1 in Eb; try typeclass.
          rewrites >> typed_environment_read_variable_merge_right_precise E2 in Ea; try typeclass.
          inverts Ea. inverts Eb. forwards~ Ix: typed_environment_read_restrict_implies_Mem E1;
            try solve [ apply~ closed_term_free_Inhab | typeclass ].
          applys~ Gfi.
          ++ rewrites >> typed_environment_read_restrict_Mem Ix in E2; try typeclass.
          ++ rewrites* >> typed_environment_read_restrict_Mem Ix in E1; try typeclass.
        -- forwards [Eb'|Eb']: typed_environment_read_merge (rm Eb).
           ++ forwards [Ea'|Ea']: typed_environment_read_merge (rm Ea).
              ** applys~ Gf Ea' Eb'.
              ** false Dx. rewrite Edr. unfold typed_environment_domain.
                 rewrite in_set_st_eq. rewrite Ea'. discriminate.
           ++ false Dx. unfold typed_environment_domain. rewrite in_set_st_eq.
             unfolds value_term, value_flow, closed_term_free. rewrite Eb'. discriminate.
Qed.

End Variables.

Section Skeleton.

Variable S : skeletal_semantics constructor filter.

Hypothesis skeleton_instance_term_Comparable : forall c, Comparable (skeleton_variable_term (S c)).
Hypothesis skeleton_instance_flow_Comparable : forall c, Comparable (skeleton_variable_flow (S c)).

Hypothesis S_wellformed :
  skeletal_semantics_wellformed in_sort out_sort filter_signature constructor_signature S _ _.

(** The concrete and abstract initial environments are consistent. **)
Lemma concreteabstractOKout_initial : forall c sigma ts Sigma sigmaa tsa Sigmaa,
  concrete_initial_state S c sigma ts = Some Sigma ->
  abstract_initial_state S c sigmaa tsa = Some Sigmaa ->
  No_duplicates (skeleton_term_variables (S c)) ->
  gamma_flow sigmaa sigma ->
  Forall2 gamma_closed_term_free tsa ts ->
  concreteabstractOKout Sigma (true, Sigmaa).
Proof.
  introv E1 E2 ND G F.
  asserts ED: (typed_environment_domain value_term value_flow Sigma
               = typed_environment_domain abstract_value_term abstract_value_flow Sigmaa).
  { unfold value_term, value_flow, closed_term_free, value.
    rewrites~ >> concrete_initial_state_domain E1.
    rewrites* >> abstract_initial_state_domain E2. }
  splits~. introv E3 E4.
  asserts I: (x \in typed_environment_domain abstract_value_term abstract_value_flow Sigmaa).
  { unfolds typed_environment_domain. rewrite in_set_st_eq. rewrite E3. discriminate. }
  rewrites~ >> abstract_initial_state_domain E2 in I. rewrite in_union_eq in I. inverts I as I.
  - rewrite in_single_eq in I. substs.
    simpl in E3. rewrites~ >> abstract_initial_state_X_sigma E2 in E3. inverts E3.
    simpl in E4. unfolds in E1. unfolds term_sort, sort, sort_term, sort_flow.
    unfolds value_term, value_flow, closed_term_free, value.
    rewrites~ >> concrete_initial_state_X_sigma E1 in E4. inverts~ E4. constructors~.
  - rewrite in_set_st_eq in I. lets (xt&Ext&M): (rm I). lets (n&N): Mem_Nth (rm M). substs.
    forwards (t&Nt): length_Nth_lt.
    { rewrites~ <- >> abstract_initial_state_defined_inv E2. applys~ Nth_lt_length N. }
    forwards (s'&Ns'): length_Nth_lt.
    { rewrites~ <- >> concrete_initial_state_defined_inv E1. applys~ Nth_lt_length N. }
    forwards~ E5: abstract_initial_state_X_t ND E2 N Nt.
    unfolds in E3. rewrite E5 in E3. inverts E3.
    forwards~ E6: concrete_initial_state_X_t ND E1 N Ns'.
    unfolds in E4. unfolds closed_term_free. rewrite E6 in E4. inverts E4.
    constructors~. rewrite Forall2_iff_forall_Nth in F. applys~ F; [ apply Nt | apply Ns' ].
Qed.

Let concrete_immediate_consequence : concrete_triple_set -> concrete_triple_set :=
  immediate_consequence concrete_filter_interpretation
                        in_sort constructor_signature basic_term_sort value_flow_sort S _ _.
Let abstract_immediate_consequence : abstract_triple_set -> abstract_triple_set :=
  abstract_immediate_consequence S _ _.

Let concrete_semantics : concrete_triple_set :=
  concrete_semantics concrete_filter_interpretation
                     in_sort constructor_signature basic_term_sort value_flow_sort S.
Let abstract_semantics : abstract_triple_set :=
  abstract_semantics S _ _.

(** The concrete and abstract immediate consequences conserve consistency. **)
(** This lemma corresponds to (the consistency part of) Lemma 5.9 of the paper.
  The well-formed part is already given by Lemmas 4.4 and 5.3, that is, by
  [immediate_consequence_wellformed_set_of_triples] and
  [abstract_immediate_consequence_wellformed_set_of_triples]. **)
Lemma immediate_consequences_consistent : forall T Ta,
  wellformed_concrete_triple_set T ->
  wellformed_abstract_triple_set Ta ->
  gamma_triple_set Ta T ->
  gamma_triple_set (abstract_immediate_consequence Ta) (concrete_immediate_consequence T).
Proof.
  introv W Wa G D Da Gs Gt. inverts D as C1 S1 S2 E1 D E2. inverts Da as C1a S1a S2a E1a Da E2a.
  forwards E: gamma_closed_term_free_eq C1a C1 Gt. inverts E as Fts.
  forwards (?&Ed&F): concreteabstract_universally_consistent D Da.
  { splits~. applys~ concreteabstractOKout_initial E1 E1a.
    - destruct (constructor_signature c) eqn: Ec. applys~ S_wellformed Ec.
    - inverts Gs as Gs. apply~ Gs.
    - rewrite Forall2_iff_forall_Nth in Fts. lets (El&F): (rm Fts).
      rewrite Forall2_iff_forall_Nth. repeat rewrite length_map in El. splits~.
      introv N1 N2. forwards E: F; try apply* map_Nth.
      applys same_structure_trans closed_term_free_to_variable_term_spec.
      + simpl. introv ? ? A. apply A.
      + simpl. introv ? Eb. substs. apply Eb.
      + applys same_structure_trans (fun b1 b2 : basic_term => b1 = b2) E.
        * simpl. introv ? ? A. apply A.
        * intros. substs~.
        * applys~ same_structure_sym closed_term_free_to_variable_term_spec.
          simpl. intros. substs~. }
  applys~ F E2a E2.
Qed.

(** An abstract triple set [T] is correct iff it is well-formed and consistent
  with the concrete semantics. **)
(** This definition corresponds to Definition 5.10 of the paper. **)
Definition correct_abstract_triple_set T :=
  wellformed_abstract_triple_set T
  /\ gamma_triple_set T concrete_semantics.

(** We can now how that the abstract semantics is correct. **)
(** This corresponds to Theorem 5.11 of the paper. **)
Theorem abstract_semantics_correct : correct_abstract_triple_set abstract_semantics.
Proof.
  splits.
  - apply~ abstract_semantics_wellformed.
  - introv C. unfolds in C. rewrite concrete_semantics_alternative_concrete_semantics in C.
    lets (n&D): (rm C). gen sigmaa ta va sigma t v. evar (G : Prop).
    asserts (GH&?): (G /\ wellformed_concrete_triple_set
                            (applyn n concrete_immediate_consequence (fun _ _ _ => False)));
      unfolds G; [ clear G | apply~ GH ].
    induction n; introv; (splits; [introv D A Gs Gt|]).
    + inverts~ D.
    + introv D. inverts~ D.
    + forwards (F&W): (rm IHn).
      applys~ immediate_consequences_consistent W abstract_semantics_wellformed Gs Gt.
      unfolds in A. rewrite <- abstract_semantics_fixpoint in A.
      * apply~ A.
      * apply~ S_wellformed.
    + forwards (?&W): IHn. rewrite applyn_fix. rewrite applyn_altern.
      applys~ immediate_consequence_wellformed_set_of_triples S_wellformed W.
Qed.

End Skeleton.

End Concretisation.


(** ** Miscellaneous **)

(** We defined [abstract_filter_interpretation] to be a relation,
  whilst it was defined as a function in the paper.  The following
  definition transforms a function to the expected shape. **)
Definition abstract_filter_interpretation_from_function fi
    : filter -> list abstract_value -> option (list abstract_value) -> Prop :=
  fun f vs vs' =>
    match fi f vs, vs' with
    | None, None => True
    | Some vs1, Some vs2 => Forall2 (fun v v' => v ⊑ v') vs1 vs2
    | _, _ => False
    end.

End Common.


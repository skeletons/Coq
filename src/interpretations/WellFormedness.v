(** Definition of the well-formedness interpretation. **)

Set Implicit Arguments.

Require Import LibHeap LibSet.
Require Export Environments Interpretations.

(** * The Well-formedness Interpretation **)

Section Sorts.

Variables constructor filter : Type.

(** ** Sorts **)

(** There are three kinds of sorts: base and program sorts
  are used for terms, and flow sorts for flow values (that
  is, intermediate values stored in flow variables). **)
Variable base_sort program_sort flow_sort : Type.

(** We have already seen the definition of term sorts in [Terms.v]. **)
Let term_sort : Type := term_sort base_sort program_sort.

Hypothesis term_sort_Inhab : Inhab term_sort.
Hypothesis flow_sort_Inhab : Inhab flow_sort.

(** We extend term sorts by flow sorts.
  This constitutes the sorts manipulated by the well-formedness interpretation. **)
Inductive sort :=
  | sort_term : term_sort -> sort
  | sort_flow : flow_sort -> sort
  .
Coercion sort_term : term_sort >-> sort.
Coercion sort_flow : flow_sort >-> sort.

Lemma term_sort_inj : forall s1 s2, (sort_term s1 : sort) = sort_term s2 -> s1 = s2.
Proof. introv E. inverts~ E. Qed.

Lemma flow_sort_inj : forall s1 s2, (sort_flow s1 : sort) = sort_flow s2 -> s1 = s2.
Proof. introv E. inverts~ E. Qed.

Variables in_sort out_sort : program_sort -> flow_sort.

(** Each filter is given a signature (written [fsort] in the paper). **)
Variable filter_signature : filter -> list sort * list sort.

Variable constructor_signature : constructor -> list term_sort * program_sort.

Section Variables.

Variable basic_term : Type.

Hypothesis basic_term_Comparable : Comparable basic_term.

Variable basic_term_sort : basic_term -> base_sort -> Prop.

Variables variable_term variable_flow : Type.

Hypothesis variable_term_Comparable : Comparable variable_term.
Hypothesis variable_flow_Comparable : Comparable variable_flow.

(** We reinstantiate what have been defined in the previous files
  with the current variables. **)

Let variable : Type := variable variable_term variable_flow.
Let variable_flow_extended : Type := variable_flow_extended variable_flow.
Let term : Type := term constructor basic_term variable_term.
Let bone : Type := bone constructor filter variable_term variable_flow.
Let skeleton : Type := skeleton constructor filter variable_term variable_flow.
Let basic_free_term : Type := basic_free_term constructor variable_term.
Let closed_term_free : Type := closed_term_free constructor basic_term.

Local Instance variable_flow_extended_Comparable : Comparable variable_flow_extended :=
  variable_flow_extended_Comparable variable_flow_Comparable.

Let sort_of_term : (variable_term -> term_sort -> Prop) -> term -> term_sort -> Prop :=
  sort_of_term constructor_signature basic_term_sort.

Let sort_of_term_basic_free :
    (variable_term -> term_sort -> Prop) -> basic_free_term -> term_sort -> Prop :=
  (sort_of_term_basic_free constructor_signature) _.

Let sort_of_term_closed : closed_term_free -> term_sort -> Prop :=
  sort_of_term_closed constructor_signature basic_term_sort.


(** Concrete environments associate variables to sorts.  There are however some constraints
  on how these sorts are distributed: term variables are only associated to term sorts and
  flow variables with flow sorts.
  We use the [typed_environment] definition from [aux/Environments.v] to properly satisfy
  these constraints. **)

Definition sorting_environment : Type :=
  typed_environment term_sort flow_sort variable_term variable_flow.

Let typed_environment_domain : sorting_environment -> set variable :=
  typed_environment_domain sort_term sort_flow (variable_flow := _).

Let typed_environment_read_variable : sorting_environment -> variable -> option sort :=
  typed_environment_read_variable sort_term sort_flow (variable_flow := _).

Let typed_environment_write_variables
    : sorting_environment -> list variable -> list sort -> option sorting_environment :=
  typed_environment_write_variables sort_term sort_flow sort_rect (variable_flow := _).


(** ** Well-formedness Interpretation **)

(** The well-formedness interpretation uses a state composed of a
  sorting environment and a set.  Morally, the set is the domain
  of the sorting environment.  In practice, it also stores
  variables that were local in branches, to avoid their reuse
  elsewhere.  This tends to simplify proofs. **)

(** These definitions correspond to Figure 8 of the paper. **)

Definition wellformedness_environment : Type := sorting_environment * set variable.

Inductive wellformedness_empty_skeleton :
    wellformedness_environment -> wellformedness_environment -> Prop :=
  | wellformedness_empty_skeleton_intro : forall Gamma1 Gamma2 D1 D2,
    typed_environment_equivalent Gamma1 Gamma2 ->
    D1 = D2 ->
    wellformedness_empty_skeleton (Gamma1, D1) (Gamma2, D2)
  .

Inductive wellformedness_hook :
    variable_flow_extended -> basic_free_term -> variable_flow_extended ->
    wellformedness_environment -> wellformedness_environment -> Prop :=
  | wellformedness_hook_intro : forall xf1 t xf2 s Gamma Gamma' D D',
    typed_environment_domain Gamma \c D ->
    D (_X_ xf1) ->
    (forall x,
        term_free_variable (basic_free_term_term t) x ->
        indom (typed_environment_term Gamma) x) ->
    sort_of_term_basic_free (read_to_set (typed_environment_read_term Gamma)) t (sort_program s) ->
    typed_environment_read_flow Gamma xf1 = Some (in_sort s) ->
    ~ D (_X_ xf2) ->
    typed_environment_equivalent Gamma' (typed_environment_write_flow Gamma xf2 (out_sort s)) ->
    D' = D \u \{ _X_ xf2 } ->
    wellformedness_hook xf1 t xf2 (Gamma, D) (Gamma', D')
  .

(** We do not check whether the type of variables and sorts match as in the paper
  as this constraint is guaranteed by type in the definition of sorting
  environments in Coq. **)
Inductive wellformedness_filter :
    filter -> list variable -> list variable ->
    wellformedness_environment -> wellformedness_environment -> Prop :=
  | wellformedness_filter_intro : forall f xs ys xss yss Gamma Gamma' D D',
    typed_environment_domain Gamma \c D ->
    Forall D xs ->
    No_duplicates ys ->
    Forall (fun y => ~ D y) ys ->
    Forall2 (fun x s => typed_environment_read_variable Gamma x = Some s) xs xss ->
    filter_signature f = (xss, yss) ->
    typed_environment_write_variables Gamma ys yss = Some Gamma' ->
    D' = D \u to_set ys ->
    wellformedness_filter f xs ys (Gamma, D) (Gamma', D')
  .

(** In contrary to the paper, indexes start from 0 and not 1. **)
Inductive wellformedness_merge :
    list (option wellformedness_environment) ->
    list variable ->
    wellformedness_environment -> wellformedness_environment -> Prop :=
  | wellformedness_merge_intro : forall O V Gamma Gamma' D D' Gammais Dis,
    length O >= 2 ->
    typed_environment_domain Gamma \c D ->
    Forall3 (fun Oi Gammai Di => Oi = Some (Gammai, Di)) O Gammais Dis ->
    Forall2 (fun Gammai Di => typed_environment_domain Gammai \c Di) Gammais Dis ->
    Forall (fun Di => D \c Di) Dis ->
    (forall i j Di Dj,
      i <> j ->
      Nth i Dis Di ->
      Nth j Dis Dj ->
      (Di \- D) \n (Dj \- D) = to_set V) ->
    Forall (fun Gammai =>
      typed_environment_equivalent Gamma'
        (typed_environment_merge Gamma (typed_environment_restrict Gammai V))) Gammais ->
    D' = set_big_union Dis ->
    wellformedness_merge O V (Gamma, D) (Gamma', D')
  .

(** We regroup these definitions into the well-formedness interpretation. **)
Definition wellformedness := {|
    interpretation_state := wellformedness_environment ;
    interpretation_result := wellformedness_environment ;
    interpretation_empty_skeleton := wellformedness_empty_skeleton ;
    interpretation_hook := wellformedness_hook ;
    interpretation_filter := wellformedness_filter ;
    interpretation_merge := wellformedness_merge
  |}.


(** ** Relation to Set of Triples **)

(** The triple-based interpretations often needs to specify that their
  triples are well-formed. **)

Section StateResultSort.

(** We first define the wellformedness of triple sets in general. **)

Variables state result : Type.

Variable state_sort : state -> flow_sort -> Prop.
Variable result_sort : result -> flow_sort -> Prop.

Definition triple_set :=
  state -> closed_term_free -> result -> Prop.

(** A well-formed set of triples is a set whose elements are well-formed. **)
(** It corresponds to Definition 4.1 of the paper. **)
Definition wellformed_set_of_triples (T : triple_set) :=
  forall sigma t v,
    T sigma t v ->
    exists c ts s,
      closed_term_free_term t = term_constructor c ts
      /\ sort_of_term_closed t (sort_program s)
      /\ state_sort sigma (in_sort s)
      /\ result_sort v (out_sort s).

End StateResultSort.

Section Interpretation.

(** We now instantiate the well-formedness of triple sets in the context of an interpretation. **)

Variable I : triple_based_interpretation constructor filter basic_term variable_term variable_flow.

Variable state_sort : triple_based_interpretation_triple_state I -> flow_sort -> Prop.
Variable result_sort : triple_based_interpretation_triple_result I -> flow_sort -> Prop.

(** The definition of [triple_set] is compatible with [triple_based_interpretation_set_of_triples]. **)
Lemma concrete_triple_set_eq :
  triple_based_interpretation_set_of_triples I
  = triple_set (triple_based_interpretation_triple_state I)
               (triple_based_interpretation_triple_result I).
Proof. reflexivity. Qed.

Definition wellformed_set_of_triples_from_interpretation :
    triple_based_interpretation_set_of_triples I -> Prop :=
  wellformed_set_of_triples state_sort result_sort.

End Interpretation.

End Variables.


(** ** Well-formedness of Skeletal Semantics **)

(** Given a skeletal semantics [S], the well-formedness checks
  that the well-formedness interpretation is defined for each
  constructor from a particular initial environment. **)

Variable S : skeletal_semantics constructor filter.

Hypothesis skeleton_instance_term_Comparable : forall c, Comparable (skeleton_variable_term (S c)).
Hypothesis skeleton_instance_flow_Comparable : forall c, Comparable (skeleton_variable_flow (S c)).

Let sorting_environment c :=
  sorting_environment (skeleton_variable_term (S c)) (skeleton_variable_flow (S c)).

Let typed_environment_read_variable c : sorting_environment c -> variable_skeleton (S c) -> option sort :=
  typed_environment_read_variable sort_term sort_flow (variable_flow := _).

Let typed_environment_write_variables c
    : sorting_environment c -> list (variable_skeleton (S c)) -> list sort ->
      option (sorting_environment c) :=
  typed_environment_write_variables sort_term sort_flow sort_rect (variable_flow := _).

(** The initial environment of the well-formedness interpretation
  associated to a particular constructor [c] maps [X_sigma] to
  the input sort of the constructor, and each term variables
  associated with [c] in the skeletal semantics [S] to its
  corresponding sort. **)
Definition wellformedness_initial_environment c s ss :=
  typed_environment_write_variables typed_environment_empty
    (_X_ X_sigma :: map (@X_t _ _) (skeleton_term_variables (S c)))
    (sort_flow (in_sort s) :: map sort_term ss).

(** Although [wellformedness_initial_environment] is an option type,
  it is actually defined. **)
Lemma wellformedness_initial_environment_defined : forall
    basic_term (basic_term_sort : basic_term -> base_sort -> Prop) V c s ts ss,
  length (skeleton_term_variables (S c)) = constructor_arity constructor_signature c ->
  sort_of_term constructor_signature basic_term_sort (fun (_ : V) _ => False)
    (term_constructor c ts) (sort_program s) ->
  Forall2 (sort_of_term constructor_signature basic_term_sort (fun _ _ => False)) ts ss ->
  exists Gamma, wellformedness_initial_environment c s ss = Some Gamma.
Proof.
  introv E Ss F. inverts Ss as E' F'. unfolds constructor_arity. rewrite E' in E.
  unfolds wellformedness_initial_environment. simpl.
  generalize (typed_environment_write_flow (typed_environment_empty : sorting_environment c)
                                           X_sigma (in_sort s)) as Gamma.
  gen E. generalize (skeleton_term_variables (S c)) as ts'.
  simpls. clear E' s. gen cas c. induction F; introv F' E; introv; inverts F' as Ss F'.
  - eexists. destruct ts'; tryfalse. reflexivity.
  - destruct ts' as [|ts0 ts']; tryfalse. forwards~ (Gamma'&EGamma): (rm IHF) (rm F') ts'.
    eexists. apply EGamma.
Qed.

Lemma wellformedness_initial_environment_defined_inv : forall c s ss Gamma,
  wellformedness_initial_environment c s ss = Some Gamma ->
  length (skeleton_term_variables (S c)) = length ss.
Proof.
  introv E. do 2 unfolds in E.
  forwards El: typed_environment_read_write_variables_length E. rew_list in El.
  repeat rewrite length_map in El. inverts~ El.
Qed.

(** The domain of the initial environment is exactly [X_sigma] and each
  term variables associated with the current constructor [c]. **)
Lemma wellformedness_initial_environment_domain : forall c s ss Gamma,
  wellformedness_initial_environment c s ss = Some Gamma ->
  typed_environment_domain sort_term sort_flow Gamma =
    \{ _X_ X_sigma }
    \u set_st (fun x => exists x0, x = X_t x0 /\ Mem x0 (skeleton_term_variables (S c))).
Proof.
  introv E. do 2 unfolds in E. rewrites~ >> typed_environment_domain_write_variables E.
  rewrites~ typed_environment_domain_empty. rewrite for_set_union_empty_l.
  rewrite~ to_set_cons. fequals.
  rewrite set_ext_eq. intro x. unfolds to_set. repeat rewrite in_set_st_eq.
  rewrite Mem_mem. rewrite map_mem. iff (x'&A&B); exists x'; rewrite Mem_mem in *; splits~.
Qed.

(** The following two lemmas specify what is the sort associated with particular
  variables in the initial environment. **)

Lemma wellformedness_initial_environment_X_sigma : forall c s ss Gamma,
  wellformedness_initial_environment c s ss = Some Gamma ->
  typed_environment_read_flow Gamma X_sigma = Some (in_sort s).
Proof.
  introv E. unfolds in E. simpl in E.
  gen Gamma. generalize (typed_environment_empty : sorting_environment c) as Gamma0,
    (skeleton_term_variables (S c)) as l2.
  induction ss using list_ind_last; introv E.
  - destruct l2 as [|t l2]; inverts E. apply~ typed_environment_read_write_flow_eq.
  - forwards [El2|(t&l2'&El2)]: last_case l2; rewrite El2 in E; rew_map in E.
    + destruct map in E; inverts* E.
    + unfolds in E.
      destruct (typed_environment_write_variables
        (typed_environment_write_flow Gamma0 X_sigma (in_sort s))
        (map (X_t (variable_flow:=skeleton_variable_flow (S c))) l2') (map sort_term ss)) eqn: E1.
      * rewrites >> typed_environment_write_variables_last in E; [ apply E1 |].
        forwards~ E2: IHss E1. inverts E. rewrite~ typed_environment_read_flow_write_term.
      * rewrites >> typed_environment_write_variables_last_None in E; [ apply E1 |]. inverts E.
Qed.

Lemma wellformedness_initial_environment_X_t : forall c s ss Gamma x sx n,
  No_duplicates (skeleton_term_variables (S c)) ->
  wellformedness_initial_environment c s ss = Some Gamma ->
  Nth n (skeleton_term_variables (S c)) x ->
  Nth n ss sx ->
  typed_environment_read_term Gamma x = Some sx.
Proof.
  introv ND E N1 N2. do 2 unfolds in E. forwards~ E': typed_environment_read_write_variables_Nth E.
  - constructors.
    + introv M. forwards (t&M'&E'): Mem_map_inv (rm M). inverts* E'.
    + apply~ Nth_No_duplicates. introv N1' N2'.
      forwards (t1&N1''&E1'): map_Nth_inv (rm N1'). forwards (t2&N2''&E2'): map_Nth_inv (rm N2').
      substs. inverts E2'. applys~ No_duplicates_Nth ND N1'' N2''.
  - apply~ Nth_next. applys map_Nth N1.
  - apply~ Nth_next. applys map_Nth N2.
  - simpl in E'. destruct typed_environment_read_term; inverts~ E'.
Qed.

(** The following definition states that a skeletal semantics is well-formed,
  based on the well-formedness interpretation. **)
(** It corresponds to Definition 3.1 of the paper. **)
Definition skeletal_semantics_wellformed_term :=
  forall basic_term (basic_term_sort : basic_term -> base_sort -> Prop)
         c (ts : list (term _ _ (skeleton_variable_term (S c)))) s,
    sort_of_term constructor_signature basic_term_sort (fun _ _ => False)
        (term_constructor c ts) (sort_program s) ->
      No_duplicates (skeleton_term_variables (S c))
      /\ exists ss Gamma Gamma' D D',
           wellformedness_initial_environment c s ss = Some Gamma
           /\ Forall2 (sort_of_term constructor_signature basic_term_sort (fun _ _ => False)) ts ss
           /\ D = typed_environment_domain sort_term sort_flow (variable_flow := _) Gamma
           /\ interpretation_skeleton (wellformedness _ _) (skeleton_skeleton (S c))
                (Gamma, D) (Gamma', D')
           /\ typed_environment_read_variable Gamma' (_X_ X_o) = Some (sort_flow (out_sort s)).

(** The following criterium is however easier to manipulate in practise,
  as it does not refer to any basic term. **)
Definition skeletal_semantics_wellformed :=
  forall c cas crs,
    constructor_signature c = (cas, crs) ->
    No_duplicates (skeleton_term_variables (S c))
    /\ exists Gamma Gamma' D D',
         wellformedness_initial_environment c crs cas = Some Gamma
         /\ D = typed_environment_domain sort_term sort_flow (variable_flow := _) Gamma
         /\ interpretation_skeleton (wellformedness _ _) (skeleton_skeleton (S c))
              (Gamma, D) (Gamma', D')
         /\ typed_environment_read_variable Gamma' (_X_ X_o) = Some (sort_flow (out_sort crs)).

(** This criterium is stronger (usually equivalent in practice, though). **)
Lemma skeletal_semantics_wellformed_instantiation :
  skeletal_semantics_wellformed ->
  skeletal_semantics_wellformed_term.
Proof.
  introv WF St. inverts St as E F. forwards (ND&Gamma&Gamma'&D&D'&EGamma&ED&DI&Exo): WF E.
  splits~. do 5 eexists; splits; try apply DI; autos*.
Qed.

(* LATER
(** The other direction requires term sorts to be instantiable. **)
Lemma skeletal_semantics_wellformed_instantiation_inv :
  (forall s, exists t, sort_of_term_basic_free constructor_signature (False_rect _) t s) ->
  skeletal_semantics_wellformed_term ->
  skeletal_semantics_wellformed.
Proof.
  introv FE WF E. forwards (ts&Hts): Forall_exists cas.
  { rewrite Forall_iff_forall_mem. intros s M. forwards (t&St): FE s.
    exists t. exact FE. }
  forwards (ND&ss&Gamma&Gamma'&D&D'&EGamma&F&ED&DI&Exo): WF (False_rect base_sort) c
    (term_constructor c (map (basic_free_term_to_basic_term False) ts)).
  - skip.
  - splits~. do 4 eexists; splits; try apply DI; autos*.
Qed.
*)

End Sorts.

Arguments sort_term [_ _ _].
Arguments sort_flow [_ _ _].
Arguments wellformed_set_of_triples_from_interpretation [_ _ _ _ _] _ _ _ [_] _ [_ _] _ _ _ _.


(** * Tactics **)

Ltac apply_inj_lemma lemma :=
  first [ applys~ lemma term_sort_inj flow_sort_inj
        | eapply lemma; try solve [ apply term_sort_inj | apply flow_sort_inj ] ];
  autos~; try typeclass.

Instance term_sort_Inhab : forall base_sort program_sort,
  Inhab program_sort ->
  Inhab (term_sort base_sort program_sort).
Proof. introv I. applys prove_Inhab. apply (sort_program arbitrary). Defined.

(** Instantiate the variables that can be instantiated **)
Ltac wellformedness_instantiate_variables :=
  repeat match goal with
  | E: _ = _ |- _ => progress inverts E as E
  end;
  try lazymatch goal with
  | |- _ = _ => reflexivity
  | |- typed_environment_equivalent ?E1 ?E2 =>
    (** What we really want here is to instantiate environments when
      one of them is an existantial variable.  One careful case is
      the merge case, where is can happen to have a goal of the form:
<<
typed_environment_equivalent
  (typed_environment_merge _ (typed_environment_restrict _ _))
  (typed_environment_merge _ (typed_environment_restrict _ _))
>>
      Usually, the various wildcares of such cases are (meant to be)
      different, but a careless application of
      [typed_environment_equivalent_refl] would unify them nevertheless.
      It is thus crucial to first check that no potentially harmful term
      appear in the environments [E1] and [E2] first. **)
    let check_for_potentially_harmful_construct E :=
      lazymatch E with
      | context [ typed_environment_merge ] => fail
      | context [ typed_environment_restrict ] => fail
      | _ => idtac
      end in
    (** At least one of [E1] and [E2] should not contain harmful constructs. **)
    (check_for_potentially_harmful_construct E1 || check_for_potentially_harmful_construct E2);
    solve [ apply_inj_lemma (@typed_environment_equivalent_refl) ]
  end.

Require Import LibInt.

(** Solve simple goals, but may decide to strangely instantiate existantial variables **)
Ltac wellformedness_reduce_basic_goals :=
  repeat (
    wellformedness_instantiate_variables;
    (** Pattern matching the goal **)
    let incl_case :=
      first [
          apply~ LibBag.incl_refl
        | progress rewrite_typed_environment_domain
        | repeat rewrite to_set_cons; repeat rewrite LibSet.to_set_nil;
          try unfold _X_f, _X_sigma, _X_o;
          solve_incl ] in
    repeat lazymatch goal with
    | |- _ = _ =>
      let rec aux :=
        first [ reflexivity
              | progress rewrite_read_typed_environment
              | solve [ autos~ ]
              | eapply set_iff_incl; try typeclass; incl_case
              | progress (simpl; fequals); solve [ repeat aux ] ] in
      aux
    | |- _ <> _ => discriminate || autos~
    | |- LibBag.incl _ _ => incl_case
    | |- No_duplicates _ => No_duplicates_solve
    | |- Forall _ _ => constructors
    | |- Forall2 _ _ _ => constructors
    | |- Forall3 _ _ _ _ => constructors
    | |- ~ typed_environment_domain _ _ _ _ =>
      solve [
          let M := fresh "M" in introv M; apply~ M;
          rewrite_read_typed_environment; autos~
        | rewrite_read_typed_environment; (discriminate || autos~);
          let M := fresh "M" in introv M; apply~ M;
          rewrite_read_typed_environment; autos~
        | autos~ ]
    | |- typed_environment_domain _ _ _ _ =>
      solve [
          rewrite_read_typed_environment; (discriminate || autos~)
        | autos~ ]
    | |- LibHeap.HeapList.indom _ _ => rewrite_read_typed_environment; try discriminate; autos~
    | |- sort_of_term_basic_free _ _ _ _ =>
      constructors; rewrite_read_typed_environment; autos~
    | |- sort_of_term _ _ _ _ _ =>
      constructors; rewrite_read_typed_environment; autos~
    | |- typed_environment_equivalent _ _ =>
      apply_inj_lemma (@typed_environment_equivalent_refl)
    | |- LibBag.union _ _ _ =>
      first [
          progress rewrite_typed_environment_domain
        | rewrite <- LibSet.in_set_st_eq; unfolds LibSet.set_st;
          repeat rewrite to_set_cons; repeat rewrite LibSet.to_set_nil;
          try unfold _X_f, _X_sigma, _X_o;
          repeat rewrite LibBag.in_union_eq; repeat rewrite LibBag.in_single_eq; autos* ]
    | |- ~ LibBag.union _ _ _ =>
      first [
          progress rewrite_typed_environment_domain
        | let I := fresh "I" in introv I;
          rewrite <- LibSet.in_set_st_eq in I; unfolds LibSet.set_st;
          repeat rewrite LibBag.in_union_eq in I; repeat rewrite LibBag.in_single_eq in I;
          repeat rewrite to_set_cons in I; repeat rewrite LibSet.to_set_nil in I;
          try unfold _X_f, _X_sigma, _X_o;
          repeat inverts I as I ]
    | |- _ >= _ => math || (rew_list; math)
    end;
    (** Pattern matching the context **)
    repeat match goal with
    | E: _ = _ |- _ => progress inverts E as E
    | M: Mem _ ?l |- _ =>
      let immediate l :=
        list_is_fully_computed l;
        let M' := fresh "M" in inverts M as M'; tryfalse in
      first [ immediate l
            | rew_list in M;
              match type of M with
              | Mem _ ?l => immediate l
              end ]
    | N: Nth _ ?l _ |- _ =>
      let immediate l :=
        list_is_fully_computed l;
        let N' := fresh "N" in inverts N as N'; tryfalse in
      first [ immediate l
            | rew_list in N;
              match type of N with
              | Nth _ ?l _ => immediate l
              end ]
    | FV: term_free_variable _ _ |- _ => simpl in FV; progress (inverts FV as FV)
    | _ =>
      progress (intros; autos~; try typeclass; try (apply term_sort_Inhab; typeclass))
    end;
    interpretation_unfold_constructors).

(** Solves some goals specific to particular cases of the interpretation. **)
Ltac wellformedness_specific_goals :=
  try solve [
    lazymatch goal with
    | |- _ = _ =>
      (** Most definitions of [in_sort] and [out_sort] are very repetitive:
        sometimes, just destructing some assumptions can be enough. **)
      repeat match goal with
      | c : ?T |- _ =>
        (** Checking whether we may want to destruct it. **)
        lazymatch goal with
        | |- context [ c ] => destruct c; try reflexivity
        end
      end
    | |- LibBag.incl _ _ =>
      rewrite_typed_environment_domain;
      repeat rewrite to_set_cons; repeat rewrite LibSet.to_set_nil;
      try unfold _X_f, _X_sigma, _X_o;
      solve_incl_heavy
    | |- typed_environment_equivalent _ _ =>
      apply_inj_lemma (@typed_environment_equivalent_on_domain);
      rewrite_typed_environment_domain; try typeclass;
      repeat rewrite to_set_cons; repeat rewrite LibSet.to_set_nil;
      try unfold _X_f, _X_sigma, _X_o;
      let I := fresh "I" in introv I; rewrite_in I; substs; try reflexivity;
      rewrite_read_typed_environment; first [ reflexivity | typeclass | autos~ ]
    end ].


(** Takes a property of the form [constructor_signature c = (cas, crs)]
  and tries to prove to associated well-formedness condition. **)
Ltac wellformedness_constructor E :=
  splits;
  [ No_duplicates_solve
  | simpl in E; inverts E;
    do 4 eexists; splits;
    (** We first unfold all what can be safely unfolded, creating
      existential variables on the way. **)
    interpretation_unfold_constructors;
    (** We then try to istantiate these existential variables. **)
    repeat wellformedness_instantiate_variables;
    (** At this stage, almost all variables are instantiated. **)
    wellformedness_reduce_basic_goals;
    (** At this stage, only very specific goals should be left. **)
    wellformedness_specific_goals ].

Ltac prove_wellformedness :=
  let E := fresh "E" in introv E;
  let rec aux :=
    lazymatch type of E with
    | _ ?c = _ => try (destruct c; try abstract wellformedness_constructor E; try aux)
    end in
  aux.


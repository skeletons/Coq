(** Definition of the concrete interpretation. **)

Set Implicit Arguments.

Require Import LibFunc LibSet LibHeap.
Require Export WellFormedness.

(** * The Concrete Interpretation **)

Section Sorts.

Variables constructor filter : Type.

Hypothesis constructor_Comparable : Comparable constructor.
Hypothesis filter_Comparable : Comparable filter.

(** For simplicity, we assume that there is at least one constructor.
  This is always the case in non-trivial semantics.
  We might optimise some proofs in TLC to get rid of this hypothesis. **)
Hypothesis constructor_Inhab : Inhab constructor.

Variable basic_term : Type.

Hypothesis basic_term_Comparable : Comparable basic_term.

Let closed_term_free : Type := closed_term_free constructor basic_term.

(** ** Values **)

(** We assume given a set of flow values. **)
Variable flow_value : Type.

(** For simplicity, we assume that they are inhabited, which is valid on non-trivial semantics.
  We might optimise some proofs in TLC to get rid of this hypothesis. **)
Hypothesis flow_value_Inhab : Inhab flow_value.

(** Values are either term values (that is, closed terms), or flow values.
  In the concrete interpretation, each variable is mapped to a value. **)
Inductive value :=
  | value_term : closed_term_free -> value
  | value_flow : flow_value -> value
  .
Coercion value_term : closed_term_free >-> value.
Coercion value_flow : flow_value >-> value.

(** We lift the equality relation [closed_term_free_eq] over values. **)
Inductive value_eq : value -> value -> Prop :=
  | value_eq_term : forall t1 t2,
    closed_term_free_eq t1 t2 ->
    value_eq (value_term t1) (value_term t2)
  | value_eq_flow : forall v : flow_value,
    value_eq v v
  .

(** The relation [value_eq] is an equivalence relation. **)

Lemma value_eq_refl : forall v,
  value_eq v v.
Proof. introv. destruct v; constructor~. apply~ closed_term_free_eq_refl. Qed.

Lemma value_eq_sym : forall v1 v2,
  value_eq v1 v2 ->
  value_eq v2 v1.
Proof. introv E. inverts E as E; constructor~. applys~ closed_term_free_eq_sym E. Qed.

Lemma value_eq_trans : forall v1 v2 v3,
  value_eq v1 v2 ->
  value_eq v2 v3 ->
  value_eq v1 v3.
Proof.
  introv E1 E2. inverts E1 as E1; inverts E2 as E2; constructor~.
  applys~ closed_term_free_eq_trans E1 E2.
Qed.

Lemma value_term_inj : forall t1 t2, value_term t1 = value_term t2 -> t1 = t2.
Proof. introv E. inverts~ E. Qed.

Lemma value_flow_inj : forall t1 t2, value_flow t1 = value_flow t2 -> t1 = t2.
Proof. introv E. inverts~ E. Qed.

Variable filter_interpretation : filter -> list value -> list value -> Prop.

(** We here need to assume the same variables than in the well-formedness interpretation. **)
Variable base_sort program_sort flow_sort : Type.

Hypothesis base_sort_Comparable : Comparable base_sort.
Hypothesis program_sort_Comparable : Comparable program_sort.
Hypothesis flow_sort_Comparable : Comparable flow_sort.

(** For simplicity, we assume that these types are inhabited,
  which can always be satisfied by adding dummy sorts.
  We might optimise some proofs in TLC to get rid of these hypotheses. **)
Hypothesis flow_sort_Inhab : Inhab flow_sort.
Hypothesis base_sort_Inhab : Inhab base_sort.
Hypothesis program_sort_Inhab : Inhab program_sort.

Let term_sort : Type := term_sort base_sort program_sort.
Let sort : Type := sort base_sort program_sort flow_sort.
Let sort_term : term_sort -> sort := @sort_term _ _ _.
Let sort_flow : flow_sort -> sort := @sort_flow _ _ _.
Local Coercion sort_term : term_sort >-> sort.
Local Coercion sort_flow : flow_sort >-> sort.

Variables in_sort out_sort : program_sort -> flow_sort.

Variable filter_signature : filter -> list sort * list sort.
Variable constructor_signature : constructor -> list term_sort * program_sort.
Variable basic_term_sort : basic_term -> base_sort -> Prop.

Let sort_of_term_closed : closed_term_free -> term_sort -> Prop :=
  sort_of_term_closed constructor_signature basic_term_sort.

Variable value_flow_sort : flow_value -> flow_sort -> Prop.

(** We lift [value_flow_sort] on general values and sorts by associating term to their sorts. **)
Inductive value_sort : value -> sort -> Prop :=
  | value_sort_term : forall t s,
    sort_of_term_closed t s ->
    value_sort t s
  | value_sort_flow : forall v s,
    value_flow_sort v s ->
    value_sort v s
  .

Hypothesis basic_term_sort_unique : forall b s1 s2,
  basic_term_sort b s1 ->
  basic_term_sort b s2 ->
  s1 = s2.

Hypothesis value_flow_sort_unique : forall v s1 s2,
  value_flow_sort v s1 ->
  value_flow_sort v s2 ->
  s1 = s2.

Lemma value_eq_sort_unique : forall v1 v2 s1 s2,
  value_eq v1 v2 ->
  value_sort v1 s1 ->
  value_sort v2 s2 ->
  s1 = s2.
Proof.
  introv E S1 S2. inverts E as E; inverts S1 as S1; inverts S2 as S2; fequals.
  - applys~ sort_of_term_closed_unique S2. applys~ sort_of_term_closed_eq S1 E.
  - applys~ value_flow_sort_unique S1 S2.
Qed.

Lemma value_sort_unique : forall v s1 s2,
  value_sort v s1 ->
  value_sort v s2 ->
  s1 = s2.
Proof. introv S1 S2. applys~ value_eq_sort_unique value_eq_refl S1 S2. Qed.

(** Filter interpretations are considered consistent with sorting. **)
Hypothesis filter_interpretation_consistent_with_sort : forall f vs1 vss1 vs2 vss2,
  filter_interpretation f vs1 vs2 ->
  filter_signature f = (vss1, vss2) ->
  Forall2 value_sort vs1 vss1 ->
  Forall2 value_sort vs2 vss2.

Section Variables.

Variables variable_term variable_flow : Type.

Hypothesis variable_term_Comparable : Comparable variable_term.
Hypothesis variable_flow_Comparable : Comparable variable_flow.

Let variable : Type := variable variable_term variable_flow.
Let variable_flow_extended : Type := variable_flow_extended variable_flow.
Let term : Type := term constructor basic_term variable_term.
Let bone : Type := bone constructor filter variable_term variable_flow.
Let skeleton : Type := skeleton constructor filter variable_term variable_flow.
Let basic_free_term : Type := basic_free_term constructor variable_term.

Local Instance variable_flow_extended_Comparable : Comparable variable_flow_extended :=
  variable_flow_extended_Comparable variable_flow_Comparable.

Let wellformedness : interpretation _ _ variable_term variable_flow :=
  wellformedness in_sort out_sort filter_signature constructor_signature _ _.

Section ConcreteState.

(** Concrete environments map flow variables to values, and term variables to (closed) terms.
  We base ourselves on [typed_environment] from [aux/Environments.v] to define them. **)

Definition concrete_state : Type :=
  typed_environment closed_term_free flow_value variable_term variable_flow.

Let typed_environment_domain : concrete_state -> set variable :=
  typed_environment_domain value_term value_flow (variable_flow := _).

Let typed_environment_read_variable : concrete_state -> variable -> option value :=
  typed_environment_read_variable value_term value_flow (variable_flow := _).

Let typed_environment_write_variables
    : concrete_state -> list variable -> list value -> option concrete_state :=
  typed_environment_write_variables value_term value_flow value_rect (variable_flow := _).


(** Concrete environments map term variables to closed terms,
  which can then be converted into any term of the form
  [Terms.term constructor _] using [closed_term_free_to_variable_term].
  The following function directly converts it into [term] after reading
  from the environment. **)
Definition read_term_from_concrete_state_direct V (Sigma : concrete_state) x :=
  LibOption.map_on (typed_environment_read_term Sigma x) (closed_term_free_to_variable_term V).

(** We can then lift this function to (open) terms.
   The predicate [read_term_from_concrete_state Sigme t t'] states that substituting
   all variables of the term [t] by their associated (closed) term in [Sigma] results
   in the term [t'].  The term [t'] is thus closed, which is shown in the next lemma. **)
Definition read_term_from_concrete_state (Sigma : concrete_state) : term -> term -> Prop :=
  same_structure_subst_option (read_term_from_concrete_state_direct _ Sigma).

Lemma read_term_from_concrete_state_closed : forall Sigma t t',
  read_term_from_concrete_state Sigma t t' ->
  closed_term t'.
Proof.
  introv R. applys~ same_structure_subst_option_closed R.
  introv ? E. unfolds in E. unfolds typed_environment_read_term.
  destruct read_option; inverts E. apply~ closed_term_free_to_variable_term_closed.
Qed.

Instance read_term_from_concrete_state_Pickable : forall Sigma t,
    Pickable_list (read_term_from_concrete_state Sigma t).
  introv. apply Pickable_option_Pickable_list.
  { introv Ea Eb. apply same_structure_refl_inv.
    applys~ same_structure_subst_option_unique Ea Eb.
    introv FV E1 E2. unfolds in E1. unfolds in E2.
    destruct typed_environment_read_term; tryfalse. inverts E1. inverts E2.
    applys~ same_structure_weaken closed_term_free_to_variable_term_same_structure.
    intros. false~. }
  apply same_structure_subst_option_Pickable_option; try typeclass.
Defined.

(** We can thus directly produce a [closed_term_free] by substituting
  in a term using an environment [Sigma]. **)
Definition read_closed_term_from_concrete_state Sigma t tc :=
  exists (t' : _) (H : _ Sigma t t'),
    closed_term_free_eq (make_closed_term_free (read_term_from_concrete_state_closed H)) tc.

(** This closed term behaves well with the closed-term equality. **)
Lemma read_closed_term_from_concrete_state_closed_term_free_eq : forall Sigma t t1 t2,
  read_closed_term_from_concrete_state Sigma t t1 ->
  closed_term_free_eq t1 t2 ->
  read_closed_term_from_concrete_state Sigma t t2.
Proof. introv (t'&H&E) E'. do 2 eexists. applys closed_term_free_eq_trans E E'. Qed.

(** In practice, however, the concrete interpretation only substitutes [basic_free_term]s
  and not [term]s.  The conversion is performed by the following function. **)
Definition read_closed_term_from_concrete_state_from_basic_free_term Sigma t :=
  read_closed_term_from_concrete_state Sigma (basic_free_term_to_basic_term _ t).

(** This closed term behaves well with the closed-term and basic-free equalities. **)
Lemma read_closed_term_from_concrete_state_from_basic_free_term_eq : forall Sigma ta tb t1 t2,
  read_closed_term_from_concrete_state_from_basic_free_term Sigma ta t1 ->
  closed_term_free_eq t1 t2 ->
  basic_free_term_eq ta tb ->
  read_closed_term_from_concrete_state_from_basic_free_term Sigma tb t2.
Proof.
  introv E1 E2 E3. forwards E4: read_closed_term_from_concrete_state_closed_term_free_eq E1 E2.
  unfolds in E1. unfolds. rewrites~ <- >> basic_free_term_eq_to_basic_term E3.
Qed.

(** If a term can be read from a state, then all its free variables
  are in the domain of the state. **)
Lemma read_term_from_concrete_state_closed_term_free_variable : forall (Sigma : concrete_state) t t',
  read_closed_term_from_concrete_state Sigma t t' ->
  map_set (@X_t _ _) (term_free_variable t) \c typed_environment_domain Sigma.
Proof.
  introv R. apply incl_prove. introv I. forwards (x'&I'&E'): in_map_set_inv (rm I). substs.
  unfolds typed_environment_domain, Environments.typed_environment_domain. rewrite in_set_st_eq.
  simpl. lets (t0&H&E): (rm R). forwards~ D: same_structure_subst_option_free_variable_inv H I'.
  unfolds read_term_from_concrete_state_direct.
  destruct typed_environment_read_term; simpl in D; tryfalse. discriminate.
Qed.


(** ** Concrete Interpretation **)

(** The concrete interpretation possibly is the most intuitive interpretation.
  It is a triple-based interpretation, which means that it assumes that a set
  of triples has already been computed, and generates a bigger set of triples.
  Triples are of the form [(sigma, t, o)], where [sigma] and [o] are [value]s
  and [t] is a closed term (of type [closed_term_free]).  Hooks directly pick
  the given [(sigma, t, o)] from the set of triples, filters apply a concrete
  interpretation of filters which can be seen as a partial function (although
  it is possible to encode non-determinism in [filter_interpretation]), and
  branches consider one path in the subskeletons that return a value. **)

(** These definitions correspond to Figure 9 of the paper. **)

Inductive concrete_interpretation_hook :
    variable_flow_extended -> basic_free_term -> variable_flow_extended ->
    value -> closed_term_free -> value ->
    concrete_state -> concrete_state -> Prop :=
  | concrete_interpretation_hook_intro : forall xf1 t xf2 Sigma Sigma' v1 t' v2,
    read_closed_term_from_concrete_state_from_basic_free_term Sigma t t' ->
    typed_environment_read_flow Sigma xf1 = Some v1 ->
    Sigma' = typed_environment_write_flow Sigma xf2 v2 ->
    concrete_interpretation_hook xf1 t xf2 v1 t' v2 Sigma Sigma'
  .

Inductive concrete_interpretation_filter :
    filter -> list variable -> list variable ->
    concrete_state -> concrete_state -> Prop :=
  | concrete_interpretation_filter_intro : forall f xs ys Sigma Sigma' vxs vys,
    Forall2 (fun x v => typed_environment_read_variable Sigma x = Some v) xs vxs ->
    filter_interpretation f vxs vys ->
    typed_environment_write_variables Sigma ys vys = Some Sigma' ->
    concrete_interpretation_filter f xs ys Sigma Sigma'
  .

Inductive concrete_interpretation_merge :
    list (option concrete_state) -> list variable ->
    concrete_state -> concrete_state -> Prop :=
  | concrete_interpretation_merge_intro : forall O V Sigma Sigma' n Sigmai,
    Nth n O (Some Sigmai) ->
    to_set V \c typed_environment_domain Sigmai ->
    typed_environment_equivalent Sigma'
      (typed_environment_merge Sigma (typed_environment_restrict Sigmai V)) ->
    concrete_interpretation_merge O V Sigma Sigma'
  .

Definition concrete := {|
    triple_based_interpretation_hook := concrete_interpretation_hook ;
    triple_based_interpretation_hook_shortcut := fun _ _ _ _ _ => False ;
    triple_based_interpretation_filter := concrete_interpretation_filter ;
    triple_based_interpretation_merge := concrete_interpretation_merge
  |}.

End ConcreteState.


(** ** Consistency of the Well-formedness and the Concrete Interpretation **)

(** We show that the concrete interpretation only produces well-formed triples
  from a well-formed set of triples. **)

Let wellformed_set_of_triples
     : triple_based_interpretation_set_of_triples concrete -> Prop :=
  wellformed_set_of_triples in_sort out_sort constructor_signature basic_term_sort
                            value_sort value_sort.

(** The well-formedness and concrete environments are in sync when
  they have the same domains and that every variable in this domain is
  associated a sort that corresponds to its associated value. **)
Definition WFconcreteOKout (GammaD : interpretation_result wellformedness)
                           (Sigma : interpretation_result concrete) :=
  let (Gamma, D) := GammaD in
  typed_environment_domain sort_term sort_flow Gamma
  = typed_environment_domain value_term value_flow Sigma
  /\ forall x v s,
       typed_environment_read_variable value_term value_flow Sigma x = Some v ->
       typed_environment_read_variable sort_term sort_flow Gamma x = Some s ->
       value_sort v s.

(** We also require the triple set to be well-formed. **)
Definition WFconcreteOKst (GammaD : interpretation_state wellformedness)
                          (SigmaT : interpretation_state concrete) :=
  let (Sigma, T) := SigmaT in
  wellformed_set_of_triples T
  /\ WFconcreteOKout GammaD Sigma.

(** This is a useful intermediary lemma for the next lemmas. **)
Lemma get_disjunction_from_merge_case : forall A Dis (D V : set A),
  length Dis >= 2 ->
  (forall i j Di Dj,
    i <> j ->
    Nth i Dis Di ->
    Nth j Dis Dj ->
    Di \- D \n Dj \- D = V) ->
  D \# V.
Proof.
  introv El F. rewrite disjoint_eq. introv I1 I2.
  destruct Dis as [|D0 Dis]; rew_list in El; try solve [ inverts El ].
  destruct Dis as [|D1 Dis]; rew_list in El; simpl in El;
    try solve [ do 2 inverts El as El ].
  forwards~ E: (rm F) 0 1; try solve [ repeat constructors ].
  rewrites <- (rm E) in I2. rewrite in_inter_eq in I2. lets (I3&I4): (rm I2).
  rewrite in_remove_eq in I3. lets (I5&N): (rm I3). applys~ N I1.
Qed.

(** This lemma states that the well-formedness and concrete interpretations
  are universally consistent. **)
(** It corresponds to Lemma 4.2 of the paper. **)
(** Note how this lemma is proven: no induction is performed.  Instead,
  the lemma [local_universal_consistency] is directly applied, which
  changes the goal into local properties to prove. **)
Lemma WFconcrete_universally_consistent :
  universally_consistent wellformedness concrete WFconcreteOKst WFconcreteOKout.
Proof.
  apply local_universal_consistency.
  - intros (Gamma&D) (Sigma&T) (Gamma'&D') Sigma' (OKT&E&F) D1 D2.
    inverts D1 as E1. inverts D2. splits.
    + rewrites~ <- >> typed_environment_domain_equivalent E1; introv I; inverts~ I.
    + introv Ea Eb. rewrite typed_environment_equivalent_spec in E1; [ rewrite <- E1 in Eb | |];
        autos*; introv I; inverts~ I.
  - intros xf1 t xf2 (Gamma&D) (Sigma&T) (Gamma'&D') Sigma' (OKT&E&F) D1 D2.
    inverts D1 as DI1 Dxf11 F1 S1' EG1 NDxf21 E1. inverts D2 as Tt2 H2; tryfalse.
    inverts H2 as R2 B2. lets (t2&Ht2&Et2): (rm R2).
    asserts S1: (sort_of_term constructor_signature basic_term_sort
         (read_to_set (typed_environment_read_term Gamma)) (basic_free_term_to_basic_term basic_term t)
         (sort_program s)).
    { applys sort_of_term_same_structure (fun x1 x2 : variable_term => x1 = x2) S1';
        try solve [ simpl; intros; tryfalse; substs~ ].
      apply~ basic_free_term_to_basic_term_same_structure. }
    do 2 splits~.
    + rewrites >> typed_environment_domain_equivalent E1; try solve [ introv E'; inverts~ E' ].
      do 2 rewrite typed_environment_domain_write_flow. fequals~.
    + introv Ea Eb. rewrites >> typed_environment_equivalent_spec in E1;
        [ apply term_sort_inj | apply flow_sort_inj |].
      rewrites (rm E1) in Eb. tests Dx: (x = _X_ xf2).
      * simpl in Ea. rewrite typed_environment_read_write_flow_eq in Ea. inverts Ea.
        forwards (c&ts&s'&E0&S0&Vsigma&Vo): OKT Tt2.
        simpl in Eb. rewrite typed_environment_read_write_flow_eq in Eb. inverts Eb.
        forwards Ss: same_structure_subst_option_sort Ht2 S1.
        { introv ? E1 E2. unfolds in E1. destruct typed_environment_read_term eqn: Ex; inverts E1.
          forwards S: F (X_t x : variable).
          - simpl. rewrite* Ex.
          - simpl. fold term_sort in E2. rewrite* E2.
          - inverts S as S. applys sort_of_term_same_structure
                                   (fun (_ : variable_term) (_ : term_sort) => False) S;
                [| | apply~ closed_term_free_to_variable_term_spec ];
              simpl; intros; substs~. }
        forwards S2: sort_of_term_closed_eq S0.
        { applys~ closed_term_free_eq_sym Et2. }
        forwards~ E': sort_of_term_unique Ss S2.
        -- intros. false.
        -- inverts~ E'.
      * applys F x.
        -- destruct x as [x|x]; simpl in Ea.
           ++ rewrite~ typed_environment_read_term_write_flow in Ea.
           ++ rewrite~ typed_environment_read_write_flow_neq in Ea. introv ?. false*.
        -- destruct x as [x|x]; simpl in Eb.
           ++ rewrite~ typed_environment_read_term_write_flow in Eb.
           ++ rewrite~ typed_environment_read_write_flow_neq in Eb. introv ?. false*.
  - intros f xs ys (Gamma&D) (Sigma&T) (Gamma'&D') (Sigma'&T') (OKT&E&F) D1 D2.
    inverts D1 as DI F1 ND F2 F3 Ef EGamma'. inverts D2 as Tf. inverts Tf as F4 FI ESigma'.
    do 2 splits~.
    + unfold sort_term, sort_flow, term_sort, sort.
      rewrites~ >> typed_environment_domain_write_variables EGamma'.
      rewrites~ >> typed_environment_domain_write_variables ESigma'. fequals~.
    + introv Ea Eb. tests I: (Mem x ys).
      * lets (n&Ny): Mem_Nth (rm I). forwards (v'&Nv): length_Nth_lt.
        { rewrites <- >> typed_environment_read_write_variables_length ESigma'.
          applys Nth_lt_length Ny. }
        forwards~ Ea': typed_environment_read_write_variables_Nth Ny Nv ESigma'.
        rewrite Ea' in Ea. inverts Ea. forwards (ys'&Nys): length_Nth_lt.
        { rewrites <- >> typed_environment_read_write_variables_length EGamma'.
          applys Nth_lt_length Ny. }
        forwards~ Eb': typed_environment_read_write_variables_Nth Ny Nys EGamma'.
        unfolds sort_term, sort_flow, term_sort, sort.
        rewrite Eb' in Eb. inverts Eb.
        forwards F5: filter_interpretation_consistent_with_sort FI Ef.
        { rewrite Forall2_iff_forall_Nth in F3. lets (El3&F3'): (rm F3).
          rewrite Forall2_iff_forall_Nth in F4. lets (El4&F4'): (rm F4).
          rewrite Forall2_iff_forall_Nth. splits~.
          - rewrite <- El3. rewrite~ <- El4.
          - introv N1 N2. forwards (x'&N3): length_Nth_lt.
            { rewrite El3. applys Nth_lt_length N2. }
            applys~ F.
            + applys~ F4' N3 N1.
            + applys~ F3' N3 N2. }
        rewrite Forall2_iff_forall_Nth in F5. lets (El5&F5'): (rm F5). applys~ F5' Nv Nys.
      * rewrites~ >> typed_environment_read_write_variables_not_in I ESigma' in Ea.
        unfolds sort_term, sort_flow, term_sort, sort.
        rewrites~ >> typed_environment_read_write_variables_not_in I EGamma' in Eb.
        applys~ F Ea Eb.
  - intros V O1 O2 (Gamma&D) (Sigma&T) (Gamma'&D') (Sigma'&T') (OKT&E&F) F' D1 D2.
    inverts D1 as O1l DI F2 F3 F4 F5 F6. inverts D2 as Tm. inverts Tm as N IV ESigma'.
    rewrite Forall2_iff_forall_Nth in F'. lets (El&FO): (rm F'). forwards (o1&N2): length_Nth_lt.
    { rewrite El. applys Nth_lt_length N. }
    rewrite Forall3_iff_forall_Nth in F2. lets (El1&El2&FG): (rm F2). forwards (Gi&N3): length_Nth_lt.
    { rewrite <- El1. applys Nth_lt_length N2. }
    forwards (Di&N4): length_Nth_lt.
    { rewrite <- El2. applys Nth_lt_length N3. }
    forwards: FG N2 N3 N4. substs. forwards~ OK: FO N2 N. lets (OKD&F7): (rm OK).
    rewrite Forall_iff_forall_mem in F6. forwards EGamma': F6.
    { applys~ Nth_mem N3. }
    do 2 splits~.
    + rewrites >> typed_environment_domain_equivalent ESigma'; try solve [ introv E'; inverts~ E' ].
      rewrites~ >> typed_environment_domain_equivalent EGamma'; try solve [ introv E'; inverts~ E' ].
      repeat rewrite~ typed_environment_domain_merge. repeat rewrite~ typed_environment_domain_restrict;
        try solve [ apply~ term_sort_Inhab | apply~ closed_term_free_Inhab ].
      repeat fequals~.
    + introv Ea Eb. rewrites >> typed_environment_equivalent_spec in EGamma';
        [ apply term_sort_inj | apply flow_sort_inj |].
      rewrites (rm EGamma') in Eb. forwards Eb': typed_environment_read_merge (rm Eb).
      rewrites >> typed_environment_equivalent_spec in ESigma';
        [ apply value_term_inj | apply value_flow_inj |].
      rewrites (rm ESigma') in Ea. forwards Ea': typed_environment_read_merge (rm Ea).
      tests I: (Mem x V).
      * rewrites~ >> typed_environment_read_restrict_Mem I in Ea';
          [ apply~ closed_term_free_Inhab |].
        rewrites~ >> typed_environment_read_restrict_Mem I in Eb'; [ apply~ term_sort_Inhab |].
        asserts ND: (~ D x).
        { introv ID. forwards DD: get_disjunction_from_merge_case F5.
          { rewrite <- El2. rewrite~ <- El1. }
          rewrite disjoint_eq in DD. applys~ DD ID I. }
        asserts NG: (~ x \in typed_environment_domain sort_term sort_flow Gamma).
        { introv Ix. apply ND. rewrite set_incl_in_eq in DI. applys~ DI Ix. }
        lets NS: NG. rewrite E in NS. unfold typed_environment_domain in NG, NS.
        rewrite in_set_st_eq in NG, NS. rew_logic in NG. rew_logic in NS.
        unfolds sort_term, sort_flow, term_sort. rewrite NG in Eb'. rewrite NS in Ea'.
        inverts Ea' as Ea; inverts Eb' as Eb; tryfalse.
        applys~ F7 Ea Eb.
      * rewrites~ >> typed_environment_read_restrict_not_Mem I in Ea';
          [ apply~ closed_term_free_Inhab |].
        rewrites~ >> typed_environment_read_restrict_not_Mem I in Eb'; [ apply~ term_sort_Inhab |].
        inverts Ea' as Ea; inverts Eb' as Eb; tryfalse.
        applys~ F Ea Eb.
Qed.

End Variables.


(** ** Concrete Interpretation of Skeletal Semantics **)

Section Skeleton.

Variable S : skeletal_semantics constructor filter.

Hypothesis skeleton_instance_term_Comparable : forall c, Comparable (skeleton_variable_term (S c)).
Hypothesis skeleton_instance_flow_Comparable : forall c, Comparable (skeleton_variable_flow (S c)).

Hypothesis S_wellformed :
  skeletal_semantics_wellformed in_sort out_sort
                                filter_signature constructor_signature S _ _.

Let concrete c : triple_based_interpretation _ _ _ _ _ :=
  concrete (skeleton_instance_term_Comparable c)
           (skeleton_instance_flow_Comparable c).

Let concrete_state c : Type :=
  concrete_state (skeleton_variable_term (S c)) (skeleton_variable_flow (S c)).

Definition concrete_triple_set : Type := triple_set constructor basic_term value value.

Let wellformed_set_of_triples
     : concrete_triple_set -> Prop :=
  wellformed_set_of_triples in_sort out_sort constructor_signature basic_term_sort
                            value_sort value_sort.

Let typed_environment_read_variable c : concrete_state c -> variable_skeleton (S c) -> option value :=
  typed_environment_read_variable value_term value_flow (variable_flow := _).

Let typed_environment_write_variables c
    : concrete_state c -> list (variable_skeleton (S c)) -> list value -> option (concrete_state c) :=
  typed_environment_write_variables value_term value_flow value_rect (variable_flow := _).

(** The concrete initial environment associates [X_sigma] to the starting
  value [sigma] of the current triple, and each term variables of the
  current rule to their instantiation in the current triple. **)
Definition concrete_initial_state c sigma ts :=
  typed_environment_write_variables typed_environment_empty
    (_X_ X_sigma :: map (@X_t _ _) (skeleton_term_variables (S c)))
    (value_flow sigma :: map value_term ts).

(** Although [concrete_initial_state] is an option type,
  it is actually defined. **)
Lemma concrete_initial_state_defined : forall c (sigma : flow_value) ts,
  length (skeleton_term_variables (S c)) = length ts ->
  exists Sigma, concrete_initial_state c sigma ts = Some Sigma.
Proof.
  introv E. unfolds concrete_initial_state. simpl.
  generalize (typed_environment_write_flow (typed_environment_empty : concrete_state c)
                                           X_sigma sigma) as Sigma.
  gen E. generalize (skeleton_term_variables (S c)) as ts'. induction ts; introv E; introv.
  - eexists. destruct ts'; tryfalse. reflexivity.
  - destruct ts' as [|ts0 ts']; tryfalse. forwards~ (Sigma'&ESigma): (rm IHts) ts'.
    eexists. apply ESigma.
Qed.

Lemma concrete_initial_state_defined_inv : forall c (sigma : flow_value) ts Sigma,
  concrete_initial_state c sigma ts = Some Sigma ->
  length (skeleton_term_variables (S c)) = length ts.
Proof.
  introv E. do 2 unfolds in E.
  forwards El: typed_environment_read_write_variables_length E. rew_list in El.
  repeat rewrite length_map in El. inverts~ El.
Qed.

(** The domain of the initial environment is exactly [X_sigma] and each
  term variables associated with the current constructor [c]. **)
Lemma concrete_initial_state_domain : forall c sigma ts Sigma,
  concrete_initial_state c sigma ts = Some Sigma ->
  typed_environment_domain value_term value_flow Sigma =
    \{ _X_ X_sigma }
    \u set_st (fun x => exists x0, x = X_t x0 /\ Mem x0 (skeleton_term_variables (S c))).
Proof.
  introv E. do 2 unfolds in E. rewrites~ >> typed_environment_domain_write_variables E.
  rewrites~ typed_environment_domain_empty; [| apply~ closed_term_free_Inhab ].
  rewrite for_set_union_empty_l. rewrite~ to_set_cons. fequals.
  rewrite set_ext_eq. intro x. unfolds to_set. repeat rewrite in_set_st_eq.
  rewrite Mem_mem. rewrite map_mem. iff (x'&A&B); exists x'; rewrite Mem_mem in *; splits~.
Qed.

(** The following two lemmas specify what is the value mapped from particular
  variables in the initial environment. **)

Lemma concrete_initial_state_X_sigma : forall c sigma ts Sigma,
  concrete_initial_state c sigma ts = Some Sigma ->
  typed_environment_read_flow Sigma X_sigma = Some sigma.
Proof.
  introv E. unfolds in E. simpl in E.
  gen Sigma. generalize (typed_environment_empty : concrete_state c) as Gamma0,
    (skeleton_term_variables (S c)) as l2.
  induction ts using list_ind_last; introv E.
  - destruct l2 as [|t l2]; inverts E. apply~ typed_environment_read_write_flow_eq.
  - forwards [El2|(t&l2'&El2)]: last_case l2; rewrite El2 in E; rew_map in E.
    + destruct map in E; inverts* E.
    + unfolds in E.
      destruct (typed_environment_write_variables
        (typed_environment_write_flow Gamma0 X_sigma sigma)
        (map (X_t (variable_flow:=skeleton_variable_flow (S c))) l2') (map value_term ts)) eqn: E1.
      * rewrites >> typed_environment_write_variables_last in E; [ apply E1 |].
        forwards~ E2: IHts E1. inverts E. rewrite~ typed_environment_read_flow_write_term.
      * rewrites >> typed_environment_write_variables_last_None in E; [ apply E1 |]. inverts E.
Qed.

Lemma concrete_initial_state_X_t : forall c sigma ts Sigma x sx n,
  No_duplicates (skeleton_term_variables (S c)) ->
  concrete_initial_state c sigma ts = Some Sigma ->
  Nth n (skeleton_term_variables (S c)) x ->
  Nth n ts sx ->
  typed_environment_read_term Sigma x = Some sx.
Proof.
  introv ND E N1 N2. do 2 unfolds in E. forwards~ E': typed_environment_read_write_variables_Nth E.
  - constructors.
    + introv M. forwards (t&M'&E'): Mem_map_inv (rm M). inverts* E'.
    + apply~ Nth_No_duplicates. introv N1' N2'.
      forwards (t1&N1''&E1'): map_Nth_inv (rm N1'). forwards (t2&N2''&E2'): map_Nth_inv (rm N2').
      substs. inverts E2'. applys~ No_duplicates_Nth ND N1'' N2''.
  - apply~ Nth_next. applys map_Nth N1.
  - apply~ Nth_next. applys map_Nth N2.
  - simpl in E'. destruct typed_environment_read_term; inverts~ E'.
Qed.

(** The immediate consequence builds new triples from a set of triples.
  Intuitively, if the concrete semantics were built using rules, the
  immediate consequence would apply one more level of rules from the
  already built derivations. **)
Inductive immediate_consequence (T : concrete_triple_set) : concrete_triple_set :=
  | immediate_consequence_intro : forall c V sigma t v ts s (Sigma Sigma' : concrete_state c),
    closed_term_free_eq t (closed_term_free_constructor V c ts) ->
    sort_of_term_closed t (sort_program s) ->
    value_sort (value_flow sigma) (in_sort s) ->
    concrete_initial_state c sigma ts = Some Sigma ->
    interpretation_skeleton (concrete c) (skeleton_skeleton (S c)) (Sigma, T) Sigma' ->
    typed_environment_read_variable Sigma' (_X_ X_o) = Some v ->
    immediate_consequence T sigma t v
  .

(** The immediate consequence is compatible with the equality over closed terms. **)
Lemma immediate_consequence_closed_term_free_eq : forall T sigma t1 t2 v,
  immediate_consequence T sigma t1 v ->
  closed_term_free_eq t1 t2 ->
  immediate_consequence T sigma t2 v.
Proof.
  introv H E. inverts H as E1 S1 S2 E2 D E3. constructors*.
  - applys~ closed_term_free_eq_trans E1. applys~ closed_term_free_eq_sym E.
  - applys~ sort_of_term_closed_eq S1 E.
Qed.

(** This lemma states that the immediate consequence, seen as a function from
  triple sets to triple sets, is monotonic. **)
(** It corresponds to Lemma 4.3 of the paper. **)
Lemma immediate_consequence_weaken : forall (T1 T2 : concrete_triple_set) sigma t v,
  (forall sigma t v, T1 sigma t v -> T2 sigma t v) ->
  immediate_consequence T1 sigma t v ->
  immediate_consequence T2 sigma t v.
Proof.
  introv I D. inverts D as E1 S1 S2 E2 D E3. applys~ immediate_consequence_intro E1 S1 S2 E2 E3.
  gen D. generalize (skeleton_skeleton (S c)). clear - I. intro s.
  eapply (skeleton_ind' (fun b => forall Sigma Sigma',
      interpretation_bone (concrete c) b (Sigma, T1) (Sigma', T1) ->
      interpretation_bone (concrete c) b (Sigma, T2) (Sigma', T2))
    (Q := fun s => forall Sigma Sigma',
      _ s (Sigma, T1) Sigma' -> (_ s (Sigma, T2) Sigma' : Prop))).
  - introv D. repeat (inverts D as D; tryfalse; econstructor; autos~).
    + applys~ I D.
    + autos*.
  - introv D. repeat (inverts D as D; constructors~).
  - introv F D. inverts D as F' D. constructors~.
    + forwards Fc: Forall2_Forall (rm F) (Forall2_swap (rm F')).
      applys Forall2_weaken (Forall2_swap (rm Fc)).
      introv (I1&I2) E. applys~ I1 I2.
    + inverts D as D. constructors~.
  - introv D. repeat (inverts D as D; constructors~).
  - introv IH F D. inverts D as D1 D2. destruct Sigma'1 as [Sigma1' T1'].
    lets: (triple_based_interpretation_transmit_set D1). substs. constructors.
    + applys~ IH D1.
    + applys~ F D2.
Qed.

Lemma immediate_consequence_only_use_finite_number_of_triples : forall T sigma t o,
  immediate_consequence T sigma t o ->
  exists L,
    (forall sigma t o, list_to_triple_set L sigma t o -> T sigma t o)
    /\ immediate_consequence (list_to_triple_set L) sigma t o.
Proof.
  introv C. inverts C as C St Sv E1 D E2.
  forwards (L&I&D'): triple_based_interpretation_only_use_finite_number_of_triples_skeleton
                       (concrete c) D.
  exists L. splits~. constructors*.
Qed.

(** The concrete and well-formedness initial environments are consistent. **)
Lemma WFconcreteOKout_initial : forall c s ss Gamma sigma ts Sigma D,
  wellformedness_initial_environment in_sort S c s ss = Some Gamma ->
  concrete_initial_state c sigma ts = Some Sigma ->
  No_duplicates (skeleton_term_variables (S c)) ->
  value_sort sigma (in_sort s) ->
  Forall2 sort_of_term_closed ts ss ->
  WFconcreteOKout (Gamma, D) Sigma.
Proof.
  introv E1 E2 ND VS F.
  asserts ED: (typed_environment_domain sort_term sort_flow Gamma
               = typed_environment_domain value_term value_flow Sigma).
  { unfold term_sort, sort, sort_term, sort_flow. unfolds in E1.
    rewrites~ >> wellformedness_initial_environment_domain E1; [ apply~ term_sort_Inhab |].
    rewrites* >> concrete_initial_state_domain E2. }
  splits~. introv E3 E4. asserts I: (x \in typed_environment_domain value_term value_flow Sigma).
  { unfolds typed_environment_domain. rewrite in_set_st_eq. rewrite E3. discriminate. }
  rewrites~ >> concrete_initial_state_domain E2 in I. rewrite in_union_eq in I. inverts I as I.
  - rewrite in_single_eq in I. substs.
    simpl in E3. rewrites~ >> concrete_initial_state_X_sigma E2 in E3. inverts E3.
    simpl in E4. unfolds in E1. unfolds term_sort, sort, sort_term, sort_flow.
    rewrites~ >> wellformedness_initial_environment_X_sigma E1 in E4. inverts~ E4.
  - rewrite in_set_st_eq in I. lets (xt&Ext&M): (rm I). lets (n&N): Mem_Nth (rm M). substs.
    forwards (t&Nt): length_Nth_lt.
    { rewrites~ <- >> concrete_initial_state_defined_inv E2. applys~ Nth_lt_length N. }
    forwards (s'&Ns'): length_Nth_lt.
    { rewrites~ <- >> wellformedness_initial_environment_defined_inv E1. applys~ Nth_lt_length N. }
    forwards~ E5: concrete_initial_state_X_t ND E2 N Nt.
    unfolds in E3. rewrite E5 in E3. inverts E3.
    forwards~ E6: wellformedness_initial_environment_X_t ND E1 N Ns'.
    unfolds in E4. unfolds term_sort. rewrite E6 in E4. inverts E4.
    constructors. rewrite Forall2_iff_forall_Nth in F. applys~ F; [ apply Nt | apply Ns' ].
Qed.

(** This lemma states that the immediate consequence preserves the well-formedness of triple sets. **)
(** It corresponds to Lemma 4.4 of the paper. **)
Lemma immediate_consequence_wellformed_set_of_triples : forall (T : concrete_triple_set),
  wellformed_set_of_triples T ->
  wellformed_set_of_triples (immediate_consequence T).
Proof.
  introv OK IC. inverts IC as E1 S1 S2 E2 D E3. forwards (ts'&E4): closed_term_free_constructor_inv E1.
  forwards (ts2&Ets2&Fts2): closed_term_free_to_variable_term_closed_term_free_constructor E4.
  do 3 eexists. splits*.
  forwards~ (ND&ss&Gamma&Gamma'&D1&D2&E5&F1&E6&D'&E7):
    skeletal_semantics_wellformed_instantiation S_wellformed c ts2.
  { applys sort_of_term_same_structure (fun b1 b2 : basic_term => b1 = b2) S1.
    - introv ?. substs~.
    - autos~.
    - rewrite E4. constructors~. apply~ Fts2. }
  forwards (E8&F2): WFconcrete_universally_consistent D' D.
  { splits~. applys~ WFconcreteOKout_initial E5 E2 ND S2.
    rewrite Forall2_iff_forall_Nth in F1. lets (El&F1'): (rm F1).
    asserts Fts: (Forall2 (fun t =>
      same_structure (fun _ _ => False) (fun b1 b2 : basic_term => b1 = b2)
                     (closed_term_free_term t)) ts ts2).
    { clear - E1 Ets2. destruct t as [X t C].
      forwards (ts'&Ets'): closed_term_free_constructor_inv E1.
      forwards (ts3&Ets3&F): closed_term_free_to_variable_term_closed_term_free_constructor Ets'.
      rewrite Ets3 in Ets2. simpls. inverts Ets2.
      unfolds in E1. simpl in E1. substs. inverts E1 as F2.
      rewrite Forall2_iff_forall_Nth in F. lets (El&F'): (rm F).
      rewrite Forall2_iff_forall_Nth in F2. lets (El2&F2'): (rm F2).
      rewrite Forall2_iff_forall_Nth. splits~.
      - rewrite <- El. rewrite El2. rewrite~ length_map.
      - introv N1 N2. forwards (t'&N3): length_Nth_lt.
        { rewrite El. applys~ Nth_lt_length N2. }
        forwards S1: F' N3 N2. forwards S2: F2' N3.
        { applys~ map_Nth N1. }
        applys~ same_structure_trans (fun (_ : closed_term_free_variable_term v1) (_ : X) => False)
                                     (fun b1 b2 : basic_term => b1 = b2) S1.
        { simpl. intros. substs~. }
        apply same_structure_swap_sym in S2.
        forwards~ S3: same_structure_True_closed_False closed_term_free_to_variable_term_closed S2.
        applys~ same_structure_trans closed_term_free_to_variable_term_spec S3.
        simpl. intros. substs~. }
    rewrite Forall2_iff_forall_Nth in Fts. lets (El'&Fts'): (rm Fts).
    rewrite Forall2_iff_forall_Nth. splits~.
    - unfolds term_sort. rewrite <- El. apply~ El'.
    - introv N1 N2. forwards (t'&N3): length_Nth_lt.
      { rewrite <- El'. applys~ Nth_lt_length N1. }
      forwards Ss: F1' N3 N2. forwards St: Fts' N1 N3.
      apply same_structure_swap_sym in St.
      applys~ sort_of_term_same_structure Ss St.
      simpl. intros. substs~. }
  unfolds typed_environment_read_variable. applys~ F2 E3 E7.
Qed.

(** The concrete semantics recursively applies the immediate consequence. **)
(** To avoid Coq refusing the definition due to a non-strictly positive
  recursive occurrence, we introduce an indirection in the definition. **)
Inductive concrete_semantics : concrete_triple_set :=
  | concrete_semantics_intro : forall (T : concrete_triple_set) sigma t o,
    (forall sigma t o, T sigma t o -> concrete_semantics sigma t o) ->
    immediate_consequence T sigma t o ->
    concrete_semantics sigma t o.

Lemma concrete_semantics_fixpoint_forwards : forall sigma t o,
  immediate_consequence concrete_semantics sigma t o ->
  concrete_semantics sigma t o.
Proof. introv D. applys~ concrete_semantics_intro concrete_semantics. Qed.

Lemma concrete_semantics_fixpoint_backwards : forall sigma t o,
  concrete_semantics sigma t o ->
  immediate_consequence concrete_semantics sigma t o.
Proof. introv D. inverts D as I D. applys~ immediate_consequence_weaken D. Qed.

(** The concrete semantics is a fixpoint of the immediate consequence. **)
Lemma concrete_semantics_fixpoint :
  immediate_consequence concrete_semantics = concrete_semantics.
Proof.
  apply func_ext_dep. introv. extens. introv. iff C.
  - applys~ concrete_semantics_fixpoint_forwards C.
  - applys~ concrete_semantics_fixpoint_backwards C.
Qed.

(** The concrete semantics is the least fixpoint of the immediate consequence. **)
(** The following lemma corresponds to the definition 4.6 of the paper. **)
Lemma concrete_immediate_smallest_fixpoint : forall (T : concrete_triple_set) sigma t o,
  (forall sigma t o, immediate_consequence T sigma t o -> T sigma t o) ->
  concrete_semantics sigma t o ->
  T sigma t o.
Proof.
  introv I D. induction D as [T' sigma t o I' IH C].
  apply~ I. applys~ immediate_consequence_weaken C.
Qed.

(** An alternative definition of the concrete semantics, based on iterations
  of the immediate consequece. **)
Definition concrete_semantics_alternative : concrete_triple_set := fun sigma t o =>
  exists n,
    applyn n immediate_consequence (fun _ _ _ => False) sigma t o.

Lemma concrete_semantics_alternative_backwards : forall sigma t o,
  concrete_semantics_alternative sigma t o ->
  concrete_semantics sigma t o.
Proof.
  introv (n&I). gen sigma t o. induction n; introv I.
  - inverts~ I.
  - rewrite applyn_fix in I. rewrite applyn_altern in I. constructors*.
Qed.

Lemma iter_immediate_consequence_weaken_immediate : forall n sigma t o,
  applyn n immediate_consequence (fun _ _ _ => False) sigma t o ->
  applyn (1 + n) immediate_consequence (fun _ _ _ => False) sigma t o.
Proof.
  induction n; introv D.
  - inverts~ D.
  - rewrite applyn_fix in *. rewrite applyn_altern in *. applys~ immediate_consequence_weaken D.
Qed.

Lemma iter_immediate_consequence_weaken : forall n m sigma t o,
  n <= m ->
  applyn n immediate_consequence (fun _ _ _ => False) sigma t o ->
  applyn m immediate_consequence (fun _ _ _ => False) sigma t o.
Proof.
  introv I D. gen sigma t o. induction~ I; introv D.
  rewrite applyn_fix. rewrite applyn_altern. applys~ immediate_consequence_weaken IHI.
  applys~ iter_immediate_consequence_weaken_immediate D.
Qed.

(** This lemma corresponds to the hard part of the proof of Lemma 4.5 of the paper. **)
Lemma concrete_semantics_alternative_increase : forall sigma t o,
  immediate_consequence concrete_semantics_alternative sigma t o ->
  concrete_semantics_alternative sigma t o.
Proof.
  introv C. unfolds concrete_semantics_alternative.
  forwards (L&I&D): immediate_consequence_only_use_finite_number_of_triples (rm C).
  asserts (n&I'): (exists n, forall sigma t o,
    list_to_triple_set L sigma t o -> applyn n immediate_consequence (fun _ _ _ => False) sigma t o).
  { clear D sigma t o. induction L as [|[[sigma t] o] L].
    - exists 0. introv M. inverts~ M.
    - lets (n&I'): (rm IHL).
      + introv M. apply~ I. applys~ Mem_next M.
      + forwards (n'&I''): I Mem_here. exists (max n n'). introv M. inverts M as M.
        * applys~ iter_immediate_consequence_weaken I''. apply~ PeanoNat.Nat.le_max_r.
        * applys~ iter_immediate_consequence_weaken I'. apply~ PeanoNat.Nat.le_max_l. }
  eexists. rewrite applyn_fix. rewrite applyn_altern. applys~ immediate_consequence_weaken I' D.
Qed.

Lemma concrete_semantics_alternative_forwards : forall sigma t o,
  concrete_semantics sigma t o ->
  concrete_semantics_alternative sigma t o.
Proof.
  introv C. applys~ concrete_immediate_smallest_fixpoint C.
  apply~ concrete_semantics_alternative_increase.
Qed.

(** This lemma states that both concrete semantics are the same. **)
(** It corresponds to Lemma 4.7 of the paper. **)
Lemma concrete_semantics_alternative_concrete_semantics :
  concrete_semantics = concrete_semantics_alternative.
Proof.
  apply func_ext_dep. introv. extens. introv. iff C.
  - applys~ concrete_semantics_alternative_forwards C.
  - applys~ concrete_semantics_alternative_backwards C.
Qed.

(** This lemma states that the concrete semantics is well-formed. **)
(** It corresponds to Lemma 4.8 of the paper. **)
Lemma concrete_semantics_wellformed : wellformed_set_of_triples concrete_semantics.
Proof.
  introv I. induction I as [T sigma t o I IH C].
  applys immediate_consequence_wellformed_set_of_triples C.
  introv I'. applys~ IH I'.
Qed.

End Skeleton.

End Sorts.

Arguments concrete_semantics [_ _ _ _] _ [_ _ _] _ _ _ _ _ {_ _}.


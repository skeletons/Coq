(** Definitions of interpretations. **)

Set Implicit Arguments.

Require Import ExtLib.Data.Member ExtLib.Data.HList ExtExtLib.
Require Export Skeletons.

(** * Interpretations **)

(** Interpretations are defined at the level of skeletal instances
  (eventually using a globally computed set of triples).  We thus
  parametrise interpretations by its local parameters. **)

Section Interpretation.

Variables constructor filter : Type.
Variable basic_term : Type.
Variables variable_term variable_flow : Type.

Let variable : Type := variable variable_term variable_flow.
Let variable_flow_extended : Type := variable_flow_extended variable_flow.
Let term : Type := term constructor basic_term variable_term.
Let bone : Type := bone constructor filter variable_term variable_flow.
Let skeleton : Type := skeleton constructor filter variable_term variable_flow.
Let closed_term_free : Type := closed_term_free constructor basic_term.
Let basic_free_term : Type := basic_free_term constructor variable_term.

(** ** General Definition **)

(** The following record is what users need to provide to define an
  interpretation.
  Interpretations are only defined on the basic blocks of skeletons:
  the three bones (hooks, filters, and branches), as well as the empty
  skeleton.  The interpretation can then be generalised to whole
  skeletons using the definition [interpretation_skeleton].  More
  generally, interpretations of skeletons aim at focussing on local
  definitions and properties to get a global property, without having
  to deal with the complexity of particular skeletons. **)

Record interpretation := make_interpretation {
    interpretation_state : Type ;
    interpretation_result : Type ;
    interpretation_empty_skeleton :
      interpretation_state -> interpretation_result -> Prop ;
    interpretation_hook :
      variable_flow_extended -> basic_free_term -> variable_flow_extended ->
      interpretation_state -> interpretation_state -> Prop ;
    interpretation_filter :
      filter -> list variable -> list variable ->
      interpretation_state -> interpretation_state -> Prop ;
    interpretation_merge :
      (** In contrary to the paper, we use a list instead of a partial function. **)
      list (option interpretation_result) ->
      list variable ->
      interpretation_state -> interpretation_state -> Prop
  }.

(** From this record, we define the interpretation
  [interpretation_skeleton] of skeletons. **)

Inductive interpretation_bone (I : interpretation) :
    bone -> interpretation_state I -> interpretation_state I -> Prop :=
  | interpretation_bone_hook : forall x1 t x2 Sigma Sigma',
    interpretation_hook I x1 t x2 Sigma Sigma' ->
    interpretation_bone I (H x1 t x2) Sigma Sigma'
  | interpretation_bone_filter : forall f xs ys Sigma Sigma',
    interpretation_filter I f xs ys Sigma Sigma' ->
    interpretation_bone I (F f xs ys) Sigma Sigma'
  | interpretation_bone_branches : forall Ss V O Sigma Sigma',
    Forall2 (fun Oi S =>
      forall Oiv,
        Oi = Some Oiv ->
        interpretation_skeleton I S Sigma Oiv) O Ss ->
    interpretation_merge I O V Sigma Sigma' ->
    interpretation_bone I (B Ss V) Sigma Sigma'

with interpretation_skeleton (I : interpretation) :
    skeleton -> interpretation_state I -> interpretation_result I -> Prop :=
  | interpretation_skeleton_nil : forall Sigma O,
    interpretation_empty_skeleton I Sigma O ->
    interpretation_skeleton I nil Sigma O
  | interpretation_skeleton_cons : forall b S Sigma Sigma' O,
    interpretation_bone I b Sigma Sigma' ->
    interpretation_skeleton I S Sigma' O ->
    interpretation_skeleton I (b :: S) Sigma O
  .

(** As the interpretation of a bone is just a call to the corresponding
  function of the interpretation record [I], we can prove all the
  following local equivalences, which happenned to be handy when dealing
  with computable content. **)

Lemma interpretation_bone_H_eq : forall I x t y,
  interpretation_bone I (H x t y) = interpretation_hook I x t y.
Proof.
  introv. extens. intros Sigma1 Sigma2. iff II.
  - inverts~ II.
  - constructors~.
Qed.

Lemma interpretation_bone_F_eq : forall I f xs ys,
  interpretation_bone I (F f xs ys) = interpretation_filter I f xs ys.
Proof.
  introv. extens. intros Sigma1 Sigma2. iff II.
  - inverts~ II.
  - constructors~.
Qed.

Lemma interpretation_skeleton_empty_eq : forall I,
  interpretation_skeleton I nil = interpretation_empty_skeleton I.
Proof.
  introv. extens. intros Sigma1 Sigma2. iff II.
  - inverts~ II.
  - constructors~.
Qed.


(** ** Triple-based Variant **)

(** An alternative presentation exists for triple-based interpretations.
  Triple-based interpretations assume the existence of a set [T] of triples
  from where to pick the hooks.  This is a frequent pattern: for instance,
  the concrete and abstract interpretations fall into this pattern.
  The functions of the record are very similar than in the [interpretation]
  record, but are more adapted to these kinds of interpretations. **)

(** The hook case is divided into two cases. The usual case is
  [triple_based_interpretation_hook]: it picks a triple from the
  triple set [T] and matches it against the proposed returned value.
  There are some interpretations which can under some conditions
  “shortcut” the set [T] and directly produce a return value (see
  [interpretations/Abstract.v] for such an example): we provide
  [triple_based_interpretation_hook_shortcut] for such cases.  In most
  triple-based interpretations though, this function will just be
  [fun _ _ _ _ _ => False]. **)

Record triple_based_interpretation := make_triple_based_interpretation {
    triple_based_interpretation_state : Type ;
    triple_based_interpretation_triple_state : Type ;
    triple_based_interpretation_triple_result : Type ;
    triple_based_interpretation_hook :
      variable_flow_extended -> basic_free_term -> variable_flow_extended ->
      triple_based_interpretation_triple_state ->
      closed_term_free ->
      triple_based_interpretation_triple_result ->
      triple_based_interpretation_state -> triple_based_interpretation_state -> Prop ;
    triple_based_interpretation_hook_shortcut :
      variable_flow_extended -> basic_free_term -> variable_flow_extended ->
      triple_based_interpretation_state -> triple_based_interpretation_state -> Prop ;
    triple_based_interpretation_filter :
      filter -> list variable -> list variable ->
      triple_based_interpretation_state -> triple_based_interpretation_state -> Prop ;
    triple_based_interpretation_merge :
      list (option triple_based_interpretation_state) ->
      list variable ->
      triple_based_interpretation_state -> triple_based_interpretation_state -> Prop
  }.

(** This kind of interpretation manipulates triples sets of the following form. **)
Definition triple_based_interpretation_set_of_triples I :=
  triple_based_interpretation_triple_state I ->
  closed_term_free ->
  triple_based_interpretation_triple_result I -> Prop.

(** We can go from triple-based interpretation to usual interpretation using
  [triple_based_interpretation_to_interpretation], defined as follows. **)

Definition triple_based_interpretation_state_interpretation I : Type :=
  triple_based_interpretation_state I
  * triple_based_interpretation_set_of_triples I.

Definition triple_based_interpretation_result_interpretation I : Type :=
  triple_based_interpretation_state I.

Inductive triple_based_interpretation_empty_skeleton_interpretation (I : triple_based_interpretation) :
    triple_based_interpretation_state_interpretation I ->
    triple_based_interpretation_result_interpretation I -> Prop :=
  | triple_based_interpretation_empty_skeleton_interpretation_intro : forall Sigma T,
    triple_based_interpretation_empty_skeleton_interpretation (Sigma, T) Sigma
  .

Inductive triple_based_interpretation_hook_interpretation (I : triple_based_interpretation) :
    variable_flow_extended -> basic_free_term -> variable_flow_extended ->
    triple_based_interpretation_state_interpretation I ->
    triple_based_interpretation_state_interpretation I -> Prop :=
  | triple_based_interpretation_hook_interpretation_pick : forall x1 t x2 Sigma Sigma' sigma t0 v
      (T : triple_based_interpretation_set_of_triples I),
    T sigma t0 v ->
    triple_based_interpretation_hook I x1 t x2 sigma t0 v Sigma Sigma' ->
    triple_based_interpretation_hook_interpretation x1 t x2 (Sigma, T) (Sigma', T)
  | triple_based_interpretation_hook_interpretation_shortcut : forall x1 t x2 Sigma Sigma'
      (T : triple_based_interpretation_set_of_triples I),
    triple_based_interpretation_hook_shortcut I x1 t x2 Sigma Sigma' ->
    triple_based_interpretation_hook_interpretation x1 t x2 (Sigma, T) (Sigma', T)
  .

Inductive triple_based_interpretation_filter_interpretation (I : triple_based_interpretation) :
    filter -> list variable -> list variable ->
    triple_based_interpretation_state_interpretation I ->
    triple_based_interpretation_state_interpretation I -> Prop :=
  | triple_based_interpretation_filter_interpretation_intro : forall f xs ys Sigma Sigma' T,
    triple_based_interpretation_filter I f xs ys Sigma Sigma' ->
    triple_based_interpretation_filter_interpretation f xs ys (Sigma, T) (Sigma', T)
  .

Inductive triple_based_interpretation_merge_interpretation (I : triple_based_interpretation) :
    list (option (triple_based_interpretation_result_interpretation I)) ->
    list variable ->
    triple_based_interpretation_state_interpretation I ->
    triple_based_interpretation_state_interpretation I -> Prop :=
  | triple_based_interpretation_merge_interpretation_intro : forall O V Sigma Sigma' T,
    triple_based_interpretation_merge I O V Sigma Sigma' ->
    triple_based_interpretation_merge_interpretation O V (Sigma, T) (Sigma', T)
  .


Definition triple_based_interpretation_to_interpretation I := {|
    interpretation_state := triple_based_interpretation_state_interpretation I ;
    interpretation_result := triple_based_interpretation_result_interpretation I ;
    interpretation_empty_skeleton := @triple_based_interpretation_empty_skeleton_interpretation I ;
    interpretation_hook := @triple_based_interpretation_hook_interpretation I ;
    interpretation_filter := @triple_based_interpretation_filter_interpretation I ;
    interpretation_merge := @triple_based_interpretation_merge_interpretation I
  |}.

(** We make this function a coercion.  This enables us to transparently manipulate
  triple-based interpretations. **)
Coercion triple_based_interpretation_to_interpretation :
  triple_based_interpretation >-> interpretation.

(** Triple sets are not changed during the evaluation of bones. **)
Lemma triple_based_interpretation_transmit_set : forall I b Sigma1 Sigma2 T1 T2,
  interpretation_bone (I : triple_based_interpretation) b (Sigma1, T1) (Sigma2, T2) ->
  T1 = T2.
Proof. introv D. do 2 inverts D as D D'; substs~; inverts~ D'. Qed.

(** Adding triples to the triple set doesn’t remove results from the interpretation. **)
Lemma triple_based_interpretation_weaken : forall I (T1 T2 : _ -> _ -> _ -> Prop),
  (forall sigma t o, T1 sigma t o -> T2 sigma t o) ->
  (forall b sigma sigma' T',
    interpretation_bone (I : triple_based_interpretation) b (sigma, T1) (sigma', T') ->
    interpretation_bone (I : triple_based_interpretation) b (sigma, T2) (sigma', T2))
  /\ (forall S sigma o,
    interpretation_skeleton (I : triple_based_interpretation) S (sigma, T1) o ->
    interpretation_skeleton (I : triple_based_interpretation) S (sigma, T2) o).
Proof.
  introv IT. apply bone_and_skeleton_ind'.
  - introv D. inverts D as D. constructors~. inverts D as D1 D2.
    + applys~ triple_based_interpretation_hook_interpretation_pick D2.
    + applys~ triple_based_interpretation_hook_interpretation_shortcut D1.
  - introv D. inverts D as D. constructors~. inverts D as D. constructors~.
  - introv F D. inverts D as F2 D. rewrite Forall_iff_forall_mem in F. constructors~.
    + rewrite Forall2_iff_forall_Nth in *. lets (El&F2'): (rm F2). splits.
      * apply El.
      * introv N1 N2 E. forwards~ D': F2' N1 N2 E. applys~ F D'. applys~ Nth_mem N2.
    + inverts D as D. constructors~.
  - introv D. inverts D as D. constructors~. inverts~ D. constructors~.
  - introv F IH D. inverts D as D1 D2. destruct Sigma' as [Sigma' T].
    forwards: triple_based_interpretation_transmit_set D1. substs. constructors~.
    + applys~ F D1.
    + applys~ IH D2.
Qed.

Lemma triple_based_interpretation_weaken_bone : forall I b (T1 T2 : _ -> _ -> _ -> Prop)
  sigma sigma' T',
  (forall sigma t o, T1 sigma t o -> T2 sigma t o) ->
  interpretation_bone (I : triple_based_interpretation) b (sigma, T1) (sigma', T') ->
  interpretation_bone (I : triple_based_interpretation) b (sigma, T2) (sigma', T2).
Proof. introv IT. applys triple_based_interpretation_weaken IT. Qed.

Lemma triple_based_interpretation_weaken_skeleton : forall I S (T1 T2 : _ -> _ -> _ -> Prop) sigma o,
  (forall sigma t o, T1 sigma t o -> T2 sigma t o) ->
  interpretation_skeleton (I : triple_based_interpretation) S (sigma, T1) o ->
  interpretation_skeleton (I : triple_based_interpretation) S (sigma, T2) o.
Proof. introv IT. applys triple_based_interpretation_weaken IT. Qed.

(** Sets of the form [A -> B -> C -> Prop] are cumbersome to define.
  This definition enables us to define them from lists. **)
Definition list_to_triple_set A B C L : A -> B -> C -> Prop :=
  fun sigma t o => Mem (sigma, t, o) L.

(** For each interpretation, only a finite number of triple is actually used.
  This lemma is crucial to prove the continuity of immediate consequences
  (see [interpretations/Concrete.v] for such an instance). **)
Lemma triple_based_interpretation_only_use_finite_number_of_triples : forall I T,
  (forall b sigma sigma' T',
     interpretation_bone (I : triple_based_interpretation) b (sigma, T) (sigma', T') ->
     exists L,
       interpretation_bone (I : triple_based_interpretation) b
                           (sigma, list_to_triple_set L) (sigma', list_to_triple_set L)
       /\ (forall sigma t o, list_to_triple_set L sigma t o -> T sigma t o))
  /\ (forall S sigma o,
     interpretation_skeleton (I : triple_based_interpretation) S (sigma, T) o ->
     exists L,
       interpretation_skeleton (I : triple_based_interpretation) S (sigma, list_to_triple_set L) o
       /\ (forall sigma t o, list_to_triple_set L sigma t o -> T sigma t o)).
Proof.
  introv. apply bone_and_skeleton_ind'.
  - introv D. inverts D as D. inverts D as D1 D2.
    + refine (ex_intro _ [_] _). splits~.
      * constructors~. applys~ triple_based_interpretation_hook_interpretation_pick D2. apply Mem_here.
      * introv I'. repeat inverts I' as I'; autos~.
    + refine (ex_intro _ nil _). splits~.
      * constructors~. applys~ triple_based_interpretation_hook_interpretation_shortcut D1.
      * introv I'. inverts~ I'.
  - introv D. refine (ex_intro _ nil _). do 2 inverts D as D. splits.
    + do 2 constructors~.
    + introv I'. inverts~ I'.
  - introv F D. inverts D as F2 D. inverts D as D. rewrite Forall_iff_forall_mem in F.
    rewrite Forall2_iff_forall_Nth in F2. lets (El&F2'): (rm F2).
    asserts F': (Forall2 (fun Oi S => exists L, (forall Oiv,
      Oi = Some Oiv ->
      interpretation_skeleton I S (sigma, T') Oiv ->
      interpretation_skeleton I S (sigma, list_to_triple_set L) Oiv)
      /\ (forall sigma t o, list_to_triple_set L sigma t o -> T' sigma t o)) O Ss).
    { rewrite Forall2_iff_forall_Nth. splits~. introv N1 N2. destruct v1.
      - forwards~ D1: F2' N1 N2. forwards~ (L&D2&OKL): F D1.
        { applys~ Nth_mem N2. }
        exists L. splits~. introv E D3. inverts~ E.
      - refine (ex_intro _ nil _). splits~.
        + introv E. inverts~ E.
        + introv I'. inverts~ I'. }
    forwards (L&F3): Forall2_exists (rm F').
    rewrite Forall3_iff_forall_Nth in F3. lets (El1&El2&F3'): (rm F3).
    exists (concat L). splits~.
    + constructors~.
      * rewrite Forall2_iff_forall_Nth. splits*. introv N1 N2 E. substs.
        forwards~ D2: F2' N1 N2. forwards (l&N3): length_Nth_lt.
        { rewrite <- El2. applys~ Nth_lt_length N2. }
        forwards~ D3: F3' N1 N2 N3. applys~ triple_based_interpretation_weaken_skeleton D3.
        introv I'. unfolds in I'. unfolds. rewrite Mem_mem in *. rewrite concat_mem.
        exists l. splits~. applys~ Nth_mem N3.
      * constructors~.
    + introv I'. unfolds in I'. rewrite Mem_mem in I'. apply concat_mem in I'.
      lets (L'&M1&M2): (rm I'). forwards (n&N): mem_Nth (rm M1). forwards (o1&N2): length_Nth_lt.
      { rewrite El2. applys~ Nth_lt_length N. }
      forwards (o2&N3): length_Nth_lt.
      { rewrite El1. applys~ Nth_lt_length N2. }
      applys~ F3' N3 N2 N. unfolds. rewrite~ Mem_mem.
  - introv D. inverts D as D. inverts D. refine (ex_intro _ nil _). splits~.
    + do 2 constructors~.
    + introv I'. inverts~ I'.
  - introv IH1 IH2 D. inverts D as D1 D2. destruct Sigma' as [Sigma' T'].
    forwards: triple_based_interpretation_transmit_set D1. substs.
    forwards (L1&D3): IH1 D1. forwards (L2&D4): IH2 D2. exists (L1 ++ L2). splits~.
    + constructors~.
      * applys~ triple_based_interpretation_weaken_bone D3.
        introv I'. unfolds in I'. unfolds. rewrite* Mem_app_or_eq.
      * applys~ triple_based_interpretation_weaken_skeleton D4.
        introv I'. unfolds in I'. unfolds. rewrite* Mem_app_or_eq.
    + introv I'. unfolds in I'. rewrite* Mem_app_or_eq in I'.
Qed.

Lemma triple_based_interpretation_only_use_finite_number_of_triples_bone : forall I b T T' sigma sigma',
  interpretation_bone (I : triple_based_interpretation) b (sigma, T) (sigma', T') ->
  exists L,
    (forall sigma t o, list_to_triple_set L sigma t o -> T sigma t o)
    /\ interpretation_bone (I : triple_based_interpretation) b
                           (sigma, list_to_triple_set L) (sigma', list_to_triple_set L).
Proof.
  introv D. apply triple_based_interpretation_only_use_finite_number_of_triples in D.
  lets* (L&D'&I'): (rm D).
Qed.

Lemma triple_based_interpretation_only_use_finite_number_of_triples_skeleton : forall I S T sigma o,
  interpretation_skeleton (I : triple_based_interpretation) S (sigma, T) o ->
  exists L,
    (forall sigma t o, list_to_triple_set L sigma t o -> T sigma t o)
    /\ interpretation_skeleton (I : triple_based_interpretation) S (sigma, list_to_triple_set L) o.
Proof.
  introv D. apply triple_based_interpretation_only_use_finite_number_of_triples in D.
  lets* (L&D'&I'): (rm D).
Qed.

End Interpretation.


(** * Interpretation Consistencies **)

(** Consistencies state that two interpretations are in sync with each others.
  They are compositional and meant to be proven locally. **)

Section Consistencies.

Variables constructor filter : Type.
Variables variable_term variable_flow : Type.

Let interpretation : Type := interpretation constructor filter variable_term variable_flow.

(** ** Definitions **)

Section Consistency.

Variables I1 I2 : interpretation.

(** We assume two predicates [OKst] and [OKout] to state how interpretation states
  and results can be considered in sync with each others. **)
Variable OKst : interpretation_state I1 -> interpretation_state I2 -> Prop.
Variable OKout : interpretation_result I1 -> interpretation_result I2 -> Prop.

(** The existential consistency states that when the first interpretation is defined,
  so does the second (and the results are in sync).  It is an assymetrical consistency. **)
(** It corresponds to the definition 3.2 of the paper. **)
Definition existentially_consistent := forall S Sigma1 Sigma2 O1,
  OKst Sigma1 Sigma2 ->
  interpretation_skeleton I1 S Sigma1 O1 ->
  exists O2,
    interpretation_skeleton I2 S Sigma2 O2 /\ OKout O1 O2.

(** The universal consistency is symmetrical and states that the results of both
  interpretations are in sync when the input states are. **)
(** It corresponds to the definition 3.3 of the paper. **)
Definition universally_consistent := forall S Sigma1 Sigma2 O1 O2,
  OKst Sigma1 Sigma2 ->
  interpretation_skeleton I1 S Sigma1 O1 ->
  interpretation_skeleton I2 S Sigma2 O2 ->
  OKout O1 O2.

(** We also provide variants of these two notions stated at the level of bones. **)

Definition existentially_consistent_bone := forall b Sigma1 Sigma2 Sigma1',
  OKst Sigma1 Sigma2 ->
  interpretation_bone I1 b Sigma1 Sigma1' ->
  exists Sigma2',
    interpretation_bone I2 b Sigma2 Sigma2' /\ OKst Sigma1' Sigma2'.

Definition universally_consistent_bone := forall b Sigma1 Sigma2 Sigma1' Sigma2',
  OKst Sigma1 Sigma2 ->
  interpretation_bone I1 b Sigma1 Sigma1' ->
  interpretation_bone I2 b Sigma2 Sigma2' ->
  OKst Sigma1' Sigma2'.

End Consistency.


(** ** Properties **)

(** *** Identity Consistency **)

Section IdentityConsistency.

Variable I : interpretation.

(** An interpretation is trivially existentially consistent with itself.
  It may however not be universally consistent, as an interpretation may
  accepts more than one result from a given input. **)

Definition identityOKst := @eq (interpretation_state I).
Definition identityOKout := @eq (interpretation_result I).

Lemma identity_existentially_consistent : existentially_consistent I I identityOKst identityOKout.
Proof. introv E D. unfolds in E. substs. eexists. splits; [ apply D | reflexivity ]. Qed.

End IdentityConsistency.

(** *** Consistency Symmetries **)

Section ConsistencySymmetries.

Variables I1 I2 : interpretation.

Variable OKst : interpretation_state I1 -> interpretation_state I2 -> Prop.
Variable OKout : interpretation_result I1 -> interpretation_result I2 -> Prop.

Hypothesis I1I2_universally_consistent : universally_consistent I1 I2 OKst OKout.

(** The universal consistency is a symmetrical relation. **)

Definition swapOKst Sigma2 Sigma1 := OKst Sigma1 Sigma2.
Definition swapOKout Sigma2 Sigma1 := OKout Sigma1 Sigma2.

Lemma swap_universally_consistent : universally_consistent I2 I1 swapOKst swapOKout.
Proof. introv E D1 D2. applys~ I1I2_universally_consistent E D2 D1. Qed.

End ConsistencySymmetries.

(** *** Consistency Composition **)

Section Composition.

Variables I1 I2 I3 : interpretation.

Variable OKst12 : interpretation_state I1 -> interpretation_state I2 -> Prop.
Variable OKst23 : interpretation_state I2 -> interpretation_state I3 -> Prop.
Variable OKout12 : interpretation_result I1 -> interpretation_result I2 -> Prop.
Variable OKout23 : interpretation_result I2 -> interpretation_result I3 -> Prop.

(** Interpretation consistency are compositional.  Composition of existential
  and universal consistencies can be done under some conditions. **)

Definition existentialCompositionOKst Sigma1 Sigma3 :=
  exists Sigma2, OKst12 Sigma1 Sigma2 /\ OKst23 Sigma2 Sigma3.

Definition existentialCompositionOKout O1 O3 :=
  exists O2, OKout12 O1 O2 /\ OKout23 O2 O3.

Lemma existentially_existentially_consistency_composition :
  existentially_consistent I1 I2 OKst12 OKout12 ->
  existentially_consistent I2 I3 OKst23 OKout23 ->
  existentially_consistent I1 I3 existentialCompositionOKst existentialCompositionOKout.
Proof.
  intros C12 C23 S Sigma1 Sigma3 O1 OK13 D1. lets (Sigma2&OK12&OK23): OK13.
  forwards~ (O2&D2&OK12'): C12 OK12 D1. forwards~ (O3&D3&OK23'): C23 OK23 D2.
  exists O3. splits~. exists* O2.
Qed.

Lemma existentially_universally_consistency_composition :
  existentially_consistent I1 I2 OKst12 OKout12 ->
  universally_consistent I2 I3 OKst23 OKout23 ->
  universally_consistent I1 I3 existentialCompositionOKst existentialCompositionOKout.
Proof.
  intros C12 C23 S Sigma1 Sigma3 O1 O3 OK13 D1 D3. lets (Sigma2&OK12&OK23): OK13.
  forwards~ (O2&D2&OK12'): C12 OK12 D1. forwards~ OK23': C23 OK23 D2 D3. exists* O2.
Qed.

End Composition.

(** *** Consistency Weakening **)

Section Weakening.

Variables I1 I2 : interpretation.

Variable OKst OKst' : interpretation_state I1 -> interpretation_state I2 -> Prop.
Variable OKout OKout' : interpretation_result I1 -> interpretation_result I2 -> Prop.

Lemma existential_consistency_weaken :
  (forall Sigma1 Sigma2, OKst' Sigma1 Sigma2 -> OKst Sigma1 Sigma2) ->
  (forall O1 O2, OKout O1 O2 -> OKout' O1 O2) ->
  existentially_consistent I1 I2 OKst OKout ->
  existentially_consistent I1 I2 OKst' OKout'.
Proof.
  introv Wst Wout C OK D. forwards (O2&D2&OK'): C D.
  { applys~ Wst OK. }
  exists O2. splits*.
Qed.

Lemma universal_consistency_weaken :
  (forall Sigma1 Sigma2, OKst' Sigma1 Sigma2 -> OKst Sigma1 Sigma2) ->
  (forall O1 O2, OKout O1 O2 -> OKout' O1 O2) ->
  universally_consistent I1 I2 OKst OKout ->
  universally_consistent I1 I2 OKst' OKout'.
Proof. introv Wst Wout C OK D1 D2. apply~ Wout. applys* C D1 D2. Qed.

End Weakening.


(** ** Proving Consistencies **)

(** The main interest for interpretations is that they define local
  behaviours, whose properties can be proven locally.  This avoid
  having to deal with the full complexity of real-world skeletons,
  as it is enough to prove their propagation in each building blocks
  of interpretations.
  Interpretation consistencies are a typical example of such properties
  meant to be proven locally.  This section provides tools for such
  proofs. **)

Section ProvingConsistencies.

Variables I1 I2 : interpretation.

Variable OKst : interpretation_state I1 -> interpretation_state I2 -> Prop.
Variable OKout : interpretation_result I1 -> interpretation_result I2 -> Prop.

(** The following two lemmas ([local_existential_consistency] and
  [local_universal_consistency]) derive the concistency of two
  interpretations from local hypotheses on the building blocks of
  interpretations.
  These two lemmas are probably the most important of this framework. **)

Section Existential.

Hypothesis local_empty_skeleton : forall Sigma1 Sigma2 O1,
  OKst Sigma1 Sigma2 ->
  interpretation_empty_skeleton I1 Sigma1 O1 ->
  exists O2,
    interpretation_empty_skeleton I2 Sigma2 O2 /\ OKout O1 O2.

Hypothesis local_hook : forall xf1 t xf2 Sigma1 Sigma2 Sigma1',
  OKst Sigma1 Sigma2 ->
  interpretation_hook I1 xf1 t xf2 Sigma1 Sigma1' ->
  exists Sigma2',
    interpretation_hook I2 xf1 t xf2 Sigma2 Sigma2' /\ OKst Sigma1' Sigma2'.

Hypothesis local_filter : forall f xs ys Sigma1 Sigma2 Sigma1',
  OKst Sigma1 Sigma2 ->
  interpretation_filter I1 f xs ys Sigma1 Sigma1' ->
  exists Sigma2',
    interpretation_filter I2 f xs ys Sigma2 Sigma2' /\ OKst Sigma1' Sigma2'.

(** The major difference between the existential and universal consistencies
   stands in the hypothesis for merge.  In the existential case, we consider
   that the two lists [O1] and [O2] are related in both their domains. **)

Hypothesis local_merge : forall V O1 O2 Sigma1 Sigma2 Sigma1',
  OKst Sigma1 Sigma2 ->
  Forall2 (eq_option OKout) O1 O2 ->
  interpretation_merge I1 O1 V Sigma1 Sigma1' ->
  exists Sigma2',
    interpretation_merge I2 O2 V Sigma2 Sigma2' /\ OKst Sigma1' Sigma2'.

Lemma local_existential_consistency_aux :
  existentially_consistent_bone I1 I2 OKst
  /\ existentially_consistent I1 I2 OKst OKout.
Proof.
  apply (bone_and_skeleton_ind' (fun b => forall Sigma1 Sigma2 Sigma1',
    OKst Sigma1 Sigma2 ->
    interpretation_bone I1 b Sigma1 Sigma1' ->
    exists Sigma2',
      interpretation_bone I2 b Sigma2 Sigma2'
      /\ OKst Sigma1' Sigma2') (Q := fun S => forall Sigma1 Sigma2 O1,
    OKst Sigma1 Sigma2 -> _ S Sigma1 O1 -> exists O2, _ S Sigma2 O2 /\ _ O1 O2)).
  - introv OK D1. inverts D1 as D1. forwards~ (Sigma2'&D2&OK'): local_hook OK D1.
    exists Sigma2'. splits~. constructor*.
  - introv OK D1. inverts D1 as D1. forwards~ (Sigma2'&D2&OK'): local_filter OK D1.
    exists Sigma2'. splits~. constructor*.
  - introv F OK D1. inverts D1 as F1 D1.
    apply Forall2_swap in F1. forwards F': Forall2_Forall F F1.
    apply Forall2_weaken with (Q := fun S Oi => exists Oi',
      eq_option (fun Oiv Oiv' =>
        interpretation_skeleton I2 S Sigma2 Oiv' /\ OKout Oiv Oiv') Oi Oi') in F'.
    + forwards (O'&FO'): Forall2_exists F'.
      rewrite Forall3_iff_forall_Nth in FO'. lets (El1&El2&FO): (rm FO').
      forwards~ (Sigma2'&D2&OK'): local_merge O' OK D1.
      * rewrite Forall2_iff_forall_Nth. splits~. introv NO NO'.
        forwards (S'&N): length_Nth_lt.
        { rewrite El1. applys~ Nth_lt_length NO. }
        forwards~ E: FO N NO NO'. destruct v1 as [v1|], v2 as [v2|];
          try apply~ eq_option_None; inverts E as (E1&E2).
        apply~ eq_option_Some.
      * exists Sigma2'. splits~. constructors*.
        rewrite Forall2_iff_forall_Nth. splits; [ rewrite~ El1 |].
        introv NO' NSs E1. forwards (Oi&NOi): length_Nth_lt.
        { rewrite <- El1. applys~ Nth_lt_length NSs. }
        forwards E: FO NSs NOi NO'. substs. destruct Oi; inverts E as (E1&E2).
        autos~.
    + intros S' Oi (Fa&Fb). destruct Oi.
      * forwards~ (O2&PO2&OK'): Fa OK. exists (Some O2). apply eq_option_Some. splits~.
      * eexists. apply eq_option_None.
  - introv OK D1. inverts D1 as D1. forwards (O2&D2&OK'): local_empty_skeleton OK D1.
    exists O2. splits~. constructor*.
  - introv F1 F2 OK D1. inverts D1 as B1 D1. forwards~ (Sigma2'&B2&OK'): F1 OK B1.
    forwards (O2&D&OK''): F2 OK' D1. exists O2. splits~. constructors*.
Qed.

(** This lemma derives the existential consistency of two interpretations from
  local properties, at the level of bones.  Intuitively, it can be seen as an
  induction principle for existential consistencies. **)
(** It corresponds to the lemma 3.4 of the paper. **)
Lemma local_existential_consistency : existentially_consistent I1 I2 OKst OKout.
Proof. apply local_existential_consistency_aux. Qed.

Lemma local_existential_consistency_bone : existentially_consistent_bone I1 I2 OKst.
Proof. apply local_existential_consistency_aux. Qed.

End Existential.

Section Universal.

Hypothesis local_empty_skeleton : forall Sigma1 Sigma2 O1 O2,
  OKst Sigma1 Sigma2 ->
  interpretation_empty_skeleton I1 Sigma1 O1 ->
  interpretation_empty_skeleton I2 Sigma2 O2 ->
  OKout O1 O2.

Hypothesis local_hook : forall xf1 t xf2 Sigma1 Sigma2 Sigma1' Sigma2',
  OKst Sigma1 Sigma2 ->
  interpretation_hook I1 xf1 t xf2 Sigma1 Sigma1' ->
  interpretation_hook I2 xf1 t xf2 Sigma2 Sigma2' ->
  OKst Sigma1' Sigma2'.

Hypothesis local_filter : forall f xs ys Sigma1 Sigma2 Sigma1' Sigma2',
  OKst Sigma1 Sigma2 ->
  interpretation_filter I1 f xs ys Sigma1 Sigma1' ->
  interpretation_filter I2 f xs ys Sigma2 Sigma2' ->
  OKst Sigma1' Sigma2'.

(** In contrary to the existential, the universal case one consider
   that the two lists [O1] and [O2] are related in the intersection
   of both their domains. **)

Hypothesis local_merge : forall V O1 O2 Sigma1 Sigma2 Sigma1' Sigma2',
  OKst Sigma1 Sigma2 ->
  Forall2 (fun O1i O2i => forall O1iv O2iv,
    O1i = Some O1iv ->
    O2i = Some O2iv ->
    OKout O1iv O2iv) O1 O2 ->
  interpretation_merge I1 O1 V Sigma1 Sigma1' ->
  interpretation_merge I2 O2 V Sigma2 Sigma2' ->
  OKst Sigma1' Sigma2'.

Lemma local_universal_consistency_aux :
  universally_consistent_bone I1 I2 OKst
  /\ universally_consistent I1 I2 OKst OKout.
Proof.
  apply (bone_and_skeleton_ind' (fun b => forall Sigma1 Sigma2 Sigma1' Sigma2',
    OKst Sigma1 Sigma2 ->
    interpretation_bone I1 b Sigma1 Sigma1' ->
    interpretation_bone I2 b Sigma2 Sigma2' ->
    OKst Sigma1' Sigma2') (Q := fun S => forall Sigma1 Sigma2 O1 O2,
    OKst Sigma1 Sigma2 -> _ S Sigma1 O1 -> _ S Sigma2 O2 -> OKout O1 O2)).
  - introv OK D1 D2. inverts D1 as D1. inverts D2 as D2. applys~ local_hook OK D1 D2.
  - introv OK D1 D2. inverts D1 as D1. inverts D2 as D2. applys~ local_filter OK D1 D2.
  - introv F OK D1 D2. inverts D1 as F1 D1. inverts D2 as F2 D2. applys~ local_merge OK D1 D2.
    apply Forall2_iff_forall_Nth in F1. lets (El1&F1'): (rm F1).
    apply Forall2_iff_forall_Nth in F2. lets (El2&F2'): (rm F2).
    apply Forall2_iff_forall_Nth. splits~.
    + rewrite El1. rewrite~ El2.
    + introv N1 N2 E1 E2. rewrite Forall_iff_forall_mem in F.
      forwards (S'&N): length_Nth_lt.
      { rewrite <- El1. applys~ Nth_lt_length N1. }
      forwards~ S1: F1' N1 N E1. forwards~ S2: F2' N2 N E2.
      applys~ F OK S1 S2. applys~ Nth_mem N.
  - introv OK D1 D2. inverts D1 as D1. inverts D2 as D2. applys~ local_empty_skeleton OK D1 D2.
  - introv F1 F2 OK D1 D2. inverts D1 as B1 D1. inverts D2 as B2 D2.
    applys~ F2 D1 D2. applys~ F1 OK B1 B2.
Qed.

(** This lemma derives the universal consistency of two interpretations from
  local properties, at the level of bones.  Intuitively, it can be seen as an
  induction principle for universal consistencies. **)
(** It corresponds to the lemma 3.5 of the paper. **)
Lemma local_universal_consistency : universally_consistent I1 I2 OKst OKout.
Proof. apply local_universal_consistency_aux. Qed.

Lemma local_universal_consistency_bone : universally_consistent_bone I1 I2 OKst.
Proof. apply local_universal_consistency_aux. Qed.

End Universal.

End ProvingConsistencies.

End Consistencies.


(** * Computable Interpretations **)

Section Compute.

Require Import Alternatives.

Variables constructor filter variable_term variable_flow : Type.

Variable I : interpretation constructor filter variable_term variable_flow.

Let Ist : Type := interpretation_state I.
Let Ires : Type := interpretation_result I.

Let variable : Type := variable variable_term variable_flow.
Let variable_flow_extended : Type := variable_flow_extended variable_flow.
Let basic_free_term : Type := basic_free_term constructor variable_term.
Let bone : Type := bone constructor filter variable_term variable_flow.
Let skeleton : Type := skeleton constructor filter variable_term variable_flow.

Variables Tst Tres : Type.

Variable Alternative_Tst : Alternative Ist Tst.
Variable Alternative_Tres : Alternative Ires Tres.

Hypothesis Alternative_Strong_Tst : Alternative_Strong Ist Tst.
Hypothesis Alternative_Strong_Tres : Alternative_Strong Ires Tres.

Hypothesis Alternative_Representation_empty_skeleton :
  Alternative_Representation (Ist -> Ires -> Prop) (Tst -> list Tres)
                             (interpretation_empty_skeleton I).

Hypothesis Alternative_Representation_hook :
  Alternative_Representation (variable_flow_extended -> basic_free_term -> variable_flow_extended ->
                              Ist -> Ist -> Prop)
                             (variable_flow_extended -> basic_free_term -> variable_flow_extended ->
                              Tst -> list Tst) (interpretation_hook I).

Hypothesis Alternative_Representation_filter :
  Alternative_Representation (filter -> list variable -> list variable ->
                              Ist -> Ist -> Prop)
                             (filter -> list variable -> list variable ->
                              Tst -> list Tst) (interpretation_filter I).

Hypothesis Alternative_Representation_merge :
  Alternative_Representation (list (option Ires) -> list variable ->
                              Ist -> Ist -> Prop)
                             (list (option Tres) -> list variable ->
                              Tst -> list Tst) (interpretation_merge I).

Definition Alternative_Representation_interpretation_skeleton_bone :
  (forall b : bone,
    Alternative_Representation (Ist -> Ist -> Prop) (Tst -> list Tst)
                               (interpretation_bone I b))
  * (forall S : skeleton,
      Alternative_Representation (Ist -> Ires -> Prop) (Tst -> list Tres)
                                 (interpretation_skeleton I S)).
Proof.
  apply bone_and_skeleton_rect_dep.
  - introv. rewrite interpretation_bone_H_eq.
    do 3 (eapply Alternative_Representation_fun; [| typeclass ]).
    apply Alternative_Representation_hook.
  - introv. rewrite interpretation_bone_F_eq.
    do 3 (eapply Alternative_Representation_fun; [| typeclass ]).
    apply Alternative_Representation_filter.
  - introv L.
    asserts OS: (forall Sigma,
                   { Os | forall O : list (option Tres),
                            Mem O Os <->
                              length O = length Ss
                              /\ forall n Oi S (N : Nth n Ss S) AR Oiv,
                                   Nth n O Oi ->
                                   hlist_Nth N L AR ->
                                   Oi = Some Oiv ->
                                   Mem Oiv (@alt _ _ _ (interpretation_skeleton I S) AR Sigma) }).
    { introv. induction L.
      - exists [nil : list (option Tres)]. introv. iff M.
        + inverts M as M; [| inverts M ].
          splits~. introv. inverts N.
        + lets (E&F): (rm M). destruct O; inverts E. constructors~.
      - destruct (decide ((alt (interpretation_skeleton I l) Sigma : list Tres) = nil)) eqn:D;
          fold_bool; rew_refl in D.
        + exists (map (fun l => None :: l) (proj1_sig IHL)).
          introv. iff M.
          * rewrite Mem_mem in M. rewrite map_mem in M. lets (O'&MO&EO): (rm M).
            rewrite <- Mem_mem in MO. apply (proj2_sig IHL) in MO. lets (MOl&MOf): (rm MO).
            substs. splits.
            -- rew_list~.
            -- introv N1 N2 E. substs. inverts N1 as N1. inverts N2 as N2. applys~ MOf N1 N2.
          * lets (EM&FM): (rm M). destruct O as [| O0 O ]; tryfalse.
            asserts: (O0 = None).
            { destruct~ O0. forwards* M: FM 0; try constructors.
              fold Ist Ires in D, M. rewrite D in M. inverts M. }
            substs. rewrite Mem_mem. rewrite map_mem. eexists. splits*.
            rewrite <- Mem_mem. apply (proj2_sig IHL). splits~.
            introv N1 N2 E. substs. apply* FM; constructors*.
        + exists (map (fun hd_tl => Some (fst hd_tl) :: snd hd_tl)
                   (list_product (alt (interpretation_skeleton I l) Sigma : list Tres)
                                 (proj1_sig IHL))).
          introv. iff M.
          * rewrite Mem_mem in M. rewrite map_mem in M. lets (ll&M'&E): (rm M).
            destruct ll as [Oc Ocs]. apply list_product_mem in M'. lets (M1&M2): (rm M').
            rewrite <- Mem_mem in M2. apply (proj2_sig IHL) in M2. lets (E'&F): (rm M2).
            substs. splits.
            -- rew_list~.
            -- introv N1 N2 EOi.
               skip. (* TODO *)
          * lets (El&F): (rm M). destruct O as [|[O0|] O]; tryfalse.
            -- rewrite Mem_mem. rewrite map_mem.
               exists (O0, O). splits~. rewrite list_product_mem. splits.
               ++ rewrite <- Mem_mem. applys~ F; constructors~.
               ++ rewrite <- Mem_mem. apply (proj2_sig IHL). splits~.
                  introv N1 N2 E. substs. applys~ F; constructors*.
            -- false.
               (* There is a design choice here:
                  - either we do not change how [OS] is written, and we change the [exists]
                    Line 832 to be of the form
                    [exists (map (fun hd_tl => fst hd_tl :: snd hd_tl)
                                 (list_product
                                   (None :: map Some (alt (interpretation_skeleton I l) Sigma : list Tres))
                                   (proj1_sig IHL)))].
                    In this case, we no longer need to do the test Line 816, and we just move the
                    proof of the first case here.
                  - either we change the property for [OS] to such that it only puts [None]
                    if indeed there is possible computation.
               *) skip. (* TODO *)
       }
    applys make_alternative_representation (interpretation_bone I (B Ss V)) (fun Sigma =>
      concat (map (fun O : list (option Tres) =>
                     (alt (interpretation_merge I) : _ -> _ -> _ -> _) O V Sigma)
                  (proj1_sig (OS Sigma)))).
    simpl. intros sigmaa sigmac R sigmaa' sigmac' R'.
    rewrite Mem_mem. rewrite concat_mem. iff H.
    + lets (O&M1&M2): (rm H). rewrite map_mem in M1. lets (O'&Ma&Mb): (rm M1).
      asserts (Oa&HOa): (exists (Oa : list (option Ires)),
        Forall2 (fun c a => eq_option altr a c) O' Oa).
      { apply Forall_exists. rewrite Forall_iff_forall_mem. intros Oi MO.
        forwards (Oia&EOia): @altr_exists_inv (option Ires) Oi.
        { typeclass. }
        exists Oia. apply EOia. }
      apply Forall2_swap in HOa.

      (*constructors.
      * skip. (* TODO *)
      **) skip. (* TODO *)
    + inverts H as F M. fold Ires in O.
      asserts (Oc&HOc): (exists (Oc : list (option Tres)), Forall2 (eq_option altr) O Oc).
      { apply Forall_exists. rewrite Forall_iff_forall_mem. intros Oi MO.
        forwards (Oic&EOic): @altr_exists (option Tres) Oi.
        { typeclass. }
        exists Oic. apply EOic. }
      eexists. splits.
      * rewrite map_mem. exists Oc. splits*.
        rewrite <- Mem_mem. applys @proj2_sig (OS sigmac).
        rewrite Forall2_iff_forall_Nth in F. lets (lF&fF): (rm F).
        rewrite Forall2_iff_forall_Nth in HOc. lets (lOc&fOc): (rm HOc). splits~.
        -- rewrite~ <- lF.
        -- introv N1 N2 E. forwards (v3&N3): length_Nth_lt.
           { rewrite lOc. applys~ Nth_lt_length N1. }
           forwards EO: (rm fOc) N3 N1. substs. inverts EO as EO.
           lets MI: (@alt_spec _ _ _ _ AR).
           simpl in MI. forwards MII: MI; try solve [ apply MII; applys~ fF N3 ]; autos~.
      * rewrite <- Mem_mem.
        forwards A: (@alt_spec _ (list (option Tres) -> _ -> Tst -> list Tst) _
                               (interpretation_merge I)).
        simpl in A. forwards EA: A HOc; try solve [ apply EA; apply M ]; autos~.
  - rewrite interpretation_skeleton_empty_eq.
    apply Alternative_Representation_empty_skeleton.
  - introv Rb RS.
    applys make_alternative_representation (interpretation_skeleton I (b :: S))
      (fun Sigma : Tst =>
        concat (map (fun Sigma : Tst => alt (interpretation_skeleton I S) Sigma)
                    ((alt (interpretation_bone I b) Sigma)))).
    simpl. intros sigmaa sigmac R oa oc Rr. rewrite Mem_mem. rewrite concat_mem.
    forwards Eb: (@alt_spec _ _ _ _ Rb) R. simpl in Eb. iff H.
    + lets (L&M1&M2): (rm H). rewrite map_mem in M1. lets (sigmac'&Ma&E): (rm M1).
      rewrite <- Mem_mem in Ma. forwards~ (sigmaa'&R'): @altr_exists_inv sigmac'.
      forwards E': (rm Eb) R'. apply E' in Ma. constructors*.
      forwards ES: (@alt_spec _ _ _ _ RS) R' Rr. substs.
      rewrite <- Mem_mem in M2. apply~ ES.
    + inverts H as Db DS. fold Ist in Sigma'. forwards~ (sigmac'&R'): @altr_exists Sigma'.
      forwards E': (rm Eb) R'. apply E' in Db.
      forwards ES: (@alt_spec _ _ _ _ RS) R' Rr. apply ES in DS. rewrite Mem_mem in *.
      eexists. rewrite* map_mem.
Defined.

Definition Alternative_Representation_interpretation_skeleton :
  Alternative_Representation (skeleton -> Ist -> Ires -> Prop) (skeleton -> Tst -> list Tres)
                             (interpretation_skeleton I).
  applys Alternative_Representation_fun_rev Alternative_Representation_interpretation_skeleton_bone.
Defined.

Definition Alternative_Representation_interpretation_bone :
  Alternative_Representation (bone -> Ist -> Ist -> Prop) (bone -> Tst -> list Tst)
                             (interpretation_bone I).
  apply Alternative_Representation_fun_rev.
  apply Alternative_Representation_interpretation_skeleton_bone.
Defined.

End Compute.


(** * Tactics **)

(** Unfolds all constructors without doing any clever choice **)
Ltac interpretation_unfold_constructors :=
  let constructors_if_matchable l := list_is_matchable l; constructors in
  repeat lazymatch goal with
  (** We unfold the constructors of the interpretation. **)
  | |- interpretation_skeleton _ _ _ _ => constructors
  | |- interpretation_bone _ _ _ _ => constructors
  | |- interpretation_empty_skeleton _ _ _ => constructors
  | |- interpretation_hook _ _ _ _ _ _ => constructors
  | |- interpretation_filter _ _ _ _ _ _ => constructors
  | |- interpretation_merge _ _ _ _ _ => constructors
  (** We also unfold some trivial constructors.
    We however don’t instantiate any existantial variable here, unless already forced. **)
  | |- Forall _ ?l => constructors_if_matchable l
  | |- Forall2 _ ?l1 ?l2 => constructors_if_matchable l1 || constructors_if_matchable l2
  | |- Forall3 _ ?l1 ?l2 ?l3 =>
    first [ constructors_if_matchable l1
          | constructors_if_matchable l2
          | constructors_if_matchable l3 ]
  | |- forall Oiv, _ = Some Oiv -> _ =>
    let E := fresh "E" in introv E; inverts E as E
  end.


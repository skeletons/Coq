(** Auxiliary file for interpretations. **)

(* FIXME: This file is for experimentation only. It is not a stable file for now. *)

Require Export Interpretations.

(** * Interpretations Equivalence **)

Section Equivalence.

Variables constructor filter : Type.
Variable basic_term : Type.
Variables variable_term variable_flow : Type.

Let interpretation : Type :=
  interpretation constructor filter variable_term variable_flow.

Variable Ia Ic : interpretation.

(** In this section, we want to be able to decide properties on the
  interpretation [Ia], which manipulates non-practical data such as
  [set]s.  To this end, we shall use [Ic], whom we assume manipulates
  similar data, but easier to be manipulated by an interpreter, such
  as [list]s. **)

Variable RelState : interpretation_state Ic -> interpretation_state Ia -> Prop.
Variable RelResult : interpretation_result Ic -> interpretation_result Ia -> Prop.

Hypothesis interpretation_universally_consistent :
  universally_consistent Ic Ia RelState RelResult.

Hypothesis interpretation_existentially_consistent :
  existentially_consistent Ic Ia RelState RelResult.

Hypothesis interpretation_existentially_consistent_swapped :
  existentially_consistent Ia Ic (fun sigma2 sigma1 => RelState sigma1 sigma2)
                                 (fun oa oc => RelResult oc oa).

(** We assume an equivalence relation for abstract results and states, and
  we assume it to be compatible with the relations [RelState] and [RelResult].
  These equivalence relations will typically be [eq]. **)
Variable RelAState : interpretation_state Ia -> interpretation_state Ia -> Prop.
Variable RelAResult : interpretation_result Ia -> interpretation_result Ia -> Prop.

Hypothesis RelState_func : forall c a1 a2,
  RelState c a1 ->
  RelState c a2 ->
  RelAState a1 a2.

Hypothesis RelResult_func : forall c a1 a2,
  RelResult c a1 ->
  RelResult c a2 ->
  RelAResult a1 a2.

Instance interpretation_equivalent_Pickable : forall S sigmaa,
    (forall sigmac, Pickable_list (interpretation_skeleton Ic S sigmac)) ->
    Pickable_list (fun oa (* No: we would like a concrete list. Right? We also don’t want all concrete representation of the abstract list. *) => exists oa', interpretation_skeleton Ia S sigmaa oa' /\ RelAResult oa oa').
  introv P.
Defined.

Lemma interpretation_equivalent : forall S sigma1 sigma2 o1,
  OKstate sigma1 sigma2 ->
  interpretation_skeleton I1 S sigma1 o1
  <-> (exists o2, interpretation_skeleton I2 S sigma2 o2 /\ OKresult o1 o2).
Proof.
  introv OKsigma. iff I.
  - applys interpretation_existentially_consistent OKsigma I.
  - lets (o2&I'&OKo): (rm I).
    forwards (o1'&I&OKsigma'): interpretation_existentially_consistent_swapped OKsigma I'.
Qed.

End Equivalence.


(** * Computability of Interpretations **)

Section Computability.

Variables constructor filter : Type.
Variable basic_term : Type.
Variables variable_term variable_flow : Type.

Let interpretation : Type :=
  interpretation constructor filter basic_term variable_term variable_flow.

Section Interpretation.

Variable I : interpretation.

(** We suppose given equivalence relations between states and result. **)

Variable eq_state : interpretation_state I -> interpretation_state I -> Prop.
Variable eq_result : interpretation_result I -> interpretation_result I -> Prop.

Hypothesis eq_state_refl : forall Sigma, eq_state Sigma Sigma.

(** We suppose these equivalence relations compatible with the interpretation. **)

Hypothesis interpretation_universally_consistent :
  universally_consistent I I eq_state eq_result.

Hypothesis interpretation_existentially_consistent :
  existentially_consistent I I eq_state eq_result.

Hypothesis interpretation_universally_consistent_bone :
  universally_consistent_bone I I eq_state.

Hypothesis interpretation_existentially_consistent_bone :
  existentially_consistent_bone I I eq_state.

(** For computability, we consider a variant of the symmetrical hypotheses of
  the existential and universal consistencies for the merge case: here, the two
  lists [O1] and [O2] are not symmetrical, [O1] enforcing where [O2] should be
  at least defined. **)

Hypothesis interpretation_merge_increase : forall O1 O2 V Sigma1 Sigma2 Sigma1',
  eq_state Sigma1 Sigma2 ->
  Forall2 (fun O1i O2i => forall O1iv,
    O1i = Some O1iv ->
    exists O2iv, O2i = Some O2iv /\ eq_result O1iv O2iv) O1 O2 ->
  interpretation_merge I O1 V Sigma1 Sigma1' ->
  exists Sigma2', interpretation_merge I O2 V Sigma2 Sigma2'.


(** ** Propagation of the Computability of Results Through Skeletons **)

(** *** Pickable Case **)

(** When the structure of interpretation states and results can be
  directly use to compute, this computability propagates through
  skeletons. **)

Section Pickable.

Hypothesis empty_skeleton_Pickable_option : forall Sigma,
  Pickable_option (interpretation_empty_skeleton I Sigma).

Hypothesis hook_Pickable_option : forall xf1 t xf2 Sigma,
  Pickable_option (interpretation_hook I xf1 t xf2 Sigma).

Hypothesis filter_Pickable_option : forall f xs ys Sigma,
  Pickable_option (interpretation_filter I f xs ys Sigma).

Hypothesis merge_Pickable_option : forall O V Sigma,
  Pickable_option (interpretation_merge I O V Sigma).

Definition compute_interpretation_skeleton :=
  skeleton_rect'
    (fun xf1 t xf2 Sigma => pick_option (interpretation_hook I xf1 t xf2 Sigma))
    (fun f xs ys Sigma => pick_option (interpretation_filter I f xs ys Sigma))
    (fun Ss V compute_skeleton_list Sigma =>
      let O := map (fun compute_skeleton => compute_skeleton Sigma) compute_skeleton_list in
      pick_option (interpretation_merge I O V Sigma))
    (fun Sigma => pick_option (interpretation_empty_skeleton I Sigma))
    (fun b S compute_skeleton compute_bone Sigma =>
      LibOption.apply_on (compute_bone Sigma) compute_skeleton).

Definition compute_interpretation_bone :=
  bone_rect'
    (fun xf1 t xf2 Sigma => pick_option (interpretation_hook I xf1 t xf2 Sigma))
    (fun f xs ys Sigma => pick_option (interpretation_filter I f xs ys Sigma))
    (fun Ss V compute_skeleton_list Sigma =>
      let O := map (fun compute_skeleton => compute_skeleton Sigma) compute_skeleton_list in
      pick_option (interpretation_merge I O V Sigma))
    (fun Sigma => pick_option (interpretation_empty_skeleton I Sigma))
    (fun b S compute_skeleton compute_bone Sigma =>
      LibOption.apply_on (compute_bone Sigma) compute_skeleton) _.

Lemma compute_interpretation_skeleton_spec : forall S Sigma,
  match compute_interpretation_skeleton S Sigma with
  | Some Sigma' => interpretation_skeleton I S Sigma Sigma'
  | None => forall Sigma', ~ interpretation_skeleton I S Sigma Sigma'
  end.
Proof.
  introv. apply (skeleton_ind' (fun b => forall Sigma,
      match compute_interpretation_bone b Sigma with
      | Some Sigma' => interpretation_bone I b Sigma Sigma'
      | None => forall Sigma', ~ interpretation_bone I b Sigma Sigma'
      end)
    (Q := fun S => forall Sigma,
       match _ S Sigma with
       | Some Sigma' => interpretation_skeleton I S Sigma Sigma'
       | None => forall Sigma', ~ _ S Sigma Sigma'
       end)).
  - introv. unfolds compute_interpretation_bone. rewrite bone_rect'_fix. simpl.
    destruct pick_option eqn: E.
    + constructors. applys~ @pick_option_correct E.
    + introv A. inverts A as A. forwards (?&EA): @pick_option_defined (ex_intro _ _ A).
      rewrite EA in E. inverts~ E.
  - introv. unfolds compute_interpretation_bone. rewrite bone_rect'_fix. simpl.
    destruct pick_option eqn: E.
    + constructors. applys~ @pick_option_correct E.
    + introv A. inverts A as A. forwards (?&EA): @pick_option_defined (ex_intro _ _ A).
      rewrite EA in E. inverts~ E.
  - introv F. introv. rewrite Forall_iff_forall_mem in F.
    unfolds compute_interpretation_bone. rewrite bone_rect'_unfold. destruct pick_option eqn: E.
    + forwards I1: @pick_option_correct (rm E). constructors*.
      apply Forall2_iff_forall_Nth. splits~.
      * do 2 rewrite~ length_map.
      * introv N NS E1. forwards (compute&N'&E'): map_Nth_inv (rm N).
        forwards (S'&N&E3): map_Nth_inv (rm N').
        forwards Pcompute: F; [ applys~ Nth_mem N |].
        asserts Ecompute: (compute = compute_interpretation_skeleton S').
        { substs. reflexivity. }
        clear E3. rewrite <- Ecompute in Pcompute. rewrite <- E' in Pcompute. substs.
        forwards~: Nth_func NS N. substs~.
    + introv A. inverts A as FA A.
      forwards (?&EA): @pick_option_defined; [ clear E | rewrite EA in E; inverts E ].
      applys interpretation_merge_increase eq_state_refl A.
      apply Forall2_iff_forall_Nth in FA. lets (El&FA'): (rm FA).
      apply Forall2_iff_forall_Nth. splits~.
      * do 2 rewrite length_map. rewrite~ El.
      * introv NO N E1. forwards (compute&N'&E'): map_Nth_inv (rm N).
        forwards (S'&N&E3): map_Nth_inv (rm N').
        forwards Pcompute: F; [ applys~ Nth_mem N |].
        asserts Ecompute: (compute = compute_interpretation_skeleton S').
        { substs. reflexivity. }
        clear E3. rewrite <- Ecompute in Pcompute. rewrite <- E' in Pcompute. substs.
        forwards~ D: FA' NO N. destruct compute_interpretation_skeleton eqn: E.
        -- eexists. splits*.
        -- false* Pcompute D.
  - introv. simpl. destruct pick_option eqn: E.
    + constructors. applys~ @pick_option_correct E.
    + introv A. inverts A as A. forwards (?&EA): @pick_option_defined (ex_intro _ _ A).
      rewrite EA in E. inverts~ E.
  - introv F1 F2. introv. simpl. unfold compute_interpretation_bone in F1. forwards F1': (rm F1) Sigma0.
    destruct bone_rect' as [Sigma'|].
    + simpl. forwards F2': F2 Sigma'. destruct compute_interpretation_skeleton.
      * constructors*.
      * introv A. inverts A as Ab AS.
        forwards~ E: interpretation_universally_consistent_bone Ab F1'.
        forwards (O2&A): interpretation_existentially_consistent E AS.
        false~ F2' A.
    + simpl. introv A. inverts A as Ab AS. false~ F1' Ab.
Qed.

Instance interpretation_skeleton_Pickable_option : forall S Sigma,
    Pickable_option (interpretation_skeleton I S Sigma).
  introv. applys @pickable_option_make (compute_interpretation_skeleton S Sigma).
  - introv E. forwards I0: compute_interpretation_skeleton_spec.
    rewrite E in I0. apply~ I0.
  - introv (Sigma'&I1). destruct compute_interpretation_skeleton eqn: E.
    + autos*.
    + forwards I2: compute_interpretation_skeleton_spec. rewrite E in I2. false~ I2 I1.
Defined.

End Pickable.

End Interpretation.


(** *** Alternative Case **)

(** Frequently though, the interpretation states and results contain
  non-computable parts (typically, sets).  To compute them
  nevertheless, one can provide an alternative definition of states
  and results. **)

Section Computable.

Variables alternative_state alternative_result : Type.

(** We here reintroduced the interpretation [I] and all its properties.
  It was removed to free the parameter [I] of
  [interpretation_skeleton_Pickable_option], which can now be applied
  on our “alternative” interpretation. **)

Variable I : interpretation.

Variable eq_state : interpretation_state I -> interpretation_state I -> Prop.
Variable eq_result : interpretation_result I -> interpretation_result I -> Prop.

Hypothesis eq_state_refl : forall Sigma, eq_state Sigma Sigma.

Hypothesis empty_skeleton_unique : forall Sigma1 Sigma2 O1 O2,
  eq_state Sigma1 Sigma2 ->
  interpretation_empty_skeleton I Sigma1 O1 ->
  interpretation_empty_skeleton I Sigma2 O2 ->
  eq_result O1 O2.

Hypothesis hook_unique : forall xf1 t xf2 Sigma1 Sigma2 Sigma1' Sigma2',
  eq_state Sigma1 Sigma2 ->
  interpretation_hook I xf1 t xf2 Sigma1 Sigma1' ->
  interpretation_hook I xf1 t xf2 Sigma2 Sigma2' ->
  eq_state Sigma1' Sigma2'.

Hypothesis filter_unique : forall f xs ys Sigma1 Sigma2 Sigma1' Sigma2',
  eq_state Sigma1 Sigma2 ->
  interpretation_filter I f xs ys Sigma1 Sigma1' ->
  interpretation_filter I f xs ys Sigma2 Sigma2' ->
  eq_state Sigma1' Sigma2'.

Hypothesis merge_unique : forall V O1 O2 Sigma1 Sigma2 Sigma1' Sigma2',
  Forall2 (fun Oi1 Oi2 => forall O1 O2,
    Oi1 = Some O1 ->
    Oi2 = Some O2 ->
    eq_result O1 O2) O1 O2 ->
  eq_state Sigma1 Sigma2 ->
  interpretation_merge I O1 V Sigma1 Sigma1' ->
  interpretation_merge I O2 V Sigma2 Sigma2' ->
  eq_state Sigma1' Sigma2'.

Hypothesis empty_skeleton_exists : forall Sigma1 Sigma2 O1,
  eq_state Sigma1 Sigma2 ->
  interpretation_empty_skeleton I Sigma1 O1 ->
  exists O2, interpretation_empty_skeleton I Sigma2 O2.

Hypothesis hook_exists : forall xf1 t xf2 Sigma1 Sigma2 Sigma1',
  eq_state Sigma1 Sigma2 ->
  interpretation_hook I xf1 t xf2 Sigma1 Sigma1' ->
  exists Sigma2', interpretation_hook I xf1 t xf2 Sigma2 Sigma2'.

Hypothesis filter_exists : forall f xs ys Sigma1 Sigma2 Sigma1',
  eq_state Sigma1 Sigma2 ->
  interpretation_filter I f xs ys Sigma1 Sigma1' ->
  exists Sigma2', interpretation_filter I f xs ys Sigma2 Sigma2'.

Hypothesis merge_exists : forall O1 O2 V Sigma1 Sigma2 Sigma1',
  eq_state Sigma1 Sigma2 ->
  Forall2 (fun Oi1 Oi2 => forall Oiv1 Oiv2,
     Oi1 = Some Oiv1 ->
     Oi2 = Some Oiv2 ->
     eq_result Oiv1 Oiv2) O1 O2 ->
  interpretation_merge I O1 V Sigma1 Sigma1' ->
  exists Sigma2', interpretation_merge I O2 V Sigma2 Sigma2'.

Variables alternative_state_to_state : alternative_state -> interpretation_state I.
Variables alternative_result_to_result : alternative_result -> interpretation_result I.

Variable alternatively_compute_empty_skeleton : alternative_state -> option alternative_result.

Inductive alternative_empty_skeleton : alternative_state -> alternative_result -> Prop :=
  | alternative_empty_skeleton_cons : forall Sigma O,
    alternatively_compute_empty_skeleton Sigma = Some O ->
    alternative_empty_skeleton Sigma O
  .

Instance alternative_empty_skeleton_Pickable_option : forall Sigma,
    Pickable_option (alternative_empty_skeleton Sigma).
  introv. eapply pickable_option_make.
  - introv E. constructors*.
  - introv (O&E). inverts* E.
Defined.

Variable alternatively_compute_hook :
  variable_flow_extended variable_flow ->
  term constructor basic_term variable_term ->
  variable_flow_extended variable_flow ->
  alternative_state -> option alternative_state.

Inductive alternative_hook : _ -> _ -> _ -> alternative_state -> alternative_state -> Prop :=
  | alternative_hook_cons : forall xf1 t xf2 Sigma Sigma',
    alternatively_compute_hook xf1 t xf2 Sigma = Some Sigma' ->
    alternative_hook xf1 t xf2 Sigma Sigma'
  .

Instance alternative_hook_Pickable_option : forall xf1 t xf2 Sigma,
    Pickable_option (alternative_hook xf1 t xf2 Sigma).
  introv. eapply pickable_option_make.
  - introv E. constructors*.
  - introv (Sigma'&E). inverts* E.
Defined.

Variable alternatively_compute_filter :
  filter ->
  list (variable variable_term variable_flow) ->
  list (variable variable_term variable_flow) ->
  alternative_state -> option alternative_state.

Inductive alternative_filter : _ -> _ -> _ -> alternative_state -> alternative_state -> Prop :=
  | alternative_filter_cons : forall f xs ys Sigma Sigma',
    alternatively_compute_filter f xs ys Sigma = Some Sigma' ->
    alternative_filter f xs ys Sigma Sigma'
  .

Instance alternative_filter_Pickable_option : forall f xs ys Sigma,
    Pickable_option (alternative_filter f xs ys Sigma).
  introv. eapply pickable_option_make.
  - introv E. constructors*.
  - introv (Sigma'&E). inverts* E.
Defined.

Variable alternatively_compute_merge :
  list (option alternative_result) ->
  list (variable variable_term variable_flow) ->
  alternative_state -> option alternative_state.

Inductive alternative_merge : _ -> _ -> alternative_state -> alternative_state -> Prop :=
  | alternative_merge_cons : forall O V Sigma Sigma',
    alternatively_compute_merge O V Sigma = Some Sigma' ->
    alternative_merge O V Sigma Sigma'
  .

Instance alternative_merge_Pickable_option : forall O V Sigma,
    Pickable_option (alternative_merge O V Sigma).
  introv. eapply pickable_option_make.
  - introv E. constructors*.
  - introv (Sigma'&E). inverts* E.
Defined.

Definition alternative_interpretation := {|
    interpretation_empty_skeleton := alternative_empty_skeleton ;
    interpretation_hook := alternative_hook ;
    interpretation_filter := alternative_filter ;
    interpretation_merge := alternative_merge
  |}.

Hypothesis alternatively_compute_empty_skeleton_correct : forall Sigma Sigmac O Oc,
  alternatively_compute_empty_skeleton Sigmac = Some Oc ->
  alternative_state_to_state Sigmac = Sigma ->
  alternative_result_to_result Oc = O ->
  interpretation_empty_skeleton I Sigma O.

Hypothesis alternatively_compute_hook_correct : forall xf1 t xf2 Sigma Sigmac Sigma' Sigmac',
  alternatively_compute_hook xf1 t xf2 Sigmac = Some Sigmac' ->
  alternative_state_to_state Sigmac = Sigma ->
  alternative_state_to_state Sigmac' = Sigma' ->
  interpretation_hook I xf1 t xf2 Sigma Sigma'.

Hypothesis alternatively_compute_filter_correct : forall f xs ys Sigma Sigmac Sigma' Sigmac',
  alternatively_compute_filter f xs ys Sigmac = Some Sigmac' ->
  alternative_state_to_state Sigmac = Sigma ->
  alternative_state_to_state Sigmac' = Sigma' ->
  interpretation_filter I f xs ys Sigma Sigma'.

Hypothesis alternatively_compute_merge_correct : forall O Oc V Sigma Sigmac Sigma' Sigmac',
  alternatively_compute_merge Oc V Sigmac = Some Sigmac' ->
  Forall2 (fun O Oc => O = LibOption.map_on Oc alternative_result_to_result) O Oc ->
  alternative_state_to_state Sigmac = Sigma ->
  alternative_state_to_state Sigmac' = Sigma' ->
  interpretation_merge I O V Sigma Sigma'.

Hypothesis alternatively_compute_empty_skeleton_defined : forall Sigma Sigmac O,
  interpretation_empty_skeleton I Sigma O ->
  alternative_state_to_state Sigmac = Sigma ->
  alternatively_compute_empty_skeleton Sigmac <> None.

Hypothesis alternatively_compute_hook_defined : forall xf1 t xf2 Sigma Sigmac Sigma',
  interpretation_hook I xf1 t xf2 Sigma Sigma' ->
  alternative_state_to_state Sigmac = Sigma ->
  alternatively_compute_hook xf1 t xf2 Sigmac <> None.

Hypothesis alternatively_compute_filter_defined : forall f xs ys Sigma Sigmac Sigma',
  interpretation_filter I f xs ys Sigma Sigma' ->
  alternative_state_to_state Sigmac = Sigma ->
  alternatively_compute_filter f xs ys Sigmac <> None.

Hypothesis alternatively_compute_merge_defined : forall O Oc V Sigma Sigmac Sigma',
  interpretation_merge I O V Sigma Sigma' ->
  Forall2 (fun O Oc => O = LibOption.map_on Oc alternative_result_to_result) O Oc ->
  alternative_state_to_state Sigmac = Sigma ->
  alternatively_compute_merge Oc V Sigmac <> None.

Hypothesis alternatively_compute_merge_increase : forall O Oc V Sigma Sigmac Sigma',
  interpretation_merge I O V Sigma Sigma' ->
  Forall2 (fun Oi Oci => forall Oiv,
    Oi = Some Oiv ->
    exists Ociv, Oci = Some Ociv /\ Oiv = alternative_result_to_result Ociv) O Oc ->
  alternative_state_to_state Sigmac = Sigma ->
  alternatively_compute_merge Oc V Sigmac <> None.

Instance alternative_interpretation_skeleton_Pickable_option : forall S Sigma,
    Pickable_option (interpretation_skeleton alternative_interpretation S Sigma).
  introv. apply interpretation_skeleton_Pickable_option with
    (eq_state := fun Sigma Sigma' =>
       eq_state (alternative_state_to_state Sigma) (alternative_state_to_state Sigma'))
    (eq_result := fun O O' =>
       eq_result (alternative_result_to_result O) (alternative_result_to_result O')).
  - introv. applys~ eq_state_refl.
  - apply~ local_universal_consistency.
    + introv E D1 D2. inverts D1 as E1. inverts D2 as E2. applys empty_skeleton_unique E.
      * applys~ alternatively_compute_empty_skeleton_correct E1.
      * applys~ alternatively_compute_empty_skeleton_correct E2.
    + introv E D1 D2. inverts D1 as E1. inverts D2 as E2. applys hook_unique E.
      * applys~ alternatively_compute_hook_correct E1.
      * applys~ alternatively_compute_hook_correct E2.
    + introv E I1 I2. inverts I1 as E1. inverts I2 as E2. applys filter_unique E.
      * applys~ alternatively_compute_filter_correct E1.
      * applys~ alternatively_compute_filter_correct E2.
    + introv E F0 I1 I2. inverts I1 as E1. inverts I2 as E2.
      applys merge_unique (map (LibOption.map alternative_result_to_result) O1)
                          (map (LibOption.map alternative_result_to_result) O2) E.
      * rewrite Forall2_iff_forall_Nth in F0. lets (El&F): (rm F0).
        rewrite Forall2_iff_forall_Nth. splits.
        -- do 2 rewrite~ length_map.
        -- introv N1 N2 E1' E2'. lets (O1'&N1'&E1''): map_Nth_inv (rm N1).
           lets (O2'&N2'&E2''): map_Nth_inv (rm N2). substs.
           destruct O1'; inverts E1''. destruct O2'; inverts E2''.
           applys~ F N1' N2'.
      * applys~ alternatively_compute_merge_correct E1. apply Forall2_swap. apply~ Forall2_map.
      * applys~ alternatively_compute_merge_correct E2. apply Forall2_swap. apply~ Forall2_map.
  - apply~ local_existential_consistency.
    + introv E I1. inverts I1 as E1. forwards (O2&I2): empty_skeleton_exists E.
      * applys~ alternatively_compute_empty_skeleton_correct E1.
      * destruct (alternatively_compute_empty_skeleton Sigma2) eqn: E2.
        -- eexists. splits; [ constructor* |]. applys empty_skeleton_unique E.
           ++ applys~ alternatively_compute_empty_skeleton_correct E1.
           ++ applys~ alternatively_compute_empty_skeleton_correct E2.
        -- false~ alternatively_compute_empty_skeleton_defined I2 E2.
    + introv E I1. inverts I1 as E1. forwards (Sigma2'&I2): hook_exists E.
      * applys~ alternatively_compute_hook_correct E1.
      * destruct (alternatively_compute_hook xf1 t xf2 Sigma2) eqn: E2.
        -- eexists. splits; [ constructor* |]. applys hook_unique E.
           ++ applys~ alternatively_compute_hook_correct E1.
           ++ applys~ alternatively_compute_hook_correct E2.
        -- false~ alternatively_compute_hook_defined I2 E2.
    + introv E I1. inverts I1 as E1. forwards (Sigma2'&I2): filter_exists E.
      * applys~ alternatively_compute_filter_correct E1.
      * destruct (alternatively_compute_filter f xs ys Sigma2) eqn: E2.
        -- eexists. splits; [ constructor* |]. applys filter_unique E.
           ++ applys~ alternatively_compute_filter_correct E1.
           ++ applys~ alternatively_compute_filter_correct E2.
        -- false~ alternatively_compute_filter_defined I2 E2.
    + introv E F0 I1. inverts I1 as E1.
      forwards (Sigma2'&I2): merge_exists (map (LibOption.map alternative_result_to_result) O1)
                                          (map (LibOption.map alternative_result_to_result) O2) E.
      -- rewrite Forall2_iff_forall_Nth in F0. lets (El&F): (rm F0).
         rewrite Forall2_iff_forall_Nth. splits.
         ++ do 2 rewrite~ length_map.
         ++ introv N1 N2 E1' E2'. lets (O1'&N1'&E1''): map_Nth_inv (rm N1).
            lets (O2'&N2'&E2''): map_Nth_inv (rm N2). substs.
            destruct O1'; inverts E1''. destruct O2'; inverts E2''.
            forwards~ E': F N1' N2'. inverts~ E'.
      -- applys~ alternatively_compute_merge_correct E1. apply Forall2_swap. apply~ Forall2_map.
      -- destruct (alternatively_compute_merge O2 V Sigma2) eqn: E2.
         ++ eexists. split; [ constructor* |].
            applys merge_unique (map (LibOption.map alternative_result_to_result) O1)
                                (map (LibOption.map alternative_result_to_result) O2) E.
            ** rewrite Forall2_iff_forall_Nth in F0. lets (El&F): (rm F0).
               rewrite Forall2_iff_forall_Nth. splits~.
               --- do 2 rewrite~ length_map.
               --- introv N1 N2 E1' E2'. lets (O1'&N1'&E1''): map_Nth_inv (rm N1).
                   lets (O2'&N2'&E2''): map_Nth_inv (rm N2). substs.
                   destruct O1'; inverts E1''. destruct O2'; inverts E2''.
                   forwards~ E': F N1' N2'. inverts~ E'.
            ** applys~ alternatively_compute_merge_correct E1.
               rewrite Forall2_iff_forall_Nth. splits~.
               --- rewrite~ length_map.
               --- introv N1 N2. lets (v1'&N1'&E1''): map_Nth_inv (rm N1).
                   forwards: Nth_func N2 N1'. substs~.
            ** applys~ alternatively_compute_merge_correct E2.
               rewrite Forall2_iff_forall_Nth. splits~.
               --- rewrite~ length_map.
               --- introv N1 N2. lets (v1'&N1'&E1''): map_Nth_inv (rm N1).
                   forwards: Nth_func N2 N1'. substs~.
         ++ false~ alternatively_compute_merge_defined I2 E2. apply Forall2_swap. apply~ Forall2_map.
  - apply~ local_universal_consistency_bone. (* TODO: Factorize *)
    + introv E D1 D2. inverts D1 as E1. inverts D2 as E2. applys empty_skeleton_unique E.
      * applys~ alternatively_compute_empty_skeleton_correct E1.
      * applys~ alternatively_compute_empty_skeleton_correct E2.
    + introv E D1 D2. inverts D1 as E1. inverts D2 as E2. applys hook_unique E.
      * applys~ alternatively_compute_hook_correct E1.
      * applys~ alternatively_compute_hook_correct E2.
    + introv E I1 I2. inverts I1 as E1. inverts I2 as E2. applys filter_unique E.
      * applys~ alternatively_compute_filter_correct E1.
      * applys~ alternatively_compute_filter_correct E2.
    + introv E F0 I1 I2. inverts I1 as E1. inverts I2 as E2.
      applys merge_unique (map (LibOption.map alternative_result_to_result) O1)
                          (map (LibOption.map alternative_result_to_result) O2) E.
      * rewrite Forall2_iff_forall_Nth in F0. lets (El&F): (rm F0).
        rewrite Forall2_iff_forall_Nth. splits.
        -- do 2 rewrite~ length_map.
        -- introv N1 N2 E1' E2'. lets (O1'&N1'&E1''): map_Nth_inv (rm N1).
           lets (O2'&N2'&E2''): map_Nth_inv (rm N2). substs.
           destruct O1'; inverts E1''. destruct O2'; inverts E2''.
           applys~ F N1' N2'.
      * applys~ alternatively_compute_merge_correct E1. apply Forall2_swap. apply~ Forall2_map.
      * applys~ alternatively_compute_merge_correct E2. apply Forall2_swap. apply~ Forall2_map.
  - skip. (* TODO
    introv E F D1. inverts D1 as E1.
    destruct (alternatively_compute_merge O2 V Sigma2) eqn: E2.
    + eexists. constructor*.
    + false~ alternatively_compute_merge_increase E2.
      * applys~ alternatively_compute_merge_correct.
        -- skip.
        -- skip.
      * skip. *)
  - typeclass.
  - typeclass.
  - typeclass.
  - typeclass.
Defined.

Definition alternative_eq_state Sigma Sigmac :=
  Sigma = alternative_state_to_state Sigmac.

Definition alternative_eq_result O Oc :=
  O = alternative_result_to_result Oc.

Lemma alternative_interpretation_universally_consistent :
  universally_consistent I alternative_interpretation alternative_eq_state alternative_eq_result.
Admitted.

Lemma alternative_interpretation_existentially_consistent :
  existentially_consistent I alternative_interpretation alternative_eq_state alternative_eq_result.
Admitted.

End Computable.

End Computability.

Arguments alternative_interpretation [_ _ _ _ _ _].


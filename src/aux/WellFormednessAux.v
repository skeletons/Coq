(** Auxiliary file for the well-formedness interpretation. **)

(* FIXME: This file is for experimentation only. It is not a stable file for now. *)

Require Import LibInt LibSet LibHeap.
Require Export InterpretationsAux.
Require Export WellFormedness.

Section Sorts.

Variables constructor filter : Type.

Variable basic_term : Type.

Variable base_sort : Type.
Variable program_sort : Type.
Variable flow_sort : Type.

Hypothesis base_sort_Comparable : Comparable base_sort.
Hypothesis program_sort_Comparable : Comparable program_sort.
Hypothesis flow_sort_Comparable : Comparable flow_sort.

Hypothesis basic_term_Comparable : Comparable basic_term.

Let term_sort : Type := term_sort base_sort program_sort.

Local Instance term_sort_Comparable : Comparable term_sort :=
  term_sort_Comparable _ _.

Hypothesis term_sort_Inhab : Inhab term_sort.
Hypothesis flow_sort_Inhab : Inhab flow_sort.

Let sort := sort base_sort program_sort flow_sort.

Instance sort_Comparable : Comparable sort.
  prove_comparable.
Defined.

Variables in_sort out_sort : program_sort -> flow_sort.

Variable filter_signature : filter -> list sort * list sort.

Variable constructor_signature : constructor -> list term_sort * program_sort.

Variable basic_term_sort : basic_term -> base_sort.

(** * The Well-formedness Interpretation **)

Section Variables.

Variables variable_term variable_flow : Type.

Hypothesis variable_term_Comparable : Comparable variable_term.
Hypothesis variable_flow_Comparable : Comparable variable_flow.

Let variable : Type := variable variable_term variable_flow.
Let variable_flow_extended : Type := variable_flow_extended variable_flow.
Let term : Type := term constructor basic_term variable_term.
Let bone : Type := bone constructor filter basic_term variable_term variable_flow.
Let skeleton : Type := skeleton constructor filter basic_term variable_term variable_flow.

Local Instance variable_flow_extended_Comparable : Comparable variable_flow_extended :=
  variable_flow_extended_Comparable variable_flow_Comparable.

Local Instance variable_Comparable : Comparable variable :=
  variable_Comparable _ _.

Let sorting_environment : Type :=
  sorting_environment base_sort program_sort flow_sort variable_term variable_flow.

Let split_fset_variable : list variable -> list variable_term * list variable_flow_extended :=
  @split_fset_variable _ _.

Let sort_of_term : (variable_term -> option term_sort) -> term -> term_sort -> Prop :=
  sort_of_term constructor_signature basic_term_sort.

Let wellformedness_environment : Type :=
  wellformedness_environment base_sort program_sort flow_sort variable_term variable_flow.

Let wellformedness_hook : variable_flow_extended -> term -> variable_flow_extended ->
                          wellformedness_environment -> wellformedness_environment -> Prop :=
  wellformedness_hook in_sort out_sort constructor_signature basic_term_sort _ _.

Let wellformedness_filter : filter -> list variable -> list variable ->
                          wellformedness_environment -> wellformedness_environment -> Prop :=
  wellformedness_filter filter_signature _ _.

Let wellformedness_merge : list (option wellformedness_environment) -> list variable ->
                          wellformedness_environment -> wellformedness_environment -> Prop :=
  wellformedness_merge _ _.

Let wellformedness : interpretation _ _ _ variable_term variable_flow :=
  wellformedness in_sort out_sort filter_signature constructor_signature basic_term_sort _ _.


(** ** Properties of the Well-formedness Interpretation **)

(** *** Local Properties of the Well-formedness Interpretation **)

Local Instance sorting_environment_Inhab : Inhab sorting_environment.
  apply~ typed_environment_Inhab.
Defined.

Definition wellformedness_eq_state (GammaD1 GammaD2 : wellformedness_environment) :=
  let (Gamma1, D1) := GammaD1 in
  let (Gamma2, D2) := GammaD2 in
  typed_environment_equivalent Gamma1 Gamma2 /\ D1 = D2.

Lemma wellformedness_eq_state_refl : forall GammaD,
  wellformedness_eq_state GammaD GammaD.
Proof.
  introv. destruct GammaD. splits~.
  applys~ typed_environment_equivalent_refl term_sort_inj flow_sort_inj.
Qed.

Lemma wellformedness_empty_skeleton_unique : forall GammaD1 GammaD2 GammaD1' GammaD2',
  wellformedness_eq_state GammaD1 GammaD2 ->
  wellformedness_empty_skeleton GammaD1 GammaD1' ->
  wellformedness_empty_skeleton GammaD2 GammaD2' ->
  wellformedness_eq_state GammaD1' GammaD2'.
Proof. introv E W1 W2. inverts W1. inverts~ W2. Qed.

Lemma wellformedness_hook_unique : forall xf1 t xf2 GammaD1 GammaD2 GammaD1' GammaD2',
  wellformedness_eq_state GammaD1 GammaD2 ->
  wellformedness_hook xf1 t xf2 GammaD1 GammaD1' ->
  wellformedness_hook xf1 t xf2 GammaD2 GammaD2' ->
  wellformedness_eq_state GammaD1' GammaD2'.
Proof.
  introv E W1 W2. inverts W1 as IdD1 Dxf11 ID1 S1 R1 NDxf21 SE1.
  inverts W2 as IdD2 Dxf12 ID2 S2 R2 NDxf22 SE2. inverts E as E1 E2.
  unfolds typed_environment_read_term. erewrite heap_equiv_read_option in S1; [| apply E1 ].
  forwards~ Es: sort_of_term_unique S2 S1. inverts Es. splits~.
  applys~ typed_environment_equivalent_trans term_sort_inj flow_sort_inj SE1.
  forwards~ SE2': typed_environment_equivalent_sym term_sort_inj flow_sort_inj SE2 .
  applys~ typed_environment_equivalent_trans term_sort_inj flow_sort_inj SE2'.
  apply~ typed_environment_write_flow_equivalent.
Qed.

Lemma wellformedness_filter_unique : forall f xs ys GammaD1 GammaD2 GammaD1' GammaD2',
  wellformedness_eq_state GammaD1 GammaD2 ->
  wellformedness_filter f xs ys GammaD1 GammaD1' ->
  wellformedness_filter f xs ys GammaD2 GammaD2' ->
  wellformedness_eq_state GammaD1' GammaD2'.
Proof.
  introv E W1 W2. inverts W1 as IdD1 Fxs1 Fys1 Fxss1 Ef1 W1.
  inverts W2 as IdD2 Fxs2 Fys2 Fxss2 Ef2 W2. inverts E as E1 E2.
  splits~. rewrite Ef1 in Ef2. inverts Ef2.
  symmetry in W1, W2. applys~ typed_environment_write_variables_equivalent E1 W1 W2.
Qed.

Lemma wellformedness_merge_unique : forall V GammaDs1 GammaDs2 GammaD1 GammaD2 GammaD1' GammaD2',
  Forall2 (fun GammaDi1 GammaDi2 => forall GammaD1 GammaD2,
    GammaDi1 = Some GammaD1 ->
    GammaDi2 = Some GammaD2 ->
    wellformedness_eq_state GammaD1 GammaD2) GammaDs1 GammaDs2 ->
  wellformedness_eq_state GammaD1 GammaD2 ->
  wellformedness_merge GammaDs1 V GammaD1 GammaD1' ->
  wellformedness_merge GammaDs2 V GammaD2 GammaD2' ->
  wellformedness_eq_state GammaD1' GammaD2'.
Proof.
  introv F E M1 M2. inverts M1 as Ol1 FOGD1 FGD1 FD1 SI1 FG1.
  inverts M2 as Ol2 FOGD2 FGD2 FD2 SI2 FG2. inverts E as E1 E2.
  rewrite Forall2_iff_forall_Nth in F. lets (El&F'): (rm F).
  rewrite Forall3_iff_forall_Nth in FOGD1. lets (El1&El1'&F1): (rm FOGD1).
  rewrite Forall3_iff_forall_Nth in FOGD2. lets (El2&El2'&F2): (rm FOGD2).
  rewrite Forall_iff_forall_mem in FG1, FG2. splits~.
  - forwards (GammaD1&N1): length_Nth_lt 0 GammaDs1.
    { gen Ol1. clear. compute. math. }
    forwards (GammaD2&N2): length_Nth_lt.
    { rewrite <- El. applys Nth_lt_length N1. }
    forwards (Gamma1&N3): length_Nth_lt.
    { rewrite <- El1. applys Nth_lt_length N1. }
    forwards (Gamma2&N4): length_Nth_lt.
    { rewrite <- El2. applys Nth_lt_length N2. }
    applys~ typed_environment_equivalent_trans term_sort_inj flow_sort_inj FG1; [ applys Nth_mem N3 |].
    applys~ typed_environment_equivalent_trans  term_sort_inj flow_sort_inj;
      [| applys~ typed_environment_equivalent_sym term_sort_inj flow_sort_inj FG2; applys Nth_mem N4 ].
    applys~ typed_environment_merge_equivalent E1.
    applys* typed_environment_restrict_equivalent.
    forwards (Di1&N5): length_Nth_lt.
    { rewrite <- El1'. applys Nth_lt_length N3. }
    forwards (Di2&N6): length_Nth_lt.
    { rewrite <- El2'. applys Nth_lt_length N4. }
    forwards Ea: F1 N1 N3 N5. forwards Eb: F2 N2 N4 N6.
    applys~ F' N1 N2 Ea Eb.
  - apply set_st_eq. introv. iff (D&M&I); lets (n&N): Mem_Nth (rm M).
    + forwards (GammaD&N1): length_Nth_lt.
      { rewrite El1'. applys Nth_lt_length N. }
      forwards (O1&N2): length_Nth_lt.
      { rewrite El1. applys Nth_lt_length N1. }
      forwards E1': F1 N2 N1 N.
      forwards (O2&N3): length_Nth_lt.
      { rewrite <- El. applys Nth_lt_length N2. }
      forwards (GammaD'&N4): length_Nth_lt.
      { rewrite <- El2. applys Nth_lt_length N3. }
      forwards (D'&N5): length_Nth_lt.
      { rewrite <- El2'. applys Nth_lt_length N4. }
      forwards E2: F2 N3 N4 N5. forwards* EGammaD: F' N2 N3. exists D'. splits~.
      * applys~ Nth_Mem N5.
      * inverts~ EGammaD.
    + forwards (GammaD&N1): length_Nth_lt.
      { rewrite El2'. applys Nth_lt_length N. }
      forwards (O1&N2): length_Nth_lt.
      { rewrite El2. applys Nth_lt_length N1. }
      forwards E1': F2 N2 N1 N.
      forwards (O2&N3): length_Nth_lt.
      { rewrite El. applys Nth_lt_length N2. }
      forwards (GammaD'&N4): length_Nth_lt.
      { rewrite <- El1. applys Nth_lt_length N3. }
      forwards (D'&N5): length_Nth_lt.
      { rewrite <- El1'. applys Nth_lt_length N4. }
      forwards E2: F1 N3 N4 N5. forwards* EGammaD: F' N3 N2. exists D'. splits~.
      * applys~ Nth_Mem N5.
      * inverts~ EGammaD.
Qed.


Lemma wellformedness_empty_skeleton_exists : forall GammaD1 GammaD2 GammaD1',
  wellformedness_eq_state GammaD1 GammaD2 ->
  wellformedness_empty_skeleton GammaD1 GammaD1' ->
  exists GammaD2', wellformedness_empty_skeleton GammaD2 GammaD2'.
Proof. introv E W. destruct GammaD2. eexists. constructors~. Qed.

Lemma wellformedness_hook_exists : forall xf1 t xf2 GammaD1 GammaD2 GammaD1',
  wellformedness_eq_state GammaD1 GammaD2 ->
  wellformedness_hook xf1 t xf2 GammaD1 GammaD1' ->
  exists GammaD2', wellformedness_hook xf1 t xf2 GammaD2 GammaD2'.
Proof.
  introv E W. inverts W as IdD Dxf1 ID S R NDxf2 SE.
  destruct GammaD2 as [Gamma2 D2]. inverts E as E1 E2.
  eexists. constructors*.
  - applys incl_trans IdD. apply incl_prove. introv M.
    unfolds typed_environment_domain. rewrite in_set_st_eq in *.
    rewrites >> typed_environment_equivalent_spec term_sort_inj flow_sort_inj in E1. rewrite~ E1.
  - introv FV. applys heap_equiv_indom.
    + apply E1.
    + applys~ ID FV.
  - lets~ E': typed_environment_equivalent_term term_sort_inj flow_sort_inj E1. rewrite* <- E'.
  - lets~ E': typed_environment_equivalent_flow term_sort_inj flow_sort_inj E1. rewrite* <- E'.
  - applys~ typed_environment_equivalent_trans term_sort_inj flow_sort_inj SE.
    applys~ typed_environment_write_flow_equivalent.
Qed.

Lemma wellformedness_filter_exists : forall f xs ys GammaD1 GammaD2 GammaD1',
  wellformedness_eq_state GammaD1 GammaD2 ->
  wellformedness_filter f xs ys GammaD1 GammaD1' ->
  exists GammaD2', wellformedness_filter f xs ys GammaD2 GammaD2'.
Proof.
  introv E W. inverts W as IdD Fxs Fys Fxss Ef W.
  destruct GammaD2 as [Gamma2 D2]. inverts E as E1 E2.
  destruct (typed_environment_write_variables
    (@sort_term _ _ _) (@sort_flow _ _ _) (@sort_rect _ _ _) Gamma2 ys yss) eqn: E'.
  - eexists. constructors*.
    + applys incl_trans IdD. apply incl_prove. introv M.
      unfolds typed_environment_domain. rewrite in_set_st_eq in *.
      rewrites >> typed_environment_equivalent_spec term_sort_inj flow_sort_inj in E1. rewrite~ E1.
    +  applys Forall2_weaken Fxss. introv E.
      rewrites >> typed_environment_equivalent_spec term_sort_inj flow_sort_inj in E1. rewrite~ <- E1.
  - false* typed_environment_write_variables_defined E1 E'.
Qed.

(* This is frustratingly not true: the fact that there is nothing that prevent
  the list [O] to not contain [None] when it could have a [Some] makes an
  unexpected source of non-determinism.  As a consequence, the statement below
  doesn’t hold for [O2] being a list of [None].
Lemma wellformedness_merge_exists : forall O1 O2 V GammaD1 GammaD2 GammaD1',
  wellformedness_eq_state GammaD1 GammaD2 ->
  Forall2 (fun Oi1 Oi2 => forall Oiv1 Oiv2,
     Oi1 = Some Oiv1 ->
     Oi2 = Some Oiv2 ->
     wellformedness_eq_state Oiv1 Oiv2) O1 O2 ->
  wellformedness_merge O1 V GammaD1 GammaD1' ->
  exists GammaD2', wellformedness_merge O2 V GammaD2 GammaD2'.
Proof.
  introv E F W. inverts W as Li F1 F2 F3 F4 F5.
  destruct GammaD2 as [Gamma2 D2]. inverts E as E1 E2.
  rewrite Forall2_iff_forall_Nth in F. lets (lO&F'): (rm F).
  rewrite Forall3_iff_forall_Nth in F1. lets (lOG&lGD&F1'): (rm F1).
  rewrite Forall2_iff_forall_Nth in F2. lets (lGD''&F2'): (rm F2).
  rewrite Forall_iff_forall_mem in F5.
  forwards (Dis'&FDis): Forall_exists (fun (Oi : option wellformedness_environment) Di =>
                                         exists Gammai, Oi = Some (Gammai, Di)) O2.
  { rewrite Forall_iff_forall_mem. introv M. forwards (n&N): mem_Nth (rm M).
    skip. (* TODO *) }
  rewrite Forall2_iff_forall_Nth in FDis. lets (lGD'&FDis'): (rm FDis).
  forwards (Gammais'&FGammais): Forall_exists (fun (Oi : option wellformedness_environment) Gammai =>
                                                 exists Di, Oi = Some (Gammai, Di)) O2.
  { skip. (* TODO *) }
  rewrite Forall2_iff_forall_Nth in FGammais. lets (lOG'&FGammais'): (rm FGammais).
  eexists. applys~ wellformedness_merge_cons Gammais' Dis'.
  - unfold wellformedness_environment in lO. unfold WellFormedness.wellformedness_environment in lO.
    rewrite~ <- lO.
  - rewrite Forall3_iff_forall_Nth. splits~.
    + rewrite~ <- lGD'.
    + introv N1 N2 N3. forwards (?&E): FDis' N1 N3. forwards (?&E'): FGammais' N1 N2.
      substs. inverts~ E'.
  - rewrite Forall2_iff_forall_Nth. splits~.
    + rewrite~ <- lGD'.
    + introv N1 N2. forwards (vo&N3): length_Nth_lt.
      { rewrite lOG'. applys Nth_lt_length N1. }
      forwards (v1'&N4): length_Nth_lt.
      { rewrite lO. applys Nth_lt_length N3. }
      forwards (v2'&N5): length_Nth_lt.
      { rewrite <- lOG. applys Nth_lt_length N4. }
      forwards (v3'&N6): length_Nth_lt.
      { rewrite <- lGD. applys Nth_lt_length N5. }
      forwards I: F2' N5 N6. forwards: F1' N4 N5 N6. substs.
      forwards (?&E): FDis' N3 N2. forwards (?&E'): FGammais' N3 N1. substs. inverts E'.
      forwards~ E: F' N4 N3. lets (Ea&Eb): (rm E).
      rewrites <- >> sorting_environment_domain_equivalent Ea. substs~.
  - skip. (* TODO *)
  - skip. (* TODO *)
  - rewrite Forall_iff_forall_mem. introv M. lets (n&N'): mem_Nth (rm M).
    forwards (?&N1): length_Nth_lt.
    { rewrite lOG'. applys Nth_lt_length N'. }
    forwards (?&N2): length_Nth_lt.
    { rewrite lO. applys Nth_lt_length N1. }
    forwards (?&N): length_Nth_lt.
    { rewrite <- lOG. applys Nth_lt_length N2. }
    forwards (?&N3): length_Nth_lt.
    { rewrite <- lGD'. applys Nth_lt_length N1. }
    forwards (?&N4): length_Nth_lt.
    { rewrite <- lGD. applys Nth_lt_length N. }
    forwards E: F5; [ applys Nth_mem N |].
    applys sorting_environment_equivalent_trans E.
    applys sorting_environment_merge_equivalent E1.
    applys* sorting_environment_restrict_equivalent.
    forwards (?&E0): FDis' N1 N3. forwards (?&E0'): FGammais' N1 N'. substs. inverts E0'.
    forwards: F1' N2 N N4. substs. forwards~ E': F' N2 N1. lets~ (Ea&Eb): (rm E').
Qed.
*)

Let typed_environment_domain : sorting_environment -> set variable :=
  typed_environment_domain (@sort_term _ _ _) (@sort_flow _ _ _) (variable_flow := _).

Let typed_environment_domain_list : sorting_environment -> list variable :=
  typed_environment_domain_list _ _ _ _.

Let typed_environment_read_variable : sorting_environment -> variable -> option sort :=
  typed_environment_read_variable (@sort_term _ _ _) (@sort_flow _ _ _) (variable_flow := _).

Let typed_environment_write_variables
    : sorting_environment -> list variable -> list sort -> option sorting_environment :=
  typed_environment_write_variables
    (@sort_term _ _ _) (@sort_flow _ _ _) (@sort_rect _ _ _) (variable_flow := _).

Definition wellformedness_decidable_environment : Type := sorting_environment * list variable.

Definition wellformedness_decidable_environment_to_environment
  : wellformedness_decidable_environment -> wellformedness_environment :=
  fun GammaD => let (Gamma, D) := GammaD in (Gamma, to_set D).

Definition wellformedness_empty_skeleton_compute := @Some wellformedness_decidable_environment.

Lemma wellformedness_empty_skeleton_compute_correct : forall GammaD GammaDc GammaD' GammaDc',
  wellformedness_empty_skeleton_compute GammaDc = Some GammaDc' ->
  wellformedness_decidable_environment_to_environment GammaDc = GammaD ->
  wellformedness_decidable_environment_to_environment GammaDc' = GammaD' ->
  wellformedness_empty_skeleton GammaD GammaD'.
Proof. introv E E1 E2. inverts E. destruct GammaDc'. inverts E1. inverts E2. constructors~. Qed.

Lemma wellformedness_empty_skeleton_compute_defined : forall GammaD GammaDc GammaD',
  wellformedness_empty_skeleton GammaD GammaD' ->
  wellformedness_decidable_environment_to_environment GammaDc = GammaD ->
  wellformedness_empty_skeleton_compute GammaDc <> None.
Proof. introv E E'. discriminate. Qed.

Definition wellformedness_hook_compute xf1 t xf2 (GammaD : wellformedness_decidable_environment)
    : option wellformedness_decidable_environment :=
  let (Gamma, D) := GammaD in
  let _ := sort_of_term_Pickable_option _ _ constructor_signature basic_term_sort
    : forall variable_term_sort t, Pickable_option (sort_of_term variable_term_sort t) in
  match pick_option (sort_of_term (typed_environment_read_term Gamma) t) with
  | Some (sort_program s) =>
    ifb typed_environment_domain_list Gamma \c D
      /\ Mem (_X_ xf1) D
      /\ Forall (indom (typed_environment_term Gamma)) (term_free_variable_list t)
      /\ read_option (typed_environment_flow Gamma) xf1 = Some (in_sort s)
      /\ ~ Mem (_X_ xf2) D
    then Some (typed_environment_write_flow Gamma xf2 (out_sort s), _X_ xf2 :: D)
    else None
  | _ => None
  end.

Lemma wellformedness_hook_compute_correct : forall xf1 t xf2 GammaD GammaDc GammaD' GammaDc',
  wellformedness_hook_compute xf1 t xf2 GammaDc = Some GammaDc' ->
  wellformedness_decidable_environment_to_environment GammaDc = GammaD ->
  wellformedness_decidable_environment_to_environment GammaDc' = GammaD' ->
  wellformedness_hook xf1 t xf2 GammaD GammaD'.
Proof.
  introv E E1 E2. destruct GammaDc as [Gamma D], GammaDc' as [Gamma' D']. substs.
  unfolds in E. destruct pick_option as [s|] eqn: E'; tryfalse.
  destruct s as [|s]; tryfalse. cases_if; inverts E. fold_bool. rew_refl in C.
  lets (IGD&M1&F&Es&NM2): (rm C). constructors~.
  - rewrite incl_in_eq. introv I. apply typed_environment_domain_list_spec in I.
    unfolds to_set. rewrite in_set_st_eq. applys~ BagInIncl I IGD.
  - introv TF. rewrite <- term_free_variable_list_spec in TF. applys~ Mem_Forall TF F.
  - applys~ @pick_option_correct E'.
  - autos~.
  - applys~ typed_environment_equivalent_refl term_sort_inj flow_sort_inj.
  - rewrite set_ext_eq. introv. unfolds to_set. rewrite in_set_st_eq.
    rewrite in_union_eq. rewrite in_set_st_eq. rewrite in_single_eq. iff M; inverts* M.
Qed.

Lemma wellformedness_hook_compute_defined : forall xf1 t xf2 GammaD GammaDc GammaD',
  wellformedness_hook xf1 t xf2 GammaD GammaD' ->
  wellformedness_decidable_environment_to_environment GammaDc = GammaD ->
  wellformedness_hook_compute xf1 t xf2 GammaDc <> None.
Proof.
  introv WF E0 E. destruct GammaDc as [Gamma D]. substs. inverts WF as IGD M1 F Es ST NM2 EG.
  unfolds wellformedness_hook_compute. destruct pick_option eqn: Ep.
  - forwards~ Et: sort_of_term_unique Es. applys @pick_option_correct Ep. substs.
    cases_if~. fold_bool. rew_refl in C. false (rm C). splits~.
    + apply BagInIncl_make. introv I. eapply typed_environment_domain_list_spec in I.
      unfolds to_set. rewrite incl_in_eq in IGD. apply IGD in I. rewrite in_set_st_eq in I. apply~ I.
    + apply Forall_iff_forall_mem. introv M. apply F. apply term_free_variable_list_spec.
      rewrite~ Mem_mem.
  - forwards Ex: @pick_option_defined.
    { eexists. apply Es. }
    lets (?&A): (rm Ex). rewrite A in Ep. inverts~ Ep.
Qed.

Definition wellformedness_filter_compute f xs ys  (GammaD : wellformedness_decidable_environment)
    : option wellformedness_decidable_environment :=
  let (Gamma, D) := GammaD in
  let (xss, yss) := filter_signature f in
  match typed_environment_write_variables Gamma ys yss with
  | Some Gamma' =>
    ifb typed_environment_domain_list Gamma \c D
      /\ Forall (fun x => mem x D) xs
      /\ Forall (fun y => ~ mem y D) ys
      /\ Forall2 (fun x s => typed_environment_read_variable Gamma x = Some s) xs xss
    then Some (Gamma', D ++ ys)
    else None
  | None => None
  end.

Lemma wellformedness_filter_compute_correct : forall f xs ys GammaD GammaDc GammaD' GammaDc',
  wellformedness_filter_compute f xs ys GammaDc = Some GammaDc' ->
  wellformedness_decidable_environment_to_environment GammaDc = GammaD ->
  wellformedness_decidable_environment_to_environment GammaDc' = GammaD' ->
  wellformedness_filter f xs ys GammaD GammaD'.
Proof.
  introv E E1 E2. destruct GammaDc as [Gamma D], GammaDc' as [Gamma' D']. substs.
  unfolds in E. destruct filter_signature as [xss yss] eqn: Ef.
  destruct typed_environment_write_variables eqn: E'; tryfalse.
  cases_if; inverts E. fold_bool. rew_refl in C.
  lets (IGD&F1&F2&F3): (rm C). constructors; try apply Ef; autos~.
  - rewrite incl_in_eq. introv I. apply typed_environment_domain_list_spec in I.
    unfolds to_set. rewrite in_set_st_eq. applys~ BagInIncl I IGD.
  - applys Forall_weaken F1. introv M. rewrite <- Mem_mem in M. apply~ M.
  - applys Forall_weaken F2. introv M. rewrite <- Mem_mem in M. apply~ M.
  - rewrite set_ext_eq. introv. rewrite in_union_eq. unfolds to_set.
    repeat rewrite in_set_st_eq. rewrite* Mem_app_or_eq.
Qed.

Lemma wellformedness_filter_compute_defined : forall f xs ys GammaD GammaDc GammaD',
  wellformedness_filter f xs ys GammaD GammaD' ->
  wellformedness_decidable_environment_to_environment GammaDc = GammaD ->
  wellformedness_filter_compute f xs ys GammaDc <> None.
Proof.
  introv WF E0 E. destruct GammaDc as [Gamma D]. substs. inverts WF as IGD F1 F2 F3 E1 E2.
  unfolds wellformedness_filter_compute. rewrite E1 in E.
  unfolds typed_environment_write_variables. rewrite <- E2 in E.
  cases_if~. fold_bool. rew_refl in C. false (rm C). splits~.
  - apply BagInIncl_make. introv I. eapply typed_environment_domain_list_spec in I.
    unfolds to_set. rewrite incl_in_eq in IGD. apply IGD in I. rewrite in_set_st_eq in I. apply~ I.
  - apply Forall_iff_forall_mem. introv M. rewrite Forall_iff_forall_mem in F1.
    rewrite <- Mem_mem. applys~ F1 M.
  - apply Forall_iff_forall_mem. introv M. rewrite Forall_iff_forall_mem in F2.
    rewrite <- Mem_mem. applys~ F2 M.
Qed.

Definition wellformedness_merge_compute O V (GammaD : wellformedness_decidable_environment)
    : option wellformedness_decidable_environment :=
  let (Gamma, D) := GammaD in
  match fold_left (fun Oi GammaDi =>
    match Oi, GammaDi with
    | Some (Gammai, Di), Some (Gammais, Dis) => Some (Gammai :: Gammais, Di :: Dis)
    | None, _ => None
    | _, None => None
    end) (Some ([], [])) (rev O) with
  | Some (Gammais, Dis) =>
    let Gamma' :=
      let Gamma0 := nth 0 Gammais in
      typed_environment_merge Gamma (typed_environment_restrict Gamma0 V) in
    ifb length O >= 2
      /\ Forall2 (fun Gammai Di => typed_environment_domain_list Gammai \c Di) Gammais Dis
      /\ Forall (fun Di => D \c Di) Dis
      /\ Forall (fun i =>
           Forall (fun j =>
             i = j \/
               ((let Di := nth i Dis in
                 let Dj := nth j Dis in
                 (Di \- D) \n (Dj \- D) \c V)
               /\ (let Di := nth i Dis in
                   let Dj := nth j Dis in
                   V \c (Di \- D) \n (Dj \- D)))) (seq 0 (length Dis))) (seq 0 (length Dis))
      /\ Forall (fun Gammai =>
           typed_environment_equivalent Gamma'
             (typed_environment_merge Gamma (typed_environment_restrict Gammai V))) Gammais
    then Some (Gamma', concat Dis)
    else None
  | None => None
  end.

Lemma wellformedness_merge_compute_correct : forall O Oc V GammaD GammaDc GammaD' GammaDc',
  wellformedness_merge_compute Oc V GammaDc = Some GammaDc' ->
  Forall2 (fun O Oc =>
    O = LibOption.map_on Oc wellformedness_decidable_environment_to_environment) O Oc ->
  wellformedness_decidable_environment_to_environment GammaDc = GammaD ->
  wellformedness_decidable_environment_to_environment GammaDc' = GammaD' ->
  wellformedness_merge O V GammaD GammaD'.
Proof.
  introv E F E1 E2. destruct GammaDc as [Gamma D], GammaDc' as [Gamma' D']. substs.
  unfolds in E. destruct fold_left as [[Gammais Dis]|] eqn: Ef; tryfalse.
  cases_if; inverts E. fold_bool. rew_refl in C. lets (Li&F1&F2&F3&F4): (rm C).
  rewrite Forall2_iff_forall_Nth in F. lets (lO&FO): (rm F).
  rewrite Forall2_iff_forall_Nth in F1. lets (lGD&FGD): (rm F1).
  rewrite Forall_iff_forall_mem in F2. lets F4': F4. rewrite Forall_iff_forall_mem in F4.
  asserts (lOG&lOD&F): (length Oc = length Gammais /\ length Oc = length Dis
    /\ forall n v1 v2 v3,
         Nth n Oc v1 ->
         Nth n Gammais v2 ->
         Nth n Dis v3 ->
         v1 = Some (v2, v3)).
  { clear - Ef. rewrite fold_left_eq_fold_right in Ef. rewrite rev_rev in Ef.
    gen Gammais Dis. induction Oc; introv Ef.
    - inverts Ef. splits~. introv N1 N2 N3. inverts~ N1.
    - rew_list in Ef. destruct a as [[Gammai Di]|]; tryfalse.
      destruct fold_right as [[Gammais' Dis']|] eqn: Ef'; inverts Ef.
      forwards* (lOG&lOD&F): (rm IHOc). splits; rew_list~.
      introv N1 N2 N3. inverts N1 as N1; inverts N2 as N2; inverts N3 as N3; autos~.
      applys~ F N1 N2 N3. }
  clear Ef. applys wellformedness_merge_cons (map (@to_set _) Dis); try apply F4'.
  - unfold wellformedness_environment in lO. unfold WellFormedness.wellformedness_environment in lO.
    rewrite~ lO.
  - rewrite Forall3_iff_forall_Nth. splits~.
    + unfold wellformedness_environment in lO. unfold WellFormedness.wellformedness_environment in lO.
      rewrite~ lO.
    + rewrite~ length_map.
    + introv N1 N2 N3. forwards (v3'&N3'&Ev3'): map_Nth_inv N3. substs~.
      forwards (Oi&N4): length_Nth_lt.
      { rewrite <- lO. applys Nth_lt_length N1. }
      rewrites >> FO N1 N4. rewrites~ >> F N4 N2 N3'.
  - rewrite Forall2_iff_forall_Nth. splits~.
    + rewrite~ length_map.
    + introv N1 N2. forwards (v3&N3&Ev3): map_Nth_inv N2. substs~.
      forwards I: FGD N1 N3. apply incl_prove. introv M. fold variable in M.
      apply typed_environment_domain_list_spec in M. unfolds to_set. rewrite in_set_st_eq.
      applys~ BagInIncl M I.
  - rewrite Forall_iff_forall_mem. introv M. apply map_mem in M. lets (Di&MDi&EDi): (rm M). substs.
    apply incl_prove. introv M. unfolds to_set. rewrite in_set_st_eq in *.
    forwards I: F2 MDi. applys~ BagInIncl M I.
  - introv Dij Ni Nj. forwards (Di'&MDi'&EDi'): map_Nth_inv (rm Ni).
    forwards (Dj'&MDj'&EDj'): map_Nth_inv (rm Nj).
    rewrite Forall_iff_forall_mem in F3. asserts Mi: (mem i (seq 0 (length Dis))).
    { rewrite <- Mem_mem. rewrite In_Mem. apply in_seq. forwards: Nth_lt_length MDi'. math. }
    lets F3': (rm F3) (rm Mi). asserts Mj: (mem j (seq 0 (length Dis))).
    { rewrite <- Mem_mem. rewrite In_Mem. apply in_seq. forwards: Nth_lt_length MDj'. math. }
    rewrite Forall_iff_forall_mem in F3'. lets F3: (rm F3') (rm Mj).
    inverts F3 as I; tryfalse. lets (I1&I2): (rm I).
    rewrites >> Nth_to_nth MDi' in *. rewrites >> Nth_to_nth MDj' in *. substs.
    rewrite set_ext_eq. introv. rewrite in_inter_eq. repeat rewrite in_remove_eq.
    unfolds to_set. repeat rewrite notin_eq. repeat rewrite in_set_st_eq. iff M.
    + lets ((M1&M2)&(M3&M4)): (rm M). applys BagInIncl I1. rewrite BagInter_list.
      repeat rewrite BagRemove_list_list. rewrite* notin_eq.
    + forwards M': BagInIncl M I2. rewrite BagInter_list in M'.
      repeat rewrite BagRemove_list_list in M'. rewrite* notin_eq in M'.
  - rewrite set_ext_eq. introv. unfolds to_set. unfolds set_big_union. repeat rewrite in_set_st_eq.
    rewrite Mem_mem. rewrite concat_mem. iff (S&M1&M2).
    + exists (to_set S). splits~.
      * apply Mem_map. rewrite~ Mem_mem.
      * unfolds to_set. rewrite in_set_st_eq. rewrite~ Mem_mem.
    + lets (L&M3&M4): Mem_map_inv (rm M1). exists L. splits~.
      * rewrite~ <- Mem_mem.
      * substs. rewrite in_set_st_eq in M2. rewrite~ <- Mem_mem.
Qed.

Lemma wellformedness_merge_compute_defined : forall O Oc V GammaD GammaDc GammaD',
  wellformedness_merge O V GammaD GammaD' ->
  Forall2 (fun O Oc =>
    O = LibOption.map_on Oc wellformedness_decidable_environment_to_environment) O Oc ->
  wellformedness_decidable_environment_to_environment GammaDc = GammaD ->
  wellformedness_merge_compute Oc V GammaDc <> None.
Proof.
  introv WF F E1 E2. destruct GammaDc as [Gamma D]. inverts WF as Li F1 F2 F3 F4 F5.
  inverts TEMP0. (* WTF *)
  rewrite Forall2_iff_forall_Nth in F. lets (lO&FO): (rm F).
  rewrite Forall3_iff_forall_Nth in F1. lets (lOC&lGD&FOGD): (rm F1).
  rewrite Forall2_iff_forall_Nth in F2. lets (?&FGD): (rm F2).
  rewrite Forall_iff_forall_mem in F3. rewrite Forall_iff_forall_mem in F5.
  unfolds wellformedness_merge_compute. destruct fold_left as [[Gammaisc Disc]|] eqn: Ef.
  - asserts (lOG&lOD&F): (length Oc = length Gammaisc /\ length Oc = length Disc
      /\ forall n v1 v2 v3,
           Nth n Oc v1 ->
           Nth n Gammaisc v2 ->
           Nth n Disc v3 ->
           v1 = Some (v2, v3)).
    { clear - Ef. rewrite fold_left_eq_fold_right in Ef. rewrite rev_rev in Ef.
      gen Gammaisc Disc. induction Oc; introv Ef.
      - inverts Ef. splits~. introv N1 N2 N3. inverts~ N1.
      - rew_list in Ef. destruct a as [[Gammai Di]|]; tryfalse.
        destruct fold_right as [[Gammais' Dis']|] eqn: Ef'; inverts Ef.
        forwards* (lOG&lOD&F): (rm IHOc). splits; rew_list~.
        introv N1 N2 N3. inverts N1 as N1; inverts N2 as N2; inverts N3 as N3; autos~.
        applys~ F N1 N2 N3. }
    cases_if. fold_bool. rew_refl in C. false (rm C). splits~.
    + unfold wellformedness_decidable_environment in lO. unfold sorting_environment in lO.
      unfold WellFormedness.sorting_environment in lO. rewrite~ <- lO.
    + rewrite Forall2_iff_forall_Nth. splits~.
      * unfold sorting_environment. unfold WellFormedness.sorting_environment.
        rewrite <- lOG. rewrite~ <- lOD.
      * introv N1 N2.
        forwards (Oci&N3): length_Nth_lt.
        { rewrite lOG. applys Nth_lt_length N1. }
        forwards: F N3 N1 N2.
        forwards (Oi&N4): length_Nth_lt.
        { rewrite lO. applys Nth_lt_length N3. }
        forwards: FO N4 N3. substs. simpl in N4.
        forwards (Gammai&N5): length_Nth_lt.
        { rewrite <- lOC. applys Nth_lt_length N4. }
        forwards (Di&N6): length_Nth_lt.
        { rewrite <- lGD. applys Nth_lt_length N5. }
        forwards E': FOGD N4 N5 N6. inverts E'. forwards I: FGD N5 N6.
        apply BagInIncl_make. introv M. eapply typed_environment_domain_list_spec in M.
        rewrite incl_in_eq in I. forwards I': I M. unfold to_set in I'.
        rewrite in_set_st_eq in I'. apply~ I'.
    + rewrite Forall_iff_forall_mem. introv M. lets (n&N2): mem_Nth (rm M).
      forwards (Gammaic&N1): length_Nth_lt.
      { rewrite <- lOG. rewrite lOD. applys Nth_lt_length N2. }
      forwards (Oci&N3): length_Nth_lt.
      { rewrite lOG. applys Nth_lt_length N1. }
      forwards: F N3 N1 N2.
      forwards (Oi&N4): length_Nth_lt.
      { rewrite lO. applys Nth_lt_length N3. }
      forwards (Gammai&N5): length_Nth_lt.
      { rewrite <- lOC. applys Nth_lt_length N4. }
      forwards (Di&N6): length_Nth_lt.
      { rewrite <- lGD. applys Nth_lt_length N5. }
      forwards I: F3; [ applys~ Nth_mem N6 |].
      forwards: FOGD N4 N5 N6. forwards E: FO N4 N3. substs. inverts E.
      applys BagInIncl_make. introv M. rewrite incl_in_eq in I. forwards R: I.
      * unfolds to_set. rewrite in_set_st_eq. apply~ M.
      * unfolds to_set. rewrite in_set_st_eq in R. apply~ R.
    + rewrite Forall_iff_forall_mem. intros i M1. rewrite Forall_iff_forall_mem. intros j M2.
      tests Dij: (i = j); autos~. right.
      rewrite <- Mem_mem in M1, M2. apply In_Mem in M1. apply In_Mem in M2.
      apply in_seq in M1. apply in_seq in M2.
      forwards (Dic1&N1): length_Nth_lt i Disc; [ math |].
      forwards (Dic2&N2): length_Nth_lt j Disc; [ math |].
      forwards (Oc1&N3): length_Nth_lt.
      { rewrite lOD. applys Nth_lt_length N1. }
      forwards (Oc2&N4): length_Nth_lt.
      { rewrite lOD. applys Nth_lt_length N2. }
      forwards (Gammac1&N5): length_Nth_lt.
      { rewrite <- lOG. applys Nth_lt_length N3. }
      forwards (Gammac2&N6): length_Nth_lt.
      { rewrite <- lOG. applys Nth_lt_length N4. }
      forwards: F N3 N5 N1. forwards: F N4 N6 N2. substs.
      forwards (O1&N7): length_Nth_lt.
      { rewrite lO. applys Nth_lt_length N3. }
      forwards (O2&N8): length_Nth_lt.
      { rewrite lO. applys Nth_lt_length N4. }
      forwards E1: FO N7 N3. clear E2. forwards E2: FO N8 N4. simpl in E1, E2. substs.
      rewrites >> Nth_to_nth N1. rewrites >> Nth_to_nth N2.
      forwards (Gamma1&N9): length_Nth_lt.
      { rewrite <- lOC. applys Nth_lt_length N7. }
      forwards (Gamma2&N10): length_Nth_lt.
      { rewrite <- lOC. applys Nth_lt_length N8. }
      forwards (Di1&N11): length_Nth_lt.
      { rewrite <- lGD. applys Nth_lt_length N9. }
      forwards (Di2&N12): length_Nth_lt.
      { rewrite <- lGD. applys Nth_lt_length N10. }
      forwards~ E: F4 N11 N12. forwards E1: FOGD N7 N9 N11. inverts E1.
      forwards E2: FOGD N8 N10 N12. inverts E2. clear - E.
      rewrite set_ext_eq in E. splits; apply BagInIncl_make; introv M.
      * rewrite BagInter_list in M. lets (M1&M2): (rm M).
        repeat rewrite BagRemove_list_list in *. rewrite notin_eq in M1, M2.
        forwards E': (rm E) t. unfolds to_set. rewrite in_inter_eq in E'.
        repeat rewrite in_remove_eq in E'. rewrite notin_eq in E'.
        repeat rewrite in_set_st_eq in E'. simpl. fold variable in E'. rewrite* <- E'.
      * fold variable. rewrite BagInter_list. repeat rewrite BagRemove_list_list.
        rewrite notin_eq. forwards E': (rm E) t. unfolds to_set. rewrite in_inter_eq in E'.
        repeat rewrite in_remove_eq in E'. rewrite notin_eq in E'.
        repeat rewrite in_set_st_eq in E'. simpl. rewrite* E'.
    + rewrite Forall_iff_forall_mem. introv M. lets (n&N1): mem_Nth (rm M).
      forwards (Dic&N2): length_Nth_lt.
      { rewrite <- lOD. rewrite lOG. applys Nth_lt_length N1. }
      forwards (Oci&N3): length_Nth_lt.
      { rewrite lOG. applys Nth_lt_length N1. }
      forwards: F N3 N1 N2.
      forwards (Oi&N4): length_Nth_lt.
      { rewrite lO. applys Nth_lt_length N3. }
      forwards (Gammai&N5): length_Nth_lt.
      { rewrite <- lOC. applys Nth_lt_length N4. }
      forwards (Di&N6): length_Nth_lt.
      { rewrite <- lGD. applys Nth_lt_length N5. }
      forwards: FOGD N4 N5 N6. forwards E: FO N4 N3. substs. inverts E.
      applys~ typed_environment_equivalent_trans term_sort_inj flow_sort_inj Gamma'.
      * applys~ typed_environment_equivalent_sym term_sort_inj flow_sort_inj. apply F5.
        forwards (Gammai0&N7): length_Nth_lt 0 Gammaisc.
        { forwards: Nth_lt_length N1. math. }
        rewrites >> Nth_to_nth N7.
        forwards (Oic0&N8): length_Nth_lt.
        { rewrite lOG. applys Nth_lt_length N7. }
        forwards (Oi0&N9): length_Nth_lt.
        { rewrite lO. applys Nth_lt_length N8. }
        forwards (Gammai0'&N10): length_Nth_lt.
        { rewrite <- lOC. applys Nth_lt_length N9. }
        forwards (Dic0'&N11): length_Nth_lt.
        { rewrite <- lOD. applys Nth_lt_length N8. }
        forwards (Di0&N12): length_Nth_lt.
        { rewrite <- lGD. applys Nth_lt_length N10. }
        forwards: F N8 N7 N11. forwards: FO N9 N8. substs. simpl in N9.
        forwards E: FOGD N9 N10 N12. inverts E. applys~ Nth_mem N10.
      * apply F5. applys~ Nth_mem N5.
  - rewrite fold_left_eq_fold_right in Ef. rewrite rev_rev in Ef.
    clear - Ef lO lOC FO FOGD lGD. gen O Gammais Dis. induction Oc; introv lO FO lOG lGD FOGD.
    + inverts~ Ef.
    + rew_list in Ef. destruct a as [[Gammai Di]|].
      * destruct fold_right as [[? ?]|] eqn: Ef'; tryfalse. destruct O as [|? O]; tryfalse.
        destruct Gammais as [|? Gammais]; tryfalse. destruct Dis as [|? Dis]; tryfalse.
        forwards: FO 0; try apply Nth_here. applys~ IHOc O Gammais Dis.
        -- introv N1 N2. applys FO; apply* Nth_next.
        -- introv N1 N2 N3. applys FOGD; apply* Nth_next.
      * destruct O; tryfalse. forwards: FO 0; try apply Nth_here.
        destruct Gammais; tryfalse. destruct Dis; tryfalse.
        forwards A: FOGD 0; try apply Nth_here. substs. inverts A.
Qed.

(* See comment on [wellformedness_merge_exists].
Instance wellformedness_skeleton_pickable_option : forall S GammaD,
    Pickable_option (interpretation_skeleton
      (alternative_interpretation wellformedness_empty_skeleton_compute
                                  wellformedness_hook_compute
                                  wellformedness_filter_compute
                                  wellformedness_merge_compute) S GammaD).
  introv. eapply alternative_interpretation_skeleton_Pickable_option with (I := wellformedness).
  - apply wellformedness_eq_state_refl.
  - apply wellformedness_empty_skeleton_unique.
  - apply wellformedness_hook_unique.
  - apply wellformedness_filter_unique.
  - apply wellformedness_merge_unique.
  - apply wellformedness_empty_skeleton_exists.
  - apply wellformedness_hook_exists.
  - apply wellformedness_filter_exists.
  - apply wellformedness_merge_exists.
  - apply wellformedness_empty_skeleton_compute_correct.
  - apply wellformedness_hook_compute_correct.
  - apply wellformedness_filter_compute_correct.
  - apply wellformedness_merge_compute_correct.
  - apply wellformedness_empty_skeleton_compute_defined.
  - apply wellformedness_hook_compute_defined.
  - apply wellformedness_filter_compute_defined.
  - apply wellformedness_merge_compute_defined.
Defined.
*)

End Variables.

(** ** Well-formedness of Skeletal Semantics **)

(* TODO *)

End Sorts.

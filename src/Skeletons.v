(** Definitions of skeletons. **)

Set Implicit Arguments.

Require Import Coq.Program.Wf.
Require Import LibFix LibInt.
Require Import ExtLib.Data.HList ExtExtLib.
Require Export Terms.

Section Skeletons.

Variable constructor : Type.

Hypothesis constructor_Comparable : Comparable constructor.

Variable filter : Type.

Hypothesis filter_Comparable : Comparable filter.

Variable basic_term : Type.

Hypothesis basic_term_Comparable : Comparable basic_term.


(** * Semantic Format: Bones and Skeletons **)

Section Variables.

(** ** Symbolic Variables **)

Variable variable_term : Type.

Hypothesis variable_term_Comparable : Comparable variable_term.

(** The type of flow variables.
  They are used as intermediate variables in the skeletons.
  These variables store flow values. **)
Variable variable_flow : Type.

Hypothesis flow_variable_Comparable : Comparable variable_flow.

(** [x_\sigma] and [x_o] are supposed to be flow variables in the paper.
  We could thus assume that there exists two flow variables with some special
  properties.  This turned out to be quite heavy to manipulate with in practice.
  Instead, we decided to extend flow variables by these two special variables. **)
Inductive variable_flow_extended : Type :=
  | X_sigma : variable_flow_extended (** Input variable **)
  | X_o : variable_flow_extended (** Output variable **)
  | X_f : variable_flow -> variable_flow_extended (** Flow variable [x_{f_i}] **)
  .

(** Symbolic variables, or more simply variables, are either term variables or flow variables. **)
Inductive variable : Type :=
  | X_t : variable_term -> variable (** Term variable [x_{t_i}] **)
  | _X_ : variable_flow_extended -> variable
  .
Coercion _X_ : variable_flow_extended >-> variable.

Global Instance variable_flow_extended_Comparable : Comparable variable_flow_extended.
  prove_comparable.
Defined.

Global Instance variable_Comparable : Comparable variable.
  prove_comparable.
Defined.

(** We provide this constructor to ease the readability of term-variable
  declarations. **)
Definition _X_t constructor variable_term (x : variable_term) :
    basic_free_term constructor variable_term :=
  basic_free_term_from_False (term_variable x).

(** Similarly, we provide an alias for the constructor case. **)
Definition _term_constructor constructor variable_term c ts :=
  @basic_free_term_constructor constructor variable_term False c ts.

(** Coq’s coercions may not always work as wanted in all context, especially
  when the coerced types take parameters.  We provide the following aliases to
  help readability in the cases where they don’t.  We refer to our examples of
  skeletons for an example of the usage of these aliases. **)
Definition _X_sigma := X_sigma : variable.
Definition _X_o := X_o : variable.
Definition _X_f := X_f : variable_flow -> variable.


(** ** Definition of the Semantic Format **)

(** Our semantic format consists of skeletons, which are lists of bones.
  Bones are local operations.  They are divided into three distinct kinds:
  - hooks [H (x_{f_1}, t, x_{f_2})] can be seen as recursive calls.  When
    reaching a hook, the term [t] is evaluated in the environment given by
    [x_{f_1}].  The result is returned in the variable [x_{f_2}].
  - filters [f (x_1..x_n) ?> y_1..y_m] represent two operations that are
    usually disjoint: testing whether the current environmnent meets some
    properties, and performing local actions.  Intuitively, the filter
    [f] can be seen as a partial function with [n] arguments (the [x_n])
    and [m] return values (the [y_m]): if the filter is not defined in
    the particular values that it is given, then the execution stops,
    otherwise, the filter returns some values that are used in the rest
    of the skeleton.
  - Branches [(S_n)_V], which carry (sub)skeletons.  Each of these
    skeletons is a potential continuation and computes the variables
    declared in the set [V].  These computations can be seen as being in
    parallel.  Often, only one of the branch will return a result, and
    (usually) each branch starts by a filter checking whether the branch
    applies.  In other words, branches can encode pattern matching. **)

(** Note that in the intuition given above, it could easily be thought
  that skeletons represent the concrete semantics.  They do not.
  Instead, skeletons carry all the information needed to build both
  the concrete and the abstract semantics: they can intuitively be seen
  as being both concrete and abstract.
  More precisely, the concrete and abstract semantics are
  interpretations of skeletons: we define in the file [Interpretations.v]
  a formal notion of interpretation, then define in the folder
  [interpretations/] some interpretations, including the concrete
  and abstract interpretations.
  This enables us to prove that the abstract interpretation is sound
  with respect to the concrete interpretation regardless of the
  considered skeleton. **)

Let term : Type := term constructor basic_term variable_term.
Let basic_free_term : Type := basic_free_term constructor variable_term.

Inductive bone : Type :=
  | H : (** Hooks of the form [H (x_{f_1}, t, x_{f_2})] **)
        variable_flow_extended -> basic_free_term -> variable_flow_extended -> bone
  | F : (** Filters of the form [f (x_1..x_n) ?> y_1..y_m] **)
        filter -> list variable -> list variable -> bone
  | B : (** Branches of the form [(S_n)_V] **)
        list (list bone) -> list variable -> bone
  .

Definition skeleton := list bone.

(** We used lists in the branch case out of simplificity.  The type
  of [B] above should intuitively be read as
  [B : fset skeleton -> fset variable -> bone] instead, with finite
  sets replacing its lists: branches are meant to be considered in
  parallel, and their order don’t matter.
  The [fset] type is unfortunately more difficult to manipulate than
  lists.
  The other occurrences of [list]s in the definition of skeletons
  are however ordered: the order of bones matters, as the order of
  variables in a filter. **)

(** Skeletons are given some constraints.  For better compositionality,
  we defined these constraints directly as an interpretation, in the file
  [interpretations/WellFormedness.v].  Defining the well-formedness as
  an interpretation enables us to compose it with other interpretations:
  for instance, if we show that the concrete interpretation is
  consistent with the well-formedness interpretation, then the abstract
  interpretation is consistent with the concrete interpretation, we get
  the consistency of the abstract interpretation with respect to the
  well-formedness interpretation for free.  (In this precise case, we
  actually proved directly that the abstract interpretation is
  consistent with the well-formedness, as this consistency helps
  relating the abstract interpretation with the concrete.  We however
  do prove composition lemmas in [Interpretations.v].) **)


(** ** Auxiliary Definitions for the Semantic Format **)

(** *** Bone Depth **)

(** We need to define some notion of measure to be able to iterate over
  the bone structure.  The definitions that follow aim to do that. **)

Definition list_max := fold_left max 0.

Lemma list_max_Mem : forall e l,
  Mem e l ->
  e <= list_max l.
Proof.
  introv M. unfolds list_max. generalize 0. induction M; introv; simpl.
  - sets_eq n': (max e n). asserts I: (e <= n'); [substs; applys~ Max.le_max_l|].
    clear EQn'. gen n'. induction l; introv I; simpl.
    + autos~.
    + applys~ IHl. transitivity n'; autos~. applys Max.le_max_r.
  - applys~ IHM.
Qed.

Lemma list_max_cons : forall e l,
  list_max (e :: l) = max e (list_max l).
Proof.
  introv. unfolds list_max. rew_list. generalize 0. gen e. induction l; introv.
  - reflexivity.
  - rew_list. rewrite <- IHl. fequals.
    do 2 rewrite Max.max_assoc. fequals. rewrite~ Max.max_comm.
Qed.

(** The depth of a bone. **)
Fixpoint bone_depth b :=
  match b with
  | H _ _ _ => 0
  | F _ _ _ => 0
  | B Ss _ =>
    1 + list_max (List.map (fun S =>
          list_max (List.map bone_depth S)) Ss)
  end.

(** The depth of a skeleton. **)
Definition skeleton_depth S := list_max (map bone_depth S).

(** *** Induction Principle **)

Section BoneInd.

Variables (P : bone -> Prop) (Q : skeleton -> Prop)
    (f_H : forall x1 t x2, P (H x1 t x2))
    (f_F : forall f xs ys, P (F f xs ys))
    (f_B : forall Ss V, Forall Q Ss -> P (B Ss V))
    (g_nil : Q nil)
    (g_cons : forall b bs, P b -> Q bs -> Q (b :: bs)).

Arguments f_B Ss V : clear implicits.
Arguments g_cons b bs : clear implicits.

(** Because of the various [list]s and the mutual induction, the recursion
  principled defined by Coq is not really useful.  Here follow alternative
  induction principles for bones and skeletons. **)

Program Fixpoint bone_ind' (b : bone) {measure (bone_depth b)} : P b :=
  match b with
  | H x1 t x2 => f_H x1 t x2
  | F f xs ys => f_F f xs ys
  | B Ss V =>
    f_B Ss V (list_ind_Mem (Forall Q) (Forall_nil _)
               (fun S _ M =>
                 Forall_cons S (list_ind_Mem Q g_nil (fun b S M' =>
                   g_cons b S (bone_ind' b)))))
  end.
Next Obligation.
  asserts I1: (bone_depth b <= list_max (List.map bone_depth S0)).
  { apply~ list_max_Mem. rewrite <- map_List_map. apply~ Mem_map. }
  asserts I2: (bone_depth b <= list_max (List.map (fun S => list_max (List.map bone_depth S)) Ss)).
  { etransitivity; [ apply I1 |]. apply~ list_max_Mem. rewrite <- map_List_map. applys Mem_map M. }
  simpl. math.
Defined.

Definition skeleton_ind' S :=
  list_ind Q g_nil (fun b S => g_cons b S (bone_ind' b)) S.

Definition bone_and_skeleton_ind' : (forall b, P b : Prop) /\ (forall S, Q S : Prop) :=
  conj bone_ind' skeleton_ind'.

End BoneInd.

Definition bone_ind_Forall P f_H f_F f_B :=
  bone_ind' P f_H f_F f_B (Forall_nil P) (fun b S => @Forall_cons _ P S b).

Definition skeleton_ind_Forall P f_H f_F f_B :=
  skeleton_ind' P f_H f_F f_B (Forall_nil P) (fun b S => @Forall_cons _ P S b).

Definition bone_and_skeleton_ind_Forall P f_H f_F f_B :
    (forall b, P b : Prop) /\ (forall S, Forall P S) :=
  conj (bone_ind_Forall f_H f_F f_B)
       (skeleton_ind_Forall f_H f_F f_B).


(** *** Recursion Principle **)

(** This section provides an equivalent version of [bone_ind'] but in [Type] instead of [Prop]. **)
Section BoneRectDep.

Variables (P : bone -> Type) (Q : skeleton -> Type)
    (f_H : forall x t y, P (H x t y))
    (f_F : forall f xs ys, P (F f xs ys))
    (f_B : forall Ss V, hlist Q Ss -> P (B Ss V))
    (g_nil : Q nil)
    (g_cons : forall b S, P b -> Q S -> Q (b :: S)).

Arguments f_B Ss V : clear implicits.
Arguments g_cons b S : clear implicits.

Program Fixpoint bone_rect_dep (b : bone) {measure (bone_depth b)} : P b :=
  match b with
  | H x1 t x2 => f_H x1 t x2
  | F f xs ys => f_F f xs ys
  | B Ss V =>
    f_B Ss V (@hlist_gen_member _ Q Ss (fun S M =>
               list_rect_Mem Q g_nil (fun b S M' q =>
                 g_cons b S (bone_rect_dep b) q)))
  end.
Next Obligation.
  asserts M0: (Mem S0 Ss).
  { apply In_Mem. applys member_In M. }
  asserts I1: (bone_depth b <= list_max (List.map bone_depth S0)).
  { apply~ list_max_Mem. rewrite <- map_List_map. apply~ Mem_map. }
  asserts I2: (bone_depth b <= list_max (List.map (fun S => list_max (List.map bone_depth S)) Ss)).
  { etransitivity; [ apply I1 |]. apply~ list_max_Mem. rewrite <- map_List_map. applys Mem_map M0. }
  simpl. math.
Defined.

Definition skeleton_rect_dep S :=
  list_rect Q g_nil (fun b S => g_cons b S (bone_rect_dep b)) S.

Definition bone_and_skeleton_rect_dep : (forall b, P b) * (forall S, Q S) :=
  (bone_rect_dep, skeleton_rect_dep).

End BoneRectDep.

(** The approach of [bone_rect_dep] works, but the terms generated by
  [Program Fixpoint] are extremelly difficult to unfold and thus to
  reason about.
  This section presents another version based on TLC’s optimal fixpoint
  combinator. **)
Section BoneRect'.

Variables (P : Type) (Q : Type)
    (f_H : variable_flow_extended -> basic_free_term -> variable_flow_extended -> P)
    (f_F : filter -> list variable -> list variable -> P)
    (f_B : list skeleton -> list variable -> list Q -> P)
    (g_nil : Q)
    (g_cons : bone -> skeleton -> Q -> P -> Q).

Hypothesis P_Inhab : Inhab P.

(** This section follows the aproach of TLC’s optimal fixpoint combinator:
  [Bone_rect'] takes as an argument a function [bone_rect'] and uses it in
  each recursive calls, then [bone_rect'] uses the [FixFun] fixpoint
  combinator to take its fixpoint.  This fixpoint is however not guaranteed
  to terminate as-is, and Coq will refuse to unfold it.  To be able to use
  this definition, we need to prove [bone_rect'_fix], showing that it is
  well-formed, and thus that it can be unfolded. **)

Definition Bone_rect' bone_rect' (b : bone) : P :=
  match b with
  | H x1 t x2 => f_H x1 t x2
  | F f xs ys => f_F f xs ys
  | B Ss V =>
    f_B Ss V (map (list_rect (fun _ => Q) g_nil (fun b S q =>
                g_cons b S q (bone_rect' b))) Ss)
  end.

Definition bone_rect' := FixFun Bone_rect'.

Lemma bone_rect'_fix : forall b,
  bone_rect' b = Bone_rect' bone_rect' b.
Proof.
  introv. applys~ FixFun_fix (LibWf.measure bone_depth).
  - applys LibWf.measure_wf.
  - introv E. destruct~ x as [| |Bs V]. simpl. fequals. induction~ Bs.
    rew_list. fequals.
    + clear IHBs. induction a as [|ba bs].
      * reflexivity.
      * asserts I: (bone_depth ba < bone_depth (B ((ba :: bs) :: Bs) V))%nat.
        { simpl. rewrite list_max_cons. apply le_lt_n_Sm.
          etransitivity; [| apply~ Nat.le_max_l ].
          rewrite list_max_cons. apply~ Nat.le_max_l. }
        simpl. fequals.
        -- apply IHbs. introv I'. apply E. unfolds. unfolds in I'. clear - I'.
           asserts I2: (bone_depth (B (bs :: Bs) V) <= bone_depth (B ((ba :: bs) :: Bs) V))%nat.
           { clear. unfolds bone_depth. simpl. rewrite le_SS. repeat rewrite list_max_cons.
             apply~ Nat.max_lub.
             - etransitivity; [| apply~ Nat.le_max_l ]. apply Nat.le_max_r.
             - apply~ Nat.le_max_r. }
           math.
        -- apply E. unfolds. math.
    + apply IHBs. introv I. apply E. unfolds. unfolds in I.
      asserts I': (bone_depth (B Bs V) <= bone_depth (B (a :: Bs) V)).
      { simpl. rewrite list_max_cons. rewrite le_SS. apply~ Nat.le_max_r. }
      math.
Qed.

End BoneRect'.

Definition skeleton_rect' P Q `{Inhab P} f_H f_F f_B g_nil g_cons (S : skeleton) :=
  list_rect (fun _ => Q) g_nil (fun b S H =>
    g_cons b S H (bone_rect' (P := P) f_H f_F f_B g_nil g_cons _ b)) S.

Lemma bone_rect'_unfold : forall P Q IP f_H f_F f_B g_nil g_cons Ss V,
  (bone_rect' f_H f_F f_B g_nil g_cons IP (B Ss V) : P)
  = f_B Ss V (LibList.map (skeleton_rect' f_H f_F f_B g_nil g_cons : _ -> Q) Ss).
Proof. introv. rewrite bone_rect'_fix. simpl. fequals. Qed.

Definition skeleton_bone_rect P Q `{Inhab P} f_H f_F f_B g_nil g_cons :=
  (fun S => @skeleton_rect' P Q _ f_H f_F f_B g_nil g_cons S,
   fun b => bone_rect' f_H f_F f_B g_nil g_cons b).

End Variables.

Global Arguments X_sigma [_].
Global Arguments X_o [_].
Global Arguments X_t [_ _].
Global Arguments X_f [_].
Global Arguments _X_ [_ _].


(** ** Definition of Skeletal Semantics **)

(** *** Definition of Skeleton Instances **)

(** In contrary what is shown in the paper, the type of term and
  intermediary variables are given by skeleton instances.  In the
  paper, terms variables are always [x_{t_1}], [x_{t_2}], etc. and
  intermediary variables are always [x_{f_1}], [x_{f_2}], etc. or
  sometimes [x_{f_{1'}}], etc.  We can here index them by any
  types. There are several reasons for that.  One is that the same
  variable can be reused in different skeletons with a different sort.
  To avoid having to think of these conflicts when building rules, it
  is useful to be able to reason about each rule independently.
  Another reason is that it improves readability in an already
  difficult to read formalisation.  Indeed, some types are more
  suitable for some skeletons (for readability reasons mostly), and we
  might let the user decide which is which for each rule.  Our example
  [examples/While.v] tries to use this freedom to help readability. **)
Record skeleton_instance := make_skeleton_instance {
    (**  The (term and flow) variables used in this specific rule. **)
    skeleton_variable_term : Type ;
    skeleton_variable_flow : Type ;
    skeleton_term_variables : list skeleton_variable_term ; (** The term variables associated with the constructor. **)
    skeleton_skeleton : skeleton skeleton_variable_term skeleton_variable_flow
  }.

(** The type [variable] instantiated to a particular skeletal instance. **)
Definition variable_skeleton (r : skeleton_instance) : Type :=
  variable (skeleton_variable_term r) (skeleton_variable_flow r).


(** *** Skeletal Semantics **)

(** There is exactly one skeleton for each constructor
  (this is Requirement 2.2 of the paper).
  This naturally translates the skeletal semantics as a function. **)

Definition skeletal_semantics := constructor -> skeleton_instance.

End Skeletons.

Global Arguments _X_t [_ _].
Global Arguments _term_constructor [_ _].

Global Arguments _X_sigma [_ _].
Global Arguments _X_o [_ _].
Global Arguments _X_f [_ _].

Global Arguments H [_ _ _ _].
Global Arguments F [_ _ _ _].
Global Arguments B [_ _ _ _].

Global Arguments make_skeleton_instance [_ _] {_ _} _ _.


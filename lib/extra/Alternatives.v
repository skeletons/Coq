(** Alternatives provide type classes for giving a computable equivalent of
  non-computable structures.  Typically, these type classes replace [set]s
  by [list]s, as well as all their associated operations. **)

Require Import TLC.LibFunc TLC.LibChoice TLC.LibList.
Require Import Common.
Set Implicit Arguments.

(** * Type Class Definitions **)

(** ** Definitions **)

(** States that [C] is a computable variant of [A], and the
  way that they are related. **)
Class Alternative (A C : Type) :=
  make_alternative {
    altr : A -> C -> Prop
  }.

(** Given associated [A] and [C], provides a variant of a constant. **)
Class Alternative_Representation A C `{Alternative A C} (a : A) :=
  make_alternative_representation {
    alt : C ;
    alt_spec : altr a alt
  }.
Arguments Alternative_Representation A C {_} a.
Arguments alt {A C _} a {_}.

(** A stronger mark stating that all elements are uniquely represented. **)
(** Note that it is very strong, as it is based on the Leibniz equality
  and not just any relation. **)
Class Alternative_Strong (A C : Type) `{Alternative A C} :=
  make_alternative_strong {
    altr_exists : forall a,
      exists c, altr a c ; (* FIXME: Do we want to put that aside? *)
    altr_exists_inv : forall c,
      exists a, altr a c ;
    altr_unique : forall a c1 c2,
      altr a c1 -> altr a c2 -> c1 = c2 ;
    altr_unique_inv : forall c a1 a2,
      altr a1 c -> altr a2 c -> a1 = a2
  }.
Arguments Alternative_Strong A C {_}.

(** Sometimes a particular representation is strong whilst the generic one isn’t. **)
Class Alternative_Representation_Strong A C `{Alternative A C} a :=
  make_alternative_representation_strong {
    alt_strong :> Alternative_Representation A C a ;
    alt_unique : forall c,
      altr a c -> c = alt a
  }.
Arguments Alternative_Representation_Strong A C {_} a.


(** ** Utilities **)

(** This utility function helps extract the hidden parameter of [Alternative_Representation]. **)
Definition Alternative_Representation_Alternative {A C H a} :
    @Alternative_Representation A C H a -> Alternative A C :=
  fun _ => H.

(** This utility forces Coq to infer the strong variant out of it. **)
Definition Alternative_Representation_Alternative_Strong {A C H a} {S : @Alternative_Strong A C H} :
    Alternative_Representation A C a -> _ :=
  fun _ => S.


(** * Generic Properties **)

(** ** Relations Between Type Classes **)

Instance Alternative_self : forall A,
  Alternative A A.
Proof. introv. applys make_alternative (@eq A). Defined.

Instance Alternative_Strong_self : forall A,
  Alternative_Strong A A.
Proof. introv. constructors; simpl; intros; substs; autos*. Qed.

Instance Alternative_fun : forall A1 A2 C1 C2,
  Alternative A1 C1 ->
  Alternative A2 C2 ->
  Alternative (A1 -> A2) (C1 -> C2).
Proof.
  introv S1 S2. applys make_alternative (fun (fa : A1 -> A2) (fc : C1 -> C2) =>
                         forall a c, altr a c -> altr (fa a) (fc c)).
Defined.

Instance Alternative_Strong_fun : forall A1 A2 C1 C2 `{Alternative A1 C1} `{Alternative A2 C2},
  Alternative_Strong A1 C1 ->
  Alternative_Strong A2 C2 ->
  Alternative_Strong (A1 -> A2) (C1 -> C2).
Proof.
  introv S1 S2. constructors; simpl; introv.
  - forwards (c&P): @func_choice (fun (c1 : C1) (c2 : C2) =>
      forall a1, altr a1 c1 -> altr (a a1) c2).
    { intro c1. lets (a1&R1): (altr_exists_inv c1).
      lets (c2&R2): (altr_exists (a a1)). exists c2.
      introv R3. forwards~: @altr_unique_inv R1 R3. substs~. }
    exists~ c.
  - forwards (a&P): @func_choice (fun (a1 : A1) (a2 : A2) =>
      forall c1, altr a1 c1 -> altr a2 (c c1)).
    { intro a1. lets (c1&R1): (altr_exists a1).
      lets (a2&R2): (altr_exists_inv (c c1)). exists a2.
      introv R3. forwards~: @altr_unique R1 R3. substs~. }
    exists~ a.
  - introv R1 R2. extens. intro c.
    forwards (a1&P): (altr_exists_inv c). applys~ altr_unique (a a1).
  - introv R1 R2. extens. intro a.
    forwards (c1&P): (altr_exists a). applys~ altr_unique_inv (c c1).
Qed.

Instance Alternative_Representation_fun : forall A1 A2 C1 C2 f a
    `{Alternative A1 C1} `{Alternative A2 C2},
  Alternative_Representation (A1 -> A2) (C1 -> C2) f ->
  Alternative_Representation A1 C1 a ->
  Alternative_Representation A2 C2 (f a).
Proof.
  introv Af Aa. applys make_alternative_representation ((alt f : C1 -> C2) (alt a)).
  applys (alt_spec (a := f)). apply alt_spec.
Defined.

(** This is a weak version as it doesn’t change the type [T]. **)
Instance Alternative_Representation_fun_rev : forall A C T f `{Alternative A C},
  (forall t, Alternative_Representation A C (f t)) ->
  Alternative_Representation (T -> A) (T -> C) f.
Proof.
  introv Af. eapply make_alternative_representation with (alt := fun t =>
                      let _ := Af t in alt (f t)).
  simpl. intros t1 t2 ?. substs. applys~ (@alt_spec _ _ _ _ (Af t2)).
Defined.

(* The following two lemmas are not instances, otherwise Coq enters a loop
  with these lemmas and [Alternative_(Strong_)self]. *)

Lemma Alternative_trans : forall A B C,
  Alternative A B ->
  Alternative B C ->
  Alternative A C.
Proof.
  introv S1 S2. applys make_alternative (fun (a : A) (c : C) =>
                          exists b, altr a b /\ altr b c).
Defined.

Lemma Alternative_Strong_trans : forall A B C `{Alternative A B} `{Alternative B C},
  let _ := Alternative_trans _ _ in
  Alternative_Strong A B ->
  Alternative_Strong B C ->
  Alternative_Strong A C.
Proof.
  introv AB BC. constructors.
  - introv. forwards (b&ab): (altr_exists a).
    forwards (c&bc): (altr_exists b). exists c b. splits*.
  - introv. forwards (b&bc): (altr_exists_inv c).
    forwards (a&ab): (altr_exists_inv b). exists a b. splits*.
  - introv (b1&Ra1&Rb1) (b2&Ra2&Rb2). forwards~: @altr_unique Ra1 Ra2. substs.
    applys~ altr_unique Rb1 Rb2.
  - introv (b1&Ra1&Rb1) (b2&Ra2&Rb2). forwards~: @altr_unique_inv Rb1 Rb2. substs.
    applys~ @altr_unique_inv Ra1 Ra2.
Qed.

(* The following two lemmas are not instances, otherwise Coq enters in a loop. *)

Lemma Alternative_sym : forall A C,
  Alternative A C ->
  Alternative C A.
Proof. introv S. applys make_alternative (fun (c : C) (a : A) => altr a c). Defined.

Lemma Alternative_Strong_sym : forall A C `{Alternative A C},
  let _ := Alternative_sym _ in
  Alternative_Strong A C ->
  Alternative_Strong C A.
Proof.
  introv S. constructors.
  - introv. apply~ altr_exists_inv.
  - introv. apply altr_exists.
  - introv R1 R2. symmetry. applys~ @altr_unique_inv R2 R1.
  - introv R1 R2. symmetry. applys~ @altr_unique R2 R1.
Qed.

Instance Alternative_product : forall A1 A2 C1 C2,
  Alternative A1 C1 ->
  Alternative A2 C2 ->
  Alternative (A1 * A2) (C1 * C2).
Proof.
  introv S1 S2. applys make_alternative (fun (a : A1 * A2) (c : C1 * C2) =>
                          altr (fst a) (fst c) /\ altr (snd a) (snd c)).
Defined.

Instance Alternative_Strong_product : forall A1 A2 C1 C2 `{Alternative A1 C1} `{Alternative A2 C2},
  Alternative_Strong A1 C1 ->
  Alternative_Strong A2 C2 ->
  Alternative_Strong (A1 * A2) (C1 * C2).
Proof.
  introv S1 S2. constructors; simpl; introv.
  - forwards (c1&R1): (altr_exists (fst a)).
    forwards (c2&R2): (altr_exists (snd a)). exists (c1, c2). split*.
  - forwards (a1&R1): (altr_exists_inv (fst c)).
    forwards (a2&R2): (altr_exists_inv (snd c)). exists (a1, a2). split*.
  - introv (Ra1&Rb1) (Ra2&Rb2).
    forwards~: @altr_unique Ra1 Ra2. forwards~: @altr_unique Rb1 Rb2.
    destruct c1, c2. simpls. substs~.
  - introv (Ra1&Rb1) (Ra2&Rb2).
    forwards~: @altr_unique_inv Ra1 Ra2. forwards~: @altr_unique_inv Rb1 Rb2.
    destruct a1, a2. simpls. substs~.
Qed.

Instance Alternative_Prop : forall A C,
  Alternative A C ->
  Alternative (A -> Prop) (C -> Prop).
Proof.
  introv S. applys make_alternative (fun (Pa : A -> Prop) (Pc : C -> Prop) =>
                     forall a c, altr a c -> Pa a <-> Pc c).
Defined.

Instance Alternative_Strong_Prop : forall A C `{Alternative A C},
  Alternative_Strong A C ->
  Alternative_Strong (A -> Prop) (C -> Prop).
Proof.
  introv S. constructors; simpl; introv.
  - forwards (c&P): @func_choice (fun (c : C) (P : Prop) =>
      forall aa, altr aa c -> a aa <-> P).
    { intro cc. forwards (aa&P): altr_exists_inv cc.
      exists (a aa). introv R. forwards: altr_unique_inv P R. substs*. }
    exists* c.
  - forwards (a&P): @func_choice (fun (a : A) (P : Prop) =>
      forall cc, altr a cc -> P <-> c cc).
    { intro aa. forwards (cc&P): altr_exists aa.
      exists (c cc). introv R. forwards: altr_unique P R. substs*. }
    exists* a.
  - introv R1 R2. extens. intro c. forwards (aa&P): altr_exists_inv c.
    rewrites <- >> R1 P. rewrites* <- >> R2 P.
  - introv R1 R2. extens. intro a. forwards (cc&P): altr_exists a.
    rewrites >> R1 P. rewrites* >> R2 P.
Qed.

Instance Alternative_lift_predicate : forall A C,
  Alternative A C ->
  Alternative (A -> Prop) C.
Proof.
  introv S. applys make_alternative (fun (P : A -> Prop) (c : C) =>
                     forall a, altr a c <-> P a).
Defined.

(** Set as a lemma and not as a typeclass to prevent loops. **)
Lemma Alternative_Strong_Alternative_Representation_Strong : forall A C a `{Alternative A C},
  Alternative_Strong A C ->
  Alternative_Representation A C a ->
  Alternative_Representation_Strong A C a.
Proof.
  introv S AR. constructors.
  introv R. applys~ @altr_unique R. apply~ alt_spec.
Qed.

Instance Alternative_Strong_Inhab : forall A C `{Alternative A C},
  Alternative_Strong A C ->
  Inhab C ->
  Inhab A.
Proof.
  introv S I. forwards (a&R): altr_exists_inv (arbitrary : C).
  applys prove_Inhab a.
Defined.

(** If the alternative predicate is decidable, then so does the initial one. **)
Instance Alternative_Prop_Decidable : forall A C P `{Alternative A C}
  (AP : Alternative_Representation (A -> Prop) (C -> Prop) P),
  Alternative_Strong A C ->
  (forall a : A, Pickable (altr a)) ->
  (forall c : C, Decidable (alt P c)) ->
  (forall a : A, Decidable (P a)).
Proof.
  introv AS Pi De. introv.
  applys Decidable_equiv (decide (alt P (pick (altr a)))).
  - symmetry. rewrite decide_spec. rewrite istrue_isTrue_iff.
    apply (@alt_spec _ _ _ _ AP). apply pick_spec. apply~ altr_exists.
  - typeclass.
Defined.

(** The following two lemmas are not marked as instances to avoid looping. **)

Lemma Alternative_Representation_Trans : forall A B C a
    `{Alternative A B} `{Alternative B C},
  let _ := Alternative_trans _ _ : Alternative A C in
  Alternative_Representation A B a ->
  (forall b, Alternative_Representation B C b) ->
  Alternative_Representation A C a.
Proof.
  introv AA AB. applys make_alternative_representation a (alt (alt a)).
  exists (alt a). splits~; apply alt_spec.
Defined.

Lemma Alternative_fun_contravariant : forall A1 A2 C1 C1' C2 F
    `{Alternative A1 C1} `{Alternative C1 C1'} `{Alternative A2 C2},
  let _ := Alternative_trans _ _ : Alternative A1 C1' in
  (forall c, Alternative_Representation C1 C1' c) ->
  Alternative_Representation (A1 -> A2) (C1' -> C2) F ->
  Alternative_Representation (A1 -> A2) (C1 -> C2) F.
Proof.
  introv Fc Af. applys make_alternative_representation F
                         (let _ := Alternative_trans _ _ : Alternative A1 C1' in
                          fun c : C1 => alt F (alt c) : C2).
  introv E. destruct Af as [FC FCspec]. simpl.
  apply FCspec. simpls. exists c. splits~. apply alt_spec.
Defined.

Instance Alternative_Representation_fun_compose : forall A1 A2 A3 C1 C2 C3 f12 f23
    `{Alternative A1 C1} `{Alternative A2 C2} `{Alternative A3 C3},
  Alternative_Representation (A1 -> A2) (C1 -> C2) f12 ->
  Alternative_Representation (A2 -> A3) (C2 -> C3) f23 ->
  Alternative_Representation (A1 -> A3) (C1 -> C3) (compose f23 f12).
Proof.
  introv S12 S23. applys make_alternative_representation (compose f23 f12)
                    (compose (alt f23) (alt f12)).
  introv R. unfolds @compose. apply S23, S12, R.
Defined.


(** ** Some Useful Instance to Get Decidability **)

Instance Alternative_pred_list : forall A C,
  Alternative A C ->
  Alternative (A -> Prop) (list C).
Proof.
  introv AC. applys make_alternative (fun P l => forall a c,
                altr a c -> (@Mem C c l <-> P a)).
Defined.

Instance Alternative_Pickable_list : forall A (P : A -> Prop),
  Pickable_list P ->
  @Alternative_Representation (A -> Prop) (list A) (Alternative_pred_list _) P.
Proof.
  introv Pl. apply make_alternative_representation with (pick_list P).
  introv R. simpl in R. substs. apply pick_list_correct.
Defined.

Instance Alternative_pred_element : forall A C,
  Alternative A C ->
  Alternative (A -> Prop) (option C).
Proof.
  introv AC. applys make_alternative (fun (P : A -> Prop) o =>
               match o with
               | None => forall a, ~ P a
               | Some c => exists a, altr a c /\ P a
               end).
Defined.

Instance Alternative_Pickable_option : forall A C (P : A -> Prop) `{Alternative A C},
  Pickable_option P ->
  (forall a, Alternative_Representation A C a) ->
  Alternative_Representation (A -> Prop) (option C) P.
Proof.
  introv Po Aa. applys make_alternative_representation P
                  (LibOption.map (fun a : A => alt a : C) (pick_option P)).
  destruct pick_option as [a|] eqn: E.
  - apply pick_option_correct in E. eexists. splits*. apply alt_spec.
  - introv Pa. forwards* (?&E'): pick_option_defined.
    rewrite E' in E. inverts~ E.
Defined.

Instance Alternative_Prop_bool : Alternative Prop bool.
Proof. applys make_alternative (fun P b => P <-> istrue b). Defined.

Instance Alternative_Strong_Prop_bool : Alternative_Strong Prop bool.
Proof.
  constructors; introv; simpl.
  - exists (isTrue a). rew_refl*.
  - exists* c.
  - introv E1 E2. rewrite E1 in E2. applys bool_ext E2.
  - introv E1 E2. rewrite <- E2 in E1. extens~.
Qed.

Instance Alternative_Decidable : forall P,
  Decidable P ->
  Alternative_Representation Prop bool P.
Proof.
  introv D. apply make_alternative_representation with (decide P).
  simpl. rewrite decide_spec. rew_refl*.
Defined.

Definition Decidable_Alternative : forall P,
  Alternative_Representation Prop bool P ->
  Decidable P.
Proof.
  introv D. apply decidable_make with (alt P). extens.
  forwards I: (@alt_spec _ _ _ P D). rew_refl*.
Defined.


(** ** Generic Functions **)

Instance Alternative_Representation_self : forall A a,
  Alternative_Representation A A a.
Proof. introv. apply make_alternative_representation with a. reflexivity. Defined.

Instance Alternative_Representation_self_Strong : forall A a,
  Alternative_Representation_Strong A A a.
Proof.
  introv. constructors.
  introv R. simpl in R. substs. reflexivity.
Qed.

Instance Alternative_Representation_func : forall A1 A2 C1 C2
    `{Alternative A1 C1} `{Alternative A2 C2} a,
  Alternative_Representation A2 C2 a ->
  Alternative_Representation (A1 -> A2) (C1 -> C2) (fun _ => a).
Proof.
  introv Aa. applys make_alternative_representation (fun _ : A1 => a) (fun _ : C1 => alt a).
  introv ?. apply~ alt_spec.
Defined.

Instance Alternative_Representation_id : forall A C `{Alternative A C},
  Alternative_Representation (A -> A) (C -> C) id.
Proof. introv. applys make_alternative_representation (@id A) (@id C). introv R. apply R. Defined.

Instance Alternative_Representation_pair : forall A1 A2 C1 C2
    `{Alternative A1 C1} `{Alternative A2 C2} a1 a2,
  Alternative_Representation A1 C1 a1 ->
  Alternative_Representation A2 C2 a2 ->
  Alternative_Representation (A1 * A2) (C1 * C2) (a1, a2).
Proof.
  introv Aa1 Aa2. applys make_alternative_representation (a1, a2) (alt a1, alt a2).
  split; [ apply Aa1 | apply Aa2 ].
Defined.

Instance Alternative_Representation_pair_Strong : forall A1 A2 C1 C2
    `{Alternative A1 C1} `{Alternative A2 C2} a1 a2,
  Alternative_Representation_Strong A1 C1 a1 ->
  Alternative_Representation_Strong A2 C2 a2 ->
  Alternative_Representation_Strong (A1 * A2) (C1 * C2) (a1, a2).
Proof.
  introv S1 S2. constructors.
  intros (c1&c2) (R1&R2). simpls. fequals; apply~ @alt_unique.
Qed.


(** ** Miscellaneous **)

Lemma Alternative_Representation_fun_Strong_weak : forall T A C f t `{Alternative A C},
  Alternative_Representation_Strong (T -> A) (T -> C) f ->
  Alternative_Representation_Strong A C (f t).
Proof.
  introv Sf. refine (let AR := ltac:(typeclass) : forall t, Alternative_Representation A C (f t) in _).
  constructors.
  introv R. simpl. lets I: (@alt_unique _ _ _ _ Sf). simpl in I.
  sets_eq fc: (fun t' : T => If t' = t then c else alt (f t')).
  forwards E: I fc.
  { introv E. substs. cases_if~. applys~ @alt_spec (f c0). }
  rewrite <- E. rewrite EQfc. cases_if~.
Qed.

(*
(* FIXME: Is there a way to generalise [Alternative_Representation_fun_Strong_weak]? *)
Instance Alternative_Representation_fun_Strong : forall A1 A2 C1 C2 f a
    `{Alternative A1 C1} `{Alternative A2 C2}
    {Af : Alternative_Representation (A1 -> A2) (C1 -> C2) f}
    {Aa : forall a, Alternative_Representation A1 C1 a},
  Alternative_Representation_Strong (A1 -> A2) (C1 -> C2) f ->
  (forall a', Alternative_Representation_Strong A1 C1 a') ->
  Alternative_Representation_Strong A2 C2 (f a).
Proof.
  introv Sf Sa. constructors.
  intros c2 R2. simpl.
  lets I: @alt_unique Sf. simpl in I.
  sets_eq fc: (fun c1 : C1 => If c1 = alt a then c2 else alt f c1).
  forwards E: I fc.
  { intros a1 c1 R1. rewrite EQfc. cases_if.
    - forwards Ea: @alt_unique (Sa a1) R1.

      lets R: (@alt_spec _ _ _ _ Af). simpl in R.
      lets R': (@alt_spec _ _ _ _ (Aa a)). rewrite Ea in R'.
      apply R in R'.

      skip.
    - lets R: (@alt_spec _ _ _ f Af). simpl in R. applys R R1. }
  rewrite <- E. rewrite EQfc. cases_if~.
Defined.
*)


(** * Instances **)

(** ** Options **)

Require Import TLC.LibOption.

Instance Alternative_option : forall A C,
  Alternative A C ->
  Alternative (option A) (option C).
Proof. introv AC. applys make_alternative (eq_option altr). Defined.

Instance Alternative_Strong_option : forall A C `{Alternative A C},
  Alternative_Strong A C ->
  Alternative_Strong (option A) (option C).
Proof.
  introv AC. constructors; simpl; introv.
  - destruct a as [a|].
    + forwards (c&E): altr_exists a. exists (Some c). constructors~.
    + eexists. constructors~.
  - destruct c as [c|].
    + forwards (a&E): altr_exists_inv c. exists (Some a). constructors~.
    + eexists. constructors~.
  - introv R1 R2. apply eq_option_eq.
    eapply eq_option_sym with (eq2 := fun c a => altr a c) in R1; [| autos* ].
    applys eq_option_trans R1 R2.
    simpl. introv E1 E2. applys~ altr_unique E1 E2.
  - introv R1 R2. apply eq_option_eq.
    eapply eq_option_sym with (eq2 := fun c a => altr a c) in R2; [| autos* ].
    applys eq_option_trans R1 R2.
    simpl. introv E1 E2. applys~ altr_unique_inv E1 E2.
Qed.

Instance Alternative_Representation_None : forall A C `{Alternative A C},
  Alternative_Representation (option A) (option C) None.
Proof. introv. applys make_alternative_representation (@None A) (@None C). constructors~. Defined.

Instance Alternative_Representation_Some : forall A C a `{Alternative A C},
  Alternative_Representation A C a ->
  Alternative_Representation (option A) (option C) (Some a).
Proof.
  introv Aa. applys make_alternative_representation (Some a) (Some (alt a)).
  constructors~. apply Aa.
Defined.

Instance Alternative_Representation_is_some : forall A C `{Alternative A C},
  Alternative_Representation (option A -> bool) (option C -> bool) (@is_some _).
Proof.
  introv. applys make_alternative_representation (@is_some A) (@is_some C).
  intros a c R. destruct a, c; (reflexivity || inverts~ R).
Defined.

Instance Alternative_Representation_unsome_default : forall A C `{Alternative A C} a,
  Alternative_Representation A C a ->
  Alternative_Representation (option A -> A) (option C -> C) (unsome_default a).
Proof.
  introv Aa. applys make_alternative_representation (unsome_default a) (unsome_default (alt a)).
  intros a' c' R. destruct a', c'; (reflexivity || inverts~ R).
  apply~ alt_spec.
Defined.

Instance Alternative_Representation_map_option : forall A1 A2 C1 C2 f
    `{Alternative A1 C1} `{Alternative A2 C2},
  Alternative_Representation (A1 -> A2) (C1 -> C2) f ->
  Alternative_Representation (option A1 -> option A2) (option C1 -> option C2) (map f).
Proof.
  introv Af. applys make_alternative_representation (map f) (map (alt f : C1 -> C2)).
  intros a c R. destruct a, c; (reflexivity || inverts R as R'); constructors~.
  applys (alt_spec (C := C1 -> C2) (a := f)) R'.
Defined.

Instance Alternative_Representation_map_on_option : forall A1 A2 C1 C2 a f
    `{Alternative A1 C1} `{Alternative A2 C2},
  Alternative_Representation (A1 -> A2) (C1 -> C2) f ->
  Alternative_Representation (option A1) (option C1) a ->
  Alternative_Representation (option A2) (option C2) (map_on a f).
Proof.
  introv Af Aa. applys make_alternative_representation (map_on a f) (map_on (alt a) (alt f : C1 -> C2)).
  unfolds map_on. apply Alternative_Representation_map_option. apply alt_spec.
Defined.

Instance Alternative_Representation_apply_option : forall A1 A2 C1 C2 f
    `{Alternative A1 C1} `{Alternative A2 C2},
  Alternative_Representation (A1 -> option A2) (C1 -> option C2) f ->
  Alternative_Representation (option A1 -> option A2) (option C1 -> option C2) (apply f).
Proof.
  introv Af. applys make_alternative_representation (apply f) (apply (alt f : C1 -> _)).
  intros a c R. destruct a, c; try inverts R as R'; try constructors.
  unfolds apply. applys (alt_spec (C := C1 -> _) (a := f)) R'.
Defined.

Instance Alternative_Representation_apply_on_option : forall A1 A2 C1 C2 a f
    `{Alternative A1 C1} `{Alternative A2 C2},
  Alternative_Representation (A1 -> option A2) (C1 -> option C2) f ->
  Alternative_Representation (option A1) (option C1) a ->
  Alternative_Representation (option A2) (option C2) (apply_on a f).
Proof.
  introv Af Aa. applys make_alternative_representation (apply_on a f)
                  (apply_on (alt a) (alt f : C1 -> _)).
  unfolds apply_on. apply Alternative_Representation_apply_option. apply alt_spec.
Defined.


(** ** Lists **)

Require Import TLC.LibList.

Instance Alternative_list : forall A C,
  Alternative A C ->
  Alternative (list A) (list C).
Proof. introv AC. applys make_alternative (Forall2 altr). Defined.

Instance Alternative_Strong_list : forall A C `{Alternative A C},
  Alternative_Strong A C ->
  Alternative_Strong (list A) (list C).
Proof.
  introv AC. constructors.
  - introv. apply Forall_exists. rewrite Forall_iff_forall_mem.
    introv M. apply~ altr_exists.
  - introv. forwards (a&F): Forall_exists; [| exists a; applys Forall2_swap F ].
    rewrite Forall_iff_forall_mem. introv M. apply~ altr_exists_inv.
  - simpl. introv R1 R2. apply Forall2_eq. repeat rewrite Forall2_iff_forall_Nth in *.
    lets (E1&F1): (rm R1). lets (E2&F2): (rm R2). splits.
    + rewrite~ <- E1.
    + introv N1 N2. forwards (a0&N): length_Nth_lt.
      { rewrite E1. applys Nth_lt_length N1. }
      apply* altr_unique.
  - simpl. introv R1 R2. apply Forall2_eq. repeat rewrite Forall2_iff_forall_Nth in *.
    lets (E1&F1): (rm R1). lets (E2&F2): (rm R2). splits.
    + rewrite~ E1.
    + introv N1 N2. forwards (c0&N): length_Nth_lt.
      { rewrite <- E1. applys Nth_lt_length N1. }
      apply* altr_unique_inv.
Qed.

Instance Alternative_nil : forall A C `{Alternative A C},
  Alternative_Representation (list A) (list C) nil.
Proof. introv. applys make_alternative_representation (@nil A) (@nil C). constructors~. Defined.

Instance Alternative_cons : forall A C a la `{Alternative A C},
  Alternative_Representation A C a ->
  Alternative_Representation (list A) (list C) la ->
  Alternative_Representation (list A) (list C) (a :: la).
Proof.
  introv Aa Ala. applys make_alternative_representation (a :: la) (alt a :: alt la).
  constructors~; [ apply Aa | apply Ala ].
Defined.

Instance Alternative_rev : forall A C la `{Alternative A C},
  Alternative_Representation (list A) (list C) la ->
  Alternative_Representation (list A) (list C) (rev la).
Proof.
  introv Ala. applys make_alternative_representation (rev la) (rev (alt la) : list C).
  applys~ Forall2_rev Ala.
Defined.

Instance Alternative_fold_left : forall A1 A2 C1 C2 f a `{Alternative A1 C1} `{Alternative A2 C2},
  Alternative_Representation (A1 -> A2 -> A2) (C1 -> C2 -> C2) f ->
  Alternative_Representation A2 C2 a ->
  Alternative_Representation (list A1 -> A2) (list C1 -> C2) (fold_left f a).
Proof.
  introv Af Aa. applys make_alternative_representation (fold_left f a)
                         (fold_left (alt f : C1 -> _) (alt a)).
  intros la lc R. lets Ra: (alt_spec (a := a)). sets_eq a': (alt a). clear EQa' Aa.
  gen a a' lc. induction la; introv Ra R; inverts R as RH RN.
  - apply Ra.
  - simpl. applys IHla RN. applys~ (alt_spec (C := C1 -> _) (a := f)) RH Ra.
Defined.

Instance Alternative_fold_right : forall A1 A2 C1 C2 f a `{Alternative A1 C1} `{Alternative A2 C2},
  Alternative_Representation (A1 -> A2 -> A2) (C1 -> C2 -> C2) f ->
  Alternative_Representation A2 C2 a ->
  Alternative_Representation (list A1 -> A2) (list C1 -> C2) (fold_right f a).
Proof.
  introv Af Aa. applys make_alternative_representation (fold_right f a)
                         (fold_right (alt f : C1 -> _) (alt a)).
  intros la lc R. lets Ra: (alt_spec (a := a)). sets_eq a': (alt a). clear EQa' Aa.
  gen a a' lc. induction la; introv Ra R; inverts R as RH RN.
  - apply Ra.
  - simpl. applys (alt_spec (C := C1 -> _) (a := f)) RH. applys~ IHla Ra RN.
Defined.

(* TODO
     Definition map : forall A C : Type, (A -> C) -> list A -> list C.
     Definition append : forall A : Type, list A -> list A -> list A.
     Definition concat : forall A : Type, list (list A) -> list A.
     Definition length : forall A : Type, list A -> nat.
     Definition filter : forall A : Type, LibOperation.predb A -> list A -> list A.
     Definition for_all : forall A : Type, LibOperation.predb A -> list A -> bool.
     Definition exists_st :
       forall A : Type, LibOperation.predb A -> list A -> bool.
     Definition count : forall A : Type, LibOperation.predb A -> list A -> nat.
     Definition head : forall A : Type, list A -> option A.
     Definition tail : forall A : Type, list A -> list A.
*)


(** ** Heaps **)

Require Import TLC.LibHeap.

Instance Alternative_heap : forall K A C,
  Comparable K ->
  Alternative A C ->
  Alternative (heap K A) (heap K C).
Proof.
  introv CK AC. applys make_alternative (fun ha hc : heap K _ => forall k,
                         altr (read_option ha k : option A) (read_option hc k : option C)).
Defined.

Instance Alternative_read_option : forall A C K ha `{Comparable K} `{Alternative A C},
  Alternative_Representation (heap K A) (heap K C) ha ->
  Alternative_Representation (K -> option A) (K -> option C) (read_option ha).
Proof.
  introv Ah. applys make_alternative_representation (read_option ha) (read_option (alt ha : heap K C)).
  intros ka kb E. simpl in E. substs. applys (alt_spec (C := heap K C) (a := ha)).
Defined.


(** ** Sets **)

Require Import TLC.LibSet.

(** We provide two representations for sets: either lists or functions to booleans. **)

Instance Alternative_set_fun : forall A, Alternative (set A) (A -> bool).
Proof. introv. applys make_alternative (fun S (f : A -> bool) => S = set_st f). Defined.

Instance Alternative_fun_list : forall A, Alternative (A -> bool) (list A).
Proof. introv. applys make_alternative (fun (f : A -> bool) l => forall e, f e <-> Mem e l). Defined.

Instance Alternative_set_list : forall A, Alternative (set A) (list A).
Proof. introv. applys Alternative_trans. Defined.

Lemma Alternative_set_list_spec : forall A S
    (Ar : Alternative_Representation (set A) (list A) S),
  S = set_st (fun e => Mem e (alt S)).
Proof. introv. lets (f&E1&E2): (alt_spec (a := S)). simpls. substs. apply~ set_st_eq. Qed.

(** Both these representations come with different precision, and one can naturally
  go from one to the other. **)

Instance Alternative_list_fun : forall A l,
  Comparable A ->
  let _ := Alternative_sym _ : Alternative (list A) (A -> bool) in
  Alternative_Representation (list A) (A -> bool) l.
Proof.
  introv C. applys make_alternative_representation l (fun a => decide (Mem a l)).
  introv. rew_refl. iff~.
Defined.

Lemma Alternative_set_list_intro : forall A S l,
  Comparable A ->
  S = set_st (fun e => Mem e l) ->
  Alternative_Representation (set A) (list A) S.
Proof.
  introv C E. applys make_alternative_representation S l.
  substs. exists
    (let _ := Alternative_sym _ : Alternative (list A) (A -> bool) in
     alt l : A -> bool).
  split~; simpl.
  - apply set_st_eq. introv. rew_refl. iff~.
  - introv. rew_refl. iff~.
Defined.

Instance Alternative_set_list_fun : forall A S,
  Comparable A ->
  Alternative_Representation (set A) (list A) S ->
  Alternative_Representation (set A) (A -> bool) S.
Proof.
  introv C Af. applys make_alternative_representation (fun a => decide (Mem a (alt S))).
  forwards E: Alternative_set_list_spec S. rewrite E at 1.
  apply set_st_eq. introv. rew_refl. iff*.
Defined.

Instance Alternative_set_fun_list : forall A B C F `{Alternative B C},
  Comparable A ->
  Alternative_Representation (set A -> B) ((A -> bool) -> C) F ->
  Alternative_Representation (set A -> B) (list A -> C) F.
Proof.
  introv CA Af. applys make_alternative_representation F (fun l =>
                         alt F (fun a : A => decide (Mem a l)) : C).
  introv (f&E1&E2). simpls. substs.
  destruct Af as [FC FCspec]. simpl. apply FCspec.
  apply set_st_eq. introv. rew_refl. apply~ E2.
Defined.

(** Each construct is then given in the type with the most information. **)

Instance Alternative_empty_impl : forall A,
  Comparable A ->
  Alternative_Representation (set A) (list A) (\{} : set A).
Proof.
  introv C. applys~ Alternative_set_list_intro (@nil A).
  rewrite set_in_extens_eq. introv. rew_set. iff Ab; inverts Ab.
Defined.

Instance Alternative_single_impl : forall A a,
  Comparable A ->
  Alternative_Representation (set A) (list A) (\{ a }).
Proof.
  introv C. applys~ Alternative_set_list_intro ([a]).
  rewrite set_in_extens_eq. introv. rew_set.
  iff Ab; repeat inverts Ab as Ab; constructor~.
Defined.

Instance Alternative_in_impl : forall A a S,
  Alternative_Representation (set A) (A -> bool) S ->
  Alternative_Representation Prop bool (a \in S).
Proof.
  introv AS. applys~ make_alternative_representation (a \in S) (alt S a).
  lets E: (alt_spec (a := S)). simpls. rewrite E at 1. rewrite in_set_st_eq. iff~.
Defined.

(* FIXME: I’m missing hypotheses there.
Instance Alternative_compl_impl : forall A S,
  Alternative_Representation (set A) (A -> bool) S ->
  Alternative_Representation (set A) (A -> bool) (compl_impl S).
Proof.
  introv AS. applys~ make_alternative_representation (compl_impl S) (fun a => negb (alt S a)).
  simpl. rewrite set_in_extens_eq. introv. unfolds compl_impl. rew_set.
  rew_refl. lets altSSpec: (alt_spec (a := S)). simpl in altSSpec.
  rewrite altSSpec at 1. unfolds pred_not. compute.
Admitted. *)

Instance Alternative_union_impl : forall A S1 S2,
  Alternative_Representation (set A) (A -> bool) S1 ->
  Alternative_Representation (set A) (A -> bool) S2 ->
  Alternative_Representation (set A) (A -> bool) (S1 \u S2).
Proof.
  introv AS1 AS2. applys~ make_alternative_representation (S1 \u S2)
                            (fun a => alt S1 a || alt S2 a).
  simpl. rewrite set_in_extens_eq. introv. rew_set.
  rewrite in_union_eq. rew_refl.
  lets E1: (alt_spec (a := S1)). lets E2: (alt_spec (a := S2)).
  simpls. rewrite E1, E2 at 1. rew_set. autos*.
Defined.

Instance Alternative_union_impl_list : forall A S1 S2,
  Comparable A ->
  Alternative_Representation (set A) (list A) S1 ->
  Alternative_Representation (set A) (list A) S2 ->
  Alternative_Representation (set A) (list A) (S1 \u S2).
Proof.
  introv C AS1 AS2. applys~ Alternative_set_list_intro (alt S1 ++ alt S2).
  rewrite set_in_extens_eq. introv. rew_set.
  rewrite in_union_eq. rewrite Mem_app_or_eq.
  forwards E1: Alternative_set_list_spec S1. forwards E2: Alternative_set_list_spec S2.
  rewrite E1, E2 at 1. rew_set. autos*.
Defined.

(* TODO
     Definition inter_impl : forall A : Type, set A -> set A -> set A.
     Definition remove_impl : forall A : Type, set A -> set A -> set A.
     Definition incl_impl : forall A : Type, set A -> set A -> Prop.
     Definition disjoint_impl : forall A : Type, set A -> set A -> Prop.
     Definition list_repr_impl : forall A : Type, set A -> list A -> Prop.
     Definition to_list : forall A : Type, set A -> list A.
     Definition to_set : forall A : Type, list A -> set A.
     Definition list_covers_impl : forall A : Type, set A -> list A -> Prop.
     Definition card_impl : forall A : Type, set A -> nat.
     Definition fold_impl :
       forall A B : Type, LibStruct.monoid_def B -> (A -> B) -> set A -> B.
*)

Instance Alternative_finite : forall A S,
  Comparable A ->
  Alternative_Representation (set A) (list A) S ->
  Alternative_Representation Prop bool (finite S).
Proof.
  introv C Af. applys~ make_alternative_representation (finite S) true.
  simpl. rew_refl. iff~. exists (alt S). introv I.
  forwards E: Alternative_set_list_spec S. rewrite E in I. unfolds in I. apply~ I.
Defined.

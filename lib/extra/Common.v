(** General lemmae.
  This file contains all the constructs that I consider should be in my libraries by are not.
  It can be seen as a personnal library.
  Note that as TLC is changing, some of these lemmae already have been added to it:
  this file may need some cleanup to update to fresher versions of TLC. **)

Require Import TLC.LibStream TLC.LibFset TLC.LibHeap TLC.LibString TLC.LibNat TLC.LibInt.
Require Export TLC.LibTactics TLC.LibReflect TLC.LibLogic TLC.LibList TLC.LibBool.

Notation " [ ] " := nil : list_scope.
Notation " [ x ] " := (cons x nil) : list_scope.
Notation " [ x ; .. ; y ] " := (cons x .. (cons y nil) ..) : list_scope.

Set Implicit Arguments.


(** * Useful tactics **)

(** ** Apply a list of hypotheses **)

Ltac apply_first_base L :=
  let L := list_boxer_of L in
  lazymatch L with
  | boxer ?P :: ?L' =>
    apply~ P || apply_first_base L'
  end.

Tactic Notation "apply_first" constr(E) :=
  apply_first_base E.
Tactic Notation "apply_first" constr(E0) constr(A1) :=
  apply_first (>> E0 A1).
Tactic Notation "apply_first" constr(E0) constr(A1) constr(A2) :=
  apply_first (>> E0 A1 A2).
Tactic Notation "apply_first" constr(E0) constr(A1) constr(A2) constr(A3) :=
  apply_first (>> E0 A1 A2 A3).
Tactic Notation "apply_first" constr(E0) constr(A1) constr(A2) constr(A3) constr(A4) :=
  apply_first (>> E0 A1 A2 A3 A4).
Tactic Notation "apply_first" constr(E0) constr(A1) constr(A2) constr(A3) constr(A4) constr(A5) :=
  apply_first (>> E0 A1 A2 A3 A4 A5).


Ltac applys_first L A :=
  let L := list_boxer_of L in
  let A := list_boxer_of A in
  lazymatch L with
  | boxer ?P :: ?L' =>
    applys_base (boxer P :: A) || applys_first L' A
  end.


(** ** Rewrite a list of equalities **)

Ltac rewrite_first L :=
  let L := list_boxer_of L in
  lazymatch L with
  | boxer ?P :: ?L' =>
    rewrite~ P || rewrite_first L'
  end.


(** ** Fresh identifiers **)

(** The original [fresh] tactic doesn’t work if given hints that are
  function applications. These tactics accept any hint.
  Unfortunately, they don’t convert Coq strings into identifiers. **)

Ltac fresh1 N :=
  match goal with
  | |- _ => let r := fresh N in r
  | |- _ => let r := fresh in r
  end.

Ltac fresh2 N1 N2 :=
  match goal with
  | |- _ => let r := fresh N1 N2 in r
  | |- _ => let r := fresh N1 in r
  | |- _ => let r := fresh N2 in r
  | |- _ => let r := fresh in r
  end.

Ltac fresh3 N1 N2 N3 :=
  match goal with
  | |- _ => let r := fresh N1 N2 N3 in r
  | |- _ => let r := fresh N1 N2 in r
  | |- _ => let r := fresh N1 N3 in r
  | |- _ => let r := fresh N2 N3 in r
  | |- _ => let r := fresh N1 in r
  | |- _ => let r := fresh N2 in r
  | |- _ => let r := fresh N3 in r
  | |- _ => let r := fresh in r
  end.


(** * May be added in TLC **)

(* The following lemmae should be added in TLC. I have a version of
  the commit in this computer, but the branch coq-8.6 of TLC is frozen
  and the branch coq-8.6-new does not yet compile. Frustration. *)

Lemma Forall_Mem : forall A (l : list A),
  Forall (fun a => Mem a l) l.
Proof.
  introv. induction l; constructors~.
  applys Forall_weaken IHl. introv M. applys~ Mem_next M.
Qed.

Lemma Mem_Forall : forall A (l : list A) P a,
  Mem a l ->
  Forall P l ->
  P a.
Proof. introv M F. rewrite Mem_mem in M. rewrite Forall_iff_forall_mem in F. applys~ F M. Qed.

Lemma Forall_map : forall A B (Pa Pb : _ -> Prop) (f : A -> B) l,
  Forall Pa l ->
  (forall a, Pa a -> Pb (f a)) ->
  Forall Pb (map f l).
Proof. introv F I. induction F; constructors~. Qed.

Lemma Forall_exists : forall A B (P : A -> B -> Prop) l,
  Forall (fun a => exists b, P a b) l ->
  exists L, Forall2 P l L.
Proof.
  introv F. induction F.
  - eexists. constructors~.
  - lets (b&Pb): (rm H). lets (L&F'): (rm IHF). eexists. constructors*.
Qed.

Instance Forall_Decidable_Mem : forall A (P : A -> Prop) l,
  (forall a, Mem a l -> Decidable (P a)) ->
  Decidable (Forall P l).
Proof.
  introv F. induction l as [|a l].
  - applys Decidable_equiv True.
    + iff~ I. constructors.
    + typeclass.
  - applys Decidable_equiv (P a /\ Forall P l).
    + iff I.
      * constructors*.
      * inverts* I.
    + apply and_decidable; typeclass.
Defined.

Lemma Forall_Forall : forall A (P Q : A -> Prop) l,
  Forall P l ->
  Forall Q l ->
  Forall (fun a => P a /\ Q a) l.
Proof.
  introv F1 F2. induction l.
  - constructors~.
  - inverts F1 as Pa F1. inverts F2 as Qa F2. constructors*.
Qed.

Lemma Forall2_inv : forall A1 A2 P (l1 : list A1) r' x1,
  Forall2 P (x1::l1) r' ->
  exists (r2:list A2) x2,
  r' = x2::r2 /\ P x1 x2 /\ Forall2 P l1 r2.
Proof using. introv H. inverts* H. Qed.

Lemma Forall2_iff_forall_Nth : forall A1 A2 (P : A1 -> A2 -> Prop) (l1 : list A1) (l2 : list A2),
  Forall2 P l1 l2 <->
  length l1 = length l2 /\ (forall n v1 v2, Nth n l1 v1 -> Nth n l2 v2 -> P v1 v2).
Proof using.
  introv. gen l2.
  induction l1 as [|x1 l1]; intros [|x2 l2]; (iff I; [ splits | inverts I as I1 I2 ]);
    try solve [ inverts~ I ]; tryfalse~.
   introv N1. inverts N1.
   constructors.
   do 2 rewrite length_cons. inverts I as I1 I2. rewrite* IHl1 in I2.
   introv N1 N2. inverts I as I1 I2. rewrite IHl1 in I2. inverts N1; inverts~ N2.
    apply* (proj2 I2).
   constructors.
    apply~ I2; constructors.
    rewrite IHl1. splits~. introv N1 N2. apply~ I2; constructors*.
Qed.

Lemma Forall2_compose : forall A1 A2 A3 (P Q R : _ -> _ -> Prop)
    (l1 : list A1) (l2 : list A2) (l3 : list A3),
  Forall2 P l1 l2 ->
  Forall2 Q l2 l3 ->
  (forall x y z, P x y -> Q y z -> R x z) ->
  Forall2 R l1 l3.
Proof using.
  introv F12 F23 I. rewrite Forall2_iff_forall_Nth in *. splits.
   transitivity (length l2); autos*.
   introv N1 N2. asserts (v&N): (exists v, Nth n l2 v); [| autos* ].
    apply length_Nth_lt. rewrite (proj1 F23). apply* Nth_lt_length.
Qed.

Lemma Forall2_Forall : forall A1 A2 (P : _ -> Prop) (Q : _ -> _ -> Prop)
    (l1 : list A1) (l2 : list A2),
  Forall P l1 ->
  Forall2 Q l1 l2 ->
  Forall2 (fun a b => P a /\ Q a b) l1 l2.
Proof.
  introv F F2. gen l2. induction F; introv F2; inverts F2; constructors~.
Qed.

Lemma Forall2_combine : forall A1 A2 A3 (P Q : _ -> _ -> Prop)
    (l1 : list A1) (l2 : list A2) (l3 : list A3),
  Forall2 P l1 l2 ->
  Forall2 Q l1 l3 ->
  Forall2 (fun a bc => P a (fst bc) /\ Q a (snd bc)) l1 (combine l2 l3).
Proof.
  introv F1 F2. gen l3. induction F1; introv F2; inverts F2; constructors~.
Qed.

Instance Forall2_Decidable_Mem : forall A B (P : A -> B -> Prop) l1 l2,
  (forall a b, Mem a l1 -> Mem b l2 -> Decidable (P a b)) ->
  Decidable (Forall2 P l1 l2).
Proof.
  introv F. gen l2. induction l1 as [|a l1]; introv F.
   destruct l2.
    applys Decidable_equiv True.
     iff~ I. constructors.
     typeclass.
    applys Decidable_equiv False.
     iff I; tryfalse. inverts~ I.
     typeclass.
   destruct l2 as [|b l2].
    applys Decidable_equiv False.
     iff I; tryfalse. inverts~ I.
     typeclass.
    applys Decidable_equiv (P a b /\ Forall2 P l1 l2).
     iff I.
      constructors*.
      inverts* I.
     apply and_decidable.
      typeclass.
      apply IHl1. introv M1 M2. apply* F.
Defined.

Global Instance Forall2_Decidable : forall A B (P : A -> B -> Prop) l1 l2,
  (forall a b, Decidable (P a b)) ->
  Decidable (Forall2 P l1 l2).
Proof.
  introv F. apply~ Forall2_Decidable_Mem.
Defined.

Lemma Forall2_eq : forall A (l1 l2 : list A),
  Forall2 (fun a1 a2 => a1 = a2) l1 l2 ->
  l1 = l2.
Proof. induction l1; introv F; inverts F as E F; fequals~. Qed.

Lemma Forall2_exists : forall A B C (P : A -> B -> C -> Prop) la lb,
  Forall2 (fun a b => exists c, P a b c) la lb ->
  exists L, Forall3 P la lb L.
Proof.
  introv F. induction F.
  - eexists. constructors~.
  - lets (b&Pb): (rm H). lets (L&F'): (rm IHF). eexists. constructors*.
Qed.

Lemma Forall3_iff_forall_Nth : forall A1 A2 A3 (P : A1 -> A2 -> A3 -> Prop) l1 l2 l3,
  Forall3 P l1 l2 l3 <->
  length l1 = length l2 /\
  length l2 = length l3 /\
  forall n v1 v2 v3, Nth n l1 v1 -> Nth n l2 v2 -> Nth n l3 v3 -> P v1 v2 v3.
Proof.
  introv. gen l2 l3.
  induction l1 as [|x1 l1]; intros [|x2 l2] [|x3 l3]; (iff I; [ splits | inverts I as I1 I2 ]);
    try lets (Ia&Ib): (rm I2);
    try (introv N1 N2 N3; try (inverts N1; inverts N2; inverts N3));
    try inverts~ I; tryfalse~;
    try match goal with F : Forall3 _ _ _ _ |- _ => apply IHl1 in F end;
    try solve [ constructors~
              | rew_list; fequals* ].
  constructors~.
  - apply~ Ib; apply Nth_here.
  - apply IHl1. splits~. introv N1 N2 N3. apply~ Ib; apply* Nth_next.
Qed.

Lemma Forall3_Forall2 : forall A1 A2 A3 (P : A1 -> A2 -> A3 -> Prop) l1 l2 l3,
  Forall3 P l1 l2 l3 ->
  Forall2 (fun a1 a2 => exists a3, P a1 a2 a3) l1 l2.
Proof. introv F. induction F; constructors*. Qed.

Lemma Forall3_wrap : forall A1 A2 A3 (P : A1 -> A2 -> A3 -> Prop) l1 l2 l3,
  Forall3 P l1 l2 l3 ->
  Forall3 (fun a2 a3 a1 => P a1 a2 a3) l2 l3 l1.
Proof. introv F. induction F; constructors*. Qed.


Definition list_rect_Mem : forall A (P : list A -> Type) l0,
   P nil ->
   (forall a l, Mem a l0 -> P l -> P (a :: l)) ->
   P l0.
  introv. induction~ l0.
Defined.

Definition list_ind_Mem : forall A (P : list A -> Prop) l0,
  P nil ->
  (forall a l, Mem a l0 -> P l -> P (a :: l)) ->
  P l0 := list_rect_Mem.


Lemma Mem_Nth : forall A l (x : A),
  Mem x l ->
  exists n, Nth n l x.
Proof. introv M. rewrite Mem_mem in M. apply* mem_Nth. Qed.

Lemma Nth_Mem : forall A l (x : A) n,
  Nth n l x ->
  Mem x l.
Proof. introv N. rewrite Mem_mem. apply* Nth_mem. Qed.

Lemma Nth_equiv : forall A (l1 l2 : list A),
  (forall n x, Nth n l1 x <-> Nth n l2 x) ->
  l1 = l2.
Proof.
  induction l1 as [|a1 l1]; introv E; destruct~ l2 as [|a2 l2].
  - forwards N: Nth_here l2 a2. apply E in N. inverts~ N.
  - forwards N: Nth_here l1 a1. apply E in N. inverts~ N.
  - fequals.
    + forwards N: Nth_here l1 a1. apply E in N. inverts~ N.
    + apply~ IHl1. introv. iff I.
      * forwards N: Nth_next a1 I. apply E in N. inverts~ N.
      * forwards N: Nth_next a2 I. apply E in N. inverts~ N.
Qed.


Lemma Mem_last_inv : forall A l (x e : A),
  Mem x (l & e) ->
  x = e \/ Mem x l.
Proof. introv M. rewrite Mem_mem in *. rewrite mem_last in M. rew_refl~ in M. Qed.

Lemma Mem_in_last : forall A l (x e : A),
  Mem x l ->
  Mem x (l & e).
Proof. introv M. rewrite Mem_mem in *. rewrite mem_last. rew_refl*. Qed.

Lemma Nth_add_last : forall A l (x e : A) n,
  Nth n l x ->
  Nth n (l & e) x.
Proof. introv N. induction N; constructors~. Qed.

Lemma Nth_last : forall A l (x : A),
  Nth (length l) (l & x) x.
Proof. introv. induction l; rew_list; constructors~. Qed.

Lemma map_List_map : map = List.map.
Proof. extens. intros A B f l. induction~ l. simpl. rew_list. fequals. Qed.

Lemma append_List_app : @append = List.app.
Proof. extens. intro A. extens. intros l1 l2. induction~ l1. simpl. rew_list. fequals. Qed.

Lemma concat_List_concat : @concat = List.concat.
Proof. extens. intros A L. induction~ L. simpl. rew_list. rewrite append_List_app. fequals. Qed.

Lemma Mem_map_inv : forall A B (f : A -> B) l y,
  Mem y (map f l) ->
  exists x,
    Mem x l /\ y = f x.
Proof.
  induction l; introv M.
  - inverts M.
  - rewrite map_cons in M. inverts M as M.
    + exists* a.
    + forwards* (x&Mx&Ex): (rm IHl) M.
Qed.

Lemma map_nth : forall A B `{Inhab A} `{Inhab B} (f : A -> B) l n,
  n < length l ->
  nth n (map f l) = f (nth n l).
Proof.
  introv I. gen l. induction n; introv I; destruct l; try solve [false; rew_list in I; nat_math]; rewrite map_cons.
  - do 2 rewrite nth_zero. reflexivity.
  - do 2 rewrite nth_succ. apply~ IHn. rew_list in I. nat_math.
Qed.

Lemma map_Nth : forall A B (f : A -> B) l n x,
  Nth n l x ->
  Nth n (map f l) (f x).
Proof. introv N. induction N; [ apply~ Nth_here | apply~ Nth_next ]. Qed.

Lemma map_Nth_inv : forall A B (f : A -> B) l n y,
  Nth n (map f l) y ->
  exists x, Nth n l x /\ y = f x.
Proof.
  introv N. gen n. induction l; rew_list in *; introv N; inverts N as N.
  - exists a. splits~. constructors.
  - lets (x&N'&E): IHl N. exists x. splits~. constructors~.
Qed.

Lemma remove_correct : forall A `{Comparable A} l (a1 a2 : A),
  mem a1 (remove a2 l) <-> mem a1 l /\ a1 <> a2.
Proof. introv. unfold remove. rewrite filter_mem_eq. rew_refl*. Qed.


Lemma head_tail : forall A l (a : A),
  head l = Some a ->
  a :: tail l = l.
Proof. introv E. destruct l; inverts~ E. Qed.

Lemma head_Nth : forall A l (a : A),
  head l = Some a <-> Nth 0 l a.
Proof.
  introv. destruct l.
  - iff I; inverts~ I.
  - iff I; inverts~ I. constructors~.
Qed.

Lemma cut_list_cons : forall A l (a : A),
  l <> nil ->
  Nth 0 l a ->
  exists l', l = a :: l'.
Proof. introv D N. destruct l; inverts* N. Qed.

Lemma Nth_rev : forall A l n (a : A),
  n < length l ->
  Nth n l a <-> Nth (length l - 1 - n) (rev l) a.
Proof.
  introv In. gen n. induction l; introv; rew_list; iff I.
  - inverts I.
  - inverts I.
  - simpl. inverts I as I.
    + eapply Nth_app_r; [constructors*|]. rew_list. math.
    + eapply Nth_app_l. apply IHl in I.
      asserts_rewrite (length l - 0 - S n0 = length l - 1 - n0); [math|].
      apply~ I. math.
  - simpl in I. forwards [N|(m&E&N)]: Nth_app_inv I.
    + destruct n.
      * apply Nth_lt_length in N. rew_list in N. false. math.
      * asserts_rewrite (length l - 0 - S n = length l - 1 - n) in N; [math|].
        apply IHl in N; [ apply~ Nth_next | math ].
    + repeat inverts N as N. rew_list in E.
      asserts: (n = 0); [ math |]. substs. constructors~.
Qed.

Lemma cut_list_last : forall A l (a : A),
  l <> nil ->
  Nth (length l - 1) l a ->
  exists l', l = l' & a.
Proof.
  introv D N. exists (rev (tail (rev l))). apply rev_inj. rew_list.
  rewrite~ head_tail. apply~ head_Nth. apply~ Nth_rev.
  + destruct l; tryfalse. rew_list. math.
  + rew_list.
    asserts_rewrite (length l - 1 - 0 = length l - 1); [ math | apply~ N ].
Qed.

Lemma length_datatype_length : forall A (l : list A),
  length l = Datatypes.length l.
Proof. introv. induction~ l. simpl. rewrite~ length_cons. Qed.

Lemma seq_length : forall start len,
  length (seq start len) = len.
Proof. introv. rewrite length_datatype_length. apply~ seq_length. Qed.

Lemma seq_Nth : forall len start n,
  n < len ->
  Nth n (seq start len) (start + n).
Proof.
  introv I. gen start n. induction len; introv I.
  - false. math.
  - destruct n.
    + simpl. asserts_rewrite (start + 0 = start); [ math | constructors~ ].
    + apply Nth_next. fold seq. rewrite <- seq_shift.
      asserts_rewrite (start + S n = S (start + n)); [math|].
      rewrite <- map_List_map. apply map_Nth. apply IHlen. math.
Qed.

Lemma seq_last : forall start len,
  seq start (S len) = seq start len & (start + len).
Proof.
  introv. apply Nth_equiv. introv. iff I.
  - forwards N: seq_Nth.
    + applys Nth_lt_length I.
    + rewrite seq_length in N. forwards: Nth_func I N. substs.
      tests: (n = len).
      * eapply Nth_app_r; [ constructors* | rewrite~ seq_length ].
      * applys Nth_app_l. apply~ seq_Nth.
        apply Nth_lt_length in I. rewrite seq_length in I. math.
  - forwards [N|(m&E&N)]: Nth_app_inv (rm I).
    + forwards N': seq_Nth.
      * applys Nth_lt_length N.
      * rewrite seq_length in N'. forwards: Nth_func N N'. substs.
        apply~ seq_Nth. apply Nth_lt_length in N. rewrite seq_length in N. math.
    + repeat inverts N as N. rewrite seq_length in E.
      asserts_rewrite (n = len); [math|]. apply~ seq_Nth. math.
Qed.

Lemma seq_0 : forall start,
  seq start 0 = nil.
Proof. reflexivity. Qed.

Lemma seq_1 : forall start,
  seq start 1 = [start].
Proof. reflexivity. Qed.

Lemma seq_split : forall start len k,
  k <= len ->
  seq start len = seq start k ++ seq (start + k) (len - k).
Proof.
  introv I. apply Nth_equiv. introv. tests I': (n < k); iff N.
  - apply Nth_app_l. forwards N': seq_Nth; [applys~ Nth_lt_length N|].
    rewrite seq_length in N'. forwards: Nth_func N N'. substs.
    applys~ seq_Nth.
  - forwards [N'|(m&E&N')]: Nth_app_inv (rm N);
      (forwards N: seq_Nth; [applys~ Nth_lt_length N'|]);
      rewrite seq_length in N; forwards: Nth_func N N'; substs.
    + applys~ seq_Nth. math.
    + rewrite seq_length in *. rewrite <- Nat.add_assoc.
      applys~ seq_Nth. math.
  - applys Nth_app_r (n - k).
    + forwards N': seq_Nth; [applys~ Nth_lt_length N|].
      rewrite seq_length in N'. forwards: Nth_func N N'. substs.
      asserts_rewrite (start + n = (start + k) + (n - k)); [math|].
      applys~ seq_Nth. apply Nth_lt_length in N. rewrite seq_length in N. math.
    + rewrite seq_length. math.
  - forwards [N'|(m&E&N')]: Nth_app_inv (rm N);
      (forwards N: seq_Nth; [applys~ Nth_lt_length N'|]);
      rewrite seq_length in N; forwards: Nth_func N N'; substs.
    + applys~ seq_Nth. apply Nth_lt_length in N. rewrite seq_length in N. math.
    + rewrite seq_length in *. rewrite <- Nat.add_assoc.
      applys~ seq_Nth. apply Nth_lt_length in N. rewrite seq_length in N. math.
Qed.

Lemma In_Mem : forall A l (a : A),
  Mem a l <-> In a l.
Proof.
  introv. iff I.
  - induction I; [left~|right~].
  - induction l; inverts I as I; constructors~.
Qed.

Lemma No_duplicates_NoDup : forall A (l : list A),
  No_duplicates l <-> NoDup l.
Proof. introv. iff D; induction D; constructors~; introv I; apply In_Mem in I; autos~. Qed.

Global Instance No_duplicates_decidable : forall A (l : list A),
    Comparable A ->
    Decidable (No_duplicates l).
  introv C. induction l.
   applys Decidable_equiv True.
    splits~.
    typeclass.
   applys Decidable_equiv (~ Mem a l /\ No_duplicates l).
    splits.
     introv (NM&ND). constructors~.
     introv ND. inverts~ ND.
    typeclass.
Defined.

Definition find_index_def A n (a : A) l :=
  fold_right (fun e n => If a = e then 0 else n + 1) n l.

Definition find_index A :=
  nosimpl (@find_index_def A 0).

Lemma find_index_def_length : forall A (a : A) l n,
  ~ Mem a l ->
  find_index_def n a l = n + length l.
Proof.
  introv N. unfold find_index_def. gen n. induction l using list_ind_last; introv.
   simpl. rewrite~ length_nil.
   rewrite fold_right_last. rewrite IHl.
    cases_if.
     false N. apply* Mem_last.
     rewrite length_last. rewrite~ PeanoNat.Nat.add_assoc.
    introv M. false N. apply* Mem_in_last.
Qed.

Lemma find_index_nth : forall A (a : A) l,
  Mem a l ->
  Nth (find_index a l) l a.
Proof.
  introv M. unfold find_index. generalize 0. induction l using list_ind_last; introv.
   inverts* M.
   unfolds find_index_def. rewrite fold_right_last. apply Mem_last_inv in M.
    tests M': (Mem a l).
     apply* Nth_add_last.
     cases_if.
      fold (find_index_def 0 a0 l). rewrite~ find_index_def_length. simpl. apply* Nth_last.
      inverts* M.
Qed.

Fixpoint nth_option A n (l : list A) {struct l} :=
  match l with
  | [] => None
  | x :: l =>
    match n with
    | 0 => Some x
    | S n => nth_option n l
    end
  end.

Lemma nth_option_length : forall A n (l : list A),
  nth_option n l = None <-> length l <= n.
Proof.
  introv. gen n. induction l; iff I.
   rew_list. math.
   reflexivity.
   rew_list. simpl in I. destruct n; inverts I as I'. rewrite IHl in I'. math.
   rew_list in I. destruct n.
    false. math.
    simpl. rewrite IHl. math.
Qed.

Lemma nth_option_defined : forall A (H : Inhab A) n (l : list A) x,
  nth_option n l = Some x ->
  nth n l = x.
Proof.
  introv E. gen n. induction l; introv E.
   inverts E.
   destruct n.
    inverts E. reflexivity.
    simpl in E. rewrite nth_succ. apply~ IHl.
Qed.

Lemma nth_option_Some : forall A (H : Inhab A) n (l : list A),
  n < length l ->
  nth_option n l = Some (nth n l).
Proof.
  introv I. destruct nth_option eqn: E.
   eapply nth_option_defined in E. rewrite~ <- E.
   apply nth_option_length in E. false. math.
Qed.

Lemma nth_option_update_eq : forall A l i (v : A),
  i < length l ->
  nth_option i (update i v l) = Some v.
Proof.
  introv I. gen i. induction l; introv I; rew_list in I.
   false. math.
   simpl. destruct i as [|i'].
    reflexivity.
    apply~ IHl. math.
Qed.

Lemma nth_option_update_neq : forall A l i j (v : A),
  i < length l ->
  i <> j ->
  nth_option j (update i v l) = nth_option j l.
Proof.
  introv I D. gen i j. induction l; introv I D; rew_list in I.
   false. math.
   simpl. destruct i as [|i'], j as [|j']; try reflexivity; tryfalse.
    simpl. apply~ IHl. math.
Qed.

Lemma nth_option_zero : forall A l (a : A),
  nth_option 0 (a :: l) = Some a.
Proof. reflexivity. Qed.

Lemma nth_option_cons : forall A l n (a : A),
  nth_option (S n) (a :: l) = nth_option n l.
Proof. reflexivity. Qed.

Lemma update_out : forall A l i (v : A),
  i >= length l ->
  update i v l = l.
Proof.
  induction l; introv I; rew_list in I.
   reflexivity.
   simpl. destruct i as [|i'].
    false. math.
    fequals. apply~ IHl. math.
Qed.


Definition list_product {A B} (la : list A) (lb : list B) :=
  nosimpl (concat (map (fun a => map (fun b => (a, b)) lb) la)).

Lemma list_product_mem : forall A B (la : list A) (lb : list B) a b,
  mem (a, b) (list_product la lb) <-> mem a la /\ mem b lb.
Proof.
  induction la; introv; simpls; rew_refl.
  - autos*.
  - unfolds @list_product. rew_list. rewrite mem_app. rew_refl. rewrite IHla.
    rewrite map_mem. iff* H. lets* [(?&?&E)|(?&?)]: (rm H). inverts* E.
Qed.


Class Pickable_option (A : Type) (P : A -> Prop) := pickable_option_make {
  pick_option : option A;
  pick_option_correct : forall a, pick_option = Some a -> P a;
  pick_option_defined : (exists a, P a) -> (exists a, pick_option = Some a) }.

Arguments pick_option [A] P {Pickable_option}.
Extraction Inline pick_option.

Global Instance Pickable_option_Pickable : forall A (P : A -> Prop) `{Inhab A},
  Pickable_option P -> Pickable P.
Proof.
  introv I [[pi|] C D].
  - applys pickable_make pi. introv _. apply~ C.
  - applys pickable_make arbitrary. introv E. forwards (a&N): D E. inverts N.
Qed.

Class Pickable_list (A : Type) (P : A -> Prop) := pickable_list_make {
  pick_list : list A;
  pick_list_correct : forall a, Mem a pick_list <-> P a }.

Arguments pick_list [A] P {Pickable_list}.
Extraction Inline pick_list.

Global Instance Pickable_list_Pickable_option : forall A (P : A -> Prop),
  Pickable_list P -> Pickable_option P.
Proof.
  introv [l C]. destruct l as [|a].
  - applys @pickable_option_make (@None A).
    + introv E. inverts~ E.
    + introv (a&E). apply C in E. inverts~ E.
  - applys @pickable_option_make (Some a).
    + introv E. inverts E. apply~ C.
    + introv (a'&E). eexists. autos~.
Defined.

Instance Pickable_option_Pickable_list : forall A (P : A -> Prop),
  (forall a b, P a -> P b -> a = b) ->
  Pickable_option P -> Pickable_list P.
Proof.
  introv E [o C D]. destruct o as [a|].
  - applys pickable_list_make [a]. iff M.
    + repeat inverts M as M. apply~ C.
    + forwards~ Pa: C. forwards: E M Pa. substs. constructors.
  - applys pickable_list_make (@nil A). iff M.
    + inverts~ M.
    + forwards* (a'&AH): D. inverts~ AH.
Defined.

Global Instance binds_Pickable : forall K V `{Comparable K} `{Inhab V} (h : heap K V) (v : K),
  Pickable_list (binds h v).
Proof.
  introv CK IV; introv. applys~ Pickable_option_Pickable_list binds_func.
  destruct (read_option h v) as [v'|] eqn: E.
  - applys pickable_option_make (Some v').
    + introv E'. inverts E'. applys~ @read_option_binds E.
    + introv (v2&B). rewrites >> (@binds_read_option) B in E. inverts* E.
  - apply read_option_not_indom in E. applys pickable_option_make (@None V).
    + introv E'. inverts~ E'.
    + introv (v2&B). false E. applys @binds_indom B.
Defined.

Global Instance False_Pickable : forall A, Pickable_list (fun (_ : A) => False).
Proof. introv. applys pickable_list_make (@nil A). iff M; inverts M. Defined.

Global Instance eq_left_Pickable : forall A (a : A), Pickable_list (fun b => b = a).
Proof.
  introv. applys pickable_list_make [a].
  iff M; repeat inverts M as M; substs; constructors~.
Defined.

Global Instance eq_right_Pickable : forall A (a : A), Pickable_list (fun b => a = b).
Proof.
  introv. applys pickable_list_make [a].
  iff M; repeat inverts M as M; substs; constructors~.
Defined.


Lemma stream_head_nth : forall A (s : stream A),
  stream_head s = LibStream.nth 0 s.
Proof. introv. destruct* s. Qed.

Lemma stream_tail_nth : forall A (s : stream A) n,
  LibStream.nth n (stream_tail s) = LibStream.nth (1 + n) s.
Proof. introv. destruct* s. Qed.


Lemma read_option_indom : forall K `{Comparable K} V (h : heap K V) k v,
  read_option h k = Some v ->
  indom h k.
Proof. introv E. forwards B: @read_option_binds E. applys~ @binds_indom B. Qed.

Lemma indom_read_option : forall K `{Comparable K} V (h : heap K V) k,
  indom h k ->
  exists v, read_option h k = Some v.
Proof.
  introv I. forwards (v&B): @indom_binds I.
  exists v. applys~ @binds_read_option B.
Qed.

Lemma read_option_write_same : forall K `{Comparable K} V (h : heap K V) k v,
  read_option (write h k v) k = Some v.
Proof. introv. apply binds_read_option. applys~ @binds_write_eq. Qed.

Lemma not_indom_write : forall K V (h : heap K V) k k' v',
  k <> k' ->
  ~ indom h k ->
  ~ indom (write h k' v') k.
Proof. introv D I1 I2. forwards~: @indom_write_inv I2. Qed.

Lemma read_option_write : forall K `{Comparable K} V (h : heap K V) k k' v,
  k <> k' ->
  read_option (write h k v) k' = read_option h k'.
Proof.
  introv D. tests I: (indom h k').
   forwards (v'&E): indom_read_option I. rewrite E.
    apply read_option_binds in E. apply binds_read_option.
    applys~ @binds_write_neq E.
   rewrite (not_indom_read_option I). forwards I': not_indom_write I.
    introv E. apply D. symmetry. apply* E.
    rewrite~ (not_indom_read_option I').
Qed.

(** A notion of equivalence for heaps. **)
Definition heap_equiv K V (h1 h2 : heap K V) :=
  forall k v, binds h1 k v <-> binds h2 k v.

Lemma heap_equiv_refl : forall K V (h : heap K V),
  heap_equiv h h.
Proof. introv. unfolds. autos*. Qed.

Lemma heap_equiv_sym : forall K V (h1 h2 : heap K V),
  heap_equiv h1 h2 ->
  heap_equiv h2 h1.
Proof. introv E. unfolds heap_equiv. introv. rewrite* E. Qed.

Lemma heap_equiv_trans : forall K V (h1 h2 h3 : heap K V),
  heap_equiv h1 h2 ->
  heap_equiv h2 h3 ->
  heap_equiv h1 h3.
Proof. introv E1 E2. unfolds heap_equiv. introv. rewrite* E1. Qed.

Lemma heap_equiv_binds : forall K V (h1 h2 : heap K V) k v,
  heap_equiv h1 h2 ->
  binds h1 k v ->
  binds h2 k v.
Proof. introv E B. unfolds in E. rewrite* <- E. Qed.

Lemma heap_equiv_indom : forall K V (h1 h2 : heap K V) k,
  heap_equiv h1 h2 ->
  indom h1 k ->
  indom h2 k.
Proof.
  introv E I. lets (v&B): @indom_binds I.
  forwards B': heap_equiv_binds E B. applys~ @binds_indom B'.
Qed.

Lemma heap_equiv_read : forall K `{Comparable K} V `{Inhab V} (h1 h2 : heap K V) k,
  heap_equiv h1 h2 ->
  indom h1 k ->
  read h1 k = read h2 k.
Proof.
  introv E I. forwards (v&B): @indom_binds I.
  rewrites >> binds_read B. symmetry. apply binds_read.
  applys~ heap_equiv_binds E B.
Qed.

Lemma heap_equiv_read_option : forall K `{Comparable K} V (h1 h2 : heap K V),
  heap_equiv h1 h2 ->
  read_option h1 = read_option h2.
Proof.
  introv E. extens. intro k. tests I: (indom h1 k).
  - forwards (v&B): @indom_binds I.
    rewrites >> (@binds_read_option) B. symmetry. apply binds_read_option.
    applys~ heap_equiv_binds E B.
  - do 2 rewrite~ @not_indom_read_option.
    introv I'. false I. applys~ heap_equiv_indom I'. applys~ heap_equiv_sym E.
Qed.

Lemma heap_equiv_write : forall K V (h1 h2 : heap K V) k v,
  heap_equiv h1 h2 ->
  heap_equiv (write h1 k v) (write h2 k v).
Proof.
  introv E. unfolds heap_equiv. iff B;
    (forwards [(?&?)|(D&B')]: @binds_write_inv B;
     [ substs; apply binds_write_eq
     | apply~ @binds_write_neq; applys~ heap_equiv_binds B'; apply~ heap_equiv_sym ]).
Qed.

Lemma heap_equiv_write_write : forall K V (h1 h2 : heap K V) k v v',
  heap_equiv h1 h2 ->
  heap_equiv (write (write h1 k v') k v) (write h2 k v).
Proof.
  introv E. unfolds heap_equiv. iff B;
    (forwards [(?&?)|(D&B')]: @binds_write_inv B;
     [ substs; apply binds_write_eq
     | repeat apply~ @binds_write_neq ]).
  - forwards~ B'': @binds_write_neq_inv B'. applys~ heap_equiv_binds B''.
  - applys~ heap_equiv_binds B'. apply* heap_equiv_sym.
Qed.

Lemma heap_equiv_write_swap : forall K V (h1 h2 : heap K V) k1 k2 v1 v2,
  k1 <> k2 ->
  heap_equiv h1 h2 ->
  heap_equiv (write (write h1 k1 v1) k2 v2) (write (write h2 k2 v2) k1 v1).
Proof.
  introv D E. unfolds heap_equiv. iff B;
    repeat (match goal with
    | B: binds (write _ _ _) _ _ |- _ =>
      let D' := fresh "D" in
      let B' := fresh "B" in
      forwards [(?&?)|(D'&B')]: @binds_write_inv (rm B)
    | |- binds (write _ _ _) _ _ =>
      apply~ @binds_write_eq || apply~ @binds_write_neq
    end; substs; tryfalse~); apply~ E.
Qed.

Lemma read_option_heap_equiv : forall K `{Comparable K} V (h1 h2 : heap K V),
  (forall k, read_option h1 k = read_option h2 k) ->
  heap_equiv h1 h2.
Proof.
  introv E. introv. iff B; apply binds_read_option in B; apply read_option_binds.
  - rewrite~ <- E.
  - rewrite~ E.
Qed.

(** As there is no openly available description of [to_list], we are
  stuck at this stage: the only way to prove the following lemmas is
  to prove them on a concrete instance.  We thus use this axiom to
  be able to minimally reason about [to_list]. **)
Parameter to_list_dom : forall K V (h : heap K V) k,
  Mem k (map fst (to_list h)) <-> indom h k.

Lemma to_list_indom : forall K V (h : heap K V) k,
  Mem k (map fst (to_list h)) ->
  indom h k.
Proof. introv M. apply~ to_list_dom. Qed.

Lemma indom_to_list : forall K V (h : heap K V) k,
  indom h k ->
  Mem k (map fst (to_list h)).
Proof. introv M. apply~ to_list_dom. Qed.

Definition heap_merge K `{Comparable K} V (h1 h2 : heap K V) :=
  fold_left (fun kv h =>
    let k := fst kv in
    match read_option h2 k with
    | Some v => write h k v
    | None => arbitrary (* Never happens *)
    end) h1 (to_list h2).

Lemma heap_merge_indom_left : forall K `{Comparable K} V (h1 h2 : heap K V) k,
  indom h1 k ->
  indom (heap_merge h1 h2) k.
Proof.
  introv I. unfolds heap_merge. lets E: to_list_indom h2. gen E h1. induction to_list; introv E I.
  - apply I.
  - rew_list. destruct read_option eqn: E'.
    + apply IHl.
      * introv M. apply~ E. rew_list*.
      * applys~ @indom_write I.
    + apply read_option_not_indom in E'. false E'. apply~ E. rew_list*.
Qed.

Lemma heap_merge_indom_right : forall K `{Comparable K} V (h1 h2 : heap K V) k,
  indom h2 k ->
  indom (heap_merge h1 h2) k.
Proof.
  introv I. unfolds heap_merge. apply to_list_dom in I.
  lets E: to_list_indom h2. gen h1 E. induction to_list; introv E.
  - inverts I.
  - rew_list. destruct read_option eqn: E'.
    + inverts I as I.
      * clear - E. destruct a as [k v']. simpls. forwards~ I: @indom_write_eq h1 k v.
        gen I. generalize (write h1 k v). induction l; introv I.
        -- apply I.
        -- rew_list. destruct read_option eqn: E'.
           ++ apply IHl.
              ** introv M. apply E. rew_list in M. rew_list. simpls. inverts* M.
              ** applys~ @indom_write I.
           ++ apply read_option_not_indom in E'. false E'. apply~ E. rew_list*.
      * applys~ IHl I. introv M. apply E. rew_list*.
    + apply read_option_not_indom in E'. false E'. apply~ E. rew_list*.
Qed.

Lemma heap_merge_binds : forall K `{Comparable K} V (h1 h2 : heap K V) k v,
  binds (heap_merge h1 h2) k v -> binds h1 k v \/ binds h2 k v.
Proof.
  introv B. unfolds heap_merge. lets E: to_list_indom h2. gen E h1. induction to_list; introv E I.
  - left~.
  - rew_list in I. destruct read_option eqn: E'.
    + apply IHl in I.
      * inverts I as I; autos~. forwards [(E1&E2)|(D&B)]: @binds_write_inv I.
        -- substs. right. applys~ @read_option_binds E'.
        -- left~.
      * introv M. apply E. rew_list*.
    + apply read_option_not_indom in E'. false E'. apply~ E. rew_list*.
Qed.

Lemma heap_merge_binds_precise : forall K `{Comparable K} V `{Inhab V} (h1 h2 : heap K V) k v,
  binds (heap_merge h1 h2) k v -> indom h2 k -> binds h2 k v.
Proof.
  introv In B I. unfolds heap_merge. lets M: indom_to_list I. lets MI: to_list_indom h2.
  gen h1. induction to_list; introv B.
  - inverts~ M.
  - rew_list in B. rew_list in M. destruct read_option eqn: E'.
    + inverts M as M.
      * tests M': (Mem (fst a) (map fst l)).
        -- applys~ IHl M' B. introv M0. apply MI. rew_list*.
        -- asserts: (v = v0).
           { clear - In MI B M'. gen B M'. generalize (fst a). intro k.
             forwards B: @binds_write_eq h1 k v0. gen B.
             generalize (write h1 k v0) as h1'. clear - In MI. intro h1.
             gen h1. induction l; introv B1 B2 NM.
             - applys~ @binds_func B2 B1.
             - rew_list in NM. rew_list in B2. forwards (?&E): indom_read_option.
               { apply MI. rew_list. apply Mem_next. apply Mem_here. }
               rewrite E in B2. applys~ IHl B2.
               + introv M. apply MI. rew_list in M. rew_list. inverts* M.
               + applys @binds_write_neq B1. introv ?. substs. false* NM. }
           substs~. applys~ @read_option_binds E'.
      * applys~ IHl B. introv M'. apply MI. rew_list*.
    + apply read_option_not_indom in E'. false E'. apply~ MI. rew_list*.
Qed.

Lemma heap_merge_equiv_aux : forall K `{Comparable K} V `{Inhab V} (h1 h2 h1' h2' : heap K V) k v,
  heap_equiv h1 h2 ->
  heap_equiv h1' h2' ->
  binds (heap_merge h1 h1') k v ->
  binds (heap_merge h2 h2') k v.
Proof.
  introv ? E1 E2 B. tests I: (indom h1' k).
  - forwards~ B1: heap_merge_binds_precise B I.
    destruct (read_option (heap_merge h2 h2') k) eqn: E.
    + forwards B2: @read_option_binds E.
      forwards~ B3: heap_merge_binds_precise B2.
      { applys~ heap_equiv_indom E2 I. }
      forwards~: @binds_func B1.
      { applys~ heap_equiv_binds (heap_equiv_sym E2) B3. }
      substs~.
    + rewrite <- not_indom_equiv_read_option in E. false E.
      applys~ heap_merge_indom_right. applys~ heap_equiv_indom E2 I.
  - apply heap_merge_binds in B. inverts B as B.
    + destruct (read_option (heap_merge h2 h2') k) eqn: E.
      * apply read_option_binds in E.
        lets B0: E. apply heap_merge_binds in E. inverts E as B'.
        -- forwards~: @binds_func B'.
           { applys~ heap_equiv_binds E1 B. }
           substs~.
        -- false I. applys~ heap_equiv_indom (heap_equiv_sym E2). applys~ @binds_indom B'.
      * rewrite <- not_indom_equiv_read_option in E. false E.
        applys~ heap_merge_indom_left. applys~ heap_equiv_indom E1. applys~ @binds_indom B.
    + false I. applys @binds_indom B.
Qed.

Lemma heap_merge_equiv : forall K `{Comparable K} V `{Inhab V} (h1 h2 h1' h2' : heap K V),
  heap_equiv h1 h2 ->
  heap_equiv h1' h2' ->
  heap_equiv (heap_merge h1 h1') (heap_merge h2 h2').
Proof. introv ? E1 E2. introv. iff B; applys~ heap_merge_equiv_aux B; apply~ heap_equiv_sym. Qed.

Definition heap_restrict K `{Comparable K} V (h : heap K V) L :=
  fold_left (fun k h' =>
    match read_option h k with
    | None => h'
    | Some v => write h' k v
    end) empty L.

Lemma heap_restrict_binds : forall K `{Comparable K} V `{Inhab V} (h : heap K V) L k v,
  binds (heap_restrict h L) k v ->
  binds h k v.
Proof.
  introv I B. unfolds heap_restrict.
  asserts A: (binds empty k v -> binds h k v).
  { introv A. false~ @not_binds_empty A. }
  gen A B. generalize (empty : heap K V). induction L; introv Im B.
  - applys~ Im B.
  - rew_list in B. applys (rm IHL) (rm B). introv B.
    rewrite read_option_def in B. cases_if~. tests N: (a = k).
    + forwards: @binds_write_eq_inv B. substs. applys~ read_binds C.
    + forwards*: @binds_write_neq_inv B.
Qed.

Lemma heap_restrict_indom : forall K `{Comparable K} V `{Inhab V} (h : heap K V) L k,
  indom (heap_restrict h L) k <-> Mem k L /\ indom h k.
Proof.
  introv I. introv. unfolds heap_restrict. rewrite fold_left_eq_fold_right.
  rewrite Mem_rev_iff. induction (rev L); rew_list.
  - iff M.
    + false~ @not_indom_empty M.
    + lets (A&?): (rm M). inverts~ A.
  - rewrite read_option_def. tests N: (k = a); iff M.
    + splits~. cases_if~. apply IHl in M. autos*.
    + lets (_&In): (rm M). cases_if~. apply~ @indom_write_eq.
    + cases_if.
      * forwards I': @indom_write_inv M N. apply IHl in I'. splits*.
      * apply IHl in M. splits*.
    + lets (M'&In): (rm M). inverts* M'. cases_if~; [ apply~ @indom_write |]; apply~ IHl.
Qed.

Lemma heap_restrict_equiv_aux : forall K `{Comparable K} V `{Inhab V} (h1 h2 : heap K V) L1 L2 h k,
  (forall x, Mem x L1 <-> Mem x L2) ->
  heap_equiv h1 h2 ->
  binds (heap_restrict h1 L1) h k ->
  binds (heap_restrict h2 L2) h k.
Proof.
  introv In EL Eh B. forwards I: @binds_indom B.
  apply heap_restrict_indom in I; autos~. lets (M&I'): (rm I).
  destruct (read_option (heap_restrict h2 L2) h) eqn: E.
  - apply read_option_binds in E. forwards~ B2: heap_restrict_binds E.
    forwards~ B1: heap_equiv_binds (heap_equiv_sym Eh) B2.
    apply heap_restrict_binds in B; autos~. forwards~: binds_func B B1. substs~.
  - apply read_option_not_indom in E. rewrite heap_restrict_indom in E; autos~.
    rew_logic in E. inverts E as E; false E.
    + rewrite* <- EL.
    + applys~ heap_equiv_indom Eh I'.
Qed.

Lemma heap_restrict_equiv : forall K `{Comparable K} V `{Inhab V} (h1 h2 : heap K V) L1 L2,
  (forall x, Mem x L1 <-> Mem x L2) ->
  heap_equiv h1 h2 ->
  heap_equiv (heap_restrict h1 L1) (heap_restrict h2 L2).
Proof.
  introv In EL Eh. introv. iff B; applys~ heap_restrict_equiv_aux B.
  - introv. rewrite* EL.
  - applys~ heap_equiv_sym Eh.
Qed.

Definition heap_dom K V (h : heap K V) : fset K :=
  from_list (map fst (to_list h)).

Lemma from_list_spec : forall A (l : list A) x,
  x \in from_list l <-> Mem x l.
Proof.
  introv. induction l.
  - rewrite from_list_nil. iff M.
    + false~ notin_empty M.
    + inverts M.
  - rewrite from_list_cons. rewrite in_union. rewrite in_singleton. rewrite IHl.
    iff M; inverts* M.
Qed.

Lemma heap_dom_spec : forall K V (h : heap K V) k,
  indom h k <-> k \in (heap_dom h).
Proof. introv. unfolds heap_dom. rewrite from_list_spec. rewrite* to_list_dom. Qed.

Global Instance heap_equiv_Decidable : forall K V (h1 h2 : heap K V),
    Comparable K ->
    Comparable V ->
    Decidable (heap_equiv h1 h2).
  introv CK CV. applys Decidable_equiv ((forall x, x \in heap_dom h1 <-> x \in heap_dom h2)
    /\ Forall (fun x => read_option h1 x = read_option h2 x) (map fst (HeapList.to_list h1))).
  - introv. iff I.
    + lets (D&E): (rm I). introv.
      asserts I1: (forall x, indom h1 x -> read_option h1 x = read_option h2 x).
      { introv I. rewrite Forall_iff_forall_mem in E. rewrite~ E.
        rewrite <- Mem_mem. applys~ indom_to_list I. }
      asserts I2: (forall x, indom h2 x -> read_option h1 x = read_option h2 x).
      { introv I. rewrite heap_dom_spec in I. rewrite <- D in I. rewrite <- heap_dom_spec in I.
        applys~ I1 I. }
      iff~ B; apply binds_read_option in B; apply read_option_binds.
      * rewrite~ <- I1. applys~ read_option_indom B.
      * rewrite~ I2. applys~ read_option_indom B.
    + splits.
      * introv. do 2 rewrite <- heap_dom_spec. iff I'; applys~ heap_equiv_indom I'.
        applys~ heap_equiv_sym I.
      * rewrite Forall_iff_forall_mem. introv M. rewrites~ >> heap_equiv_read_option I.
  - apply and_decidable.
    + applys Decidable_equiv (Forall (fun x => Mem x (map fst (to_list h1))) (map fst (to_list h2))
               /\ (Forall (fun x => Mem x (map fst (to_list h2))) (map fst (to_list h1)))).
      * do 2 rewrite Forall_iff_forall_mem. unfolds heap_dom.
        iff I; split; introv; try lets (I1&I2): (rm I); repeat rewrite from_list_spec in *;
          ((introv I0; rewrite Mem_mem in I0; autos*) || rewrite* <- Mem_mem).
      * applys and_decidable; typeclass. (* WFT *)
    + typeclass.
Defined.


Inductive eq_option A B (eq : A -> B -> Prop) : option A -> option B -> Prop :=
  | eq_option_None :
    eq_option eq None None
  | eq_option_Some : forall a1 a2,
    eq a1 a2 ->
    eq_option eq (Some a1) (Some a2)
  .

Lemma eq_option_refl : forall A eq,
  (forall a : A, eq a a : Prop) ->
  forall o, eq_option eq o o.
Proof. introv R. introv. destruct o; constructors~. Qed.

Lemma eq_option_sym : forall A B (eq1 : A -> B -> Prop) (eq2 : B -> A -> Prop),
  (forall a1 a2, eq1 a1 a2 -> eq2 a2 a1) ->
  forall o1 o2, eq_option eq1 o1 o2 -> eq_option eq2 o2 o1.
Proof. introv S E. destruct o1, o2; inverts E; constructors~. Qed.

Lemma eq_option_trans : forall A1 A2 A3
    (eq12 : A1 -> A2 -> Prop) (eq23 : A2 -> A3 -> Prop) (eq13 : A1 -> A3 -> Prop),
  (forall a1 a2 a3, eq12 a1 a2 -> eq23 a2 a3 -> eq13 a1 a3) ->
  forall o1 o2 o3, eq_option eq12 o1 o2 -> eq_option eq23 o2 o3 -> eq_option eq13 o1 o3.
Proof. introv S E1 E2. destruct o1, o2, o3; inverts E1; inverts E2; constructors*. Qed.

Lemma eq_option_None_inv : forall A B (eq : A -> B -> Prop) o,
  eq_option eq o None ->
  o = None.
Proof. introv E. destruct~ o. inverts~ E. Qed.

Lemma eq_option_Some_inv : forall A B (eq : A -> B -> Prop) o b,
  eq_option eq o (Some b) ->
  exists a, o = Some a.
Proof. introv E. destruct* o. inverts~ E. Qed.

Instance eq_None_Decidable : forall A (o : option A), Decidable (o = None).
  introv. destruct o.
  - applys Decidable_equiv False.
    + iff I; inverts~ I.
    + typeclass.
  - applys Decidable_equiv True.
    + iff~ I.
    + typeclass.
Defined.

Instance None_eq_Decidable : forall A (o : option A), Decidable (None = o).
  introv. applys Decidable_equiv (o = None).
  - iff~ I.
  - typeclass.
Defined.

Lemma eq_option_eq : forall A (o1 o2 : option A),
  eq_option eq o1 o2 ->
  o1 = o2.
Proof. introv E. destruct o1, o2; inverts~ E. Qed.


Definition list_to_string l :=
  fold_left (fun c str => String c str) EmptyString (rev l).


Fixpoint string_to_list (str : string) :=
  match str with
  | EmptyString => nil
  | String c str =>
    c :: string_to_list str
  end.

Definition ascii_to_string c := list_to_string [c].

Global Coercion ascii_to_string : Ascii.ascii >-> string.

Fixpoint divide_list {A} (l : list A) :=
  match l with
  | nil => (nil, nil)
  | x :: nil => ([x], nil)
  | x :: y :: l =>
    let (l1, l2) := divide_list l in
    (x :: l1, y :: l2)
  end.

Lemma divide_list_cons : forall A (l l1 l2: list A) x,
  divide_list l = (l1, l2) ->
  divide_list (x :: l) = (x :: l2, l1).
Proof.
  introv E. gen l1 l2. induction l; introv E.
  - inverts~ E.
  - simpls. destruct (divide_list l) as [la lb]. forwards~ E': (rm IHl).
    destruct l as [|e l]; simpls.
    + inverts E. inverts~ E'.
    + destruct divide_list. inverts E. inverts~ E'.
Qed.

Lemma divide_list_Mem : forall A l (x : A) l1 l2,
  Mem x l ->
  divide_list l = (l1, l2) ->
  Mem x l1 \/ Mem x l2.
Proof.
  introv M E. gen l1 l2. induction M; introv E.
  - destruct l; inverts E as E; autos~. destruct divide_list. inverts~ E.
  - destruct (divide_list l) as [la lb] eqn: El. erewrite divide_list_cons in E; autos*.
    inverts E. forwards*: (rm IHM).
Qed.

Lemma divide_list_Mem_inv : forall A l (x : A) l1 l2,
  Mem x l1 \/ Mem x l2 ->
  divide_list l = (l1, l2) ->
  Mem x l.
Proof.
  induction l; introv O E.
  - inverts E. inverts* O.
  - destruct (divide_list l) as [la lb] eqn: El. erewrite divide_list_cons in E; autos*.
    inverts E. inverts O as O.
    + inverts O as O.
      * constructors*.
      * constructors. eapply IHl; [| reflexivity ]. right~.
    + constructors*.
Qed.

Lemma No_duplicates_single : forall A (a : A),
  No_duplicates [a].
Proof. introv. apply~ No_duplicates_cons. introv I. inverts I. Qed.

Lemma divide_list_No_duplicates : forall A (l l1 l2 : list A),
  No_duplicates l ->
  divide_list l = (l1, l2) ->
  No_duplicates l1 /\ No_duplicates l2.
Proof.
  introv ND E. gen l1 l2. induction l; introv E.
  - inverts~ E.
  - destruct (divide_list l) as [la lb] eqn: El. erewrite divide_list_cons in E; autos*.
    inverts E. forwards~ (ND1&ND2): (rm IHl).
    + inverts~ ND.
    + splits~. constructors~. introv M. forwards M': divide_list_Mem_inv El.
      * right*.
      * inverts* ND.
Qed.

Lemma divide_list_Mem_No_duplicates : forall A l (x : A) l1 l2,
  Mem x l ->
  No_duplicates l ->
  divide_list l = (l1, l2) ->
  Mem x l1 ->
  Mem x l2 ->
  False.
Proof.
  introv M ND E. gen l1 l2. induction l; introv E M1 M2.
  - inverts E. invert M1.
  - destruct (divide_list l) as [la lb] eqn: El. erewrite divide_list_cons in E; autos*.
    inverts E. inverts M1 as M1.
    + forwards: divide_list_Mem_inv El.
      * left*.
      * inverts* ND.
    + applys~ IHl.
      * applys~ divide_list_Mem_inv El.
      * inverts~ ND.
Qed.


Lemma Z_to_nat_sub : forall a b : nat,
  a >= b ->
  Z.to_nat (a - b) = (a - b)%nat.
Proof.
  introv I. gen a. induction b; introv I.
  - asserts_rewrite (a - 0%nat = a)%I; [math|]. rewrite Nat2Z.id. math.
  - destruct a.
    + false. math.
    + simpl. rewrite <- IHb; try fequals; math.
Qed.


Instance nat_lt_Decidable : forall n1 n2 : nat,
    Decidable (n1 < n2)%nat.
  intros. applys Decidable_equiv.
   symmetry. apply nat_compare_lt.
   typeclass.
Defined.

Instance positive_lt_Decidable : forall n1 n2,
    Decidable (n1 < n2)%positive.
  typeclass.
Defined.

Instance Z_lt_Decidable : forall n1 n2,
    Decidable (n1 < n2)%Z.
  intros. applys Decidable_equiv Z.compare_lt_iff. typeclass.
Defined.

Instance lt_Decidable : forall n1 n2,
    Decidable (n1 < n2).
  intros. applys Decidable_equiv (n1 < n2)%Z.
   math.
   typeclass.
Defined.

Instance lt_nat_Decidable : forall n1 n2,
    Decidable (@lt nat (@lt_from_le nat le_nat_inst) n1 n2).
  intros. applys Decidable_equiv (lt_Decidable n1 n2). math.
Defined.

Instance nat_gt_Decidable : forall n1 n2 : nat,
    Decidable (n1 > n2)%nat.
  intros. applys Decidable_equiv.
   symmetry. apply nat_compare_lt.
   typeclass.
Defined.

Instance positive_gt_Decidable : forall n1 n2,
    Decidable (n1 > n2)%positive.
  typeclass.
Defined.

Instance Z_gt_Decidable : forall n1 n2,
    Decidable (n1 > n2)%Z.
  intros. applys sumbool_decidable Z_gt_dec.
Defined.

Instance gt_Decidable : forall n1 n2,
    Decidable (n1 > n2).
  intros. applys Decidable_equiv (n1 > n2)%Z.
   math.
   typeclass.
Defined.

Instance gt_nat_Decidable : forall n1 n2,
    Decidable (@gt nat (@gt_from_le nat le_nat_inst) n1 n2).
  intros. applys Decidable_equiv (gt_Decidable n1 n2). math.
Defined.

Instance ge_nat_Decidable : forall n1 n2 : nat,
    Decidable (@ge nat (@ge_from_le nat le_nat_inst) n1 n2).
  intros. applys Decidable_equiv (n1 >= n2)%Z.
   math.
   typeclass.
Defined.

Instance positive_ge_Decidable : forall n1 n2,
    Decidable (n1 >= n2)%positive.
  typeclass.
Defined.

Instance ge_Decidable : forall n1 n2 : int,
    Decidable (n1 >= n2).
  intros. applys Decidable_equiv (n1 >= n2)%Z.
   math.
   typeclass.
Defined.

Instance le_nat_Decidable : forall n1 n2 : nat,
    Decidable (@le nat le_nat_inst n1 n2).
  intros. applys Decidable_equiv (n1 <= n2)%Z.
   math.
   typeclass.
Defined.

Instance positive_le_Decidable : forall n1 n2,
    Decidable (n1 <= n2)%positive.
  typeclass.
Defined.

Instance le_Decidable : forall n1 n2,
    Decidable (n1 <= n2).
  intros. applys Decidable_equiv (n1 <= n2)%Z.
   math.
   typeclass.
Defined.


Instance positive_Comparable : Comparable positive.
  apply make_comparable. intros.
  applys Decidable_equiv (let (x, y) := (Pos.to_nat x, Pos.to_nat y) in x >= y /\ x <= y).
  - transitivity (Pos.to_nat x = Pos.to_nat y).
    + nat_math.
    + apply Pos2Nat.inj_iff.
  - typeclass.
Defined.

Instance Ascii_comparable : Comparable Ascii.ascii.
  apply make_comparable. intros. applys sumbool_decidable Ascii.ascii_dec.
Defined.


(** * Tactics about Comparable **)

Instance Comparable_Decidable : forall A (a1 a2 : A),
    Comparable A ->
    Decidable (a1 = a2).
  introv C. inverts* C.
Defined.

(** Builds an instance of [Comparable] from a non-recursive inductive. **)

Ltac prove_decidable_eq :=
  let prove_injection Eq :=
    solve [ injection Eq; intros; substs~
          | intros; substs~; reflexivity ] in
  let rec t tr :=
    match goal with
    (** Trivial cases **)
    | _ =>
      applys decidable_make false; abstract (fold_bool; rew_refl; discriminate)
    | _ =>
      applys decidable_make true; abstract (fold_bool; rew_refl; reflexivity)
    (** Look for already defined typeclasses **)
    | _ =>
      typeclass || (apply Comparable_Decidable; typeclass)
    (** A little trivial case **)
    | _ =>
      lazymatch goal with
      |- Decidable (?f1 = ?f2) =>
        abstract (
          let D := fresh "D" in asserts D: (tr f1 <> tr f2);
          [ discriminate
          | applys decidable_make false; fold_bool; rew_refl;
            let AE := fresh "AE" in intro AE; rewrite AE in *; false* ])
      end
    (** General case **)
    | |- Decidable (?f1 ?x1 = ?f2 ?x2) =>
      let tf := type of f1 in
      let Decf := fresh "Dec_f" in let Decx := fresh "Dec_x" in
      let tr' := constr:(fun f : tf => tr (f x1)) in
      asserts Decx: (Decidable (x1 = x2));
      [ let T := type of x1 in try (typeclass || solve [ t (@id T) ])
      | asserts Decf: (Decidable (f1 = f2));
        [ t tr'
        | applys decidable_make (decide (f1 = f2 /\ x1 = x2));
          abstract (
            let I := fresh "I" in
            let I1 := fresh "I_f" in let I2 := fresh "I_x" in
            rewrite decide_spec; rewrite isTrue_eq_isTrue; iff I;
            [ lets (I1&I2): (rm I); try rewrite I1; try rewrite I2; reflexivity
            | inverts I as I; splits~;
              let Eq := fresh "Eq" in
              asserts Eq: (tr (f1 x1) = tr (f2 x2));
              [ rewrite~ I | prove_injection Eq ]
            ]) ] ]
    | _ =>
      (** Nothing intelligent to do, let us nevertheless simplify the
         task of the user to know what is the context. **)
      let TR := fresh "tr" in set (TR := tr)
    end in
  lazymatch goal with
  | |- Decidable (?x = _) =>
    let T := type of x in
    t (@id T)
  end.

Ltac prove_comparable_simple_inductive :=
  apply make_comparable;
  let x := fresh "x" in let y := fresh "y" in
  intros x y; destruct x; destruct y;
  prove_decidable_eq.

Ltac prove_comparable_inductively_inductive :=
  apply make_comparable;
  let x := fresh "x" in
  intro x; induction x;
   let y := fresh "y" in
  intro y; induction y;
  prove_decidable_eq.

Ltac prove_comparable_trivial_inductive :=
  let aux T f :=
    refine (let f : (T -> T -> bool) :=
      ltac:(intros t1 t2; remember t1 as t1' eqn: E1; remember t2 as t2' eqn: E2; destruct t1, t2;
            let t1 := type of E1 in let t2 := type of E2 in clear E1 E2;
            lazymatch t1 with
            | _ = ?t =>
              match t2 with
              | _ = t => exact true
              | _ => exact false
              end
            end) in _);
    let I := fresh "I" in
    constructors; intros t1 t2; applys Decidable_equiv (f t1 t2); [| apply bool_decidable ];
    destruct t1, t2; simpl; iff I; (inversion I || reflexivity) in
  match goal with
  | |- Comparable ?T =>
    let f := fresh T "_compare" in
    aux T f
  | |- Comparable ?T =>
    let f := fresh "compare" in
    aux T f
  end.

(* The next two tactics are inspired from the message of Pierre Courtieu:
  [Coq-Club] using Ltac to retrieve the name of an induction principle from the type. *)
Ltac hd_of_app t :=
  lazymatch t with
  | (?f _) => hd_of_app f
  | _ => t
  end.

Ltac find_elem typ :=
  constr:(ltac:(constructors*):typ).

Ltac find_rec typ :=
  let X := constr:(fun x : typ => ltac:(induction x; exact I) : True) in
  let h := find_elem typ in
  let app := eval lazy beta in (X h) in
  let hd := hd_of_app app in
  hd.

Ltac find_rect t typ :=
  let T := type of t in
  let X := constr:(fun x : typ => ltac:(induction x; exact t) : T) in
  let h := find_elem typ in
  let app := eval lazy beta in (X h) in
  let hd := hd_of_app app in
  hd.

Ltac list_all_constructors :=
  lazymatch goal with
  | |- list ?T =>
    let rec aux t :=
      match t with
      | ?C = _ -> ?t =>
        let l := aux t in
        constr:(C :: l)
      | _ ?C -> ?t =>
        let l := aux t in
        constr:(C :: l)
      | _ => constr:(@nil T)
      end in
    let h := find_elem T in
    let ind_princip := find_rec T in
    let ind := constr:(ind_princip (fun x => x = h)) in
    let t := type of ind in
    let l := aux t in
    exact l
  end.

Ltac prove_comparable_trivial_inductive_faster :=
  let aux T f :=
    refine (let f : (T -> nat) := ltac:(
      lazymatch goal with
      | |- ?T -> _ =>
        let rec aux i t b :=
          lazymatch t with
          | ?C -> ?t =>
            let f := aux (S i) t b in
            constr:(f i)
          | _ => constr:(b)
          end in
        let ind_princip := find_rect O T in
        let ind := constr:(ind_princip (fun _ => nat)) in
        let t := type of ind in
        let f := aux O t ind in
        exact f
      end) in _);
    let I := fresh "I" in
    constructors; intros t1 t2; applys Decidable_equiv (f t1 = f t2); [| typeclass ];
    iff I; [ destruct t1; abstract (destruct t2; solve [ inversion I | reflexivity ]) | rewrite~ I ] in
  match goal with
  | |- Comparable ?T =>
    let f := fresh T "_to_nat" in
    aux T f
  | |- Comparable ?T =>
    let f := fresh "to_nat" in
    aux T f
  end.

Ltac prove_comparable :=
  solve [ repeat first [
              typeclass
            | solve [ autos~ ]
            | solve [ prove_comparable_trivial_inductive_faster ]
            | solve [ prove_comparable_trivial_inductive ]
            | prove_comparable_inductively_inductive
            | prove_comparable_simple_inductive
            | prove_decidable_eq
            | apply make_comparable; intros; apply sumbool_decidable; typeclass
            | solve [ autos* ] ] ].

Global Instance unit_comparable : Comparable unit.
  prove_comparable.
Defined.

Global Instance False_comparable : Comparable False.
  prove_comparable.
Defined.


(** * Some tactics about Mem and Forall. **)

Ltac Mem_solve :=
  solve [
      repeat first [ solve [ apply Mem_here ] | apply Mem_next ]
    | let M := fresh "M" in introv M; solve [ repeat inverts M as M ] ].

Ltac No_duplicates_solve :=
  repeat constructors; Mem_solve.

Ltac Forall_splits :=
  repeat splits;
  repeat first [ apply Forall_nil | apply Forall_cons ].

Ltac Forall2_splits :=
  repeat splits;
  repeat first [ apply Forall2_nil | apply Forall2_cons ].


(** * Some extensions of LibBag. **)

Require Import TLC.LibBag.

Global Instance Comparable_BagIn_list : forall T,
    Comparable T ->
    BagIn T (list T) :=
  fun T C => Build_BagIn (@Mem _).

Global Instance Comparable_BagIn_Decidable : forall T t l,
  Comparable T ->
  Decidable (t \in l).
Proof. introv C. simpl. typeclass. Qed.

Global Instance Comparable_BagIn_Decidable_not : forall T t l,
  Comparable T ->
  Decidable (t \notin l).
Proof. introv C. typeclass. Qed.

Lemma BagIn_cons : forall T `{Comparable T} (x y : T) l,
  x \in (y :: l) <-> x = y \/ x \in l.
Proof. introv. iff I; (inverts I as I; [ left~ | right~ ]). Qed.

Global Instance BagEmpty_list : forall T,
    BagEmpty (list T) :=
  fun T => Build_BagEmpty nil.

Lemma BagInEmpty_list : forall T `{Comparable T} (t : T),
  t \notin (\{} : list T).
Proof. introv I. simpl in I. rewrite Mem_mem in I. rewrite* mem_nil in I. Qed.

Global Instance BagSingle_list : forall T,
    BagSingle T (list T) :=
  fun T => Build_BagSingle (fun t => [t]).

Lemma BagInSingle_list : forall T `{Comparable T} (t1 t2 : T),
  t1 \in (\{t2} : list T) <-> t1 = t2.
Proof. introv. simpl. iff I; [repeat inverts I as I | substs ]; autos~. Qed.

Global Instance Comparable_BagRemove_list : forall T,
    Comparable T ->
    BagRemove (list T) T :=
  fun T C => Build_BagRemove (fun l t => LibList.remove t l).

Lemma BagRemove_list_notin : forall T `{Comparable T} l (t : T),
  t \notin l \- t.
Proof.
  introv I. simpl in I.
  rewrite Mem_mem in I. rewrite remove_correct in I. false*.
Qed.

Lemma BagRemove_list_in : forall T `{Comparable T} l (t1 t2 : T),
  t1 <> t2 ->
  t1 \in l ->
  t1 \in l \- t2.
Proof. introv D I. simpl in *. rewrite Mem_mem in *. rewrite* remove_correct. Qed.

Global Instance Comparable_BagRemove_list_list : forall T,
    Comparable T ->
    BagRemove (list T) (list T) :=
  fun T C => Build_BagRemove (fun l1 l2 =>
    filter (fun t => decide (t \notin l2)) l1).

Lemma BagRemove_list_list : forall T `{Comparable T} (l1 l2 : list T) t,
  t \in l1 \- l2 <-> t \in l1 /\ t \notin l2.
Proof. introv. simpl. repeat rewrite Mem_mem. rewrite filter_mem_eq. rew_refl*. Qed.

Global Instance Comparable_BagUnion_list : forall T,
    Comparable T ->
    BagUnion (list T) :=
  fun T C => Build_BagUnion (fun l1 l2 => l1 ++ (l2 \- l1)).

Lemma BagUnion_list : forall T `{Comparable T} (l1 l2 : list T) t,
  t \in l1 \u l2 <-> t \in l1 \/ t \in l2.
Proof.
  introv. simpl. repeat rewrite Mem_mem.
  rewrite mem_app. rewrite filter_mem_eq. rew_refl. iff~ [I|I].
   inverts~ I.
   tests I': (t \in l1); autos~. simpl in I'. rewrite~ Mem_mem in I'.
Qed.

Global Instance Comparable_BagInter_list : forall T,
    Comparable T ->
    BagInter (list T) :=
  fun T C => Build_BagInter (fun l1 l2 =>
    filter (fun t => decide (t \in l1)) l2).

Lemma BagInter_list : forall T `{Comparable T} (l1 l2 : list T) t,
  t \in l1 \n l2 <-> t \in l1 /\ t \in l2.
Proof.
  introv. simpl. repeat rewrite Mem_mem.
  rewrite filter_mem_eq. rew_refl. rewrite* Mem_mem.
Qed.

Global Instance In_inter_inv_list : forall T `{Comparable T}, @In_inter_inv T (list T) _ _.
Proof. introv. constructors. introv I. rewrite~ BagInter_list in I. Qed.

Global Instance Comparable_BagIncl_list : forall T,
    Comparable T ->
    BagIncl (list T) :=
  fun T C => Build_BagIncl (fun l1 l2 => Forall (fun t => Mem t l2) l1).

Global Instance Comparable_BagIncl_Decidable : forall T `{Comparable T} (l1 l2 : list T),
  Decidable (l1 \c l2).
Proof. introv. simpl. typeclass. Qed.

Lemma BagIncl_refl : forall T `{Comparable T} (l : list T),
  l \c l.
Proof. introv. simpl. rewrite Forall_iff_forall_mem. introv I. rewrite* Mem_mem. Qed.

Lemma BagInIncl_list : forall T `{Comparable T} (l1 l2 : list T) t,
  t \in l1 ->
  l1 \c l2 ->
  t \in l2.
Proof.
  introv I C. simpls. rewrite Forall_iff_forall_mem in C.
  rewrite Mem_mem in I. applys~ C I.
Qed.

Global Instance Incl_inv_list : forall T `{Comparable T}, @Incl_inv T (list T) _ _.
Proof. introv. constructors. introv I1 I2. applys~ BagInIncl_list I2 I1. Qed.

Lemma BagInIncl_make : forall T `{Comparable T} (l1 l2 : list T),
  (forall_ t \in l1, t \in l2) ->
  l1 \c l2.
Proof.
  introv I. simpls. rewrite Forall_iff_forall_mem.
  introv M. rewrite <- Mem_mem in M. applys~ I M.
Qed.

Global Instance Incl_prove_list : forall T `{Comparable T}, @Incl_prove T (list T) _ _.
Proof. introv. constructors. introv I. applys BagInIncl_make I. Qed.

Global Instance Remove_incl_list : forall T `{Comparable T}, @Remove_incl (list T) _ _.
Proof.
  introv. constructors. introv. apply BagInIncl_make.
  introv I. rewrite* BagRemove_list_list in I.
Qed.

Global Instance Incl_trans_list : forall T `{Comparable T}, @Incl_trans (list T) _.
Proof.
  introv. constructors. introv I1 I2. apply BagInIncl_make.
  introv I. applys BagInIncl_list I2. applys~ BagInIncl_list I I1.
Qed.

Lemma BagInclEmpty : forall T `{Comparable T} (l : list T),
  \{} \c l.
Proof. introv. apply BagInIncl_make. introv I. false~ BagInEmpty_list I. Qed.

Lemma BagIncl_cons : forall T `{Comparable T} l1 l2 (t : T),
  t \in l2 ->
  l1 \c l2 ->
  (t :: l1) \c l2.
Proof. introv M I. applys~ Forall_cons. Qed.

Lemma BagUnionIncl_left : forall T `{Comparable T} (l1 l2 : list T),
  l1 \c l1 \u l2.
Proof. introv. apply BagInIncl_make. introv I. rewrite* BagUnion_list. Qed.

Lemma BagUnionIncl_right : forall T `{Comparable T} (l1 l2 : list T),
  l2 \c l1 \u l2.
Proof. introv. apply BagInIncl_make. introv I. rewrite* BagUnion_list. Qed.

Lemma BagInterIncl_left : forall T `{Comparable T} (l1 l2 : list T),
  l1 \n l2 \c l1.
Proof. introv. apply BagInIncl_make. introv I. rewrite* BagInter_list in I. Qed.

Lemma BagInterIncl_right : forall T `{Comparable T} (l1 l2 : list T),
  l1 \n l2 \c l2.
Proof. introv. apply BagInIncl_make. introv I. rewrite* BagInter_list in I. Qed.

Lemma BagUnionIncl_combine : forall T `{Comparable T} (l1 l2 l3 : list T),
  l1 \c l3 ->
  l2 \c l3 ->
  l1 \u l2 \c l3.
Proof.
  introv I1 I2. apply BagInIncl_make. introv Iu. apply BagUnion_list in Iu.
  inverts Iu as I; applys~ BagInIncl_list I.
Qed.

Lemma BagInterIncl_combine : forall T `{Comparable T} (l1 l2 l3 : list T),
  l1 \c l2 ->
  l1 \c l3 ->
  l1 \c l2 \n l3.
Proof.
  introv I1 I2. apply BagInIncl_make. introv I.
  apply BagInter_list. splits~; applys~ BagInIncl_list I.
Qed.

Global Instance Union_incl_list : forall T `{Comparable T}, @Union_incl (list T) _ _.
Proof. introv. constructors. introv I1 I2. applys BagUnionIncl_combine I1 I2. Qed.

Global Instance Comparable_BagDisjoint_list : forall T,
    Comparable T ->
    BagDisjoint (list T) :=
  fun T C => Build_BagDisjoint (fun l1 l2 => Forall (fun t => ~ Mem t l1) l2).

Global Instance Comparable_BagDisjoint_Decidable : forall T `{Comparable T} (l1 l2 : list T),
  Decidable (l1 \# l2).
Proof. introv. simpl. typeclass. Qed.

Lemma BagDisjoint_in : forall T `{Comparable T} (l1 l2 : list T),
  l1 \# l2 <-> ~ exists t, t \in l1 /\ t \in l2.
Proof.
  introv. simpl. rewrite Forall_iff_forall_mem. rew_logic. iff I.
   introv (I1&I2). applys~ I; rewrite Mem_mem in *; autos*.
   introv I1 I2. applys~ I. repeat rewrite Mem_mem in *. autos*.
Qed.

Lemma BagDisjoint_com : forall T `{Comparable T} (l1 l2 : list T),
  l1 \# l2 <-> l2 \# l1.
Proof. introv. repeat rewrite BagDisjoint_in. rew_logic*. Qed.

(* Note: the definition of [In_inter] in TLC looks wrong. *)

Lemma BagInter_left : forall A T `{BagIncl T} `{BagIn A T} `{BagInter T} (l1 l2 l3 : T),
  Incl_prove ->
  In_inter_inv ->
  Incl_inv ->
  l1 \c l3 ->
  l1 \n l2 \c l3.
Proof.
  introv IP III II I. apply~ incl_prove. introv I'.
  forwards (I1&I2): @in_inter_inv III I'. applys @incl_inv II I I1.
Qed.

Lemma BagInter_right : forall A T `{BagIncl T} `{BagIn A T} `{BagInter T} (l1 l2 l3 : T),
  Incl_prove ->
  In_inter_inv ->
  Incl_inv ->
  l2 \c l3 ->
  l1 \n l2 \c l3.
Proof.
  introv IP III II I. apply~ incl_prove. introv I'.
  forwards (I1&I2): @in_inter_inv III I'. applys @incl_inv II I I2.
Qed.

Lemma BagRemove_empty : forall T `{BagEmpty T} `{BagIncl T} `{BagRemove T T} A `{BagIn A T} (l1 l2 : T),
  Incl_empty_inv ->
  Incl_prove ->
  In_remove_eq ->
  Incl_inv ->
  l1 \c l2 ->
  l1 \- l2 = \{}.
Proof.
  introv EI IP RE II I. applys @incl_empty_inv EI. applys @incl_prove IP.
  introv I'. rewrite in_remove_eq in I'. lets (I1&NI2): (rm I'). false NI2.
  applys~ @incl_inv II I I1.
Qed.

Lemma BagRemoveUnion : forall T `{BagIncl T} `{BagUnion T} `{BagRemove T T} A `{BagIn A T}
    (l1 l2 l3 l4 : T),
  Incl_prove ->
  In_remove_eq ->
  Incl_inv ->
  In_union_inv ->
  l1 \- l3 \c l4 ->
  l2 \- l3 \c l4 ->
  (l1 \u l2) \- l3 \c l4.
Proof.
  introv IP RE II UI I1 I2. applys @incl_prove IP. introv I'.
  rewrite in_remove_eq in I'. lets (I3&NI): (rm I'). lets [I4|I4]: @in_union_inv UI I3.
  - applys @incl_inv II I1. rewrite* in_remove_eq.
  - applys @incl_inv II I2. rewrite* in_remove_eq.
Qed.

Lemma BagRemoveRemove : forall T `{BagUnion T} `{BagRemove T T} A `{BagIn A T} (l1 l2 l3 : T),
  In_extens_eq ->
  In_remove_eq ->
  In_union_eq ->
  (l1 \- l2) \- l3 = l1 \- (l2 \u l3).
Proof.
  introv EE RE UE. applys @in_extens_eq EE. introv. extens.
  repeat rewrite in_remove_eq. unfold notin. rewrite* in_union_eq.
Qed.

Lemma BagRemoveEmpty : forall T `{BagEmpty T} `{BagRemove T T} A `{BagIn A T} (l : T),
  In_extens_eq ->
  In_remove_eq ->
  Notin_empty ->
  l \- \{} = l.
Proof.
  introv EE RE NE. applys @in_extens_eq EE. introv. extens.
  rewrite in_remove_eq. iff* I. splits~. apply~ NE.
Qed.

Lemma BagInterEmpty_left : forall T `{BagEmpty T} `{BagInter T} `{BagIncl T} A `{BagIn A T} (l : T),
  Incl_empty ->
  Incl_prove ->
  In_inter_inv ->
  Incl_inv ->
  \{} \n l = \{}.
Proof. introv EI IP III II. rewrite <- incl_empty. applys~ BagInter_left. rewrite~ incl_empty. Qed.

Lemma BagInterEmpty_right : forall T `{BagEmpty T} `{BagInter T} `{BagIncl T} A `{BagIn A T} (l : T),
  Incl_empty ->
  Incl_prove ->
  In_inter_inv ->
  Incl_inv ->
  l \n \{} = \{}.
Proof. introv EI IP III II. rewrite <- incl_empty. applys~ BagInter_right. rewrite~ incl_empty. Qed.

Global Instance Incl_empty_list : forall T `{Comparable T}, Incl_empty (T := list T).
Proof.
  introv. constructors. introv. compute. extens. destruct E.
  - iff; try solve [ constructors ].
  - iff I; tryfalse. do 2 inverts I as I.
Qed.

Global Instance Notin_empty_list : forall T `{Comparable T}, Notin_empty (T := list T).
Proof. introv. constructors. introv M. inverts M. Qed.

Global Instance In_union_eq_list : forall T `{Comparable T}, In_union_eq (T := list T).
Proof. introv. constructors. introv. extens. rewrite* BagUnion_list. Qed.

Global Instance Incl_empty_inv_list : forall T `{Comparable T}, Incl_empty_inv (T := list T).
Proof. introv. constructors. introv I. destruct~ E. do 2 inverts I as I. Qed.

Global Instance In_remove_eq_list : forall T `{Comparable T}, In_remove_eq (T := list T).
Proof. introv. constructors. introv. extens. rewrite* BagRemove_list_list. Qed.

Global Instance In_union_inv_list : forall T `{Comparable T}, In_union_inv (T := list T).
Proof. introv. applys~ in_union_inv_from_in_union_eq. typeclass. Qed.

Global Instance Incl_inter_list : forall T `{Comparable T}, Incl_inter (T := list T).
Proof. introv. constructors. introv I1 I2. applys~ BagInterIncl_combine I1 I2. Qed.

Global Instance In_empty_eq_list : forall T `{Comparable T}, In_empty_eq (T := list T).
Proof. introv. constructors. introv. extens. iff I; inverts I. Qed.

Global Instance In_single_eq_list : forall T `{Comparable T}, In_single_eq (T := list T).
Proof. introv. constructors. introv. extens. iff I; repeat inverts I as I; substs~. constructors. Qed.

Global Instance In_inter_eq_list : forall T `{Comparable T}, In_inter_eq (T := list T).
Proof. introv. constructors. introv. extens. rewrite* BagInter_list. Qed.

Global Instance Incl_refl_list : forall T `{Comparable T}, Incl_refl (T := list T).
Proof. introv. constructors. introv. apply~ BagIncl_refl. Qed.

Global Instance Empty_incl_list : forall T `{Comparable T}, Empty_incl (T := list T).
Proof. introv. constructors. introv. apply~ BagInclEmpty. Qed.

Global Instance Single_incl_r_list : forall T `{Comparable T}, Single_incl_r (A := T) (T := list T).
Proof. introv. constructors. introv I. do 2 constructors~. Qed.

Global Instance Incl_union_l_list : forall T `{Comparable T}, Incl_union_l (T := list T).
Proof.
  introv. constructors. introv I. apply BagInIncl_make. introv M.
  rewrite~ BagUnion_list. left. applys BagInIncl_list M I.
Qed.

Global Instance Incl_union_r_list : forall T `{Comparable T}, Incl_union_r (T := list T).
Proof.
  introv. constructors. introv I. apply BagInIncl_make. introv M.
  rewrite~ BagUnion_list. right. applys BagInIncl_list M I.
Qed.

Global Instance Is_empty_prove_list : forall T `{Comparable T}, Is_empty_prove (T := list T).
Proof. introv. constructors. introv I. destruct~ E. false I. constructors*. Qed.


Lemma eq_double_incl : forall T `{BagIncl T} A `{BagIn A T} (l1 l2 : T),
  Incl_in_eq ->
  In_extens_eq ->
  l1 \c l2 ->
  l2 \c l1 ->
  l1 = l2.
Proof.
  introv IE EE I1 I2. applys @in_extens_eq EE. introv.
  erewrite @incl_in_eq in *; try exact IE. extens. iff* I.
Qed.

Lemma eq_remove_divide : forall T `{BagUnion T} `{BagRemove T T} A `{BagIn A T} (l1 l2 la lb : T),
  In_extens_eq ->
  In_union_eq ->
  In_remove_eq ->
  l1 = la \u lb ->
  l1 \- l2 = (la \- l2) \u (lb \- l2).
Proof.
  introv EE UE RE E. rewrite E. applys @in_extens_eq EE. introv. extens.
  rewrite in_union_eq. do 3 rewrite in_remove_eq. rewrite* in_union_eq.
Qed.

Lemma eq_empty_if_remove_single_notin : forall A T `{BagIn A T} `{BagSingle A T} `{BagRemove T T}
    (x : A) (l : T),
  Is_single_eq ->
  In_single_eq ->
  In_remove_eq ->
  ~ x \in l ->
  \{ x } \- l = \{ x }.
Proof.
  introv ISE SE RE NI. rewrite is_single_eq. splits~.
  - rewrite in_remove_eq. splits~. rewrite~ in_single_eq.
  - introv I. rewrite in_remove_eq in I. rewrite* in_single_eq in I.
Qed.

Lemma eq_inter_union : forall T `{BagUnion T} `{BagInter T} A `{BagIn A T} l1 l2 la lb,
  In_extens_eq ->
  In_inter_eq ->
  In_union_eq ->
  l1 = la \u lb ->
  l1 \n l2 = (la \n l2) \u (lb \n l2).
Proof.
  introv EE IE UE E. substs. applys @in_extens_eq EE. introv. extens.
  rewrite~ in_inter_eq. repeat rewrite~ in_union_eq. repeat rewrite~ in_inter_eq. autos*.
Qed.


(** Checks that a list is computed, at least to the first constructor. **)
Ltac list_is_matchable l :=
  let l := eval simpl in l in
  lazymatch l with
  | nil => idtac
  | _ :: _ => idtac
  end.

(** Checks that a list is fully computed, at least to the first constructor. **)
Ltac list_is_fully_computed l :=
  let l := eval simpl in l in
  let rec aux l :=
    lazymatch l with
    | nil => idtac
    | _ :: ?l' => aux l'
    end in
  aux l.

(** There are a lot of typeclasses called in these tactics, and Coq’s profiling
  states that we are getting a lot of time looking for them and often failing.
  This tactic tries to only search for typeclasses if it is plausible that it
  would help. **)
Ltac solve_typeclass_if_plausible :=
  lazymatch goal with
  | |- ~ _ => fail
  | |- _ \in _ => fail
  | |- _ \c _ => fail
  | |- _ \/ _ => fail
  | |- _ /\ _ => fail
  | |- False => fail
  | _ => solve_typeclass
  end.

(** This tactic assumes an hypothesis [I] of the form [_ \in _].  It
  tries to solve the goal, but doesn’t fail if it can’t, leaving its
  rewritings intact. **)
Ltac rewrite_in_aux rewrite_goal I :=
  let direct_solve :=
    solve [ lazymatch goal with
            | |- False => autos~
            | _ => autos~; false~
            end ] in
  let rec aux I :=
    lazymatch type of I with
    | _ \in _ \u _ =>
      let I' := fresh "I" in
      forwards [I'|I']: @in_union_inv (rm I); try solve_typeclass_if_plausible;
      aux I'
    | _ \in _ \n _ =>
      let I1 := fresh "I" in
      let I2 := fresh "I" in
      forwards (I1&I2): @in_inter_inv (rm I); try solve_typeclass_if_plausible;
      aux I1; aux I2
    | _ \in _ \- _ =>
      let I1 := fresh "I" in
      let I2 := fresh "I" in
      erewrite @in_remove_eq in I; try solve_typeclass_if_plausible;
      lets (I1&I2): (rm I);
      aux I1;
      try (false I2; rewrite_goal tt)
    | _ \in \{} =>
      erewrite @in_empty_eq in I; try solve_typeclass_if_plausible; inverts I
    | _ \in \{ _ } =>
      erewrite @in_single_eq in I; try solve_typeclass_if_plausible; try inverts I
    | _ => idtac
    end in
  aux I; try direct_solve.

(** Simplifies an hypothesis of the form [_ \in _]. **)
Ltac rewrite_in I := rewrite_in_aux ltac:(fun _ => fail) I.

(** Tries to solve a goal of the form [x \in l] or [x \notin l]. **)
Ltac solve_in_notin_aux heavy :=
  let direct_solve :=
    solve [ lazymatch goal with
            | |- False => autos~
            | _ => autos~; false~
            end ] in
  let rec rewrite_goal _ :=
    (** This tactic solves the goal or fail. **)
    let rewrite_in_context :=
      lazymatch heavy with
      | true =>
        repeat match goal with
        | I : _ \in _ \u _ |- _ => rewrite_in_aux ltac:(rewrite_goal) I
        | I : _ \in _ \n _ |- _ => rewrite_in_aux ltac:(rewrite_goal) I
        | I : _ \in _ \- _ |- _ => rewrite_in_aux ltac:(rewrite_goal) I
        | I : _ \in \{} |- _ => rewrite_in_aux ltac:(rewrite_goal) I
        | I : _ \in \{ _ } |- _ => rewrite_in_aux ltac:(rewrite_goal) I
          end
      | false => idtac
      end in
    let failcase :=
      try unfolds @notin;
      try direct_solve;
      rewrite_in_context;
      try direct_solve;
      lazymatch goal with
      | I' : ~ _ \in _ |- _ =>
        false I'; clear I'; rewrite_goal tt
      end in
    try unfolds @notin;
    lazymatch goal with
    | |- ~ _ =>
      let I := fresh "I" in
      introv I; rewrite_in_aux ltac:(rewrite_goal) I; rewrite_goal tt
    | |- _ \in _ \u _ =>
      erewrite @in_union_eq; try solve_typeclass_if_plausible;
      (** To avoid duplicating rewritings, we rewrite the context before branching. **)
      rewrite_in_context;
      solve [ left; rewrite_goal tt
            | right; rewrite_goal tt
            | failcase ]
    | |- _ \in _ \n _ =>
      erewrite @in_inter_eq; try solve_typeclass_if_plausible; splits; rewrite_goal tt
    | |- _ \in _ \- _ =>
      erewrite @in_remove_eq; try solve_typeclass_if_plausible; splits; rewrite_goal tt
    | |- _ \in \{ _ } =>
      erewrite @in_single_eq; try solve_typeclass_if_plausible; substs; failcase
    | |- _ \in \{} =>
      erewrite @in_empty_eq; try solve_typeclass_if_plausible; failcase
    | _ => failcase
    end in
  rewrite_goal tt.

Ltac solve_in_notin := solve_in_notin_aux false.
Ltac solve_in_notin_heavy := solve_in_notin_aux true.

(** Auxiliary tactic for [solve_incl] that tries to efficiently (but eagerly)
  look through large unions of bags. **)
Ltac search_solve_incl :=
  lazymatch goal with
  | |- ?l1 \c ?l2 \u ?l3 =>
    match l2 with
    | context [ l1 ] =>
      apply @LibBag.incl_union_l; try solve_typeclass_if_plausible; [idtac]; search_solve_incl
    | _ =>
      match l3 with
      | context [ l1 ] =>
        apply @LibBag.incl_union_r; try solve_typeclass_if_plausible; [idtac]; search_solve_incl
      end
    end
  | |- ?l1 \n ?l2 \c ?l3 =>
    match l1 with
    | context [ l3 ] =>
      eapply @BagInter_left; try solve_typeclass_if_plausible; [idtac]; search_solve_incl
    | _ =>
      match l2 with
      | context [ l3 ] =>
        eapply @BagInter_right; try solve_typeclass_if_plausible; [idtac]; search_solve_incl
      end
    end
  | _ => idtac
  end.

(** Tries to immediately solve a goal o the form [l1 \c l2]. **)
Ltac solve_incl_direct_solve :=
  let direct_solve :=
    lazymatch goal with
    | |- ?l \c ?l =>
      (eapply BagIncl_refl || eapply @incl_refl); autos~; solve_typeclass_if_plausible
    | |- \{} \c _ =>
      (eapply BagInclEmpty || eapply @empty_incl); autos~; solve_typeclass_if_plausible
    | |- _ \n _ \c _ =>
      solve [ apply~ BagInterIncl_left || apply~ BagInterIncl_right ]
    | |- \{ _ } \c _ =>
      solve [ eapply @single_incl_r; try solve_typeclass_if_plausible; solve_in_notin ]
    | |- ?l1 \c ?l2 =>
      solve [
          list_is_fully_computed l1;
          repeat (apply~ BagIncl_cons; [Mem_solve|]); apply~ BagInclEmpty
        | autos~ ]
    end in
  solve [ direct_solve
        | simpl; direct_solve
        | lazymatch goal with
          | |- ?l1 \- ?l2 \c ?l3 =>
            eapply @incl_trans; [ solve_typeclass_if_plausible
                                | eapply remove_incl; solve_typeclass_if_plausible
                                | search_solve_incl ];
            solve [ direct_solve | simpl; direct_solve ]
          end ].

(** Tries to divide a goal of the form [l1 \c l2] to simpler goals. **)
Ltac solve_incl_divide :=
  repeat lazymatch goal with
  | |- _ \u _ \c _ => eapply @union_incl; try solve_typeclass_if_plausible; [idtac|idtac]
  | |- _ \c _ \n _ => eapply @incl_inter; try solve_typeclass_if_plausible; [idtac|idtac]
  | |- (_ \u _) \- _ \c _ => eapply @BagRemoveUnion; try solve_typeclass_if_plausible; [idtac|idtac]
  end.

(** Tries to solve a goal of the form [l1 \c l2]. **)
Ltac solve_incl_basic :=
  (** We first try to limit the search. **)
  try search_solve_incl;
  try solve_incl_direct_solve;
  solve_incl_divide;
  (** We now do more heavy things **)
  first [
      solve_incl_direct_solve
    | search_solve_incl; solve_incl_direct_solve
    | simpl; search_solve_incl; solve_incl_direct_solve ].

(** From a bag given as a union of subbags, and another bag given as a union of subbags,
  divide the first bags into two subbags: one being the ones in the union of all the
  other subbags and the other not.  This enables us to separate expressions of the form
  [(… \u …) \- (… \u …)] into an expression of the form [(… \- (… \u …)) \u (… \- (… \u …))]
  where the first [… \- (… \u …)] expression is equivalent to [\{}] and can thus be
  removed from the current expression. **)
Ltac built_union_remove l lr :=
  let is_in_lr l0 :=
    let rec aux lr :=
      lazymatch lr with
      | ?lr1 \u ?lr2 =>
        let r1 := aux lr1 in
        lazymatch r1 with
        | true => constr:(true)
        | false => aux lr2
        end
      | l0 => constr:(true)
      | _ => constr:(false)
      end in
    aux lr in
  let mnil :=
    let t := type of l in
    constr:(\{} : t) in
  let munion l1 l2 :=
    lazymatch l1 with
    | \{} => constr:(l2)
    | _ =>
      lazymatch l2 with
      | \{} => constr:(l1)
      | _ => constr:(l1 \u l2)
      end
    end in
  lazymatch l with
  | ?l1 \u ?l2 =>
    let r1 := built_union_remove l1 lr in
    lazymatch r1 with
    | (?l1a, ?l1b) =>
      let r2 := built_union_remove l2 lr in
      lazymatch r2 with
      | (?l2a, ?l2b) =>
        let la := munion l1a l2a in
        let lb := munion l1b l2b in
        constr:((la, lb))
      end
    end
  | ?l =>
    lazymatch is_in_lr l with
    | true => constr:((l, mnil))
    | false => constr:((mnil, l))
    end
  end.

(** Tries to simplify the current expression using bag lemmas. **)
Ltac rew_bag :=
  repeat match goal with
  | |- context [ @LibBag.remove ?T ?K ?BR ?l1 ?l2 ] =>
    let E := fresh "E" in
    (** Unfortunately, it happens that Coq generates different implicit arguments:
      we have to match them exactly here. **)
    asserts E: (@LibBag.remove T K BR l1 l2 = \{});
    [ eapply @BagRemove_empty; try solve_typeclass_if_plausible; solve_incl_basic
    | rewrite E; clear E ]
  | |- context [ (?l1 \- ?l2) \- ?l3 ] =>
    rewrites >> (@BagRemoveRemove) l1 l2 l3; try solve_typeclass_if_plausible; [idtac]
  | |- context [ ?l \- \{} ] =>
    rewrites >> (@BagRemoveEmpty) l; try solve_typeclass_if_plausible; [idtac]
  | |- context [ \{} \u ?l ] =>
    rewrites >> (@union_empty_l) l; [solve_typeclass_if_plausible|]
  | |- context [ ?l \u \{} ] =>
    rewrites >> (@union_empty_r) l; [solve_typeclass_if_plausible|]
  | |- context [ \{} \n ?l ] =>
    rewrites >> (@BagInterEmpty_left) l; try solve_typeclass_if_plausible; [idtac]
  | |- context [ ?l \n \{} ] =>
    rewrites >> (@BagInterEmpty_right) l; try solve_typeclass_if_plausible; [idtac]
  (** The clauses below this comment are more complex and must stay below this comment. **)
  | |- context [ \{ ?x } \- ?l ] =>
    let E := fresh "E" in
    asserts E: (\{ x } \- l = \{ x });
    [| (** We first sets up the implicit variables **) rewrite E; clear E ];
    [ eapply @eq_empty_if_remove_single_notin; try solve_typeclass_if_plausible; solve_in_notin |]
  | |- context [ ?l1 \- ?l2 ] =>
    let r := built_union_remove l1 l2 in
    lazymatch r with
    (** These two cases involving [\{}] would only make the current context more complex. **)
    | (\{}, _) => fail
    | (_, \{}) => fail
    | (?la, ?lb) =>
      rewrites >> (@eq_remove_divide) l1 l2 la lb; try solve_typeclass_if_plausible;
      lazymatch goal with
      | |- l1 = la \u lb =>
        solve [ reflexivity
              | eapply @eq_double_incl; try solve_typeclass_if_plausible; solve_incl_basic ]
      | _ => idtac
      end
    end
  | |- context [ ?l1 \n ?l2 ] =>
    let r := built_union_remove l1 l2 in
    lazymatch r with
    (** This case involving [\{}] would only make the current context more complex. **)
    | (_, \{}) => fail
    | (\{}, _) =>
      (** In this case, the intersection is empty. **)
      rewrites >> (@is_empty_prove) (l1 \n l2); try solve_typeclass_if_plausible;
      lazymatch goal with
      | |- l1 \n l2 = \{} =>
        let I := fresh "I" in introv I; solve_in_notin
      end
    | (?la, ?lb) =>
      rewrites >> (@eq_inter_union) l1 l2 la lb; try solve_typeclass_if_plausible;
      lazymatch goal with
      | |- l1 = la \u lb =>
        solve [ reflexivity
              | eapply @eq_double_incl; try solve_typeclass_if_plausible; solve_incl_basic ]
      | _ => idtac
      end
    end
  end.


(** Tries to solve a goal of the form [l1 \c l2] by performing clever rewritings. **)
Ltac solve_incl :=
  let rec aux :=
    try solve_incl_basic;
    repeat (rew_bag; solve_incl_divide; try solve_incl_basic);
    lazymatch goal with
    | |- _ \c _ \u _ =>
      solve [
          eapply @incl_union_l; try solve_typeclass_if_plausible; aux
        | eapply @incl_union_r; try solve_typeclass_if_plausible; aux ]
    | |- _ \n _ \c _ =>
      solve [
          eapply @BagInter_right; try solve_typeclass_if_plausible; aux
        | eapply @BagInter_left; try solve_typeclass_if_plausible; aux ]
    end in
  solve [ aux ].

(** Like [solve_incl] but gets into more into bruteforce proof. **)
Ltac solve_incl_heavy :=
  try solve_incl;
  match goal with
  | |- _ \c _ =>
    solve [
        eapply BagInIncl_make; intros; try solve_typeclass_if_plausible;
        tryfalse~; autos~; solve_in_notin_heavy
      | rewrite LibSet.set_incl_in_eq; intros; try solve_typeclass_if_plausible;
        tryfalse~; autos~; solve_in_notin_heavy ]
  end.


(** * Some Extension of LibSet **)

Require Import TLC.LibSet.

Definition set_big_union A (L : list (set A)) : set A :=
  set_st (fun x => exists S, Mem S L /\ x \in S).

Lemma set_big_union_intro : forall A (L : list (set A)) S x,
  Mem S L ->
  x \in S ->
  x \in set_big_union L.
Proof. introv M I. unfolds set_big_union. rewrite* in_set_st_eq. Qed.

Lemma to_set_cons : forall A (l : list A) a,
  to_set (a :: l) = \{ a } \u to_set l.
Proof.
  introv. unfolds to_set. rewrite set_ext_eq. introv.
  rewrite set_in_union_eq. repeat rewrite in_set_st_eq.
  rewrite in_single_eq. iff M; inverts~ M.
Qed.

Lemma set_iff_incl : forall A (E1 E2 : set A),
  E1 \c E2 -> E2 \c E1 -> E1 = E2.
Proof. introv I1 I2. rewrite set_ext_eq. rewrite set_incl_in_eq in I1, I2. iff* I. Qed.

Global Instance Union_incl_set : forall T, @Union_incl (set T) _ _.
Proof.
  introv. constructors. introv I1 I2. rewrite set_incl_in_eq in *.
  introv I. rewrite set_in_union_eq in I. inverts* I.
Qed.


Inductive map_set A B (f : A -> B) (E : set A) : set B :=
  | map_set_intro : forall a,
    E a ->
    map_set f E (f a).

Lemma in_map_set_inv : forall A B (f : A -> B) E x,
  x \in map_set f E ->
  exists y, y \in E /\ x = f y.
Proof. introv I. inverts I as I. eexists. splits*. apply I. Qed.

Lemma in_map_set : forall A B (f : A -> B) E x,
  x \in E ->
  f x \in map_set f E.
Proof. introv I. apply map_set_intro. apply I. Qed.

Lemma in_map_set_eq : forall A B (f : A -> B) E x,
  x \in map_set f E = exists y, y \in E /\ x = f y.
Proof.
  introv. extens. iff I.
  - apply~ in_map_set_inv.
  - lets (y&I'&E'): (rm I). substs. apply~ in_map_set.
Qed.

Lemma incl_map_set : forall A B (f : A -> B) E1 E2,
  E1 \c E2 ->
  map_set f E1 \c map_set f E2.
Proof.
  introv I. apply incl_prove. introv I'. inverts I' as I'. constructors.
  eapply incl_inv in I; [ apply I | apply I' ].
Qed.

Lemma union_map_set : forall A B (f : A -> B) E1 E2,
  map_set f E1 \u map_set f E2 = map_set f (E1 \u E2).
Proof.
  introv. rewrite set_ext_eq. introv. rewrite in_union_eq. repeat rewrite in_map_set_eq. iff I.
  - inverts I as (y&I&E); substs; eexists; rewrite in_union_eq; splits*.
  - lets (y&I'&E): (rm I). rewrite in_union_eq in I'. inverts* I'.
Qed.

Lemma inter_map_set : forall A B (f : A -> B) E1 E2,
  map_set f (E1 \n E2) \c map_set f E1 \n map_set f E2.
Proof.
  introv. apply incl_prove. introv I. rewrite in_inter_eq. repeat rewrite in_map_set_eq in *.
  lets (y&I'&E): (rm I). rewrite in_inter_eq in I'. lets (I1&I2): (rm I'). splits*.
Qed.

Lemma remove_map_set : forall A B (f : A -> B) E1 E2,
  map_set f E1 \- map_set f E2 \c map_set f (E1 \- E2).
Proof.
  introv. apply incl_prove. introv I. rewrite in_remove_eq in I. repeat rewrite in_map_set_eq in *.
  lets ((y&I'&E)&N): (rm I). exists y. splits~. rewrite in_remove_eq. splits~.
  introv I. false N. substs. apply~ in_map_set.
Qed.

Lemma empty_map_set : forall A B (f : A -> B),
  map_set f \{} = \{}.
Proof.
  introv. rewrite set_ext_eq. introv. rewrite in_map_set_eq. iff I; tryfalse.
  lets (y&I'&E): (rm I). false.
Qed.

Lemma empty_map_set_inv : forall A B (f : A -> B) E,
  map_set f E = \{} ->
  E = \{}.
Proof.
  introv E'. apply is_empty_prove. introv I.
  rewrite is_empty_eq in E'. false E'. applys in_map_set I.
Qed.

Lemma single_map_set : forall A B (f : A -> B) x,
  map_set f \{ x } = \{ f x }.
Proof.
  introv. rewrite set_ext_eq. introv. rewrite in_map_set_eq. iff I.
  - lets (y&I'&E): (rm I). rewrite @in_single_eq in *; try typeclass. substs~.
  - eexists. rewrite @in_single_eq in *; try typeclass; autos*.
Qed.


Lemma Pickable_list_set : forall A (P : A -> Prop) `{Pickable_list _ P},
  to_set (pick_list P) = set_st P.
Proof.
  introv. unfolds to_set. apply set_ext. introv.
  do 2 rewrite in_set_st_eq. apply~ pick_list_correct.
Qed.

Global Instance to_set_Pickable : forall A (l : list A),
  Pickable_list (to_set l).
Proof.
  introv. applys pickable_list_make l.
  introv. unfolds to_set. unfolds set_st. iff~ I.
Defined.

Definition option_to_set A o : set A :=
  set_st (fun a => o = Some a).

Lemma option_to_set_None : forall A,
  option_to_set None = (\{} : set A).
Proof.
  introv. unfolds option_to_set. apply set_ext.
  introv. rewrite in_set_st_eq. rewrite in_empty_eq. iff I; inverts~ I.
Qed.

Lemma option_to_set_Some : forall A (a : A),
  option_to_set (Some a) = \{ a }.
Proof.
  introv. unfolds option_to_set. apply set_ext.
  introv. rewrite in_set_st_eq. rewrite in_single_eq. iff I; inverts~ I.
Qed.


Lemma sig_apply : forall A B (f : A -> B) (PA PB : _ -> Prop),
  (forall a, PA a -> PB (f a)) ->
  sig PA -> sig PB.
Proof. introv F (a&Ha). exists~ (f a). Defined.


(** * Miscellaneous **)

(** See message “[Coq-Club] finer control over typeclass instance refinement” on the Coq list. **)

Tactic Notation "oexact'" open_constr(term) :=
  exact term.

Tactic Notation "oexact" uconstr(term) :=
  lazymatch goal with
  |- ?G => oexact' (term : G)
  end.

Tactic Notation "orefine" uconstr(term) :=
  unshelve oexact term;
  shelve_unifiable.

Tactic Notation "simple" "orefine" uconstr(term) :=
  unshelve oexact term.

(** See https://github.com/coq/coq/issues/2386 **)

Tactic Notation "eabstract" tactic3(tac) :=
  let G := match goal with |- ?G => G end in
  let pf := constr:(ltac:(tac) : G) in
  abstract exact_no_check pf.


Tactic Notation "typeclass" integer(n) :=
  let go _ := eauto n with typeclass_instances in
  solve [ go tt | constructor; go tt ].

Tactic Notation "solve_typeclass" integer(n) :=
  solve [ eauto n with typeclass_instances ].

